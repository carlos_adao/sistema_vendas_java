/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

/**
 *
 * @author carlos
 */
public class GASTOS { 
    float  quanto;
    String comQue,observacao, dia, mes, ano;

    public GASTOS(float quanto, String comQue,String observacao, String dia, String mes, String ano) {
        this.quanto = quanto;
        this.comQue = comQue;
        this.observacao = observacao;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public GASTOS(String observacao) {
        this.observacao = observacao;
    }

    public GASTOS() {
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getComQue() {
        return comQue;
    }

    public void setComQue(String comQue) {
        this.comQue = comQue;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public float getQuanto() {
        return quanto;
    }

    public void setQuanto(float quanto) {
        this.quanto = quanto;
    }

    @Override
    public String toString() {
        return(quanto + ";" + comQue+ ";" + observacao+";"+ dia + ";" + mes + ";" + ano + ";");
    }
 
}   
