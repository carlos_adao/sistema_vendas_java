/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import BancoDados.ARQUIVO_GASTOS;
import Interface_Alternavel.AlternavelCadastroProduto;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author postgres
 */
public class Calculos_ExibeGastos {

    String quemGastou;
    String comOque;
    String quantoGastou;
    String data;
    float gastos_box, gastos_carlos, gastos_eliane;

    ArrayList<BOX> ListaDeGastos = new ArrayList<>(); //carrega para memoria a lista de gastos
    BOX gasto = new BOX(); //instancia um gasto para percorrer a lista
    ArrayList<String[]> dados = new ArrayList<>();// usado para enviar a lista gasto em forma de String
    Converte converte = new Converte();//isntancia a classe para realizar converção

    public ArrayList<String[]> RetornaValoresParaExiberGastos(String Consutar) {
        gastos_box = 0;
        gastos_carlos = 0;
        gastos_eliane = 0;

        dados.clear();
        ListaDeGastos.clear();
        ListaDeGastos = ARQUIVO_GASTOS.retorna();

        Collections.sort(ListaDeGastos);

        if (ListaDeGastos.size() > 0) {

            for (BOX ListaDeGasto : ListaDeGastos) {
                gasto = (BOX) ListaDeGasto;
                if (gasto.getDia().equalsIgnoreCase(Consutar)) {

                    quemGastou = gasto.getNome();
                    comOque = gasto.getComQue();
                    quantoGastou = converte.FloatEmString(gasto.getQuanto());
                    data = gasto.getDia();

                    String[] ListaGastos = new String[]{quemGastou, comOque, quantoGastou, data};
                    dados.add(ListaGastos);

                    if (quemGastou.equalsIgnoreCase("Box")) {
                        gastos_box = gastos_box + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Carlos")) {
                        gastos_carlos = gastos_carlos + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Eliane")) {
                        gastos_eliane = gastos_eliane + gasto.getQuanto();

                    }

                }
                if (gasto.getMes().equalsIgnoreCase(Consutar)) {

                    quemGastou = gasto.getNome();
                    comOque = gasto.getComQue();
                    quantoGastou = converte.FloatEmString(gasto.getQuanto());
                    data = gasto.getDia();

                    String[] ListaGastos = new String[]{quemGastou, comOque, quantoGastou, data};
                    dados.add(ListaGastos);

                    if (quemGastou.equalsIgnoreCase("Box")) {
                        gastos_box = gastos_box + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Carlos")) {
                        gastos_carlos = gastos_carlos + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Eliane")) {
                        gastos_eliane = gastos_eliane + gasto.getQuanto();

                    }

                }
                if (gasto.getAno().equalsIgnoreCase(Consutar)) {

                    quemGastou = gasto.getNome();
                    comOque = gasto.getComQue();
                    quantoGastou = converte.FloatEmString(gasto.getQuanto());
                    data = gasto.getDia();

                    String[] ListaGastos = new String[]{quemGastou, comOque, quantoGastou, data};
                    dados.add(ListaGastos);

                    if (quemGastou.equalsIgnoreCase("Box")) {
                        gastos_box = gastos_box + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Carlos")) {
                        gastos_carlos = gastos_carlos + gasto.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Eliane")) {
                        gastos_eliane = gastos_eliane + gasto.getQuanto();

                    }

                }
            }
            return dados;
        }
        return null;
    }

    public String RetornaGastosBox() {
        return converte.FloatEmString(gastos_box);
    }

    public String RetornaGastosCarlos() {
        return converte.FloatEmString(gastos_carlos);

    }

    public String RetornaGastosEliane() {
        return converte.FloatEmString(gastos_eliane);

    }
}
