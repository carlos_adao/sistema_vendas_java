/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import java.text.DecimalFormat;

/**
 *
 * @author carlos
 */
public class Converte {
    
    public float StringEmFloat(String string){
        
         return Float.parseFloat(string.replace(",", "."));
    }
    
    public String FloatEmString(float Float) {
        
        DecimalFormat fmt = new DecimalFormat("0.00");   //limita o número de casas decimais      
        String string = fmt.format(Float);
        String[] part = string.split("[,]");
        String stringg = part[0] + "." + part[1];
        return stringg;
    }
    
    public int StringEmInteiro(String string){
       return Integer.parseInt(string);
    
    }
    
    public String InteiroEmString(int Int){
    
    return  String.valueOf(Int);
    }
    
    
   
}
