/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import javax.swing.*;  
  import java.awt.*;  
  import java.util.*;  
    
  public class CData extends JFrame {  
    private Calendar cal = Calendar.getInstance();  
    private JLabel[] lblDatas = new JLabel[6 * 7]; // Note que um calendário pode ter até 6 linhas.  
    private JLabel[] lblTopo = new JLabel[7];  
    private Container painel;  
    private JPanel pnMain, pnTopo;  
    private String[] topo = {"D", "S", "T", "Q", "Q", "S", "S"};  
    
    public CData() {  
        super("Calendário");  
    
        painel = getContentPane();  
        painel.setLayout(new BorderLayout());  
    
        pnMain = new JPanel();  
        pnMain.setLayout(new GridLayout(6, 7)); // linhas, colunas  
    
        pnTopo = new JPanel();  
        pnTopo.setLayout(new GridLayout(1, 7));  
    
        for (int i = 0; i < lblDatas.length; i++) {  
            lblDatas[i] = new JLabel("");  
            pnMain.add(lblDatas[i]);  
        }  
                 // Qual é o dia da semana do dia primeiro DESTE mês?  
                 cal.setTime (new java.util.Date());  
                 cal.set (Calendar.DAY_OF_MONTH, 1);  
                 int diaPrimeiro = cal.get(Calendar.DAY_OF_WEEK); // Calendar.SUNDAY ... Calendar.SATURDAY  
                 // Qual é o último dia deste mês?  
                 int ultimoDia = cal.getActualMaximum (Calendar.DAY_OF_MONTH);  
                 int pos = diaPrimeiro - Calendar.SUNDAY; // a partir de que JLabel lblDatas vamos contar de 1 a ultimoDia  
                 //-- Pondo os dias de 1 a ultimoDia  
                 for (int i = 1; i <= ultimoDia; ++i, ++pos) {  
                     lblDatas[pos].setText (Integer.toString (i));  
                 }  
    
                 // Pondo os labels dos dias da semana  
        for (int i = 0; i < lblTopo.length; i++) {  
            lblTopo[i] = new JLabel(topo[i]);  
            pnTopo.add(lblTopo[i]);  
        }  
    
        painel.add(pnTopo, BorderLayout.NORTH);  
        painel.add(pnMain, BorderLayout.CENTER);  
    
        setSize(400, 300);  
        setVisible(true);  
    
    }  
    
    public static void main(String args[]) {  
        CData app = new CData();  
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
    }  
  }  