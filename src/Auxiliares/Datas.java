/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.swing.JOptionPane;

public class Datas {

    private Calendar cal = Calendar.getInstance();
    ArrayList<String> ListaDia_Mes = new ArrayList<>();
    ArrayList<String> ListaMes_Ano = new ArrayList<>();
    ArrayList<String> ListaAno = new ArrayList<>();
    Date anohoje;
    int ultimoDia = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    int diaPrimeiro = cal.get(Calendar.DAY_OF_WEEK);
    Converte converte = new Converte();

    public Datas() {
    }

    //Só o dia 
    public String retornaDia() {
        Date diahoje = new Date();
        String formato2 = "dd";
        SimpleDateFormat diaformatado = new SimpleDateFormat(formato2);

        return diaformatado.format(diahoje);
    }

    //Só o mês  
    public String retornaMes() {
        Date meshoje = new Date();
        String formato3 = "MM";
        String formato31 = "MMM";
        SimpleDateFormat mesformatado = new SimpleDateFormat(formato3);
        //System.out.println("O mes formatado e: "+ mesformatado.format(meshoje));  
        //SimpleDateFormat mesformatado2 = new SimpleDateFormat(formato31);  
        //System.out.println("Ou por extenso "+ mesformatado2.format(meshoje));  
        return mesformatado.format(meshoje);
    }

    public int RetornaDiaInteiro() {
        Date diahoje = new Date();
        String formatoDiaInt = "dd";
        SimpleDateFormat DiaIntFormatado = new SimpleDateFormat(formatoDiaInt);

        return converte.StringEmInteiro(DiaIntFormatado.format(diahoje));
    }

    public int retornaMesInteiro() {

        return converte.StringEmInteiro(retornaMes());
    }

    public int retornaAnoInteiro() {

        return converte.StringEmInteiro(retornaAno());
    }

    public String retornaAno() {
        //Só o ano 
        anohoje = new Date();
        String formato4 = "yyyy"; //Se colocar só dois vem os dois ultimos números do ano  
        SimpleDateFormat anoformatado = new SimpleDateFormat(formato4);
        //System.out.println("O ano formatado e: "+ anoformatado.format(anohoje));  

        return (anoformatado.format(anohoje));
    }

    public String retornaData() {
        Date hoje = new Date();
        String formato = "dd/MM/yyyy";
        SimpleDateFormat dataformatada = new SimpleDateFormat(formato);
        //System.out.println("A data formatada e: "+ dataformatada.format(hoje));  

        return dataformatada.format(hoje);
    }
    public String retornaData_format(){
        Date hoje = new Date();
        String formato = "dd-MM-yyyy";
        SimpleDateFormat dataformatada = new SimpleDateFormat(formato);
        //System.out.println("A data formatada e: "+ dataformatada.format(hoje));  

        return dataformatada.format(hoje);
    }
    
    public ArrayList<String> RetornaDias_Mes() {
        int i;
        ListaDia_Mes.clear();
        
        for (i = 1; i <= RetornaDiaInteiro(); ++i) {
            if (i < 10) {
                ListaDia_Mes.add("0" + i + "/" + retornaMes() + "/" + retornaAno());
            } else {
                ListaDia_Mes.add(i + "/" + retornaMes() + "/" + retornaAno());
            }
        }
       
        return ListaDia_Mes;
    }

    public ArrayList<String> RetornaMes_Ano() {
        int i;
        ListaMes_Ano.clear();
        for (i = 1; i <= retornaMesInteiro(); ++i) {
            if (i < 10) {
                ListaMes_Ano.add("0" + i);
            } else {
                ListaMes_Ano.add("" + i);
            }
        }
        return ListaMes_Ano;
    }

    public ArrayList<String> RetornaAnos() {
        int i;
        ListaAno.clear();
        for (i = 2014; i <= retornaAnoInteiro(); ++i) {

            ListaAno.add("" + i);

        }
        return ListaAno;
    }

    public String CapturaDiaDaSemana(String data) {
        Locale locale = new Locale("pt", "BR");
        SimpleDateFormat sdfEntrada = new SimpleDateFormat("dd/MM/yyyy", locale);

        Date date;
        try {
            date = sdfEntrada.parse(data);
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            return CapturaNomeDiaSemana(cal.get(Calendar.DAY_OF_WEEK));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String CapturaNomeDiaSemana(int valor) {

        if (valor == 1) {
            return "Domingo";
        } else if (valor == 2) {
            return "Segunda-feira";
        } else if (valor == 3) {
            return "Terca-feira";
        } else if (valor == 4) {
            return "Quarta-feira";
        } else if (valor == 5) {
            return "Quinta-feira";
        } else if (valor == 6) {
            return "Sexta-feira";
        } else {
            return "Sábado";
        }

    }

    public String CapturaNomeMesAno(int valor) {

        if (valor == 1) {
            return "Janeiro";
        } else if (valor == 2) {
            return "Fevereiro";
        } else if (valor == 3) {
            return "Março";
        } else if (valor == 4) {
            return "Abril";
        } else if (valor == 5) {
            return "Maio";
        } else if (valor == 6) {
            return "Junho";
        } else if (valor == 7) {
            return "Julho";
        } else if (valor == 8) {
            return "Agosto";
        } else if (valor == 9) {
            return "setembro";
        } else if (valor == 10) {
            return "Outubro";
        } else if (valor == 11) {
            return "Novembro";
        } else if (valor == 12) {
            return "Dezembro";

        }
        return null;
    }
}
