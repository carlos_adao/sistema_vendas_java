/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Compras.ARMAZENAR_COMPRA_NOTA;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class ARQUIVO_ARMAZENAR_COMPRA_NOTA {

    private static String formecedor;
    private static String diaArmazenamento;
    private static String dataArmazenamento;
    private static String obcervacao;
    private static float valorArmazenado;
    private static ARMAZENAR_COMPRA_NOTA armazenaPagamento  = new ARMAZENAR_COMPRA_NOTA();
    private static File file = new File("Arquivo Armazenar Compra Nota.csv");
    private static File arq = new File("Arquivo Armazenar Compra Nota.txt");

    public static void Armazena(ArrayList<ARMAZENAR_COMPRA_NOTA> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 


        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);

        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Produtos.size();
        for (i = 0; i < n; i++) {
            gravarArq.printf("%s%n", Produtos.get(i));
            bw.write(Produtos.get(i).toString());

        }
        gravarArq.close();
        bw.close();
        fw.close();

        System.out.println("gravado com sucesso!!!");
    }
     public static ArrayList<ARMAZENAR_COMPRA_NOTA> retorna(){ // retorna o dados do arquivo para a classe produto 
          ArrayList<ARMAZENAR_COMPRA_NOTA> ListaPagamentosArmazenados = new ArrayList<>();       
       
       try{
        
        FileReader fr = new FileReader(file);
        BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
     
        String linha = lerArq.readLine();
      
        int j = 0;
        
        while(linha != null){
           
                String[] produto = linha.split(";");

                formecedor = new String(produto[0]);
                diaArmazenamento = new String(produto[1]);
                dataArmazenamento = new String(produto[2]);
                obcervacao = new String(produto[3]);
                valorArmazenado = Float.parseFloat(produto[4]);

                armazenaPagamento = new ARMAZENAR_COMPRA_NOTA(formecedor, diaArmazenamento, dataArmazenamento, obcervacao,
                        valorArmazenado);

                linha = lerArq.readLine();

                ListaPagamentosArmazenados.add(armazenaPagamento);

            }

            fr.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
            ListaPagamentosArmazenados = null;
        }
        return ListaPagamentosArmazenados;
    }
}
