/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Entradas.ENTRADAS;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class ARQUIVO_ENTRADAS {

    private static ENTRADAS Entrada;
    private static int primarykey;
    private static float entrada, saida;
    private static String dia, mes, ano;
    private static boolean verf_entrada, verf_saida;

    private static File file = new File("Arquivo Entrada.csv");
    private static File arq = new File("Arquivo Entrada.txt");

    public static void Armazena(ArrayList<ENTRADAS> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);

        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Produtos.size();
        for (i = 0; i < n; i++) {

            gravarArq.printf("%s%n", Produtos.get(i));
            bw.write(Produtos.get(i).toString());

        }

        gravarArq.close();
        bw.close();
        fw.close();

        System.out.println("gravado com sucesso!!!");
    }

    public static ArrayList<ENTRADAS> retorna() { // retorna o dados do arquivo para a classe produto 
        ArrayList<ENTRADAS> ListaEntradas_saidas = new ArrayList<>();
        
        try {

            FileReader fr = new FileReader(file);
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));

            String linha = lerArq.readLine();

            int j = 0;

            while (linha != null) {

                String[] produto = linha.split(";");
                
                 primarykey  = Integer.parseInt(produto[1]);
                 entrada = Float.parseFloat(produto[2]);
                 saida = Float.parseFloat (produto[3]);
                 dia =  new String(produto[4]);
                 mes = new String(produto[5]);
                 ano = new String(produto[6]);
                 verf_entrada = Boolean.valueOf(produto[7]);
                 verf_saida = Boolean.valueOf(produto[8]);
                 
                   Entrada = new ENTRADAS(primarykey, entrada, saida, dia, mes, ano, verf_entrada, verf_saida);

               
                linha = lerArq.readLine();

                ListaEntradas_saidas.add(Entrada);

            }

            fr.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
        }
        return ListaEntradas_saidas;
    }

}
