/*
 * CLASSE ULILIZADA PARA FAZER  LIGAÇÃO ENTRE PAINEIS
 * 
 */
package BancoDados;

import Interface_Alternavel.PRODUTO_VENDA;
import java.io.*;
import java.util.ArrayList;

public class ARQUIVO_CAIXA_TEMPORARIO {

    private static PRODUTO_VENDA produtos;
    private static int primarykey; 
    private static String nomeProduto;
    private static String codigoProduto;
    private static String data;
    private static String Ano;
    private static String mes;
    private static String linha;
    private static int quantidadeProduto;
    private static float precoVenda_unidade, LucroVenda, descontoVenda, subTotal, valor_Venda;
    private static File file = new File("Arquivo Caixa temporario.csv");
    private static File arq = new File("Arquivo Caixa temporario.txt");
    private static int cont = 0;

public static void Armazena(ArrayList<PRODUTO_VENDA> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        
        if(!file.exists()){
          file.createNewFile();
        }
        try (FileWriter fw = new FileWriter(file); 
            BufferedWriter bw = new BufferedWriter(fw)) {
            
            FileWriter fxTxt = new FileWriter(arq);
            PrintWriter bwTxt = new PrintWriter(fxTxt);
            
            for (int i = 0; i < Produtos.size(); i++) {
               bw.write(Produtos.get(i).toString());
               bwTxt.printf("%s%n", Produtos.get(i));
            }
        }
        System.out.println("gravado com sucesso!!!");
        
        
    }
  public static ArrayList<PRODUTO_VENDA> retorna(){ // retorna o dados do arquivo para a classe produto 
          ArrayList<PRODUTO_VENDA> ListaProdutos = new ArrayList<PRODUTO_VENDA>();       
          
          
       try{
        
           if(!file.exists()){
             file.createNewFile();
           }
           
           FileReader fr = new FileReader(file);
           BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
           
           String linha = lerArq.readLine();
        
       
           while(linha != null){
              
              String[] produto = linha.split(";");
              
                 
              primarykey = Integer.parseInt(produto[0]);
              nomeProduto = new String(produto[1]);
              codigoProduto = new String( produto[2]);
              quantidadeProduto = Integer.parseInt(produto[3]);
              precoVenda_unidade = Float.parseFloat(produto[4]);
              descontoVenda = Float.parseFloat(produto[5]);
              subTotal = Float.parseFloat(produto[6]);
              LucroVenda = Float.parseFloat(produto[7]);
              valor_Venda = Float.parseFloat(produto[8]);
              data = new String( produto[9]);
              mes = new String( produto[10]);
              Ano = new String( produto[11]);
                
             
              
              produtos = new PRODUTO_VENDA(primarykey,nomeProduto, codigoProduto, quantidadeProduto, precoVenda_unidade, 
                                           descontoVenda, subTotal, LucroVenda, valor_Venda, data, mes, Ano);
             
             ListaProdutos.add(produtos);  
             linha = lerArq.readLine();
       }
        
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
        return ListaProdutos;
    }

}
