/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import BancoDados.ARQUIVO_GASTOS;
import Auxiliares.Datas;
import Compras.Visualizar.TabelaModel;
import Gastos.BOX;
import Gastos.P_Gastos;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author carlos
 */
public final class Alternavel_Caixa_Consuta extends JPanel {

    private final ARQUIVO_ESTOQUE_VENDA ArquivoVendas = new ARQUIVO_ESTOQUE_VENDA();
    private final ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = new ArrayList<>();
    private final PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA();
    private PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
    private ArrayList<PRODUTO_COMPRA> ListaProdutosComprados = new ArrayList<>();
    private ARQUIVO_ESTOQUE_COMPRA ArquivoCompras = new ARQUIVO_ESTOQUE_COMPRA();
    private PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();
    private ArrayList<PRODUTO_ESTOQUE> ListaProdutosEstoque = new ArrayList<>();
    private ARQUIVO_ESTOQUE ArquivoEstoque = new ARQUIVO_ESTOQUE();
    /* parte de instancia da classe gasto*/
    private BOX gastos = new BOX();
    private P_Gastos p_gastos = new P_Gastos();
    private ARQUIVO_GASTOS arquivoGastos = new ARQUIVO_GASTOS();
    private ArrayList<BOX> ListaGastos = new ArrayList<>();
    private Datas Data = new Datas();
    String data;
    String dataVenda;
    private String dia;
    private String mes;
    private String ano;
    private String total;
    private String dataArquivo;
    private String Data_Txt;
    private float totalVendas = 0;
    private float lucrof = 0;
    private String lucros;
    private float lucroTotal = 0;
    private String diaVenda;
    Graphics g;
    //String[] produtos;
    JLabel dataTitulo = new JLabel();
    JLabel JLtotalVenda = new JLabel();
    JLabel JLlucroVenda = new JLabel();
    JButton Botao_Voltar = new JButton();
    JNumberFormatField JTtotalVenda = new JNumberFormatField(new DecimalFormat("0.00"));
    JNumberFormatField JTlucroVenda = new JNumberFormatField(new DecimalFormat("0.00"));
    private JLabel JlData = new JLabel();
    private JTextField JtData = new JTextField();
    private JLabel Jldia = new JLabel();
    private JLabel Jlmes = new JLabel();
    private JLabel Jlano = new JLabel();
    JLabel JlgastosBox = new JLabel();
    JNumberFormatField JtgastosBox = new JNumberFormatField(new DecimalFormat("0.00"));
    FrameEditaVendas frameEditaVendas;
    TabelaModel modelo = new TabelaModel();
    boolean condicao = false;

    Calculos_ExibeVendas calculos_exibeVendas = new Calculos_ExibeVendas();
    boolean verificaInstancia = false;
    JComboBox<String> JCopcoes;
    String opcoes[];
    boolean ver = false;
    String dia_mes_ou_Ano;
    Converte convert = new Converte();

    final JTable jtable = new JTable();
    JScrollPane JSprodutosVendidos = new JScrollPane(jtable);

    public Alternavel_Caixa_Consuta() {

        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);

    }

    public void Funcionamento(int Selec_dt_ms_an, String QualData_Mes_Ano, String dataMesOuAno, String[] todosDias_Mes_Ano) {

        if (Selec_dt_ms_an == 1) {
            Tabela(calculos_exibeVendas.RetornaValoresParaExibirVendas(QualData_Mes_Ano));
            iniciaizaComponetes("Vendas do dia", dataMesOuAno, calculos_exibeVendas.RetornaTotalVendas(), calculos_exibeVendas.RetornaTotalLucro(), todosDias_Mes_Ano);
        } else if (Selec_dt_ms_an == 2) {
            Tabela(calculos_exibeVendas.RetornaValoresParaExibirVendas(QualData_Mes_Ano));
            iniciaizaComponetes("Vendas do Mês", dataMesOuAno, calculos_exibeVendas.RetornaTotalVendas(), calculos_exibeVendas.RetornaTotalLucro(), todosDias_Mes_Ano);
        } else {
            Tabela(calculos_exibeVendas.RetornaValoresParaExibirVendas(QualData_Mes_Ano));
            iniciaizaComponetes("Vendas do Ano", dataMesOuAno, calculos_exibeVendas.RetornaTotalVendas(), calculos_exibeVendas.RetornaTotalLucro(), todosDias_Mes_Ano);
        }

    }

    public void iniciaizaComponetes(String titulo, String dataMesOuAno, String vendasTotal, String lucrototal, String[] todosDias_Mes_Ano) {// o tituo referece ao dia mes ou ano.  o var é o dia  o mes ou o ano

        dataTitulo.setBounds(new Rectangle(168, 135, 400, 30));
        dataTitulo.setText(titulo);
        dataTitulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 15));
        dataTitulo.setLocation(7, 10);
        add(dataTitulo);
        if (!ver) {
            JCopcoes = new JComboBox(todosDias_Mes_Ano);

            JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
            JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JCopcoes.setSize(150, 20);
            JCopcoes.setLocation(10, 36);
        } else {

            JCopcoes.setModel(new DefaultComboBoxModel<>(todosDias_Mes_Ano));
        }

        JCopcoes.addItemListener(
                new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {

                            Tabela(calculos_exibeVendas.RetornaValoresParaExibirVendas(JCopcoes.getSelectedItem().toString()));
                            if (Data.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString()) != null) {
                                dia_mes_ou_Ano = Data.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString());
                            } else if (Data.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString())) != null) {
                                dia_mes_ou_Ano = Data.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString()));
                            } else {
                                dia_mes_ou_Ano = JCopcoes.getSelectedItem().toString();
                            }
                            AtualizarValores(calculos_exibeVendas.RetornaTotalVendas(), calculos_exibeVendas.RetornaTotalLucro(), dia_mes_ou_Ano);
                            if (calculos_exibeVendas.RetornaValoresParaExibirVendas(JCopcoes.getSelectedItem().toString()).isEmpty()) {
                                if (!condicao) {
                                    JOptionPane.showMessageDialog(null, "NÃO EXISTE VENDAS CADASTRA PARA ESSE DIA");
                                    condicao = true;
                                }
                            }
                            if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {

                            }

                        }
                        condicao = false;
                    }
                });
        add(JCopcoes);

        Jldia.setVisible(true);
        Jldia.setBounds(new Rectangle(168, 135, 400, 30));
        Jldia.setText(dataMesOuAno);
        Jldia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 35));
        Jldia.setLocation(250, 20);
        add(Jldia);

        JLtotalVenda.setBounds(new Rectangle(168, 135, 400, 50));
        JLtotalVenda.setText("Total Vendas");
        JLtotalVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLtotalVenda.setLocation(880, 40);
        add(JLtotalVenda);

        JTtotalVenda.setBounds(new Rectangle(200, 135, 120, 17));
        JTtotalVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTtotalVenda.setSize(120, 50);
        JTtotalVenda.setLocation(880, 80);//
        JTtotalVenda.setText(vendasTotal);
        add(JTtotalVenda);

        JLlucroVenda.setBounds(new Rectangle(168, 135, 400, 50));
        JLlucroVenda.setText("Lucro Vendas");
        JLlucroVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLlucroVenda.setLocation(880, 120);
        add(JLlucroVenda);

        JTlucroVenda.setBounds(new Rectangle(200, 135, 120, 17));
        JTlucroVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTlucroVenda.setSize(120, 50);
        JTlucroVenda.setLocation(880, 160);//
        JTlucroVenda.setText(lucrototal);
        JTlucroVenda.setVisible(true);
        add(JTlucroVenda);

        JlgastosBox.setBounds(new Rectangle(168, 135, 400, 50));
        JlgastosBox.setText("Gastos");
        JlgastosBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JlgastosBox.setLocation(850, 210);
        JlgastosBox.setVisible(false);
        add(JlgastosBox);

        JtgastosBox.setBounds(new Rectangle(200, 135, 120, 17));
        JtgastosBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JtgastosBox.setSize(120, 50);
        JtgastosBox.setLocation(850, 250);//
        JtgastosBox.setVisible(false);
        add(JtgastosBox);

        ver = true;
    }

    public void AtualizarValores(String TotalVenda, String TotalLucros, String DiaDaSemana) {

        Jldia.setText(DiaDaSemana);
        JTtotalVenda.setText(TotalVenda);
        JTlucroVenda.setText(TotalLucros);

    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Key", "Quantidade", "Codigo", "Descrição", "Preço venda", "Desconto", "Sub Total", "Lucro", "Data"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);

        jtable.setModel(modelo);

        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(5);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(8).setPreferredWidth(20);

        jtable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int linha;

                if (e.getClickCount() == 2) {

                    linha = jtable.getSelectedRow();
                    String Chave = String.valueOf(jtable.getValueAt(linha, 0));
                    
                   
                    
                    RemoverProdutoVendido(Chave);
                    ReEstabeleceChave();
                    jtable.resetKeyboardActions();
                    

                    //JOptionPane.showMessageDialog(null, ListaProdutosVendidos.get(ListaProdutosVendidos.size()-1));
                }
            }
        });
        jtable.resetKeyboardActions();

        JSprodutosVendidos.setLocation(10, 80);
        JSprodutosVendidos.getVerticalScrollBar();
        JSprodutosVendidos.setSize(850, 250);
        JSprodutosVendidos.revalidate();
        add(JSprodutosVendidos);

    }

    public void RemoverProdutoVendido(String chave) {
        ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
        Operacao operacao = new Operacao();
        //JOptionPane.showMessageDialog(null, chave);
        for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
            if (convert.StringEmInteiro(chave) == ListaProdutosVendidos.get(i).getPrimarykey()) {
                JOptionPane.showMessageDialog(null,convert.StringEmInteiro(chave));
                JOptionPane.showMessageDialog(null,ListaProdutosVendidos.get(i).getPrimarykey());
                JOptionPane.showMessageDialog(null,convert.StringEmInteiro(chave) == ListaProdutosVendidos.get(i).getPrimarykey());
                JOptionPane.showMessageDialog(null, "removendo");
                JOptionPane.showMessageDialog(null, ListaProdutosVendidos.get(i));
                operacao.DiminueOUsomaProdutoDoEstoque(ListaProdutosVendidos.get(i), 1);
                ListaProdutosVendidos.remove(ListaProdutosVendidos.get(i));
                SalvanoArquivo(ListaProdutosVendidos);
            }
        }
    }

    public void ReEstabeleceChave() {

        ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();

        for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
            ListaProdutosVendidos.get(i).setPrimarykey(i);
        }
        SalvanoArquivo(ListaProdutosVendidos);
    }

    public void SalvanoArquivo(ArrayList<PRODUTO_VENDA> ListaProdutosVendidos) {
        try {
            ARQUIVO_ESTOQUE_VENDA.Armazena(ListaProdutosVendidos);
        } catch (IOException ex) {
            Logger.getLogger(Alternavel_Caixa_Consuta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public float retornaGastos(String data) {//DADO UMA DATA RETORNA OS GASTOS DESSA DATA

        float totalGastos = 0;
        ListaGastos = ARQUIVO_GASTOS.retorna();

        for (int i = 0; i < ListaGastos.size(); i++) {
            gastos = (BOX) ListaGastos.get(i);

            if (data.equalsIgnoreCase(gastos.getDia())) {
                totalGastos = totalGastos + gastos.getQuanto();
            }
            if (data.equalsIgnoreCase(gastos.getMes())) {
                totalGastos = totalGastos + gastos.getQuanto();
            }

            if (data.equalsIgnoreCase(gastos.getAno())) {
                totalGastos = totalGastos + gastos.getQuanto();
            }
        }
        return totalGastos;
    }

    /*  public void RemoveProduto_VendaAguardandoPagamento(PRODUTO_VENDA produto_venda) {
     PRODUTO_VENDA Local_produto_venda = new PRODUTO_VENDA();
     ArrayList<String[]> Local_dados = new ArrayList<>();
     int operacao = 1;

     for (int i = 0; i < L_ProdutoVenda_aguardandoPagamento.size(); i++) {
     Local_produto_venda = (PRODUTO_VENDA) L_ProdutoVenda_aguardandoPagamento.get(i);
     if (produto_venda.getCodigoProduto().equalsIgnoreCase(Local_produto_venda.getCodigoProduto())) {
     L_ProdutoVenda_aguardandoPagamento.remove(Local_produto_venda);
     CriaTabela(TransformaListaEmString(L_ProdutoVenda_aguardandoPagamento));
     DiminueOUsomaProdutoDoEstoque(Local_produto_venda, operacao);
     colocaValoresP_resultado(Local_produto_venda, 0);
     }

     }*/
}
