/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import Cadastro.Datas;
import Compras.Visualizar.TabelaModel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author carlos
 */
public class Alternavel_Compras_Consuta extends JPanel {
    
    private JScrollPane JSprodutosComprados;
    private ARQUIVO_ESTOQUE_COMPRA ArquivoCompras = new ARQUIVO_ESTOQUE_COMPRA();
    private ArrayList<PRODUTO_COMPRA> ListaProdutosComprados = new ArrayList<>();
    private PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
    private String dia;
    private String mes;
    private String ano;
    private String total;
    private String dataArquivo;
    private String Data_Txt;
    private float totalF = 0;
    private ArrayList<String[]> dados = new ArrayList<>();
    String quantidade,codigo,nome,precoCompraUni,precoCompraTotal;
    JLabel JLtotalCompras = new JLabel();
    JNumberFormatField JTtotalComprs = new JNumberFormatField(new DecimalFormat("0.00")) ;
    private JLabel JlData = new JLabel();
    private JLabel Jldia = new JLabel();
    private JLabel Jlmes = new JLabel();
    private JLabel Jlano = new JLabel();
    private JTextField JtData = new JTextField(dia);
    private JRadioButton JR_dia, JR_mes, JR_ano;
    private ButtonGroup radioGroup = new ButtonGroup();
    JTable jtable;
    TabelaModel modelo = new TabelaModel(); 
    Calculos_ExibeCompras Calculos_ExibeCompras = new Calculos_ExibeCompras();
  
    JLabel dataTitulo = new JLabel();
    JComboBox<String> JCopcoes;
    String opcoes[];
    boolean ver = false;
    private Auxiliares.Datas Data = new Auxiliares.Datas();
    String dia_mes_ou_Ano;
    Converte convert = new Converte();
    boolean condicao = false;

    public Alternavel_Compras_Consuta() {
        this.setLayout(null);
        this.setSize(1336, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
        jtable = new JTable();
        JSprodutosComprados = new JScrollPane(jtable);
        
        JSprodutosComprados.setLocation(1, 60);
        JSprodutosComprados.getVerticalScrollBar();
        JSprodutosComprados.setSize(800, 250);
        JSprodutosComprados.revalidate();
        add(JSprodutosComprados);
    }
     public void Funcionamento(int Selec_dt_ms_an, String QualData_Mes_Ano, String dataMesOuAno, String[] todosDias_Mes_Ano) {
        
        if (Selec_dt_ms_an == 1) {
            Tabela(Calculos_ExibeCompras.RetornaValoresParaExibirCompras(QualData_Mes_Ano));
            iniciaizaComponetes("Compras do dia", dataMesOuAno, Calculos_ExibeCompras.RetornaTotalCompras(),  todosDias_Mes_Ano);
        } else if (Selec_dt_ms_an == 2) {
            Tabela(Calculos_ExibeCompras.RetornaValoresParaExibirCompras(QualData_Mes_Ano));
            iniciaizaComponetes("Compras do Mês", dataMesOuAno, Calculos_ExibeCompras.RetornaTotalCompras(), todosDias_Mes_Ano);
        } else {
            
            Tabela(Calculos_ExibeCompras.RetornaValoresParaExibirCompras(QualData_Mes_Ano));
            iniciaizaComponetes("Compras do Ano", dataMesOuAno, Calculos_ExibeCompras.RetornaTotalCompras(), todosDias_Mes_Ano);
        }

    }
    
     public void iniciaizaComponetes(String titulo, String dataMesOuAno, String TotalCompras, String[] todosDias_Mes_Ano) {// o tituo referece ao dia mes ou ano.  o var é o dia  o mes ou o ano
      
        dataTitulo.setBounds(new Rectangle(168, 135, 400, 30));
        dataTitulo.setText(titulo);
        dataTitulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 15));
        dataTitulo.setLocation(7, 10);
        add(dataTitulo);
        if (!ver) {
            JCopcoes = new JComboBox(todosDias_Mes_Ano);

            JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
            JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JCopcoes.setSize(150, 20);
            JCopcoes.setLocation(10, 36);
        } else {
          
        JCopcoes.setModel(new DefaultComboBoxModel<>(todosDias_Mes_Ano));
        }

        JCopcoes.addItemListener(
                new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {

                            Tabela(Calculos_ExibeCompras.RetornaValoresParaExibirCompras(JCopcoes.getSelectedItem().toString()));
                            if (Data.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString()) != null) {
                                dia_mes_ou_Ano = Data.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString());
                            } else if (Data.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString())) != null) {
                                dia_mes_ou_Ano = Data.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString()));
                            } else {
                                dia_mes_ou_Ano = JCopcoes.getSelectedItem().toString();
                            }
                            AtualizarValores(Calculos_ExibeCompras.RetornaTotalCompras(), dia_mes_ou_Ano);
                            if (Calculos_ExibeCompras.RetornaValoresParaExibirCompras(JCopcoes.getSelectedItem().toString()).isEmpty()) {
                                if (!condicao) {
                                    JOptionPane.showMessageDialog(null, "NÃO EXISTE COPRA CADASTRADA!");
                                    condicao = true;
                                }
                            }
                            if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {

                            }

                        }
                        condicao = false;
                    }
                 });
         add(JCopcoes);

         Jldia.setVisible(true);
         Jldia.setBounds(new Rectangle(168, 135, 400, 30));
         Jldia.setText(dataMesOuAno);
         Jldia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 35));
         Jldia.setLocation(250, 20);
         add(Jldia);

         JLtotalCompras.setBounds(new Rectangle(168, 135, 400, 50));
         JLtotalCompras.setText("Valor total Compras");
         JLtotalCompras.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
         JLtotalCompras.setLocation(800, 40);
         add(JLtotalCompras);

         JTtotalComprs.setBounds(new Rectangle(200, 135, 120, 17));
         JTtotalComprs.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
         JTtotalComprs.setSize(120, 50);
         JTtotalComprs.setLocation(850, 80);//
         JTtotalComprs.setText(TotalCompras);
         add(JTtotalComprs);

        ver = true;
    }
      public void AtualizarValores(String TotalCompras, String DiaDaSemana) {
      
        Jldia.setText(DiaDaSemana);
        JTtotalComprs.setText(TotalCompras);
      

    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Quantidade", "Codigo", "Descrição", "Preco uni ", "Preco Tot ", "Data"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);

        JSprodutosComprados.setLocation(10, 80);
        JSprodutosComprados.getVerticalScrollBar();
        JSprodutosComprados.setSize(800, 250);
        JSprodutosComprados.revalidate();
        add(JSprodutosComprados);

    }

    //FAZ O CALCULO DAS COMPRAS CASO SEJA EM FORMA DE DIA 
    public void AcoesParoDia(String text, String data) {
        Jldia.setVisible(true);
        Jldia.setBounds(new Rectangle(168, 135, 400, 30));
        Jldia.setText(text + data);
        Jldia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Jldia.setLocation(180, 20);
        add(Jldia);
    }
  
}
