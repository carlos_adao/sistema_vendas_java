/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ArquivoProdutos;
import Cadastro.CadastroProduto;
import Cadastro.Datas;
import Compras.P_CompraNota;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public final class Alternavel_Adc_ProdEstoque extends JPanel {

    int cont = 0;
    String precoUnidade, PrecoTotal, nomePAquivo, nomePProcurar, CodigoPArquivo, precoPorUnidade;
    private JCheckBoxMenuItem styleItems[];
    JMenu Menu_Compras;//, ComprasAguandando, editarEstoque;
    JButton Botao_salvar, Botao_cancelar;
    ARQUIVO_ESTOQUE arquivo;
    CadastroProduto Produto = new CadastroProduto();                                      //ISTANCIA UM NOVO PRODUTO DA CLASSE CADASTRO PRODUTO 
    ArrayList<CadastroProduto> ListaProdutoCadastrados = new ArrayList<CadastroProduto>();//ISTANCIA UM ARRAY COM VARIOS PRODUTOS CLASSE CADASTRO PRODUTO
    PRODUTO_ESTOQUE ProdutoEstoque = new PRODUTO_ESTOQUE();                                  //ISTANCIA A NOVO PRODUTO DA CLASSE ProdutoEstoque
    PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();//ISTANCIA UM NOVO PRODUTO COMPRA PARA VERIFICAR SE ELE JA ESTA NO ESTOQUE                                 
    ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<PRODUTO_ESTOQUE>();  //ISTANCIA UM ARRAY COM VARIOS PRODUTO DA CLASSE COMPRA
    ArquivoProdutos BancoDadosProduto;
    EnterPrecionadoBtSalvar enterPrecionadoBtSalvar = new EnterPrecionadoBtSalvar();
    AlternavelEst alternavel_Est = new AlternavelEst();
    //ISTANCIANO VALORES DA CLASSE COMPRA
    PRODUTO_COMPRA ProdutoCompra = new PRODUTO_COMPRA();
    PRODUTO_COMPRA Produto_Compra = new PRODUTO_COMPRA();
    ArrayList<PRODUTO_COMPRA> ListaProduto_Compra = new ArrayList<PRODUTO_COMPRA>();
    ARQUIVO_ESTOQUE_COMPRA ArquivoCompra;
    //DATAS PARA USAR NA CLASSE COMPRA
    Datas data = new Datas();
    String dia = data.retornaData();
    String mes = data.retornaMes();
    String ano = data.retornaAno();
    float precoTotal;
    float precoCompraUnidade;//variavel usada no JTextfield Preco de compra por unidade
    float precoVendaUnidade;//variavel usada no JTextfield Preco de compra por unidade
    String lucroCompraUnidade;//variavel usada no JTextfield Preco de compra por unidade
    int verificao, quantidadeProduto;
    JLabel JLnomeMercadoria = new JLabel();
    JLabel JLcodigoMercadoria = new JLabel();
    JLabel JLprecoVendaUnidade = new JLabel();
    JLabel JLlucroUnidade = new JLabel();
    JLabel JLquantidaMercadoria = new JLabel();
    JLabel JLprecoCompraTotal = new JLabel();
    JLabel JLprecoCompraUni = new JLabel();
    JLabel JLlucroTotal = new JLabel();
    JTextField JTcodigo_mercadoria = new JtextFieldSomenteNumeros();
    JTextField JTnome_mercadoria = new JTextField(15);
    JTextField JtQuantidade_mercadoria = new JtextFieldSomenteNumeros();
    private JNumberFormatField JtPrecoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtPreco_total = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtPrecoCompraUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtLucroProdutoUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtLucroProdutoTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    private JTextField Tf_produto;
    EnterPrecionadoQuantidade enterPrecionadoQuantidade = new EnterPrecionadoQuantidade();
    EnterPrecionadoPreCmTotal EnterPrecionadoPreCmTotal = new EnterPrecionadoPreCmTotal();
    EnterPrecionadoPvendUnid EnterPrecionadoPvendUnid = new EnterPrecionadoPvendUnid();
    Teclado teclado = new Teclado();
    protected String str1 = "";
    private JComboBox cBox_prod;
    P_CompraNota P_CompraNota = new P_CompraNota();
    String[] nomes = P_CompraNota.pegaTodosNomesProdutos();
    AutoCompleteProduto AutoCompleteProduto = new AutoCompleteProduto(nomes);

    public Alternavel_Adc_ProdEstoque() {

        this.setLayout(null);
        this.setSize(1336, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);

        //----------------------JTextField--------------------------------------------//

        InicializaLabel();

        cBox_prod = AutoCompleteProduto;
        cBox_prod.repaint();
        cBox_prod.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_prod.addKeyListener(teclado);
        cBox_prod.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        cBox_prod.setSize(250, 25);
        cBox_prod.setLocation(1, 45);
        cBox_prod.setEditable(true);
        add(cBox_prod);


        //** JTextField Codigo da mercadoria**//
        JTcodigo_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JTcodigo_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JTcodigo_mercadoria.setSize(250, 25);
        JTcodigo_mercadoria.setLocation(270, 45);
        add(JTcodigo_mercadoria);

        /*
         * JTextField preço de compra por unidade
         */
        JtPrecoVendaUnidade.setBounds(new Rectangle(200, 135, 120, 17));
        JtPrecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtPrecoVendaUnidade.setSize(250, 25);
        JtPrecoVendaUnidade.setLocation(1, 145);//
        JtPrecoVendaUnidade.addKeyListener(EnterPrecionadoPvendUnid);
        JtPrecoVendaUnidade.addFocusListener(EnterPrecionadoPvendUnid);
        add(JtPrecoVendaUnidade);

        JtLucroProdutoUnidade.setBounds(new Rectangle(200, 135, 120, 17));
        JtLucroProdutoUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtLucroProdutoUnidade.setSize(250, 25);
        JtLucroProdutoUnidade.setLocation(1, 195);
        JtLucroProdutoUnidade.setEditable(false);
        add(JtLucroProdutoUnidade);

        //** JTextField quantidade**//
        JtQuantidade_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JtQuantidade_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtQuantidade_mercadoria.setSize(250, 25);
        JtQuantidade_mercadoria.setLocation(1, 95);
        JtQuantidade_mercadoria.addKeyListener(enterPrecionadoQuantidade);
        add(JtQuantidade_mercadoria);

        //** JTextField preco de compra total**//
        JtPreco_total.setBounds(new Rectangle(200, 135, 120, 17));
        JtPreco_total.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtPreco_total.setSize(250, 25);
        JtPreco_total.setLocation(270, 95);//
        JtPreco_total.addFocusListener(EnterPrecionadoPreCmTotal);
        JtPreco_total.addKeyListener(EnterPrecionadoPreCmTotal);
        add(JtPreco_total);

        /*
         * JTextField Preco de compra Unidade
         */
        JtPrecoCompraUnidade.setBounds(new Rectangle(200, 135, 120, 17));
        JtPrecoCompraUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtPrecoCompraUnidade.setSize(250, 25);
        JtPrecoCompraUnidade.setLocation(270, 145);
        JtPrecoCompraUnidade.setEditable(false);
        add(JtPrecoCompraUnidade);

        /*
         * JTextField Lucro do produto por Unidade
         */
        JtLucroProdutoTotal.setBounds(new Rectangle(200, 135, 120, 17));
        JtLucroProdutoTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JtLucroProdutoTotal.setSize(250, 25);
        JtLucroProdutoTotal.setLocation(270, 195);
        JtLucroProdutoTotal.setEditable(false);
        add(JtLucroProdutoTotal);

        //----------Botões-Salvar-e-Cancelar-----------------------//

        //**Jbutton Salvar**//
        Botao_salvar = new JButton();
        Botao_salvar.setText("ADCIONAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.addActionListener(enterPrecionadoBtSalvar);
        Botao_salvar.addKeyListener(enterPrecionadoBtSalvar);
        Botao_salvar.setLocation(1, 300);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        // this.Botao_cancelar.addActionListener();
        Botao_cancelar.setLocation(100, 300);
        add(Botao_cancelar);

    }

    public void inicializa() {

        String[] nomes = P_CompraNota.pegaTodosNomesProdutos();
        AutoCompleteProduto.setModel(new DefaultComboBoxModel<>(nomes));

    }

    public void SalvarProduto() {
        AlternavelEst alternavelEst = new AlternavelEst();
        boolean flag = false;
        boolean ver1 = true;

        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        ProdutoEstoque = RetornaProdutoEstoque();


        /*
         * CASO A LISTA ESTEJA VAZIA ADICIONA UM PRODUTO NELA
         */
        if (ListaProduto_Estoque.isEmpty()) {
            ListaProduto_Estoque.add(RetornaProdutoEstoque());
        } else {
            for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
                Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);

                if (Produto_Estoque.getNomeProduto().equalsIgnoreCase(Tf_produto.getText())) {
                    flag = true;
                    int quantidadeGeral = Integer.parseInt(JtQuantidade_mercadoria.getText()) + Produto_Estoque.getQuantidadeProduto();
                    float precoCompGerd = Float.parseFloat(JtPreco_total.getText().replace(",", ".")) + Produto_Estoque.getPrecoCompra_Geral();
                    float precoCompraUni = precoCompGerd / quantidadeGeral;
                    float precoVendaUni = Float.parseFloat(JtPrecoVendaUnidade.getText().replace(",", "."));
                    float lucroUni = precoVendaUni - precoCompraUni;
                    float lucroTot = quantidadeGeral * lucroUni;

                    //APENAS PARA VERIFICAÇÃO
                    float precCompGerlAq = Produto_Estoque.getPrecoCompra_Geral();
                    float precCompTxt = Float.parseFloat(JtPreco_total.getText().replace(",", "."));

                    Produto_Estoque.setQuantidadeProduto(quantidadeGeral);
                    Produto_Estoque.setPrecoCompra_Geral(precoCompGerd);
                    Produto_Estoque.setPrecoCompra_unidade(precoCompraUni);
                    Produto_Estoque.setPrecoVenda_unidade(precoVendaUni);
                    Produto_Estoque.setLucroUnidade(lucroUni);
                    Produto_Estoque.setLucroGeral(lucroTot);

                }

            }

            if (!flag) {
                ListaProduto_Estoque.add(ProdutoEstoque);
            }
        }

        //COLOCANDO O PRODUTO NO ESTOQUE 
        try {
            arquivo.Armazena(ListaProduto_Estoque);
        } catch (IOException ex) {
            Logger.getLogger(AlternavelCadastroProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        alternavelEst.atualiza();
        JTnome_mercadoria.requestFocus();
        LimparTextField_();
        try {
            ArquivoProdutos.Armazena(ListaProdutoCadastrados);
        } catch (IOException ex) {
            Logger.getLogger(AlternavelCadastroProduto.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public PRODUTO_ESTOQUE RetornaProdutoEstoque() {

        //COLOCANDO VALORES NA CLASSE PRODUTO ESTOQUE
        ProdutoEstoque.setNomeProduto(Tf_produto.getText());
        ProdutoEstoque.setCodigoProduto(JTcodigo_mercadoria.getText());
        ProdutoEstoque.setQuantidadeProduto(Integer.parseInt(JtQuantidade_mercadoria.getText()));
        ProdutoEstoque.setPrecoCompra_unidade(Float.parseFloat(JtPrecoCompraUnidade.getText().replace(",", ".")));
        ProdutoEstoque.setPrecoVenda_unidade(Float.parseFloat(JtPrecoVendaUnidade.getText().replace(",", ".")));
        ProdutoEstoque.setPrecoCompra_Geral(Float.parseFloat(JtPreco_total.getText().replace(",", ".")));
        ProdutoEstoque.setLucroUnidade(Float.parseFloat(JtLucroProdutoUnidade.getText().replace(",", ".")));
        ProdutoEstoque.setLucroGeral(Float.parseFloat(JtLucroProdutoTotal.getText().replace(",", ".")));

        return ProdutoEstoque;
    }

    public void ColocaCodigoProduto() {
        //verifica se o produto já é cadastrado  
        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        if (P_CompraNota.RetornaCodigoProduto(cBox_prod.getSelectedItem().toString()).equalsIgnoreCase("")) {
            String codigo = ListaProduto_Estoque.get(ListaProduto_Estoque.size() - 1).getCodigoProduto();
            int Codigo = Integer.parseInt(codigo);
            Codigo++;
            JTcodigo_mercadoria.setText(String.valueOf(Codigo));
            JtQuantidade_mercadoria.requestFocus();

        } else {
            JTcodigo_mercadoria.setText(P_CompraNota.RetornaCodigoProduto(cBox_prod.getSelectedItem().toString()));
            JtQuantidade_mercadoria.requestFocus();
        }
    }

    private void LimparTextField_() {
        JTcodigo_mercadoria.setText(null);
        Tf_produto.setText(null);
        JtPreco_total.setText(null);
        JtPrecoVendaUnidade.setText(null);
        JtQuantidade_mercadoria.setText(null);
        JtPrecoCompraUnidade.setText(null);
        JtLucroProdutoTotal.setText(null);
        JtLucroProdutoUnidade.setText(null);
    }

    public static String converterFloatString(float precofloat) {
        /*
         * Transformando um double em 2 casas decimais
         */
        DecimalFormat fmt = new DecimalFormat("0.00");   //limita o número de casas decimais      
        String string = fmt.format(precofloat);
        String[] part = string.split("[,]");
        String preco = part[0] + "." + part[1];
        return preco;
    }

    public void InicializaLabel() {
        //----------------------JLabel--------------------------------------------//


        /*
         * JLabel nome da mercadoria
         */
        JLnomeMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLnomeMercadoria.setText("Nome da mercadoria");
        JLnomeMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLnomeMercadoria.setLocation(1, 20);
        add(JLnomeMercadoria);

        // ** JLabel Codigo da mercadoria**//
        JLcodigoMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLcodigoMercadoria.setText("Código da Mercadoria");
        JLcodigoMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLcodigoMercadoria.setLocation(270, 20);

        add(JLcodigoMercadoria);

        //** JLabel preço por unidade mercadoria**//
        JLprecoVendaUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLprecoVendaUnidade.setText("preço de venda unid");
        JLprecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLprecoVendaUnidade.setLocation(1, 120);
        add(JLprecoVendaUnidade);

        /*
         * JLabel Lucro do Produto por unidade
         */
        JLlucroUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLlucroUnidade.setText("Lucro por uni");
        JLlucroUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLlucroUnidade.setLocation(1, 170);
        add(JLlucroUnidade);

        //** JLabel quantidade mercadoria**//
        JLquantidaMercadoria = new JLabel();
        JLquantidaMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLquantidaMercadoria.setText("Quantidade");
        JLquantidaMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLquantidaMercadoria.setLocation(1, 70);
        add(JLquantidaMercadoria);

        //** JLabel preço total mercadoria**//  
        JLprecoCompraTotal.setBounds(new Rectangle(168, 135, 400, 30));
        JLprecoCompraTotal.setText("preço Compra tot");
        JLprecoCompraTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLprecoCompraTotal.setLocation(270, 70);
        add(JLprecoCompraTotal);

        /*
         * JLabel preco de compra por unidade
         */
        JLprecoCompraUni.setBounds(new Rectangle(168, 135, 400, 30));
        JLprecoCompraUni.setText("Preco de Compra Uni");
        JLprecoCompraUni.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLprecoCompraUni.setLocation(270, 120);
        add(JLprecoCompraUni);

        JLlucroTotal.setBounds(new Rectangle(168, 135, 400, 30));
        JLlucroTotal.setText("Lucro Total");
        JLlucroTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        JLlucroTotal.setLocation(270, 170);
        add(JLlucroTotal);
    }

    public class Teclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //preciona enter no nome produto
            if (e.getKeyCode() == 10) {
                ColocaCodigoProduto();

            }
        }
    }

    public class EnterPrecionadoQuantidade implements FocusListener, KeyListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            if (JTcodigo_mercadoria.getText() == null) {
                JTcodigo_mercadoria.requestFocus();
            } else {
                JtPreco_total.requestFocus();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10 || e.getKeyCode() == 39) {
                JtPreco_total.requestFocus();
            }

        }
    }

    public class EnterPrecionadoPreCmTotal implements FocusListener, KeyListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (JtQuantidade_mercadoria.getText().isEmpty()) {
            } else {
                quantidadeProduto = Integer.parseInt(JtQuantidade_mercadoria.getText());
                precoTotal = Float.parseFloat(JtPreco_total.getText().replace(",", "."));
                precoTotal = precoTotal / quantidadeProduto;
                precoPorUnidade = converterFloatString(precoTotal);
                JtPrecoCompraUnidade.setText(precoPorUnidade);
                JtPrecoVendaUnidade.requestFocus();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10 || e.getKeyCode() == 39) {
                if (JtQuantidade_mercadoria.getText().isEmpty()) {
                } else {
                    quantidadeProduto = Integer.parseInt(JtQuantidade_mercadoria.getText());
                    precoTotal = Float.parseFloat(JtPreco_total.getText().replace(",", "."));
                    precoTotal = precoTotal / quantidadeProduto;
                    precoPorUnidade = converterFloatString(precoTotal);
                    JtPrecoCompraUnidade.setText(precoPorUnidade);
                    JtPrecoVendaUnidade.requestFocus();
                }

            }
        }
    }

    public class EnterPrecionadoBtSalvar implements ActionListener, KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {

            if (e.getKeyCode() == 10) {
                SalvarProduto();
                inicializa();
                //Tf_produto.requestFocus();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {
                SalvarProduto();
                inicializa();
                // Tf_produto.requestFocus();
            }
        }
    }

    public class EnterPrecionadoPvendUnid implements FocusListener, KeyListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (JtQuantidade_mercadoria.getText().isEmpty()) {
            } else {
                quantidadeProduto = Integer.parseInt(JtQuantidade_mercadoria.getText());
                precoVendaUnidade = Float.parseFloat(JtPrecoVendaUnidade.getText().replace(",", "."));
                precoCompraUnidade = Float.parseFloat(JtPrecoCompraUnidade.getText().replace(",", "."));
                precoCompraUnidade = (precoVendaUnidade - precoCompraUnidade);
                if (precoCompraUnidade < 0) {
                    JOptionPane.showMessageDialog(null, "Prejuizo!!!\n O preco deve ser superio a: R$" + precoVendaUnidade + "0");
                    JtPrecoVendaUnidade.setText(null);

                } else {
                    precoVendaUnidade = quantidadeProduto * precoCompraUnidade;
                    lucroCompraUnidade = converterFloatString(precoCompraUnidade);
                    String lucroCompraTotal = converterFloatString(precoVendaUnidade);
                    JtLucroProdutoTotal.setText(lucroCompraTotal);
                    JtLucroProdutoUnidade.setText(lucroCompraUnidade);
                    Botao_salvar.requestFocus();
                }
            }

        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10 || e.getKeyCode() == 39) {
                if (JtQuantidade_mercadoria.getText().isEmpty()) {
                } else {
                    quantidadeProduto = Integer.parseInt(JtQuantidade_mercadoria.getText());
                    precoVendaUnidade = Float.parseFloat(JtPrecoVendaUnidade.getText().replace(",", "."));
                    precoCompraUnidade = Float.parseFloat(JtPrecoCompraUnidade.getText().replace(",", "."));
                    precoCompraUnidade = (precoVendaUnidade - precoCompraUnidade);
                    if (precoCompraUnidade < 0) {
                        JOptionPane.showMessageDialog(null, "Prejuizo!!!\n O preco deve ser superio a: R$" + precoVendaUnidade + "0");
                        JtPrecoVendaUnidade.setText(null);

                    } else {
                        precoVendaUnidade = quantidadeProduto * precoCompraUnidade;
                        lucroCompraUnidade = converterFloatString(precoCompraUnidade);
                        String lucroCompraTotal = converterFloatString(precoVendaUnidade);
                        JtLucroProdutoTotal.setText(lucroCompraTotal);
                        JtLucroProdutoUnidade.setText(lucroCompraUnidade);
                        Botao_salvar.requestFocus();

                    }
                }

            }
        }
    }

    public class AutoCompleteProduto extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        Teclado Teclado = new Teclado();
        int contador = 0;

        private AutoCompleteProduto() {
        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        //Auto Completa o Tf_produto
        public AutoCompleteProduto(Object[] items) {
            super(items);

            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                Tf_produto = (JTextField) getEditor().getEditorComponent();

                if (Tf_produto != null) {
                    Tf_produto.addKeyListener(Teclado);

                    Tf_produto.setDocument(new AutoCompleteProduto.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            Tf_produto = (JTextField) getEditor().getEditorComponent();
                            Tf_produto.setSize(230, 25);
                            String text = Tf_produto.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {

                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    Tf_produto.setText(current);
                                    Tf_produto.setSelectionStart(text.length());
                                    Tf_produto.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }
}
