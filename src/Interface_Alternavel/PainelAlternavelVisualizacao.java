/*
 * nesse painel terá a visualiação dos dados do sistema 
 * como vendas, compras, compras notas, gastos 
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import Auxiliares.Datas;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Compras.PAGAMENTO_NOTA;
import Compras.Visualizar.Calculos_ExibePagamentosCompraNota;
import Compras.Visualizar.TabelaModel;
import Entradas.Calculos_Entradas;
import Gastos.BOX;
import Gastos.Calculos_ExibeGastos;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

public final class PainelAlternavelVisualizacao extends JPanel {

    JButton Botao_Atualizar = new JButton();

  
    private BALANCO balaco = new BALANCO();
    private BOX gastos = new BOX();
    private ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = new ArrayList<>();
    private ArrayList<PRODUTO_COMPRA> ListaProdutosComprados = new ArrayList<>();
    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = new ArrayList<>();
    private ArrayList<BOX> ListaGastos = new ArrayList<>();
    private ArrayList<String[]> ListaBalaco = new ArrayList<>();
    Datas datas = new Datas();
    Converte convert = new Converte();
    ArrayList<String[]> dados = new ArrayList<>();
    ArrayList<String[]> dados_mes = new ArrayList<>();
     ArrayList<String[]> dados_ano = new ArrayList<>();
    
    TabelaModel modelo = new TabelaModel();
    JTable jtable = new JTable();
    JScrollPane JSbalancoDia = new JScrollPane(jtable);

    TabelaModel modelo_mes = new TabelaModel();
    JTable jtable_mes = new JTable();
    JScrollPane JSbalancoMes = new JScrollPane(jtable_mes);
    
    TabelaModel modelo_ano = new TabelaModel();
    JTable jtable_ano = new JTable();
    JScrollPane JSbalancoAno = new JScrollPane(jtable_ano);

    Calculos_ExibeGastos Calculos_ExibeGastos = new Calculos_ExibeGastos();// classe que retorna os gasto de cada dia mes ou ano
    Calculos_ExibeVendas Calculos_ExibeVendas = new Calculos_ExibeVendas();// classe que retorna as vendas para determinada data mes ou ano 
    Calculos_ExibeCompras Calculos_ExibeCompras = new Calculos_ExibeCompras();
    Calculos_ExibePagamentosCompraNota Calculos_ExibePagamentosCompraNota = new Calculos_ExibePagamentosCompraNota();
    Calculos_Entradas Calculos_Entradas = new Calculos_Entradas();
    
    acao_botao acao_botao = new acao_botao();

    public PainelAlternavelVisualizacao() {
        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);

        inicialiacao();
        TabelaDia(ListaBalaco);
        //TabelaMes(ListaBalaco);
        //TabelaAno(ListaBalaco);
        capturaDados();
        FormataDadosParaTabela();

    }

    public void inicialiacao() {

        Botao_Atualizar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Atualizar.setSize(200, 20);
        Botao_Atualizar.setText("ATUALIZAR");
        Botao_Atualizar.addActionListener(acao_botao);
        Botao_Atualizar.setLocation(780, 30);
        //add(Botao_Atualizar);

    }

    public ArrayList<String[]> capturaDados() {

        ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
        ListaProdutosComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();
        ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();

        for (int i = 0; i < ListaGastos.size(); i++) {
            gastos = (BOX) ListaGastos.get(i);

        }

        return null;
    }

    public void FormataDadosParaTabela() {

        //Calculos_Entradas.SalvaArquivo();
        dados.clear();
        dados_mes.clear();
        dados_ano.clear();
        for (int i = datas.RetornaDias_Mes().size() - 1; i >= 0; i--) {

            Calculos_ExibeGastos.RetornaValoresParaExiberGastos(datas.RetornaDias_Mes().get(i));
            Calculos_ExibeVendas.RetornaValoresParaExibirVendas(datas.RetornaDias_Mes().get(i));
            Calculos_ExibeCompras.RetornaValoresParaExibirCompras(datas.RetornaDias_Mes().get(i));
            Calculos_Entradas.RetornaTodasEntradas(datas.RetornaDias_Mes().get(i), 1);

            String vendasDia = Calculos_ExibeVendas.RetornaTotalVendas();
            String entradaDia = Calculos_Entradas.RetornaEntradas();
            String saidaDia = Calculos_Entradas.RetornaSaida();
            String compraDia = Calculos_ExibeCompras.RetornaTotalCompras();
            String compraNotaDia = Calculos_ExibePagamentosCompraNota.RetornaValoresParaExibirPagamentosCompraNota(datas.RetornaDias_Mes().get(i));
            String gastosCarlos = Calculos_ExibeGastos.RetornaGastosCarlos();
            String gastosEliane = Calculos_ExibeGastos.RetornaGastosEliane();
            String gastosBox = Calculos_ExibeGastos.RetornaGastosBox();

            String SaidaSistemaDia = convert.FloatEmString((convert.StringEmFloat(entradaDia) + convert.StringEmFloat(vendasDia)) - (convert.StringEmFloat(compraDia) + convert.StringEmFloat(compraNotaDia) + convert.StringEmFloat(gastosBox) + convert.StringEmFloat(gastosCarlos) + convert.StringEmFloat(gastosEliane)));

            String[] balanco = new String[]{datas.RetornaDias_Mes().get(i), entradaDia, vendasDia, compraDia, compraNotaDia, gastosCarlos, gastosEliane, gastosBox, SaidaSistemaDia, saidaDia};

            dados.add(balanco);
        }
        for (int i = datas.RetornaMes_Ano().size() - 1; i >= 0; i--) {

            Calculos_ExibeGastos.RetornaValoresParaExiberGastos(datas.RetornaMes_Ano().get(i));
            Calculos_ExibeVendas.RetornaValoresParaExibirVendas(datas.RetornaMes_Ano().get(i));
            Calculos_ExibeCompras.RetornaValoresParaExibirCompras(datas.RetornaMes_Ano().get(i));
          

            String vendasMes = Calculos_ExibeVendas.RetornaTotalVendas();
            String compraMes = Calculos_ExibeCompras.RetornaTotalCompras();
            
            
            String gastosCarlos = Calculos_ExibeGastos.RetornaGastosCarlos();
            String gastosEliane = Calculos_ExibeGastos.RetornaGastosEliane();
            String gastosBox = Calculos_ExibeGastos.RetornaGastosBox();

            String[] balanco = new String[]{datas.RetornaMes_Ano().get(i), vendasMes, compraMes,  gastosCarlos, gastosEliane, gastosBox};

            dados_mes.add(balanco);
        }
        for (int i = datas.RetornaAnos().size() - 1; i >= 0; i--) {

            Calculos_ExibeGastos.RetornaValoresParaExiberGastos(datas.RetornaAnos().get(i));
            Calculos_ExibeVendas.RetornaValoresParaExibirVendas(datas.RetornaAnos().get(i));
            Calculos_ExibeCompras.RetornaValoresParaExibirCompras(datas.RetornaAnos().get(i));
           

            String vendasAno = Calculos_ExibeVendas.RetornaTotalVendas();
            String compraAno = Calculos_ExibeCompras.RetornaTotalCompras();
           
            String gastosCarlos = Calculos_ExibeGastos.RetornaGastosCarlos();
            String gastosEliane = Calculos_ExibeGastos.RetornaGastosEliane();
            String gastosBox = Calculos_ExibeGastos.RetornaGastosBox();

            String[] balanco = new String[]{datas.RetornaMes_Ano().get(i), vendasAno, compraAno,  gastosCarlos, gastosEliane, gastosBox};

            dados_ano.add(balanco);
        }
       
        TabelaDia(dados);
        TabelaMes(dados_mes);
        TabelaAno(dados_ano);
    }

    public void TabelaDia(ArrayList<String[]> dados) {
       
        String[] colunas = new String[]{"DATA", "ENTRADA", "VENDAS", "COMPRAS", "COMPRAS NOTA", "G CARLOS ", "G ELIANE", "G BOX", "SAIDA S", "SAIDA C"};

        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(8).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(9).setPreferredWidth(20);
        
       
        
        JSbalancoDia.setLocation(1, 60);
        JSbalancoDia.setSize(880, 150);
        add(JSbalancoDia);
    }

    public void TabelaMes(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"MES", "VENDAS", "COMPRAS", "G CARLOS ", "G ELIANE", "G BOX"};
        modelo_mes.setLinhas(dados);
        modelo_mes.setColunas(colunas);
        jtable_mes.setModel(modelo_mes);
        jtable_mes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable_mes.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable_mes.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable_mes.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable_mes.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable_mes.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable_mes.getColumnModel().getColumn(5).setPreferredWidth(20);
        

       

       
        JSbalancoMes.setLocation(1, 250);
        JSbalancoMes.setSize(880, 100);
        add(JSbalancoMes);
    }

    public void TabelaAno(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"ANO", "VENDAS", "COMPRAS",  "G CARLOS ", "G ELIANE", "G BOX"};
        modelo_ano.setLinhas(dados);
        modelo_ano.setColunas(colunas);
        jtable_ano.setModel(modelo_ano);
        jtable_ano.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable_ano.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable_ano.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable_ano.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable_ano.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable_ano.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable_ano.getColumnModel().getColumn(5).setPreferredWidth(20);
        

        JSbalancoAno.setLocation(1, 380);
        JSbalancoAno.setSize(880,80 );
        add(JSbalancoAno);
    }
    
    public class acao_botao implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
           if (e.getSource() == Botao_Atualizar) {

               FormataDadosParaTabela();  
            }
        }
    }
}
