/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_ESTOQUE;
import Cadastro.Datas;
import Compras.Visualizar.TabelaModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public final class AlternavelEst extends JPanel {

    private ARQUIVO_ESTOQUE ArquivoEstoque = new ARQUIVO_ESTOQUE();
    private ArrayList<PRODUTO_ESTOQUE> ListaProdutosEstoque = new ArrayList<>();
    PRODUTO_ESTOQUE produtoCompra = new PRODUTO_ESTOQUE();
    private Datas Data = new Datas();
    private JButton Botao_Pequisar, Botao_Editar, Botao_Remover;
    String data;
    String dataVenda;
    ArrayList<String[]> dados = new ArrayList<>();
    String quantidade, codigo, nome, precoVenda, precoCompra, precoCompraGeral, LucroUnidade, LucroGeral;
    AcaoBotao acao_botao = new AcaoBotao();
    Ordena OrdenaEstoque = new Ordena();
    PainelRemoveEstoque PainelRemoveEstoque;
    public Teclado teclado = new Teclado();
    JComboBox comboBox;
    JTextField tf;
    AlternavelPesquisa alternavelPesquisa = new AlternavelPesquisa();
    TecladoExclusivo tecladoExclusivo = new TecladoExclusivo();
    Converte convert = new Converte();

    TabelaModel modelo = new TabelaModel();
    JTable jtable = new JTable();
    JScrollPane JSprodutosEstoque;
    Boolean soUmavez = false;

    public AlternavelEst() {
        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
        jtable.requestFocus();
        inicializarBotoes();
        JSprodutosEstoque = new JScrollPane(jtable);
    }

    public void remove() {
        dados.clear();
        Tabela(dados);
    }

    public void limpa() {
        dados.clear();
        Tabela(dados);

    }

    public ArrayList<String[]> criaTabela() {

        ListaProdutosEstoque = ARQUIVO_ESTOQUE.retorna();

        Collections.sort(ListaProdutosEstoque);

        if (ListaProdutosEstoque.size() > 0) {
            data = Data.retornaData();

            for (int i = 0; i < ListaProdutosEstoque.size(); i++) {
                produtoCompra = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(i);
                quantidade = String.valueOf(produtoCompra.getQuantidadeProduto());
                codigo = produtoCompra.getCodigoProduto();
                nome = produtoCompra.getNomeProduto();
                precoVenda = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoVenda_unidade());
                precoCompra = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoCompra_unidade());
                precoCompraGeral = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoCompra_Geral());
                LucroUnidade = AlternavelCadastroProduto.converterFloatString(produtoCompra.getLucroUnidade());
                LucroGeral = AlternavelCadastroProduto.converterFloatString(produtoCompra.getLucroGeral());

                String[] produtos = new String[]{quantidade, codigo, nome, precoCompra, precoVenda, precoCompraGeral, LucroUnidade, LucroGeral};
                dados.add(produtos);
            }

            Tabela(dados);
        }
        return dados;
    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Quantidade", "Codigo", "Descrição", "Preço Compra", "Preço venda", "Preço geral", "Lucro Uni", "Lucro Geral"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);

        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);

        jtable.addMouseListener(new MouseAdapter() {
            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {

                if ((e.getClickCount() == 2) && (!soUmavez)) {

                    linha = jtable.getSelectedRow();
                    
                    String quantidadeProduto = String.valueOf(jtable.getValueAt(linha, 0));
                    String codigoProduto = String.valueOf(jtable.getValueAt(linha, 1));
                    String nomeProduto = String.valueOf(jtable.getValueAt(linha, 2));
                    String precoCompraUnidade = String.valueOf(jtable.getValueAt(linha, 3));
                    String precoVenda = String.valueOf(jtable.getValueAt(linha, 4));
                    
                    ListaProdutosEstoque = ArquivoEstoque.retorna();

                    for (int i = 0; i < ListaProdutosEstoque.size(); i++) {
                        produtoCompra = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(i);
                        if (nomeProduto.equalsIgnoreCase(produtoCompra.getNomeProduto())) {
                            PainelRemoveEstoque painel_remove_estoque = new PainelRemoveEstoque(quantidadeProduto, codigoProduto, nomeProduto, precoVenda,precoCompraUnidade, i);
                        }
                    }
                    //soUmavez = true;
                }

            }
        });

        jtable.addKeyListener(teclado);
        JSprodutosEstoque.setSize(900, 400);
        JSprodutosEstoque.addKeyListener(teclado);
        add(JSprodutosEstoque);

    }

    public int PegaPosicaoProdutoTabela(String nomeProduto) {

        ListaProdutosEstoque = ARQUIVO_ESTOQUE.retorna();
        PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();

        if (ListaProdutosEstoque.size() > 0) {
            data = Data.retornaData();

            for (int i = 0; i < ListaProdutosEstoque.size(); i++) {
                produtoEstoque = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(i);
                if (produtoEstoque.getNomeProduto().equalsIgnoreCase(nomeProduto)) {
                    return convert.StringEmInteiro(produtoEstoque.getCodigoProduto());
                }

            }
        }
        return 0;
    }

    public void atualiza() {
        /*remove();
         criaTabela();
         dados.clear();
         Tabela(dados);*/
    }

    public void inicializarBotoes() {

        Botao_Pequisar = new JButton();
        Botao_Pequisar.setText("PESQUISAR");
        Botao_Pequisar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Pequisar.setSize(100, 25);
        //Botao_Pequisar.addKeyListener(enterPrecionadoBtSalvar);
        Botao_Pequisar.setLocation(1200, 10);
        add(Botao_Pequisar);

        Botao_Editar = new JButton();
        Botao_Editar.setText("EDITAR");
        Botao_Editar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Editar.setSize(100, 25);
        Botao_Editar.setLocation(1200, 50);
        add(Botao_Editar);

        Botao_Remover = new JButton();
        Botao_Remover.setText("REMOVER");
        Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Remover.setSize(100, 25);
        Botao_Remover.addActionListener(acao_botao);
        Botao_Remover.setLocation(1200, 90);
        add(Botao_Remover);
    }

    public class AcaoBotao implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_Remover) {

            }
        }
    }

    public class PainelRemoveEstoque extends JFrame {

        private int largura;
        private int altura;
        private Container container;
        P_remove p_remove;

        public PainelRemoveEstoque(String quantidadeProduto, String codigoProduto, String nomeProduto, String precoVenda,String precoCompraUnidade, int Localizacao) {
        
            p_remove = new P_remove(quantidadeProduto, codigoProduto, nomeProduto, precoVenda,precoCompraUnidade, Localizacao);

            container = getContentPane();

            this.container.add(p_remove);
            this.setLayout(null);
            this.setSize(500, 450); // alt, larg
            this.largura = -125;
            this.altura = 300;
            this.setBackground(Color.BLUE);
            this.setVisible(true);
            this.addKeyListener(teclado);

            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(20, 230);
            soUmavez = true;

        }

        public class P_remove extends JPanel {

            private JButton Botao_Salvar, Botao_Cancelar;
            private String str1 = null;
            private JLabel JLcodigoProduto = new JLabel();
            private JLabel JLnome = new JLabel();
            private JTextField JTnome = new JTextField();
            private JTextField JTcodigoProduto = new JTextField();
            private JLabel JLprecoVenda = new JLabel();
            private JLabel JLprecoCompra = new JLabel();
            private JNumberFormatField JTprecoCompra = new JNumberFormatField(new DecimalFormat("0.00"));
            private JLabel JLquantidade = new JLabel();
            private JTextField JTquantidade = new JTextField();
            private JNumberFormatField JTprecoVenda = new JNumberFormatField(new DecimalFormat("0.00"));
            private JScrollPane JSprodutosEstoque = new JScrollPane();
            private ARQUIVO_ESTOQUE arquivo_estoque = new ARQUIVO_ESTOQUE();
            private ArrayList<String> dados = new ArrayList<>();
            private PainelAltenavelCaixa painel_alternavel_caixa = new PainelAltenavelCaixa();
            private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<PRODUTO_ESTOQUE>();//UMA LISTA DE PRODUTOS DO MEU ESTOQUE
            private PRODUTO_ESTOQUE produto_estoque = new PRODUTO_ESTOQUE();
            AcaoBotaoSalvar acaoBotaoSalvar = new AcaoBotaoSalvar();
            AcaoBotaoRemover acao_botao_remover = new AcaoBotaoRemover();

            public P_remove(String quantidadeProduto, String codigoProduto, String nomeProduto, String precoVenda,String precoCompraUnidade, int Localizacao) {
                this.setLayout(null);
                this.setSize(500, 400); // larg, alt
                this.setLocation(0, 50);
                this.setBackground(Color.white);
               
                ListaProduto_Estoque = arquivo_estoque.retorna();

                JLcodigoProduto.setBounds(new Rectangle(168, 135, 400, 50));
                JLcodigoProduto.setText("Digite o novo código do produto");
                JLcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLcodigoProduto.setLocation(10, 0);
                add(JLcodigoProduto);

                JTcodigoProduto.setBounds(new Rectangle(200, 135, 120, 17));
                JTcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTcodigoProduto.setSize(160, 25);
                JTcodigoProduto.setLocation(10, 35);//
                JTcodigoProduto.setText(codigoProduto);
                // JTcodigoProduto.addKeyListener(acao);
                add(JTcodigoProduto);

                JLnome.setBounds(new Rectangle(168, 135, 400, 50));
                JLnome.setText("Digite o novo nome do produto");
                JLnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLnome.setLocation(10, 60);
                add(JLnome);

                JTnome.setBounds(new Rectangle(200, 135, 120, 17));
                JTnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTnome.setSize(160, 25);
                JTnome.setLocation(10, 95);//
                //JTnome.addKeyListener(acao);
                JTnome.setText(nomeProduto);
                add(JTnome);

                JLquantidade.setBounds(new Rectangle(168, 135, 400, 50));
                JLquantidade.setText("Digite a nova quantidade do produto");
                JLquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLquantidade.setLocation(10, 120);
                add(JLquantidade);

                JTquantidade.setBounds(new Rectangle(200, 135, 120, 17));
                JTquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTquantidade.setSize(160, 25);
                JTquantidade.setLocation(10, 155);//
                //JTquantidade.addKeyListener(acao);
                JTquantidade.setText(quantidadeProduto);
                add(JTquantidade);

                JLprecoVenda.setBounds(new Rectangle(168, 135, 400, 50));
                JLprecoVenda.setText("Digite o novo Preço de venda do produto");
                JLprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLprecoVenda.setLocation(10, 180);
                add(JLprecoVenda);

                JTprecoVenda.setBounds(new Rectangle(200, 135, 120, 17));
                JTprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTprecoVenda.setSize(160, 25);
                JTprecoVenda.setLocation(10, 215);//
                JTprecoVenda.setText(precoVenda);
                //JTprecoVenda.addKeyListener(acao);
                add(JTprecoVenda);
                
                JLprecoCompra.setBounds(new Rectangle(168, 135, 400, 50));
                JLprecoCompra.setText("Digite o novo Preço de Compra do produto");
                JLprecoCompra.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLprecoCompra.setLocation(10, 250);
                add(JLprecoCompra);
                
                JTprecoCompra.setBounds(new Rectangle(200, 135, 120, 17));
                JTprecoCompra.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTprecoCompra.setSize(160, 25);
                JTprecoCompra.setLocation(10, 285);//
                JTprecoCompra.setText(precoCompraUnidade);
                add(JTprecoCompra);

                Botao_Salvar = new JButton();
                Botao_Salvar.setText("SALVAR");
                Botao_Salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Salvar.setSize(100, 25);
                Botao_Salvar.addActionListener(acaoBotaoSalvar);
                Botao_Salvar.setLocation(50, 320);
                add(Botao_Salvar);

                Botao_Cancelar = new JButton();
                Botao_Cancelar.setText("CANCELAR");
                Botao_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Cancelar.setSize(100, 25);
                Botao_Cancelar.setLocation(200, 320);
                add(Botao_Cancelar);

                Botao_Remover = new JButton();
                Botao_Remover.setText("REMOVER");
                Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Remover.setSize(100, 25);
                Botao_Remover.setLocation(350, 320);
                Botao_Remover.addActionListener(acao_botao_remover);
                add(Botao_Remover);

                acaoBotaoSalvar.pegarLocalizacao(Localizacao);

            }

            public class AcaoBotaoSalvar implements ActionListener {

                int Loc;

                public int pegarLocalizacao(int localizacao) {
                    Loc = localizacao;

                    return Loc;

                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    ListaProduto_Estoque.get(Loc).setNomeProduto(JTnome.getText());
                    ListaProduto_Estoque.get(Loc).setCodigoProduto(JTcodigoProduto.getText());
                    ListaProduto_Estoque.get(Loc).setQuantidadeProduto(Integer.parseInt(JTquantidade.getText()));
                    ListaProduto_Estoque.get(Loc).setPrecoVenda_unidade(Float.parseFloat(JTprecoVenda.getText().replace(",", ".")));
                    ListaProduto_Estoque.get(Loc).setPrecoCompra_unidade(convert.StringEmFloat(JTprecoCompra.getText()));
                    ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(convert.StringEmInteiro(JTquantidade.getText()) * convert.StringEmFloat(JTprecoCompra.getText()));
                    float lucroUnidade = (convert.StringEmFloat(JTprecoVenda.getText()) - convert.StringEmFloat(JTprecoCompra.getText()));
                    ListaProduto_Estoque.get(Loc).setLucroUnidade(lucroUnidade);
                    ListaProduto_Estoque.get(Loc).setLucroGeral(lucroUnidade * Integer.parseInt(JTquantidade.getText()));

                    if (Integer.parseInt(JTquantidade.getText()) == 0) {
                        float FLAG = (float) 0.00;
                        ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(FLAG);
                        ListaProduto_Estoque.get(Loc).setPrecoCompra_unidade(FLAG);
                        ListaProduto_Estoque.get(Loc).setLucroGeral(FLAG);
                        ListaProduto_Estoque.get(Loc).setLucroUnidade(FLAG);
                    }

                    try {
                        arquivo_estoque.Armazena(ListaProduto_Estoque);
                    } catch (IOException ex) {
                        Logger.getLogger(EditaEstoque.P_remove.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //atualiza();
                    soUmavez = false;
                    PainelRemoveEstoque.this.dispose();
                    limpa();
                    criaTabela();

                }
            }

            public void limpaTxt() {
                JTnome.setText(null);
                JTcodigoProduto.setText(null);
                JTprecoVenda.setText(null);
                JTquantidade.setText(null);

            }

            public class AcaoBotaoRemover implements ActionListener {

                @Override
                public void actionPerformed(ActionEvent e) {

                    try {
                        arquivo_estoque.Armazena(ListaProduto_Estoque);
                    } catch (IOException ex) {
                        Logger.getLogger(EditaEstoque.P_remove.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    atualiza();
                    criaTabela();
                    PainelRemoveEstoque.this.dispose();
                }
            }
        }
    }

    public class Teclado implements KeyListener, MouseListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {
                alternavelPesquisa.createAndShowGUI_2();
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
          
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

    public class AlternavelPesquisa extends JComboBox implements JComboBox.KeySelectionManager {

        private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
        private ArrayList<String> listaNomesProdutos = new ArrayList<>();
        private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
        private String searchFor;
        private long lap;
        int contador = 0;
        JFrame f = new JFrame("AutoCompleteComboBox");

        private AlternavelPesquisa() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AlternavelPesquisa(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);
            tf = null;
            if (getEditor() != null) {
                tf = (JTextField) getEditor().getEditorComponent();

                if (tf != null) {
                    //para fazer apenas uma verificação de enter precionado
                    tf.setDocument(new AlternavelPesquisa.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            tf = (JTextField) getEditor().getEditorComponent();
                            tf.setSize(230, 25);
                            String text = tf.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    tf.setText(current);
                                    tf.setSelectionStart(text.length());
                                    tf.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }
            tf.addKeyListener(tecladoExclusivo);
            tf.setText(null);
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {

                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();

                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }

        public String[] pegaTodosNomesProdutos() {
            int i, j = 0;
            String test;
            String[] names = {""};
            ArrayList<String> Sentinela = new ArrayList<>();

            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
            names = new String[ListaProduto_Estoque.size()];
            Sentinela.add("");

            for (i = 0; i < ListaProduto_Estoque.size(); i++) {

                Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
                Sentinela.add(Produto_Estoque.getNomeProduto());
            }

            names = Sentinela.toArray(names);
            Arrays.sort(names, 0, ListaProduto_Estoque.size());

            return (names);
        }

        void createAndShowGUI_2() {

            // f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setSize(265, 200);
            f.setLocation(550, 200);
            Container cp = f.getContentPane();
            cp.setLayout(null);
            pegaTodosNomesProdutos();

            comboBox = new AlternavelPesquisa(pegaTodosNomesProdutos());
            comboBox.setEditable(true);
            comboBox.setBounds(50, 50, 100, 25);
            comboBox.setMaximumRowCount(20);
            comboBox.setSize(250, 25);
            comboBox.setLocation(0, 0);
            cp.add(comboBox);
            new JavaApplication4(comboBox);

            Locale[] locales = Locale.getAvailableLocales();//

            f.setVisible(true);

        }

        String RetornaNome() {

            return tf.getText();
        }

        public void FechaPainel() {
            f.dispose();

        }
    }

    public class TecladoExclusivo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {

                alternavelPesquisa.createAndShowGUI_2();
            }
            if (e.getKeyCode() == 10) {

                jtable.changeSelection(PegaPosicaoProdutoTabela(tf.getText()) - 1, PegaPosicaoProdutoTabela(tf.getText()) - 1, false, false);
                alternavelPesquisa.FechaPainel();
            }
        }
    }

}
