/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class Operacao {

    public void DiminueOUsomaProdutoDoEstoque(PRODUTO_VENDA produto_venda, int operacao) {

        ArrayList<PRODUTO_ESTOQUE> Local_ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
        PRODUTO_ESTOQUE Local_Produto_Estoque = new PRODUTO_ESTOQUE();
        int quantidadeGeralProdutoEstoque = 0, quantidadeGeralProdutoEstoque_venda = 0;
        float precoCompraGeral_venda = 0, precoCompraGeral = 0;// variavel para subitrair do arquivo o preco da compra geral 
        float lucroGeralProdutoEstoque = 0, lucroGeralProdutoEstoque_venda = 0;

        for (int i = 0; i < Local_ListaProduto_Estoque.size(); i++) {
            Local_Produto_Estoque = (PRODUTO_ESTOQUE) Local_ListaProduto_Estoque.get(i);

            if (Local_Produto_Estoque.getCodigoProduto().equalsIgnoreCase(produto_venda.getCodigoProduto())) {

                quantidadeGeralProdutoEstoque = Local_Produto_Estoque.getQuantidadeProduto();
                quantidadeGeralProdutoEstoque_venda = produto_venda.getQuantidadeProduto();
                precoCompraGeral_venda = (produto_venda.getQuantidadeProduto() * Local_Produto_Estoque.getPrecoCompra_unidade());
                precoCompraGeral = Local_Produto_Estoque.getPrecoCompra_Geral();
                lucroGeralProdutoEstoque_venda = (produto_venda.getQuantidadeProduto() * Local_Produto_Estoque.getLucroUnidade());
                lucroGeralProdutoEstoque = Local_Produto_Estoque.getLucroGeral();

                if (operacao == 0) {

                    Local_Produto_Estoque.setQuantidadeProduto(quantidadeGeralProdutoEstoque - quantidadeGeralProdutoEstoque_venda);
                    Local_Produto_Estoque.setPrecoCompra_Geral(precoCompraGeral - precoCompraGeral_venda);
                    Local_Produto_Estoque.setLucroGeral(lucroGeralProdutoEstoque - lucroGeralProdutoEstoque_venda);
                }
                if (operacao == 1) {

                    Local_Produto_Estoque.setQuantidadeProduto(quantidadeGeralProdutoEstoque + quantidadeGeralProdutoEstoque_venda);
                    Local_Produto_Estoque.setPrecoCompra_Geral(precoCompraGeral + precoCompraGeral_venda);
                    Local_Produto_Estoque.setLucroGeral(lucroGeralProdutoEstoque + lucroGeralProdutoEstoque_venda);
                }

            }
        }
          SalvaNoarquivoEstoque(Local_ListaProduto_Estoque);
    }

    public void SalvaNoarquivoEstoque(ArrayList<PRODUTO_ESTOQUE> Local_ListaProduto_Estoque) {

        try {
            ARQUIVO_ESTOQUE.Armazena(Local_ListaProduto_Estoque);
        } catch (IOException ex) {
            Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
