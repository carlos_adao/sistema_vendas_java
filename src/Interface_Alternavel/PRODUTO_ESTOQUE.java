/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

/**
 *
 * @author carlos
 */
public class PRODUTO_ESTOQUE  implements Comparable<Object>{
   public  String nomeProduto;
   public String codigoProduto;
   public int quantidadeProduto;
   public float precoCompra_unidade,precoVenda_unidade,precoCompra_Geral,LucroUnidade,LucroGeral;

    public PRODUTO_ESTOQUE(String nomeProduto, String codigoProduto, int quantidadeProduto, float precoCompra_unidade, float precoVenda_unidade, float precoCompra_Geral, float LucroUnidade, float LucroGeral) {
        this.nomeProduto = nomeProduto;
        this.codigoProduto = codigoProduto;
        this.quantidadeProduto = quantidadeProduto;
        this.precoCompra_unidade = precoCompra_unidade;
        this.precoVenda_unidade = precoVenda_unidade;
        this.precoCompra_Geral = precoCompra_Geral;
        this.LucroUnidade = LucroUnidade;
        this.LucroGeral = LucroGeral;
    }

    public PRODUTO_ESTOQUE() {
      
    }

    public float getLucroGeral() {
        return LucroGeral;
    }

    public void setLucroGeral(float LucroGeral) {
        this.LucroGeral = LucroGeral;
    }

    public float getLucroUnidade() {
        return LucroUnidade;
    }

    public void setLucroUnidade(float LucroUnidade) {
        this.LucroUnidade = LucroUnidade;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public float getPrecoCompra_Geral() {
        return precoCompra_Geral;
    }

    public void setPrecoCompra_Geral(float precoCompra_Geral) {
        this.precoCompra_Geral = precoCompra_Geral;
    }

    public float getPrecoCompra_unidade() {
        return precoCompra_unidade;
    }

    public void setPrecoCompra_unidade(float precoCompra_unidade) {
        this.precoCompra_unidade = precoCompra_unidade;
    }

    public float getPrecoVenda_unidade() {
        return precoVenda_unidade;
    }

    public void setPrecoVenda_unidade(float precoVenda_unidade) {
        this.precoVenda_unidade = precoVenda_unidade;
    }

    public int getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(int quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    @Override
    public String toString() {
        return (nomeProduto + ";" + codigoProduto + ";" + quantidadeProduto + ";" + precoCompra_unidade + ";" + precoVenda_unidade + ";" 
                + precoCompra_Geral + ";" + LucroUnidade + ";" + LucroGeral +";"+"\n");
    }

    @Override
    public int compareTo(Object o) {
        PRODUTO_ESTOQUE outro =(PRODUTO_ESTOQUE)o;
        
        return this.codigoProduto.compareToIgnoreCase(outro.codigoProduto);
    }

    
    
}
