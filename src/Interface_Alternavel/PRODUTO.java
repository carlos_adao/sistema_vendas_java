/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

/**
 *
 * @author carlos
 */
public class PRODUTO {
    int quantidade;
    String nomeProduto;
    float precoUnidade;
    float subTotal;
    float desconto = 0;
    float lucro = 0;

    PRODUTO(int quantidade, String nomeProduto, float precoUnidade, float subTotal) {
        this.quantidade = quantidade;
        this.nomeProduto = nomeProduto;
        this.precoUnidade = precoUnidade;
        this.subTotal = subTotal;
 }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }

    public float getLucro() {
        return lucro;
    }

    public void setLucro(float lucro) {
        this.lucro = lucro;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public float getPrecoUnidade() {
        return precoUnidade;
    }

    public void setPrecoUnidade(float precoUnidade) {
        this.precoUnidade = precoUnidade;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return (quantidade + ";"+ nomeProduto +";" + precoUnidade +";"+ subTotal+";"+ desconto +";"+ lucro+";");
    }

}
