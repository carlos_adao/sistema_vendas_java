/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_ENTRADAS;
import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import BancoDados.ArquivoProdutos;
import Cadastro.CadastroProduto;
import Cadastro.Datas;
import Compras.Visualizar.TabelaModel;
import Entradas.ENTRADAS;
import Gerando_pdf.testando_pdf;
import InterfacePrincipal.PainelResultado;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author carlos
 */
public class PainelAltenavelCaixa extends JPanel {

    boolean controladorClicks = false;
    public PainelDinheiroEntrada Entrada;
    PainelAltenavelCaixa.AlternavelPesquisa alternavelPesquisa = new PainelAltenavelCaixa.AlternavelPesquisa();
    JTextField tf;
    float total = 0;
    int vrifcaComp_prod = 0;
    int verificao = 0;
    int btFecharPress = 0;
    String nome, codigoArquivo, codigoText, precoVendaUnidadeS;
    float precoVendaUnidadeF;
    float Total = 0;
    private boolean checaQuantida = false;
    ArrayList<CadastroProduto> ListaProdutoCadastrados = new ArrayList<>();
    private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>(); //UMA LISTA DE PRODUTOS COMPRADOS E COLOCADOS NO ESTOQUE 
    private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();   //UM PRODUTO DA CLASSE COMPRA 
    private ARQUIVO_ESTOQUE ArquivoProduto_Estoque; //ARQUIVO PARA VERIFICAR OS PRODUTOS QUE ESTÃO NO ESTOQUE 
    private ARQUIVO_ESTOQUE arquivo;//ARQUIVO PARA ATUALIZAR PRODUTOS NO ESTOQUE;
    private PRODUTO_VENDA Produto_Venda = new PRODUTO_VENDA();
    private ArrayList<PRODUTO_VENDA> Lista_ProdutoVenda = new ArrayList<>();

    ARQUIVO_ESTOQUE_VENDA Arquivo_Estoque_venda = new ARQUIVO_ESTOQUE_VENDA();
    private CadastroProduto produto = new CadastroProduto();
    ArquivoProdutos BancoDadosProduto;
    private ArrayList<String> linha_pdf;
    JLabel JLnomeMercadoria = new JLabel();
    JLabel JLcodigoMercadoria = new JLabel();
    JLabel JLprecoVendaUnidade = new JLabel();
    JLabel JLquantidaMercadoria = new JLabel();
    JLabel JLdescontoVendaUnidade = new JLabel();
    JLabel JLdataDoSistema = new JLabel();
    JLabel JLdataManual = new JLabel();

    //-------------------------------------------------------------------------------------
    //componentes do radio para selecionar o tipo de deconto
    JRadioButton JrDescontoTot, JrDescontoUni;
    String DescUnidade = "Uni";
    String DescTotal = "Tot";
    int controla_desconto = 0;// variavel ultilizada para fazer o contro do desco por unidade ou total

    //---------------------------------------------------------------------------------------
    JFormattedTextField JTdataManual = new JFormattedTextField(Mascara("##/##/####"));
    JFormattedTextField JTmesManual = new JFormattedTextField(Mascara("##"));
    JFormattedTextField JTanoManual = new JFormattedTextField(Mascara("####"));
    JComboBox comboBox;
    JLabel JLhoraSistema = new JLabel();
    JTextField JTcodigo_mercadoria = new JtextFieldSomenteNumeros();
    JTextField JTnome_mercadoria;
    JTextField JtQuantidade_mercadoria;
    private DateFormat df = new SimpleDateFormat("hh:mm:ss");
    private JNumberFormatField JtPrecoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JTtotal = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtDescontoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));//JTXT DESCONTO DO PRODUTO
    JLabel JLsubTotal = new JLabel();
    JLabel JLtotal = new JLabel();
    private JNumberFormatField JTsubTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    ArrayList<PRODUTO_VENDA> L_ProdutoVenda_aguardandoPagamento = new ArrayList<>();
    
    ArrayList<String> listaNomesProdutos = new ArrayList<>();
    public ArrayList<String[]> dados = new ArrayList<>();
    private Datas data = new Datas();//ISTANCIA A CLASSE DATA 
    String dia = data.retornaData();
    String mes = data.retornaMes();
    String ano = data.retornaAno();
    JLabel lblHora;
    PainelResultado P_resultado = new PainelResultado();
    PainelAltenavelCaixa.HandlerText handlerText = new PainelAltenavelCaixa.HandlerText();
    PainelAltenavelCaixa.FocoNome focoNome = new PainelAltenavelCaixa.FocoNome();
    PainelAltenavelCaixa.EnterPrecionadoJTxtCodigo EnterPrecionado = new PainelAltenavelCaixa.EnterPrecionadoJTxtCodigo();
    int verificacao;
    String str1 = null;
    AlternavelEst alternavelEst = new AlternavelEst();
    PainelAltenavelCaixa.Teclado teclado = new PainelAltenavelCaixa.Teclado();
    PainelAltenavelCaixa.TecladoExclusivo tecladoExclusivo = new PainelAltenavelCaixa.TecladoExclusivo();
    AlternavelPesquisa AlternavelPesquisa = new AlternavelPesquisa();

    /*VARIAVEIS E INTANCIAS DE CLASSE PARA MANIPULAÇÃO DO DINHEIRO DE ENTRADA*/
    ENTRADAS ENTRADAS = new ENTRADAS();

    /*VARIAVEIS DO PRODUTO ESTOQUE*/
    public String nomeProduto;
    public String codigoProduto;
    public int quantidadeProduto;
    public float precoCompra_unidade, precoVenda_unidade, precoCompra_Geral, LucroUnidade, LucroGeral;
    /*----------------------------------------------------------------------------------------------*/

    PRODUTO_ESTOQUE PRODUTO_ESTOQUE = new PRODUTO_ESTOQUE();
    PRODUTO_VENDA PRODUTO_VENDA = new PRODUTO_VENDA();

    TabelaModel modelo = new TabelaModel();
    JTable jtable = new JTable();
    JScrollPane JSprodutosVendidosAguardandoPagamento = new JScrollPane(jtable);

    Converte convert = new Converte();
    
    int GuardaPrimarykey = 0;

    public PainelAltenavelCaixa() {

        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

        new Thread(new PainelAltenavelCaixa.AtualizadorDeHora()).start();
        String a = "a";

        //JOptionPane.showMessageDialog(null, BuscaProduto(a));
        add(P_resultado);

        JLcodigoMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLcodigoMercadoria.setText("Cód da Merc");
        JLcodigoMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLcodigoMercadoria.setLocation(10, 40);//
        add(JLcodigoMercadoria);

        JTcodigo_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JTcodigo_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTcodigo_mercadoria.setSize(110, 25);
        JTcodigo_mercadoria.setLocation(10, 75);
        JTcodigo_mercadoria.addFocusListener(focoNome);
        JTcodigo_mercadoria.addKeyListener(EnterPrecionado);
        JTcodigo_mercadoria.addKeyListener(teclado);
        add(JTcodigo_mercadoria);//
        /*
         * JLabel nome da Mercadoria
         */
        JLnomeMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLnomeMercadoria.setText("Nome da mercadoria");
        JLnomeMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLnomeMercadoria.setLocation(10, 110);//

        add(JLnomeMercadoria);

        JTnome_mercadoria = new JTextField();
        JTnome_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JTnome_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTnome_mercadoria.setSize(250, 25);
        JTnome_mercadoria.setLocation(10, 140);//
        JTnome_mercadoria.addActionListener(handlerText);
        JTnome_mercadoria.addFocusListener(focoNome);
        JTnome_mercadoria.addKeyListener(teclado);
        JTnome_mercadoria.setEditable(false);
        add(JTnome_mercadoria);

        JLquantidaMercadoria = new JLabel();
        JLquantidaMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLquantidaMercadoria.setText("Quantidade");
        JLquantidaMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLquantidaMercadoria.setLocation(152, 40);
        add(JLquantidaMercadoria);

        //** JTextField quantidade**//
        JtQuantidade_mercadoria = new JtextFieldSomenteNumeros();
        JtQuantidade_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JtQuantidade_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtQuantidade_mercadoria.setSize(110, 25);
        JtQuantidade_mercadoria.setLocation(152, 75);
        JtQuantidade_mercadoria.addKeyListener(handlerText);
        JtQuantidade_mercadoria.addKeyListener(teclado);
        add(JtQuantidade_mercadoria);

        JLprecoVendaUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLprecoVendaUnidade.setText("preço de venda unid");
        JLprecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLprecoVendaUnidade.setLocation(279, 110);
        add(JLprecoVendaUnidade);

        JtPrecoVendaUnidade.setBounds(new Rectangle(200, 135, 120, 17));
        JtPrecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtPrecoVendaUnidade.setSize(178, 25);
        JtPrecoVendaUnidade.setLocation(279, 140);
        JtPrecoVendaUnidade.setEditable(false);
        add(JtPrecoVendaUnidade);

        //LABEL E TXT DO DESCONTO DO PRODUTO
        JLdescontoVendaUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLdescontoVendaUnidade.setText("desconto");
        JLdescontoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLdescontoVendaUnidade.setLocation(280, 40);
        add(JLdescontoVendaUnidade);

        JtDescontoVendaUnidade.setBounds(new Rectangle(100, 135, 120, 17));
        JtDescontoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtDescontoVendaUnidade.setSize(178, 25);
        JtDescontoVendaUnidade.setLocation(280, 75);//
        JtDescontoVendaUnidade.addKeyListener(handlerText);
        JtDescontoVendaUnidade.addKeyListener(teclado);
        //JtDescontoVendaUnidade.setForeground(Color.BLUE);
        add(JtDescontoVendaUnidade);

        //--------------------------------------------------------------
        //add(JLdescontoUni);
        RadioListener myListener = new RadioListener();
        JrDescontoUni = new JRadioButton(DescUnidade);
        JrDescontoUni.setSize(60, 25);
        JrDescontoUni.setLocation(380, 40);
        JrDescontoUni.setSelected(true);
        JrDescontoUni.setMnemonic('b');
        add(JrDescontoUni);

        JrDescontoTot = new JRadioButton(DescTotal);
        JrDescontoTot.setSize(60, 25);
        JrDescontoTot.setLocation(460, 40);
        JrDescontoTot.setMnemonic('c');
        add(JrDescontoTot);

        ButtonGroup grupoBotao = new ButtonGroup();
        grupoBotao.add(JrDescontoUni);
        grupoBotao.add(JrDescontoTot);
        JrDescontoUni.addActionListener(myListener);
        JrDescontoTot.addActionListener(myListener);

        //------------------------------------------------------------------
        JLdataManual.setBounds(new Rectangle(168, 135, 400, 30));
        JLdataManual.setText("Digite a data");
        JLdataManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 10));
        JLdataManual.setLocation(10, 400);
        //add(JLdataManual);

        JTdataManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTdataManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTdataManual.setSize(100, 25);
        JTdataManual.setLocation(0, 438);//
        JTdataManual.addKeyListener(teclado);
        JTdataManual.setText(dia);
        add(JTdataManual);

        JTmesManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTmesManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTmesManual.setSize(30, 25);
        JTmesManual.setText(mes);
        JTmesManual.setLocation(100, 438);//
        JTmesManual.addKeyListener(teclado);
        add(JTmesManual);

        JTanoManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTanoManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTanoManual.setSize(60, 25);
        JTanoManual.setText(ano);
        JTanoManual.setLocation(130, 438);//
        JTanoManual.addKeyListener(teclado);
        add(JTanoManual);

        JLdataDoSistema.setBounds(new Rectangle(168, 135, 400, 30));
        JLdataDoSistema.setText("Ilhéus, " + dia);
        JLdataDoSistema.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        JLdataDoSistema.setLocation(10, 438);
        //add(JLdataDoSistema);

        String hora = df.format(new Date());

        JLhoraSistema.setBounds(new Rectangle(168, 135, 400, 30));
        JLhoraSistema.setText(hora);
        JLhoraSistema.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        JLhoraSistema.setLocation(1250, 438);
        add(JLhoraSistema);

    }

    /**
     * Listens to the radio buttons.
     */
    class RadioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equalsIgnoreCase("Uni")) {
                controla_desconto = 0;
            } else {
                controla_desconto = 1;
            }
        }
    }
    public int pegaUtimaChave(){
      int primarykey;
      ArrayList<PRODUTO_VENDA> L_ProdutoVenda_auxiliar = ARQUIVO_ESTOQUE_VENDA.retorna();
      
      primarykey = 1 + L_ProdutoVenda_auxiliar.get(L_ProdutoVenda_auxiliar.size()-1).getPrimarykey();
      
      return primarykey;
    }
    public void ArmazenaDinheiroEntrada() {
        ArrayList<ENTRADAS> ListaEntradascadastradas = new ArrayList<>();
        ListaEntradascadastradas = ARQUIVO_ENTRADAS.retorna();

        if (!VerificaSeExixteEntradaArmazenada(ListaEntradascadastradas)) {
            try {
                float entrada = convert.StringEmFloat(JOptionPane.showInputDialog("Ola!\n Por favor digite o dinheiro de entrada."));

                ArmazenaEntrada(entrada, ListaEntradascadastradas);

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "entrada invalida!!!");
            }

        }
    }

    // metodo faz a verificação pra ver se já existe entrada cadastrada
    public boolean VerificaSeExixteEntradaArmazenada(ArrayList<ENTRADAS> ListaEntradascadastradas) {

        if (ListaEntradascadastradas.size() > 0) {
            for (int i = 0; i < ListaEntradascadastradas.size(); i++) {
                ENTRADAS = (ENTRADAS) ListaEntradascadastradas.get(i);

                if (ENTRADAS.getDia().equalsIgnoreCase(data.retornaData())) {

                    return true;
                }
            }
        }

        return false;
    }

    public void ArmazenaEntrada(float entrada, ArrayList<ENTRADAS> ListaEntradascadastradas) {
        ENTRADAS ENTRADA = new ENTRADAS();

        if (ListaEntradascadastradas.size() > 0) {

            ENTRADA.setPrimaryKey(ListaEntradascadastradas.size() + 1);
        } else {
            ENTRADA.setPrimaryKey(1);
        }
        ENTRADA.setValorEntrada(entrada);
        ENTRADA.setValorSaida((float) 0.0);
        ENTRADA.setVerf_valorEntrada(true);
        ENTRADA.setVerf_valorSaida(false);
        ENTRADA.setDia(dia);
        ENTRADA.setMes(mes);
        ENTRADA.setAno(ano);

        ListaEntradascadastradas.add(ENTRADA);
        try {
            ARQUIVO_ENTRADAS.Armazena(ListaEntradascadastradas);
        } catch (IOException ex) {
            Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AtualizaListaProdutosAutoComplete() {
        String[] nomes = AlternavelPesquisa.pegaTodosNomesProdutos();
        AlternavelPesquisa.setModel(new DefaultComboBoxModel<>(nomes));
        // JOptionPane.showMessageDialog(null, "inicializa");
    }

    public MaskFormatter Mascara(String Mascara) {

        MaskFormatter F_Mascara = new MaskFormatter();
        try {
            F_Mascara.setMask(Mascara); //Atribui a mascara  
            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
        } catch (Exception excecao) {
            excecao.printStackTrace();
        }
        return F_Mascara;
    }

    public class AtualizadorDeHora implements Runnable {

        private SimpleDateFormat sdf = null;

        public AtualizadorDeHora() {
            sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        }

        @Override
        public void run() {
            System.out.println(sdf.format(new Date()));
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }

    private class HandlerText implements ActionListener, KeyListener, FocusListener {

        ArrayList<PRODUTO> Produto = new ArrayList<>();
        ArrayList<String> RetornoTabela = new ArrayList<>();
        //VARIAVEIS DA NOVA CLASSE PRODUTO VENDA
        String codigo;
        String nomeProduto;
        int quantidade;
        float precoUni;
        float subTot;

        @Override
        public void actionPerformed(ActionEvent e) {
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyChar() == 8) { // Para apagar caractere 1 a 1
                if (str1.length() > 0) {
                    str1 = str1.substring(0, str1.length() - 1);
                }
            } else {
                str1 += e.getKeyChar();
            }

        }

        @Override
        public void keyPressed(KeyEvent e) {

            boolean verificaVenda = true;

            if (e.getKeyCode() == 10) {

                int operacao = 0;
                controladorClicks = false;
                if (!"".equals(JtQuantidade_mercadoria.getText())) {
                    PRODUTO_VENDA Produto_Venda = new PRODUTO_VENDA();
                    int quantidadeProduto = convert.StringEmInteiro(JtQuantidade_mercadoria.getText());
                    String nomeProdut = JTnome_mercadoria.getText();
                    String codigoProduto = JTcodigo_mercadoria.getText();
                    float descontoUnidVendaProduto;

                    if (controla_desconto == 0) {
                        descontoUnidVendaProduto = convert.StringEmFloat(JtDescontoVendaUnidade.getText());
                    } else {
                        descontoUnidVendaProduto = (convert.StringEmFloat(JtDescontoVendaUnidade.getText()) / convert.StringEmInteiro(JtQuantidade_mercadoria.getText()));
                    }

                    float lucroUniVendaProduto = (PRODUTO_ESTOQUE.getLucroUnidade() - descontoUnidVendaProduto);
                    float lucroVendaProduto = lucroUniVendaProduto * quantidadeProduto;
                    float precoUniVendaProduto = convert.StringEmFloat(JtPrecoVendaUnidade.getText());
                    float subTotal = (precoUniVendaProduto * quantidadeProduto) - (descontoUnidVendaProduto * quantidadeProduto);

                    if (PRODUTO_ESTOQUE.getQuantidadeProduto() >= quantidadeProduto) {
                        
                        if(GuardaPrimarykey == 0){
                          Produto_Venda.setPrimarykey(pegaUtimaChave());
                          GuardaPrimarykey = pegaUtimaChave();
                         
                        }else{
                        
                          Produto_Venda.setPrimarykey(GuardaPrimarykey+1);
                          GuardaPrimarykey = GuardaPrimarykey + 1;
                         
                        }                      
                        Produto_Venda.setNomeProduto(nomeProdut);
                        Produto_Venda.setCodigoProduto(codigoProduto);
                        Produto_Venda.setQuantidadeProduto(quantidadeProduto);
                        Produto_Venda.setValor_Venda(precoUniVendaProduto - descontoUnidVendaProduto);
                        Produto_Venda.setLucroVenda(lucroVendaProduto);
                        Produto_Venda.setDescontoVenda(descontoUnidVendaProduto * quantidadeProduto);
                        Produto_Venda.setSubTotal(subTotal);
                        Produto_Venda.setData(JTdataManual.getText());
                        Produto_Venda.setMes(JTmesManual.getText());
                        Produto_Venda.setAno(JTanoManual.getText());

                        /*CONFIGURA OS DADOS DO PRODUTO EM FORMA DE STRING PARA ENVIAR PRA TABELA*/
                        L_ProdutoVenda_aguardandoPagamento.add(Produto_Venda);
                        CriaTabela(TransformaListaEmString(L_ProdutoVenda_aguardandoPagamento));
                        DiminueOUsomaProdutoDoEstoque(Produto_Venda, operacao);
                        colocaValoresP_resultado(Produto_Venda, 1);
                        JTcodigo_mercadoria.requestFocus();
                        LimpaCampos();

                    }//FECHANDO O FOR QUE PERCORRE A LISTA DE PRODUTOS NO ESTOQUE
                    else if (PRODUTO_ESTOQUE.getQuantidadeProduto() > 0) {
                        JOptionPane.showMessageDialog(null, "A quantidade deve ser menor ou igual a: " + PRODUTO_ESTOQUE.getQuantidadeProduto());
                        JtQuantidade_mercadoria.setText(null);
                        JtQuantidade_mercadoria.requestFocus();
                    } else {
                        JOptionPane.showMessageDialog(null, "Produto em falta");
                        LimpaCampos();
                        JTcodigo_mercadoria.requestFocus();
                    }

                }//FECHANDO IF

            }//FECHA A OPCAO BOTÃO SALVAR 

        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

            for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
                Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);

                nome = Produto_Estoque.getNomeProduto();
                precoVendaUnidadeF = Produto_Estoque.getPrecoVenda_unidade();
                codigoText = JTcodigo_mercadoria.getText();
                codigoArquivo = Produto_Estoque.getCodigoProduto();
                precoVendaUnidadeS = AlternavelCadastroProduto.converterFloatString(precoVendaUnidadeF);

                if (codigoArquivo.equalsIgnoreCase(codigoText)) {//COMPARA O CODIGO DO PRODUTO DO ARQUIVO COM O CODIGO QUE TÁ NO TXT 

                    JTnome_mercadoria.setText(Produto_Estoque.getNomeProduto()); //SETA O NOME DO PRODUTO 
                    JtPrecoVendaUnidade.setText(AlternavelCadastroProduto.converterFloatString(Produto_Estoque.getPrecoVenda_unidade()));//SETA PRECO DE VENDA POR UNIDADE

                    //COLOCANDO VALORES NA CLASSE VENDA 
                    Produto_Venda.setNomeProduto(JTnome_mercadoria.getText());
                    Produto_Venda.setCodigoProduto(JTcodigo_mercadoria.getText());
                    Produto_Venda.setQuantidadeProduto(Integer.parseInt(JtQuantidade_mercadoria.getText()));

                } else {
                    verificao++;
                }

            }
            if (verificao == ListaProduto_Estoque.size()) {
                JTnome_mercadoria.setText(null);
                JtQuantidade_mercadoria.setText(null);
                JtPrecoVendaUnidade.setText(null);
                JOptionPane.showMessageDialog(null, "Produto não cadastrado");
            }
        }
    }
    /*trasforma uma lista de objetos em uma lista de [] String para usar na tabela*/

    public ArrayList<String[]> TransformaListaEmString(ArrayList<PRODUTO_VENDA> Lista_ProdutoVenda_aguardandoPagamento) {
        PRODUTO_VENDA LOCAL_PRODUTO_VENDA = new PRODUTO_VENDA();
        ArrayList<String[]> Local_dados = new ArrayList<>();

        for (int i = 0; i < Lista_ProdutoVenda_aguardandoPagamento.size(); i++) {
            LOCAL_PRODUTO_VENDA = (PRODUTO_VENDA) Lista_ProdutoVenda_aguardandoPagamento.get(i);
            Local_dados.add(TransformaDadoEmString(LOCAL_PRODUTO_VENDA));
        }
        return Local_dados;
    }

    public void DiminueOUsomaProdutoDoEstoque(PRODUTO_VENDA produto_venda, int operacao) {

        ArrayList<PRODUTO_ESTOQUE> Local_ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
        PRODUTO_ESTOQUE Local_Produto_Estoque = new PRODUTO_ESTOQUE();
        int quantidadeGeralProdutoEstoque = 0, quantidadeGeralProdutoEstoque_venda = 0;
        float precoCompraGeral_venda = 0, precoCompraGeral = 0;// variavel para subitrair do arquivo o preco da compra geral 
        float lucroGeralProdutoEstoque = 0, lucroGeralProdutoEstoque_venda = 0;

        for (int i = 0; i < Local_ListaProduto_Estoque.size(); i++) {
            Local_Produto_Estoque = (PRODUTO_ESTOQUE) Local_ListaProduto_Estoque.get(i);

            if (Local_Produto_Estoque.getCodigoProduto().equalsIgnoreCase(produto_venda.getCodigoProduto())) {

                quantidadeGeralProdutoEstoque = Local_Produto_Estoque.getQuantidadeProduto();
                quantidadeGeralProdutoEstoque_venda = produto_venda.getQuantidadeProduto();
                precoCompraGeral_venda = (produto_venda.getQuantidadeProduto() * Local_Produto_Estoque.getPrecoCompra_unidade());
                precoCompraGeral = Local_Produto_Estoque.getPrecoCompra_Geral();
                lucroGeralProdutoEstoque_venda = (produto_venda.getQuantidadeProduto() * Local_Produto_Estoque.getLucroUnidade());
                lucroGeralProdutoEstoque = Local_Produto_Estoque.getLucroGeral();

                if (operacao == 0) {

                    Local_Produto_Estoque.setQuantidadeProduto(quantidadeGeralProdutoEstoque - quantidadeGeralProdutoEstoque_venda);
                    Local_Produto_Estoque.setPrecoCompra_Geral(precoCompraGeral - precoCompraGeral_venda);
                    Local_Produto_Estoque.setLucroGeral(lucroGeralProdutoEstoque - lucroGeralProdutoEstoque_venda);
                }
                if (operacao == 1) {

                    Local_Produto_Estoque.setQuantidadeProduto(quantidadeGeralProdutoEstoque + quantidadeGeralProdutoEstoque_venda);
                    Local_Produto_Estoque.setPrecoCompra_Geral(precoCompraGeral + precoCompraGeral_venda);
                    Local_Produto_Estoque.setLucroGeral(lucroGeralProdutoEstoque + lucroGeralProdutoEstoque_venda);
                }

            }
        }
        try {
            ARQUIVO_ESTOQUE.Armazena(Local_ListaProduto_Estoque);
        } catch (IOException ex) {
            Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String[] TransformaDadoEmString(PRODUTO_VENDA produto_venda) {

        String nome = produto_venda.getNomeProduto();
        String codigo = produto_venda.getCodigoProduto();
        String quantidade = convert.InteiroEmString(produto_venda.getQuantidadeProduto());
        String precoVendaUnid = convert.FloatEmString(produto_venda.getValor_Venda());
        String desconto = convert.FloatEmString(produto_venda.getDescontoVenda());
        String subTotal = convert.FloatEmString(produto_venda.getSubTotal());

        String[] produto = new String[]{codigo, quantidade, nome, precoVendaUnid, desconto, subTotal};

        return produto;

    }

    public void RemoveProduto_VendaAguardandoPagamento(PRODUTO_VENDA produto_venda) {
        PRODUTO_VENDA Local_produto_venda = new PRODUTO_VENDA();
        ArrayList<String[]> Local_dados = new ArrayList<>();
        int operacao = 1;

        for (int i = 0; i < L_ProdutoVenda_aguardandoPagamento.size(); i++) {
            Local_produto_venda = (PRODUTO_VENDA) L_ProdutoVenda_aguardandoPagamento.get(i);
            if (produto_venda.getCodigoProduto().equalsIgnoreCase(Local_produto_venda.getCodigoProduto())) {
                L_ProdutoVenda_aguardandoPagamento.remove(Local_produto_venda);
                CriaTabela(TransformaListaEmString(L_ProdutoVenda_aguardandoPagamento));
                DiminueOUsomaProdutoDoEstoque(Local_produto_venda, operacao);
                colocaValoresP_resultado(Local_produto_venda, 0);
            }

        }

    }

    private class FocoNome implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            if (!JTcodigo_mercadoria.getText().isEmpty()) {
                PRODUTO_ESTOQUE = RetornaOprodutodoEstoque(JTcodigo_mercadoria.getText());

                if (PRODUTO_ESTOQUE != null) {
                    JTnome_mercadoria.setText(PRODUTO_ESTOQUE.getNomeProduto());
                    JtPrecoVendaUnidade.setText(convert.FloatEmString(PRODUTO_ESTOQUE.getPrecoVenda_unidade()));
                } else {
                    LimpaCampos();
                    JTcodigo_mercadoria.requestFocus();
                }
            }

        }
    }

    // dado um codigo procura e retorna o produto referente no estoque
    public PRODUTO_ESTOQUE RetornaOprodutodoEstoque(String cod) {
        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        for (PRODUTO_ESTOQUE ListaProduto_Estoque1 : ListaProduto_Estoque) {
            Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque1;

            codigoProduto = Produto_Estoque.getCodigoProduto();
            nomeProduto = Produto_Estoque.getNomeProduto();
            quantidadeProduto = Produto_Estoque.getQuantidadeProduto();
            precoCompra_unidade = Produto_Estoque.getPrecoCompra_unidade();
            precoCompra_Geral = Produto_Estoque.getPrecoCompra_Geral();
            LucroUnidade = Produto_Estoque.getLucroUnidade();
            LucroGeral = Produto_Estoque.getLucroGeral();

            if (codigoProduto.equalsIgnoreCase(cod)) {//COMPARA O CODIGO DO PRODUTO DO ARQUIVO COM O CODIGO QUE TÁ NO TXT

                return Produto_Estoque;

            }
        }

        return null;
    }

    // Limpa os campos de cadastrar venda
    public void LimpaCampos() {
        JTnome_mercadoria.setText(null);
        JtQuantidade_mercadoria.setText(null);
        JTcodigo_mercadoria.setText(null);
        JtPrecoVendaUnidade.setText(null);
        JtDescontoVendaUnidade.setText(null);

    }

    //dado um data verifica se o produto já foi vendido nela 
    public PRODUTO_VENDA VerificaSeOProdutoJaFoiVendido(PRODUTO_ESTOQUE PRODUTO) {
        PRODUTO_VENDA prod_venda;
        Lista_ProdutoVenda = ARQUIVO_ESTOQUE_VENDA.retorna();

        for (PRODUTO_VENDA Lista_ProdutoVenda1 : Lista_ProdutoVenda) {
            prod_venda = (PRODUTO_VENDA) Lista_ProdutoVenda1;
            if (prod_venda.getCodigoProduto().equalsIgnoreCase(PRODUTO.getCodigoProduto())) {
                if (prod_venda.getData().equalsIgnoreCase(PRODUTO_ESTOQUE.getCodigoProduto())) {
                    return prod_venda;
                }
            }
        }
        return null;
    }

    public void CriaTabela(ArrayList<String[]> ListaProdutosString) {

        checaQuantida = false;
        String[] colunas = new String[]{"Cód", "Quantidade", "Descrição", "Preço Uni", "desconto", "Subtotal"};
        modelo.setLinhas(ListaProdutosString);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setLocation(1, 145);
        jtable.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(5);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtable.setAutoCreateRowSorter(true);

        jtable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int linha;
                controladorClicks = false;

                if (e.getClickCount() == 2) {

                    if (!controladorClicks) {
                        PRODUTO_VENDA LOCAL_PRODUTO_VENDA = new PRODUTO_VENDA();

                        linha = jtable.getSelectedRow();
                        String codigoProduto = String.valueOf(jtable.getValueAt(linha, 0));
                        String quantidadeProduto = String.valueOf(jtable.getValueAt(linha, 1));
                        String nomeProduto = String.valueOf(jtable.getValueAt(linha, 2));
                        String precoVenda = String.valueOf(jtable.getValueAt(linha, 3));
                        String desconto = String.valueOf(jtable.getValueAt(linha, 4));
                        String subTotal = String.valueOf(jtable.getValueAt(linha, 5));

                        LOCAL_PRODUTO_VENDA.setCodigoProduto(codigoProduto);
                        LOCAL_PRODUTO_VENDA.setQuantidadeProduto(convert.StringEmInteiro(quantidadeProduto));
                        LOCAL_PRODUTO_VENDA.setNomeProduto(nomeProduto);
                        LOCAL_PRODUTO_VENDA.setPrecoVenda_unidade(convert.StringEmFloat(precoVenda));
                        LOCAL_PRODUTO_VENDA.setDescontoVenda(convert.StringEmFloat(desconto));
                        LOCAL_PRODUTO_VENDA.setSubTotal(convert.StringEmFloat(subTotal));

                        RemoveProduto_VendaAguardandoPagamento(LOCAL_PRODUTO_VENDA);

                        controladorClicks = true;
                        JOptionPane.showMessageDialog(null, "PRODUTO RECOLOCADO NO ESTOQUE");
                    }

                }

            }
        });

        JSprodutosVendidosAguardandoPagamento.setLocation(10, 200);
        JSprodutosVendidosAguardandoPagamento.getVerticalScrollBar();
        JSprodutosVendidosAguardandoPagamento.setSize(550, 100);
        JSprodutosVendidosAguardandoPagamento.revalidate();
        add(JSprodutosVendidosAguardandoPagamento);

    }

    public void colocaValoresP_resultado(PRODUTO_VENDA Produto_Venda, int Op) {
        float subtotal = 0;
        if (Op == 0) {
            subtotal = Produto_Venda.getSubTotal();
            total = total - subtotal;
            subtotal = 0;
        } else if (Op == 1) {

            subtotal = Produto_Venda.getSubTotal();
            total = total + subtotal;
        } else {
            subtotal = 0;
            total = 0;
        }
        P_resultado.ColocaNoSubTotal(convert.FloatEmString(subtotal));
        P_resultado.ColocarNoTotal(convert.FloatEmString(total));
    }

    private class EnterPrecionadoJTxtCodigo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                Entrada = new PainelDinheiroEntrada();

            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    public class PainelDinheiroEntrada extends JFrame {

        JButton JBfecha = new JButton();
        JButton JBvoltar = new JButton();
        JLabel JLentrada = new JLabel();
        JLabel JLtroco = new JLabel();
        private JNumberFormatField JTentrada = new JNumberFormatField(new DecimalFormat("0.00"));
        private JNumberFormatField JTtroco = new JNumberFormatField(new DecimalFormat("0.00"));
        private int largura;
        private int altura;
        PainelAltenavelCaixa.PainelDinheiroEntrada.P_Entrada Entrada = new PainelAltenavelCaixa.PainelDinheiroEntrada.P_Entrada();
        PainelAltenavelCaixa.PainelDinheiroEntrada.P_Troco Troco = new PainelAltenavelCaixa.PainelDinheiroEntrada.P_Troco();
        private Container container;

        public PainelDinheiroEntrada() {

            container = getContentPane();

            this.container.add(Entrada);
            this.container.add(Troco);
            this.setLayout(null);
            this.setSize(300, 200); // alt, larg
            this.largura = 0;
            this.altura = 300;
            this.setBackground(Color.BLUE);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(490, 250);

            Troco.setVisible(false);

        }

        public class P_Entrada extends JPanel {

            PainelAltenavelCaixa.PainelDinheiroEntrada.pressEnterEntrada PrecionaEnter = new PainelAltenavelCaixa.PainelDinheiroEntrada.pressEnterEntrada();

            public P_Entrada() {

                this.setLayout(null);
                this.setSize(300, 200); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                JLentrada.setBounds(new Rectangle(168, 135, 400, 50));
                JLentrada.setText("Entrada");
                JLentrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 50));
                JLentrada.setLocation(20, 10);
                add(JLentrada);

                JTentrada.setBounds(new Rectangle(200, 135, 120, 17));
                JTentrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JTentrada.setSize(160, 50);
                JTentrada.setLocation(20, 65);//
                JTentrada.addKeyListener(PrecionaEnter);
                add(JTentrada);

            }
        }

        public class P_Troco extends JPanel {

            PainelAltenavelCaixa.PainelDinheiroEntrada.pressEntarBTfechar preciobarEnterBTfechar = new PainelAltenavelCaixa.PainelDinheiroEntrada.pressEntarBTfechar();

            public P_Troco() {

                this.setLayout(null);
                this.setSize(300, 200); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                JLtroco.setBounds(new Rectangle(168, 135, 400, 50));
                JLtroco.setText("Troco");
                JLtroco.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 50));
                JLtroco.setLocation(20, 10);
                add(JLtroco);

                JTtroco.setBounds(new Rectangle(200, 135, 120, 17));
                JTtroco.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JTtroco.setSize(160, 50);
                JTtroco.setLocation(20, 65);//
                add(JTtroco);

                JBfecha.setText("fechar");
                JBfecha.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                JBfecha.setSize(100, 25);
                JBfecha.setLocation(19, 125);
                JBfecha.addKeyListener(preciobarEnterBTfechar);
                add(JBfecha);

                JBvoltar.setText("voltar");
                JBvoltar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                JBvoltar.setSize(100, 25);
                JBvoltar.setLocation(125, 125);
                add(JBvoltar);

            }
        }

        public void SalvarVenda() throws DocumentException {
            ArrayList<PRODUTO_VENDA> Lista_ProdutoVenda = ARQUIVO_ESTOQUE_VENDA.retorna();
            PRODUTO_VENDA LOCAL_PRODUTO_VENDA = new PRODUTO_VENDA();

            for (int i = 0; i < L_ProdutoVenda_aguardandoPagamento.size(); i++) {
                LOCAL_PRODUTO_VENDA = (PRODUTO_VENDA) L_ProdutoVenda_aguardandoPagamento.get(i);
                Lista_ProdutoVenda.add(LOCAL_PRODUTO_VENDA);
            }
            try {
                ARQUIVO_ESTOQUE_VENDA.Armazena(Lista_ProdutoVenda);
            } catch (IOException ex) {
                Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
            }
            /*CRIANDO O PDF DA NOTA*/
            try {
                testando_pdf testando_pdf = new testando_pdf((L_ProdutoVenda_aguardandoPagamento));

            } catch (IOException ex) {
                Logger.getLogger(testando_pdf.class.getName()).log(Level.SEVERE, null, ex);
            }

            L_ProdutoVenda_aguardandoPagamento.clear();
            CriaTabela(TransformaListaEmString(L_ProdutoVenda_aguardandoPagamento));
            colocaValoresP_resultado(Produto_Venda, 2);

        }

        public class pressEnterEntrada implements KeyListener {

            int quant;

            public void pegaQuantidade(int quantidade) {
                quant = quantidade;

            }

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    float tot = P_resultado.RetornaTotal();
                    float dinheiroEntrada = Float.parseFloat(JTentrada.getText().replace(",", "."));
                    float total = Float.parseFloat(JTtotal.getText().replace(",", "."));

                    if (dinheiroEntrada < tot) {
                        JOptionPane.showMessageDialog(null, "A entrada deve ser maior ou igual a: R$" + tot);
                        JTentrada.setText(null);
                        JTentrada.requestFocus();
                    } else {
                        try {
                            SalvarVenda();
                        } catch (DocumentException ex) {
                            Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Entrada.setVisible(false);
                        Troco.setVisible(true);

                        JTtroco.setText(AlternavelCadastroProduto.converterFloatString(dinheiroEntrada - tot));
                        JBfecha.requestFocus();

                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // throw new UnsupportedOperationException("Not supported yet.");
            }
        }

        public class pressEntarBTfechar implements KeyListener {

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    Entrada.setVisible(true);
                    Troco.setVisible(false);
                    JTentrada.setText(null);
                    JTtroco.setText(null);
                    Total = 0;
                    P_resultado.LimpaTxt();

                    PainelAltenavelCaixa.PainelDinheiroEntrada.this.dispose();
                    dados.clear();

                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }
        }

    }

    ///PAINEL DE PESQUISA///
    public class AlternavelPesquisa extends JComboBox implements JComboBox.KeySelectionManager {

        private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
        private ArrayList<String> listaNomesProdutos = new ArrayList<>();
        private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
        private String searchFor;
        private long lap;
        int contador = 0;
        JFrame f = new JFrame("AutoCompleteComboBox");

        private AlternavelPesquisa() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AlternavelPesquisa(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);
            tf = null;
            if (getEditor() != null) {
                tf = (JTextField) getEditor().getEditorComponent();

                if (tf != null) {
                    //para fazer apenas uma verificação de enter precionado
                    tf.setDocument(new PainelAltenavelCaixa.AlternavelPesquisa.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            tf = (JTextField) getEditor().getEditorComponent();
                            tf.setSize(230, 25);
                            String text = tf.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    tf.setText(current);
                                    tf.setSelectionStart(text.length());
                                    tf.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }
            tf.addKeyListener(tecladoExclusivo);
            tf.setText(null);
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {

                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();

                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }

        public String[] pegaTodosNomesProdutos() {
            int i, j = 0;
            String test;
            String[] names = {""};
            ArrayList<String> Sentinela = new ArrayList<>();

            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
            names = new String[ListaProduto_Estoque.size()];
            Sentinela.add("");

            for (i = 0; i < ListaProduto_Estoque.size(); i++) {

                Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
                Sentinela.add(Produto_Estoque.getNomeProduto());
            }

            names = Sentinela.toArray(names);
            Arrays.sort(names, 0, ListaProduto_Estoque.size());

            return (names);
        }

        void createAndShowGUI_2() {

            // f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setSize(265, 200);
            f.setLocation(550, 200);
            Container cp = f.getContentPane();
            cp.setLayout(null);
            pegaTodosNomesProdutos();

            comboBox = new PainelAltenavelCaixa.AlternavelPesquisa(pegaTodosNomesProdutos());
            comboBox.setEditable(true);
            comboBox.setBounds(50, 50, 100, 25);
            comboBox.setMaximumRowCount(20);
            comboBox.setSize(250, 25);
            comboBox.setLocation(0, 0);
            cp.add(comboBox);
            new JavaApplication4(comboBox);

            Locale[] locales = Locale.getAvailableLocales();//

            f.setVisible(true);

        }

        String RetornaNome() {

            return tf.getText();
        }

        public void FechaPainel() {
            f.dispose();

        }
    }

    public class Teclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {

                alternavelPesquisa.createAndShowGUI_2();
            }

        }

    }

    //exclusivo para pesquisa
    public class TecladoExclusivo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {

                alternavelPesquisa.createAndShowGUI_2();
            }
            if (e.getKeyCode() == 10) {

                ColocaComponentesEmTxt(alternavelPesquisa.RetornaNome());
                alternavelPesquisa.FechaPainel();
            }
        }
    }

    public void ColocaComponentesEmTxt(String nome) {
        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        for (PRODUTO_ESTOQUE ListaProduto_Estoque1 : ListaProduto_Estoque) {
            Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque1;
            if (Produto_Estoque.getNomeProduto().equalsIgnoreCase(nome)) {
                JTnome_mercadoria.setText(Produto_Estoque.getNomeProduto());
                JTcodigo_mercadoria.setText(Produto_Estoque.getCodigoProduto());
                JtPrecoVendaUnidade.setText(AlternavelCadastroProduto.converterFloatString(Produto_Estoque.getPrecoVenda_unidade()));
                JtQuantidade_mercadoria.requestFocus();

            }
        }

    }

}
