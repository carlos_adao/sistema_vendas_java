/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import Auxiliares.Datas;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class Calculos_ExibeCompras {


    float tot_Compras = 0;
    String quantidade,codigo,nome,precoCompraUni,precoCompraTotal, diaCompra;
    private ArrayList<PRODUTO_COMPRA> ListaProdutosComprados = new ArrayList<>(); //carrega para memoria a lista de produtos vendidos
    private PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA(); //instancia um produto do tipo venda para fazer manipulações
    ArrayList<String[]> dados = new ArrayList<>();// usado para enviar a lista de vendas em forma de String
    Converte converte = new Converte();//isntancia a classe para realizar converção
    private final Datas datas = new Datas();

    /*retorna todas as vendas da consulta solicitada*/
    public ArrayList<String[]> RetornaValoresParaExibirCompras(String Consutar) {// consutar pode ser o dia o mes ou o ano de acordo com o que deseja visualizar
        tot_Compras = 0;
     
        dados.clear();
        ListaProdutosComprados.clear();
        ListaProdutosComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();

        if (ListaProdutosComprados.size() > 0) {

            for (int i = 0; i < ListaProdutosComprados.size(); i++) {
                produtoCompra = (PRODUTO_COMPRA) ListaProdutosComprados.get(i);

                if (produtoCompra.getDia().equalsIgnoreCase(Consutar)) {

                    quantidade = String.valueOf(produtoCompra.getQuantidade());
                    codigo = produtoCompra.getCodigo();
                    nome = produtoCompra.getNome();

                    precoCompraUni = converte.FloatEmString(produtoCompra.getPrecoCompraUnidade());
                    precoCompraTotal = converte.FloatEmString(produtoCompra.getPrecoCompraTotal());
                    diaCompra = produtoCompra.getDia();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoCompraUni, precoCompraTotal, diaCompra};
                    dados.add(produtos);

                    tot_Compras = tot_Compras + produtoCompra.getPrecoCompraTotal();
                    

                }

                if (produtoCompra.getMes().equalsIgnoreCase(Consutar) && produtoCompra.getAno().equalsIgnoreCase(datas.retornaAno())) {

                   quantidade = String.valueOf(produtoCompra.getQuantidade());
                    codigo = produtoCompra.getCodigo();
                    nome = produtoCompra.getNome();

                    precoCompraUni = converte.FloatEmString(produtoCompra.getPrecoCompraUnidade());
                    precoCompraTotal = converte.FloatEmString(produtoCompra.getPrecoCompraTotal());
                    diaCompra = produtoCompra.getDia();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoCompraUni, precoCompraTotal, diaCompra};
                    dados.add(produtos);

                    tot_Compras = tot_Compras + produtoCompra.getPrecoCompraTotal();
                }
                if (produtoCompra.getAno().equalsIgnoreCase(Consutar)) {

                    quantidade = String.valueOf(produtoCompra.getQuantidade());
                    codigo = produtoCompra.getCodigo();
                    nome = produtoCompra.getNome();

                    precoCompraUni = converte.FloatEmString(produtoCompra.getPrecoCompraUnidade());
                    precoCompraTotal = converte.FloatEmString(produtoCompra.getPrecoCompraTotal());
                    diaCompra = produtoCompra.getDia();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoCompraUni, precoCompraTotal, diaCompra};
                    dados.add(produtos);

                    tot_Compras = tot_Compras + produtoCompra.getPrecoCompraTotal();

                }
            }

            return dados;
        }
        return null;
    }
     
    public String RetornaTotalCompras(String consuta) {
        if (ListaProdutosComprados.size() > 0) {

            for (int i = 0; i < ListaProdutosComprados.size(); i++) {
                produtoCompra = (PRODUTO_COMPRA) ListaProdutosComprados.get(i);

                if (produtoCompra.getDia().equalsIgnoreCase(consuta)) {
                     tot_Compras = tot_Compras + produtoCompra.getPrecoCompraTotal();
                }

            }
        }
        return converte.FloatEmString( tot_Compras);
    }
    public String RetornaTotalCompras() {
        return converte.FloatEmString(tot_Compras);
    }

}
