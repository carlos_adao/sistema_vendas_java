/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import Auxiliares.Converte;
import Auxiliares.Mascara_Data;
import BancoDados.*;
import Auxiliares.Datas;
import Compras.ARMAZENAR_COMPRA_NOTA;
import Compras.COMPRA_NOTA;
import Compras.CalculaDiferencaEntreDatas;
import Compras.PAGAMENTO_NOTA;
import Gerando_pdf.Comprovante_compraNota;
import Interface_Alternavel.*;
import com.itextpdf.text.DocumentException;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Convidado
 */
public class P_CompraNotasVisualizar extends JPanel {

    ARQUIVO_PAGAMENTO_NOTA Arquivo_Pagamento = new ARQUIVO_PAGAMENTO_NOTA();
    PAGAMENTO_NOTA Pagamentos = new PAGAMENTO_NOTA();
    ArrayList<String[]> dados = new ArrayList<>();
    PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();
    ARQUIVO_ESTOQUE aquivo_estoque = new ARQUIVO_ESTOQUE();
    ArrayList<PRODUTO_ESTOQUE> ListaprodutoEstoque = new ArrayList<>();
    ArrayList<PRODUTO_ESTOQUE> Listaprodutos = new ArrayList<>();
    COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    ARMAZENAR_COMPRA_NOTA ArmazenarPagamentos = new ARMAZENAR_COMPRA_NOTA();
    ArrayList<COMPRA_NOTA> ListaCompraNota = new ArrayList<>();
    ARQUIVO_COMPRA_NOTA arquivoNotas = new ARQUIVO_COMPRA_NOTA();
    JLabel JlnomeFornecedor = new JLabel();
    JLabel JlvalorTotalDividas = new JLabel();
    JTextField JtvalorTotalDividas = new JTextField();
    JLabel JldinheiroDia = new JLabel();
    JTextField JtdinheiroDia = new JTextField();
    JScrollPane JSfornecedores;
    JScrollPane JSnotas;
    JScrollPane JSprodutos;
    JScrollPane JSarmazena;
    JScrollPane JSpagamentosEfetuados;
    JTextArea JTAfornecedores = new JTextArea();
    private JList JListFonecedores;//UMA LISTA PARA OS FORNECEDORES
    private DefaultListModel ListaModel = new DefaultListModel();//UMA LISTA(MODEL) PARA OS FORNECEDORES
    private DefaultListSelectionModel ListaModelSelection = new DefaultListSelectionModel();//USADA QUANDO CLICLA NO NOME NO TEXT AREA

    //COMPONETES DA TABELA//
    TabelaModel modelo = new TabelaModel();

    PFonecedorProduto P_FonecedorProduto;
    static String nomeFornecedor;
    Datas datas = new Datas();
    ArrayList< PRODUTO_ESTOQUE> ProdutoEstoqueAguard = new ArrayList<>();
    boolean flag = false;
    String nomeProduto;
    String codigoProduto;
    String quantidadeProduto;
    String precoCompra_unidade;
    String precoVenda_unidade;
    String precoCompra_Geral;
    String LucroUnidade;
    String LucroGeral;
    ArrayList<ARMAZENAR_COMPRA_NOTA> ListaarmazenarPagamento = new ArrayList<>();
    ARMAZENA_DINHEIRO armazena_dinheiro = new ARMAZENA_DINHEIRO();

    boolean Verifica = false;
    TabelaModel TabelaModel;
    Converte Converte = new Converte();
    int quantidade_linhas_tabela = 0;

    /*
     * CLASSE DE ACÃO DO TECLADO
     */
    private AcaoTeclado acaoTeclado = new AcaoTeclado();

    //componentes usados nos paineis efetuar  pagamentos
    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
    ArrayList<ARMAZENAR_COMPRA_NOTA> ListaArmazenamentos = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();
    Calculos_ExibePagamentosCompraNota Calculos = new Calculos_ExibePagamentosCompraNota();
    float Aux_totalpago = Calculos.PagaValorTotalPagamentosEfetuados(nomeFornecedor, ListaPagamentos);
    float Aux_totalArmazenado = Calculos.PagaValorTotalArmazenadoEfetuados(nomeFornecedor, ListaArmazenamentos);

    public P_CompraNotasVisualizar() {
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);

    }

    public void Componentes() {//METODO PARA INICIAR OS COMPONENTES DO PAINEL 

        AdicionarClientesJLista();//CHAMADA DO METODO PARA ADICIONAR FORNECEDORES A LISTAMODE

        JSfornecedores = new JScrollPane(JListFonecedores);
        JSfornecedores.setLocation(20, 50);//LADO OUTRO & CIMA BAIXO
        JSfornecedores.setSize(200, 400);//LARGURA & ALTURA

        JlvalorTotalDividas.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorTotalDividas.setText("Divida Total");
        JlvalorTotalDividas.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorTotalDividas.setLocation(900, 20);//
        add(JlvalorTotalDividas);

        JtvalorTotalDividas.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorTotalDividas.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorTotalDividas.setSize(95, 35);
        JtvalorTotalDividas.setLocation(900, 45);
        JtvalorTotalDividas.setText(AlternavelCadastroProduto.converterFloatString(RetornaDividaTotal()));
        add(JtvalorTotalDividas);

        JldinheiroDia.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldinheiroDia.setText("Dinheiro Dia");
        JldinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldinheiroDia.setLocation(900, 100);//
        add(JldinheiroDia);

        JtdinheiroDia.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdinheiroDia.setSize(95, 35);
        JtdinheiroDia.setLocation(900, 125);
        JtdinheiroDia.setText(AlternavelCadastroProduto.converterFloatString(RetornaDinheiroTotalDia()));
        // JtvalorTotalDividas.addFocusListener(acaoFocoProduto);
        add(JtdinheiroDia);

    }

    public void criaTabela() {

        Datas data = new Datas();
        String dia = data.retornaData();
        String nomeFornecedor;
        String valorDivida;
        String dinheiroDia;
        String dataPagamento;
        String dinheiroEntrada;
        String telefone;

        dados.clear();
        Componentes();

        ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();

        if (ListaCompraNota.size() > 0) {

            for (int j = 0; j < ListaCompraNota.size(); j++) {
                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(j);

                if (compra_nota.getDataEfetuarPagamento().equalsIgnoreCase("null") && compra_nota.getTelefone().equalsIgnoreCase("null")) {
                    ListaCompraNota.remove(compra_nota);

                }
            }

            for (int i = 0; i < ListaCompraNota.size(); i++) {
                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

                int quantidadeDias = CalculaDiferencaEntreDatas.calcula(dia, compra_nota.getDataEfetuarPagamento());
                nomeFornecedor = compra_nota.getNomeFornecedor();
                valorDivida = AlternavelCadastroProduto.converterFloatString(compra_nota.getValorDivida());

                if (quantidadeDias < 0) {
                    dinheiroDia = "ATRAZADO";
                } else if (quantidadeDias == 0) {
                    dinheiroDia = AlternavelCadastroProduto.converterFloatString(compra_nota.getValorDivida());
                } else {

                    dinheiroDia = AlternavelCadastroProduto.converterFloatString((compra_nota.getValorDivida() - PagaValorTotalArmazenado(compra_nota.getNomeFornecedor())) / quantidadeDias);
                }

                dinheiroEntrada = AlternavelCadastroProduto.converterFloatString(compra_nota.getDinheiroEntrada());
                dataPagamento = compra_nota.getDataEfetuarPagamento();
                telefone = compra_nota.getTelefone();

                String[] Nota = new String[]{nomeFornecedor, valorDivida, dinheiroDia, dinheiroEntrada, dataPagamento, telefone};

                dados.add(Nota);
            }
            Tabela(dados);
        }

    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Fornecedor", "Divida", "Dinheiro Dia", "Entrada", "Data Pag", "Telefône"};

        modelo.setLinhas(dados);
        modelo.setColunas(colunas);

        final JTable jtable = new JTable();
        JScrollPane JSprodutosVendidos = new JScrollPane(jtable);
        jtable.setModel(modelo);
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(50);

        jtable.addKeyListener(acaoTeclado);
        jtable.addMouseListener(new MouseAdapter() {

            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 2) {

                    flag = true;
                    linha = jtable.getSelectedRow();

                    String nomeFornecedo = String.valueOf(jtable.getValueAt(linha, 0));
                    String dividaTotal = String.valueOf(jtable.getValueAt(linha, 1));
                    String dataPagamento = String.valueOf(jtable.getValueAt(linha, 4));

                    Frame_Movimentacao Frame_Movimentacao = new Frame_Movimentacao(nomeFornecedo, dataPagamento, dividaTotal);

                }
            }
        });

        JSprodutosVendidos.setLocation(10, 30);
        JSprodutosVendidos.getVerticalScrollBar();
        JSprodutosVendidos.setSize(800, 250);

        add(JSprodutosVendidos);

    }

    public void removeCompraNota(int posicao) {
        ListaCompraNota.remove(ListaCompraNota.get(posicao));
        try {
            ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
            JOptionPane.showMessageDialog(null, "Compra nota Removida!");
        } catch (IOException ex) {
            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private float PagaValorTotalArmazenado(String nomeFornecedor) {
        ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();
        float ValorTotal = 0;

        for (int i = 0; i < ListaarmazenarPagamento.size(); i++) {
            ArmazenarPagamentos = (ARMAZENAR_COMPRA_NOTA) ListaarmazenarPagamento.get(i);

            if (ArmazenarPagamentos.getFormecedor().equalsIgnoreCase(nomeFornecedor)) {
                ValorTotal = ValorTotal + ArmazenarPagamentos.getValorArmazenado();
            }
        }

        return ValorTotal;
    }

    public void AdicionarClientesJLista() {  //  ADICIONA FORNECEDORES NA LISTAMODEL

        this.ListaModel.clear();

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            try {
                this.ListaModel.addElement(arquivoNotas.retorna().get(i).getNomeFornecedor());
            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "Acesso a memoria indevida!");
                break;
            }
        }
    }

    public float RetornaDividaTotal() {
        float dividaTotal = 0;

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            dividaTotal += arquivoNotas.retorna().get(i).getValorDivida();

        }
        return dividaTotal;
    }

    public float RetornaDinheiroTotalDia() {
        float dinheiroTotalDia = 0;

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            dinheiroTotalDia += arquivoNotas.retorna().get(i).getDinheiroDia();

        }
        return dinheiroTotalDia;
    }

    public class AcaoTeclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            int linha;

            if (e.getKeyCode() == 112) {

            }

        }
    }

    public void Correcao(String nameFornecedor) {
        float totalCompra = 0;
        float valorDia = 0;
        ListaCompraNota = arquivoNotas.retorna();

        //JOptionPane.showMessageDialog(null, "correção"); 
        for (int i = 0; i < ListaCompraNota.size(); i++) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
            //JOptionPane.showMessageDialog(null, "NomeFornecedorTabela: "+nameFornecedor+"\n"+"nomeFornecedorArquivo: "+ compra_nota.getNomeFornecedor());
            if (compra_nota.getNomeFornecedor().equalsIgnoreCase(nameFornecedor)) {

                //ProdutoEstoqueAguard = compra_nota.getProdutoEstoque(); 
                for (int j = 0; j < ProdutoEstoqueAguard.size(); j++) {

                    totalCompra += ProdutoEstoqueAguard.get(j).getPrecoCompra_Geral();

                }

                compra_nota.setValorDivida(totalCompra);

            }
        }
        try {
            ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
        } catch (IOException ex) {
            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public class PFonecedorProduto extends JFrame {

        private Container container;
        PainelFonrecedoProdutos Painel_FonrecedoProdutos;
        Painel_Visualizar_Pagamentos Painel_Visualizar_Pagamentos;
        P_CompraNotasVisualizar P_CompraNotasVisualizar = new P_CompraNotasVisualizar();
        Painel_Geral Painel_Topo = new Painel_Geral(); // cabeçalho que ira mudar de acordo com a função

        public PFonecedorProduto(String nome, String dividaTotal, String dataPagamento) {

            container = getContentPane();

            this.setSize(800, 300);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(180, 380);

            Painel_FonrecedoProdutos = new PainelFonrecedoProdutos(nome, dividaTotal, dataPagamento);
            //Painel_Visualizar_Pagamentos = new Painel_Visualizar_Pagamentos(nome);

            //this.container.add(Painel_Topo);
            this.container.add(Painel_FonrecedoProdutos);
            //this.container.add(Painel_Visualizar_Pagamentos);
            //Painel_Visualizar_Pagamentos.setVisible(false);
            //Painel_FonrecedoProdutos.setVisible(false);
        }

        public final class PainelFonrecedoProdutos extends JPanel {

            JLabel JlnomeForn = new JLabel();
            JLabel JlvalorDividaTotal = new JLabel();
            JLabel JlvalorArmazenado = new JLabel();
            JLabel JldataPagamento = new JLabel();
            JLabel JldiaPagamento = new JLabel();
            JLabel JlquantidadeDias = new JLabel();
            JLabel JldatPagamento = new JLabel();
            JLabel JlvalorPagamento = new JLabel();
            JLabel JlquemRecebeu = new JLabel();
            JLabel JlguardaNomeFornecedor = new JLabel();
            JTextField JtdiaPagamento = new JTextField();
            JFormattedTextField JtdatPagamento = new JFormattedTextField(Mascara("##/##/####"));
            JNumberFormatField JtvalorPagamento = new JNumberFormatField(new DecimalFormat("0.00"));
            JTextField JtquemRecebeu = new JTextField();
            /*
             * --------COMPANENTES DE ARMAZENAMENTO-------
             */
            JLabel JldiaArmazenamento = new JLabel();
            JTextField JtdiaArmazenamento = new JTextField();
            JLabel JldataArmazenamento = new JLabel();
            JFormattedTextField JtdataArmazenamento = new JFormattedTextField(Mascara("##/##/####"));
            JLabel JlvalorArmazenamento = new JLabel();
            JNumberFormatField JtvalorArmazenamento = new JNumberFormatField(new DecimalFormat("0.00"));
            JLabel Jlobservacao = new JLabel();
            JTextField Jtobservacao = new JTextField();
            JButton Botao_salvarArmazenamento, Botao_cancelarArmazenamento;
            /*
             * --------------------------------------------
             */
            ARMAZENAR_COMPRA_NOTA armazenarPagamento = new ARMAZENAR_COMPRA_NOTA();// istanciando um objeto para guardar armazenamento
            ArrayList<ARMAZENAR_COMPRA_NOTA> ListaarmazenarPagamento = new ArrayList<>();
            String[] data;
            ArrayList<String[]> produtosAgardandoPagamendo = new ArrayList<>();
            JComboBox<String> JCopcoes;
            private String opcoes[] = {"Selecione Opcao", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};
            /*
             * CLASSE QUE PEGA A DATA DO SISTEMA
             */
            private final Datas Datas = new Datas();
            String dia = Datas.retornaData();
            /*
             * BOTÃO PARA FAZER ARQUIVAMENTO E CANCELAMENTO DOS PAGAMENTOS
             */
            private JButton Botao_salvar, Botao_cancelar;
            /*
             * CLASSE DE ACAO DOS BOTOES
             */
            //SALVAR ARMAZENAMENTO
            private AcaoBotoes acaoBotoes = new AcaoBotoes();
            /*
             * CLASSE COMPRA PARA AQUIVA A COMPRA NOTA QUE FOI PAGA
             */
            PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
            ARQUIVO_ESTOQUE_COMPRA ArquivoProdutoCompra = new ARQUIVO_ESTOQUE_COMPRA();
            ArrayList<PRODUTO_COMPRA> ListaProdutoComprados = new ArrayList<>();
            ArrayList<PAGAMENTO_NOTA> ListaPagamentos = new ArrayList<>();
            PAGAMENTO_NOTA PagamentoNota = new PAGAMENTO_NOTA();

            public PainelFonrecedoProdutos(final String nome, final String dividaTotal, final String dataPagamento) {
                this.setLayout(null);
                this.setSize(300, 300); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                data = dataPagamento.split("/");

                nomeFornecedor = nome;

                int ano = Integer.parseInt(data[2]);
                int mes = Integer.parseInt(data[1]);
                int dia = Integer.parseInt(data[0]);

                JlnomeForn.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlnomeForn.setText(nome);
                JlnomeForn.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JlnomeForn.setBackground(Color.BLUE);
                JlnomeForn.setLocation(20, 20);//
                add(JlnomeForn);

                JlvalorDividaTotal.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorDividaTotal.setText("Divida Total R$" + dividaTotal);
                JlvalorDividaTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorDividaTotal.setLocation(20, 40);//
                add(JlvalorDividaTotal);

                JlvalorArmazenado.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorArmazenado.setText("Armazenado R$" + PagaValorTotalArmazenado(nome));
                JlvalorArmazenado.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorArmazenado.setLocation(250, 40);//
                JlvalorArmazenado.setBackground(Color.red);
                add(JlvalorArmazenado);

                JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataPagamento.setText("Data pagamento:" + dataPagamento);
                JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataPagamento.setLocation(20, 60);//
                add(JldataPagamento);

                JlquantidadeDias.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlquantidadeDias.setText(String.valueOf("Qtd Dias Restantes " + RetornaQuantidadeDiasRestante(nome)));
                JlquantidadeDias.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlquantidadeDias.setLocation(250, 60);//
                add(JlquantidadeDias);

                JCopcoes = new JComboBox(opcoes);
                JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JCopcoes.setSize(220, 20);
                JCopcoes.setLocation(500, 80);
                JCopcoes.addItemListener(
                        new ItemListener() {

                            @Override
                            public void itemStateChanged(ItemEvent e) {
                                if (e.getStateChange() == ItemEvent.SELECTED) {

                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Pagamento")) {
                                        EfetuaPagamento(nome);
                                        JOptionPane.showMessageDialog(null, "testando");

                                    }
                                    //ARMAZENA UM PAGAMENTO PARA UMA NOTA
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Armazenamento")) {
                                        ArmazenarPagamento(nome);
                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Armazenamento")) {
                                        ArmazenarPagamento(nome);
                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Armazenamento")) {
                                        CriaTabelaExibeArmazenamentos(nome);
                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Divida")) {
                                        try {
                                            RemoveDivida(nome);
                                        } catch (IOException ex) {
                                            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        JOptionPane.showMessageDialog(null, "Compra Nota Removida!!!");
                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Pagamento")) {

                                        Painel_FonrecedoProdutos.setVisible(false);
                                        Painel_Visualizar_Pagamentos.setVisible(true);

                                    }

                                }
                            }
                        });
                add(JCopcoes);

            }

            public String RetornaQuantidadeDiasRestante(String NomeFornecedor) {
                int quantidadeDias = 0;

                for (int i = 0; i < ListaCompraNota.size(); i++) {
                    compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

                    if (compra_nota.getNomeFornecedor().equalsIgnoreCase(NomeFornecedor)) {
                        quantidadeDias = CalculaDiferencaEntreDatas.calcula(datas.retornaData(), compra_nota.getDataEfetuarPagamento());
                    }
                }

                return String.valueOf(quantidadeDias);
            }
            //REMOVER A DIVIDA 

            public void RemoveDivida(String nome) throws IOException {
                ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();

                for (int i = 0; i < ListaCompraNota.size(); i++) {
                    compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

                    if (compra_nota.getNomeFornecedor().equalsIgnoreCase(nome)) {
                        ListaCompraNota.remove(compra_nota);

                        ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);

                    }
                }
            }

            public void EfetuaPagamento(String nomeFonecedor) {
                JSprodutos.setVisible(false);

                JlguardaNomeFornecedor.setText(nomeFonecedor);

                JldiaPagamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
                JldiaPagamento.setText("Dia Pagamento");
                JldiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldiaPagamento.setLocation(20, 100);
                add(JldiaPagamento);

                JtdiaPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdiaPagamento.setSize(200, 25);
                JtdiaPagamento.setLocation(20, 130);
                add(JtdiaPagamento);

                JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataPagamento.setText("Data pagamento");
                JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataPagamento.setLocation(260, 100);
                add(JldataPagamento);

                JtdatPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdatPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdatPagamento.setSize(200, 25);
                JtdatPagamento.setLocation(260, 130);
                JtdatPagamento.setText(dia);

                add(JtdatPagamento);

                JlvalorPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorPagamento.setText("Valor pagamento");
                JlvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorPagamento.setLocation(500, 100);
                add(JlvalorPagamento);

                JtvalorPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtvalorPagamento.setSize(200, 25);
                JtvalorPagamento.setLocation(500, 130);
                add(JtvalorPagamento);

                JlquemRecebeu.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlquemRecebeu.setText("Quem Recebeu Pagamento");
                JlquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlquemRecebeu.setLocation(20, 160);
                add(JlquemRecebeu);

                JtquemRecebeu.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtquemRecebeu.setSize(400, 25);
                JtquemRecebeu.setLocation(20, 190);
                add(JtquemRecebeu);

                //**Jbutton Salvar**//
                Botao_salvar = new JButton();
                Botao_salvar.setText("SALVAR");
                Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_salvar.setSize(90, 25);
                Botao_salvar.addActionListener(acaoBotoes);
                Botao_salvar.addKeyListener(acaoBotoes);
                Botao_salvar.setLocation(500, 190);
                add(Botao_salvar);

                Botao_cancelar = new JButton();
                Botao_cancelar.setText("CANCELAR");
                Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_cancelar.setSize(100, 25);
                //Botao_cancelar.addActionListener(this);
                Botao_cancelar.setLocation(600, 190);
                add(Botao_cancelar);

            }

            public void ArmazenarPagamento(String nomeFonecedor) {
                JSprodutos.setVisible(false);

                JlguardaNomeFornecedor.setText(nomeFonecedor);

                JldiaArmazenamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
                JldiaArmazenamento.setText("Dia Armazenamento");
                JldiaArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldiaArmazenamento.setLocation(20, 100);
                add(JldiaArmazenamento);

                JtdiaArmazenamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdiaArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdiaArmazenamento.setSize(200, 25);
                JtdiaArmazenamento.setLocation(20, 130);
                add(JtdiaArmazenamento);

                JldataArmazenamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataArmazenamento.setText("Data Armazenamento");
                JldataArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataArmazenamento.setLocation(260, 100);
                add(JldataArmazenamento);

                JtdataArmazenamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdataArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdataArmazenamento.setSize(200, 25);
                JtdataArmazenamento.setLocation(260, 130);
                add(JtdataArmazenamento);

                JlvalorArmazenamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorArmazenamento.setText("Valor Armazenamento");
                JlvalorArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorArmazenamento.setLocation(500, 100);
                add(JlvalorArmazenamento);

                JtvalorArmazenamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtvalorArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtvalorArmazenamento.setSize(200, 25);
                JtvalorArmazenamento.setLocation(500, 130);
                add(JtvalorArmazenamento);

                Jlobservacao.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                Jlobservacao.setText("Observação");
                Jlobservacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                Jlobservacao.setLocation(20, 160);
                add(Jlobservacao);

                Jtobservacao.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                Jtobservacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                Jtobservacao.setSize(400, 25);
                Jtobservacao.setLocation(20, 190);
                add(Jtobservacao);

                //**Jbutton Salvar**//
                Botao_salvarArmazenamento = new JButton();
                Botao_salvarArmazenamento.setText("SALVAR");
                Botao_salvarArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_salvarArmazenamento.setSize(90, 25);
                Botao_salvarArmazenamento.addActionListener(acaoBotoes);
                //Botao_salvar.addKeyListener(enterPrecionadoBtSalvar);
                Botao_salvarArmazenamento.setLocation(500, 190);
                add(Botao_salvarArmazenamento);

                Botao_cancelar = new JButton();
                Botao_cancelar.setText("CANCELAR");
                Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_cancelar.setSize(100, 25);
                //Botao_cancelar.addActionListener(this);
                Botao_cancelar.setLocation(600, 190);
                add(Botao_cancelar);

            }

            public void CriaTabelaExibeArmazenamentos(String name) {

                String[] colunas = new String[]{"Fornecedor", "Dia Arm.", "Data Arm", "Obs", "Valor Arm."};
                TabelaModel modelo = new TabelaModel(PegaArmazenamentoEfetuados(name), colunas);
                final JTable jtable = new JTable(modelo);
                jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
                jtable.getColumnModel().getColumn(0).setPreferredWidth(100);
                jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(3).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(4).setPreferredWidth(50);

                JSarmazena = new JScrollPane(jtable);
                JSarmazena.setLocation(20, 100);
                JSarmazena.getVerticalScrollBar();
                JSarmazena.setSize(700, 150);
                JSarmazena.revalidate();
                add(JSarmazena);

            }

            public void CriaTabelaExibePagamentos(String nome) {

                String[] colunas = new String[]{"Fornecedor", "Dia Pag.", "Data Pag", "Q.recebeu", "Valor pag."};
                TabelaModel modelo = new TabelaModel(PegaPagamentoEfetuados(nome), colunas);
                final JTable jtable = new JTable(modelo);
                jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
                jtable.getColumnModel().getColumn(0).setPreferredWidth(100);
                jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(3).setPreferredWidth(50);
                jtable.getColumnModel().getColumn(4).setPreferredWidth(50);

                JSpagamentosEfetuados = new JScrollPane(jtable);
                JSpagamentosEfetuados.setLocation(20, 100);
                JSpagamentosEfetuados.getVerticalScrollBar();
                JSpagamentosEfetuados.setSize(700, 150);
                JSpagamentosEfetuados.revalidate();
                add(JSpagamentosEfetuados);

            }
            /*Pega uma lista de armazenamentos efetuados para uma determinada nota*/

            public ArrayList<String[]> PegaArmazenamentoEfetuados(String name) {
                String[] armazenamentos;
                ArrayList<String[]> ListaArmazenamentos = new ArrayList<>();
                ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();

                for (int i = 0; i < ListaarmazenarPagamento.size(); i++) {
                    ArmazenarPagamentos = (ARMAZENAR_COMPRA_NOTA) ListaarmazenarPagamento.get(i);
                    if (name.equalsIgnoreCase(ArmazenarPagamentos.getFormecedor())) {
                        armazenamentos = new String[]{ArmazenarPagamentos.getFormecedor(),
                            ArmazenarPagamentos.getDiaArmazenamento(),
                            ArmazenarPagamentos.getDataArmazenamento(),
                            ArmazenarPagamentos.getObcervacao(),
                            AlternavelCadastroProduto.converterFloatString(ArmazenarPagamentos.getValorArmazenado())};

                        ListaArmazenamentos.add(armazenamentos);
                    }
                }

                return ListaArmazenamentos;
            }

            /*Pega uma lista de pagamentos efetuados para uma determinada nota*/
            public ArrayList<String[]> PegaPagamentoEfetuados(String name) {
                String[] pagamentos;
                ArrayList<String[]> ListaPagamentosEfetuados = new ArrayList<>();
                ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();

                for (int i = 0; i < ListaPagamentos.size(); i++) {
                    PagamentoNota = (PAGAMENTO_NOTA) ListaPagamentos.get(i);
                    if (name.equalsIgnoreCase(PagamentoNota.getFornecedor())) {
                        pagamentos = new String[]{PagamentoNota.getFornecedor(),
                            PagamentoNota.getDiaRecebeu(),
                            PagamentoNota.getDataRecebeu(),
                            PagamentoNota.getNomeRecebeu(),
                            AlternavelCadastroProduto.converterFloatString(PagamentoNota.getValorRecebeu())};

                        ListaPagamentosEfetuados.add(pagamentos);
                    }
                }

                return ListaPagamentosEfetuados;
            }

            public ArrayList<String[]> RetornaPagamentosEfetuados() {
                String[] pagamentos;
                ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
                return null;
            }

            public float PagaValorTotalArmazenado(String nomeFornecedor) {
                ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();
                float ValorTotal = 0;
                for (int i = 0; i < ListaarmazenarPagamento.size(); i++) {
                    ArmazenarPagamentos = (ARMAZENAR_COMPRA_NOTA) ListaarmazenarPagamento.get(i);

                    if (ArmazenarPagamentos.getFormecedor().equalsIgnoreCase(nomeFornecedor)) {
                        ValorTotal = ValorTotal + ArmazenarPagamentos.getValorArmazenado();
                    }
                }

                return ValorTotal;
            }

            public PAGAMENTO_NOTA RetornaPagamento(String fornecedor) {
                Pagamentos.setFornecedor(fornecedor);
                Pagamentos.setNomeRecebeu(JtquemRecebeu.getText());
                Pagamentos.setDiaRecebeu(JtdiaPagamento.getText());
                Pagamentos.setDataRecebeu(JtdatPagamento.getText());
                Pagamentos.setValorRecebeu(Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")));

                return Pagamentos;

            }
            //quando é concluido um pagamento de compra nota é eliminado os pagamentos arquivados

            public void EliminaPagamentosConcluidos(String nomeFornecedor) {
                ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();

                for (int i = 0; i < ListaCompraNota.size(); i++) {
                    COMPRA_NOTA pagamento = (COMPRA_NOTA) ListaCompraNota.get(i);
                    if (pagamento.getNomeFornecedor().equalsIgnoreCase(nomeFornecedor)) {
                        ListaCompraNota.remove(pagamento);
                    }
                }
                ArquivaPagamento(ListaPagamentos);

            }

            //metodo para aquiva pagamentos
            public void ArquivaPagamento(ArrayList<PAGAMENTO_NOTA> ListaPagamentos) {
                try {
                    ARQUIVO_PAGAMENTO_NOTA.Armazena(ListaPagamentos);
                } catch (IOException ex) {
                    Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            //METODO PARA ARMAZENAR DINHEIRO PARA PAGAMENTO DE NOTA
            public class AcaoBotoes implements ActionListener, KeyListener {

                @Override
                public void actionPerformed(ActionEvent e) {

                    ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
                    ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();
                    ListaProdutoComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();//retorna todos meus produtos comprados
                    ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
                    JOptionPane.showMessageDialog(null, "Chegando na classe salvar");

                    if (e.getSource() == Botao_salvar) {
                        if (JtdatPagamento.getText().isEmpty() || JtdiaPagamento.getText().isEmpty() || JtvalorArmazenamento.getText().isEmpty()) {

                            JtdiaPagamento.requestFocus();
                        } else {

                            for (int i = 0; i < ListaCompraNota.size(); i++) {

                                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

                                if (compra_nota.getNomeFornecedor().equalsIgnoreCase(JlguardaNomeFornecedor.getText())) {

                                    if (Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")) >= compra_nota.getValorDivida()) {

                                        for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {

                                            produtoCompra.setNome(compra_nota.getProdutoEstoque().get(j).getNomeProduto().replace("[", "").replace(",", "").trim());
                                            produtoCompra.setQuantidade(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                                            produtoCompra.setCodigo(compra_nota.getProdutoEstoque().get(j).getCodigoProduto());
                                            produtoCompra.setPrecoCompraTotal(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                                            produtoCompra.setPrecoCompraUnidade(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                                            produtoCompra.setDia(datas.retornaData());
                                            produtoCompra.setMes(datas.retornaMes());
                                            produtoCompra.setAno(datas.retornaAno());
                                            ListaProdutoComprados.add(produtoCompra);

                                        }

                                        ListaCompraNota.remove(compra_nota);

                                        if (ListaPagamentos != null) {
                                            EliminaPagamentosConcluidos(compra_nota.getNomeFornecedor());

                                        }
                                    } else {
                                        ListaPagamentos.add(RetornaPagamento(compra_nota.getNomeFornecedor()));
                                        compra_nota.setValorDivida(compra_nota.getValorDivida() - Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")));

                                    }

                                }

                            }

                            ArquivaPagamento(ListaPagamentos);

                            try {
                                ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
                            } catch (IOException ex) {
                                Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                
                                ARQUIVO_ESTOQUE_COMPRA.Armazena(ListaProdutoComprados);
                            } catch (IOException ex) {
                                Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            P_CompraNotasVisualizar.criaTabela();
                            PFonecedorProduto.this.dispose();
                        }

                    }
                    if (e.getSource() == Botao_salvarArmazenamento) {
                         JOptionPane.showMessageDialog(null, "Chegando na classe salvar");
                        if (JtdataArmazenamento.getText().isEmpty() || JtdiaArmazenamento.getText().isEmpty() || JtdinheiroDia.getText().isEmpty()) {
                            JOptionPane.showMessageDialog(null, "Existe campos que não foram Preenchidos");
                            JtdiaPagamento.requestFocus();
                        } else {

                            armazenarPagamento.setFormecedor(JlguardaNomeFornecedor.getText());
                            armazenarPagamento.setDiaArmazenamento(JtdiaArmazenamento.getText());
                            armazenarPagamento.setDataArmazenamento(JtdataArmazenamento.getText());
                            armazenarPagamento.setValorArmazenado(Float.parseFloat(JtvalorArmazenamento.getText().replace(",", ".")));
                            armazenarPagamento.setObcervacao(Jtobservacao.getText());

                            ListaarmazenarPagamento.add(armazenarPagamento);

                            try {
                                ARQUIVO_ARMAZENAR_COMPRA_NOTA.Armazena(ListaarmazenarPagamento);
                            } catch (IOException ex) {
                                Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                    }
                }

                @Override
                public void keyTyped(KeyEvent e) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            }
        }

        public MaskFormatter Mascara(String Mascara) {

            MaskFormatter F_Mascara = new MaskFormatter();
            try {
                F_Mascara.setMask(Mascara); //Atribui a mascara  
                F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
            } catch (Exception excecao) {
                excecao.printStackTrace();
            }
            return F_Mascara;
        }
    }

    /*
     * METODO QUE RETORNA UMA ARRAYLIST DE STRING CONTENDO OS PRODUTOS PARA UM
     * DETERMINADO FORNECEDOR DE ACORCO COM O PARAMETRO DE ENTRADA
     */
    public ArrayList<String[]> PegaprodutosParaCadaFornecedor(String nomeFornecedor) {
        ListaCompraNota = arquivoNotas.retorna();
        ArrayList<String[]> produtosAgardandoPagamendo = new ArrayList<String[]>();//cria um arraylist para ser o retornar os produtos de cada cliente
        String[] produtos;
        String quantidadeProdutoS, precoCompra_unidadeS, precoVenda_unidadeS, precoCompra_GeralS, LucroUnidadeS, LucroGeralS;

        for (int i = 0; i < ListaCompraNota.size(); i++) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

            if (nomeFornecedor.equalsIgnoreCase(compra_nota.getNomeFornecedor())) {

                for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                    nomeProduto = compra_nota.getProdutoEstoque().get(j).nomeProduto.replace("[", "").replace(",", "").trim();
                    codigoProduto = compra_nota.getProdutoEstoque().get(j).codigoProduto;
                    quantidadeProdutoS = String.valueOf(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                    precoCompra_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                    precoVenda_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoVenda_unidade());
                    precoCompra_GeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                    LucroUnidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroUnidade());
                    LucroGeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroGeral());

                    produtos = new String[]{quantidadeProdutoS, codigoProduto, nomeProduto, precoCompra_unidadeS, precoCompra_GeralS, precoVenda_unidadeS, LucroUnidadeS, LucroGeralS};

                    produtosAgardandoPagamendo.add(produtos);

                }

            }
        }

        return produtosAgardandoPagamendo;
    }

    /*
     * METODO QUE CRIA UMA TABLEA DE PRODUTOS PARA A VISUALIZAÇÃO NO PAINEL
     * FONECEDOR PRODUTO
     */
    public void TabelaProdutos(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Quant prod.", "Código", "Nome Prod.", "Compra uni.", "Compra Ger.", "Venda uni.", "Lucro uni.", "Lucro Ger."};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(30);

        TableColumn coluna = null;

        add(jtable);

        JSprodutos = new JScrollPane(jtable);
        JSprodutos.setLocation(20, 50);
        JSprodutos.setSize(400, 400);

    }

    public String retornarDiaSemana(int ano, int mes, int dia) {

        Calendar calendario = new GregorianCalendar(ano, mes, dia);
        int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);

        return pesquisarDiaSemana(diaSemana);
    }

    //faz a pesquisa, dado um inteiro de 1 a 7  
    public String pesquisarDiaSemana(int _diaSemana) {
        String diaSemana = null;

        switch (_diaSemana) {

            case 1: {
                diaSemana = "Domingo";
                break;
            }
            case 2: {
                diaSemana = "Segunda";
                break;
            }
            case 3: {
                diaSemana = "Terça";
                break;
            }
            case 4: {
                diaSemana = "Quarta";
                break;
            }
            case 5: {
                diaSemana = "Quinta";
                break;
            }
            case 6: {
                diaSemana = "Sexta";
                break;
            }
            case 7: {
                diaSemana = "Sábado";
                break;
            }

        }
        return diaSemana;

    }

    /*EU SEI QUE ISSO ÉSTÁ MUITO ERRADO NÃO É ORIENTAÇÃO A OBJETO MAS NAO TEM COMO 
     PORQUE COMECEI ERRADO*/
    public final class Frame_Movimentacao extends JFrame {

        Container container;

        Painel_Geral Painel_Geral = new Painel_Geral();
        Painel_Topo Painel_Topo;
        Painel_Visualizar_Produtos Painel_Visualizar_Produtos = new Painel_Visualizar_Produtos();
        Painel_EfetuarPagamento Painel_EfetuarPagamento;
        Painel_Visualizar_Pagamentos Painel_Visualizar_Pagamentos = new Painel_Visualizar_Pagamentos();
        Painel_EfetuarArmazenamento Painel_EfetuarArmazenamento;
        Painel_visualizar_Armazenamentos Painel_visualizar_Armazenamentos = new Painel_visualizar_Armazenamentos();
        TableModel TabelaModelo;

        //faz as operacoes de pegar produtos, valores, forncecedores
        Calculos_ExibePagamentosCompraNota Calculos = new Calculos_ExibePagamentosCompraNota();

        public Frame_Movimentacao(String nomeFornecedor, String dataPagamento, String dividaAtual) {

            Painel_Topo = new Painel_Topo(nomeFornecedor, dataPagamento, dividaAtual);
            Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
            Painel_EfetuarArmazenamento = new Painel_EfetuarArmazenamento(nomeFornecedor);
            container = getContentPane();
            this.setLayout(null);
            this.setSize(800, 300);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(180, 380);

            //this.container.add(Painel_Geral);
            this.container.add(Painel_Topo);
            this.container.add(Painel_Visualizar_Produtos);
            this.container.add(Painel_EfetuarPagamento);
            this.container.add(Painel_Visualizar_Pagamentos);
            this.container.add(Painel_EfetuarArmazenamento);
            this.container.add(Painel_visualizar_Armazenamentos);
            DeixaTodosPainelFalso();
        }

        public void FechaFrame() {
            this.dispose();
        }

        public void DeixaTodosPainelFalso() {
            Painel_EfetuarPagamento.setVisible(false);
            Painel_Visualizar_Produtos.setVisible(false);
            Painel_Visualizar_Pagamentos.setVisible(false);
            Painel_EfetuarArmazenamento.setVisible(false);
            Painel_visualizar_Armazenamentos.setVisible(false);
        }

        public void DeixaPainelVerdadeiro() {
            Painel_EfetuarPagamento.setVisible(true);
        }

        public class Painel_Movimentacao extends JPanel {

            public Painel_Movimentacao() {
                this.setLayout(null);
                this.setSize(800, 200); // larg, alt
                this.setLocation(0, 90);
                this.setBackground(Color.YELLOW);

            }

        }

        public final class Painel_Topo extends JPanel {
            /*Componentes Graficos do Painel*/

            JLabel JlnomeForn = new JLabel();
            JLabel JlvalorDividaInicial = new JLabel();
            JLabel JlvalorDividaAtual = new JLabel();
            JLabel JlvalorPagamentosArmazenados = new JLabel();
            JLabel JlvalorPagamentoEfetuados = new JLabel();
            JLabel JldataLimitePagamento = new JLabel();
            JLabel JlquantidadeDiasRestantes = new JLabel();
            JLabel JltotalPagamentosArmazenados = new JLabel();
            JComboBox<String> JCopcoes;
            String opcoes[];

            /*Classes usadas para pegar as listas de armazenamentos e pagamentos*/
            ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
            ArrayList<ARMAZENAR_COMPRA_NOTA> ListaArmazenamentos = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();

            /*Classe que faz os calculos de total divida, quantidade de dias entre outros*/
            Calculos_ExibePagamentosCompraNota Calculos = new Calculos_ExibePagamentosCompraNota();

            /*PARA PASSAR O NOME DO FORNECEDOR PARA OUTRAS CLASSES*/
            String NomeFornecedor;
            String Divida_atual;

            //Frame_Movimentacao frame_Movimentacao = new Frame_Movimentacao();
            Painel_EfetuarPagamento Painel_EfetuarPagamento;

            public Painel_Topo(String nomeFornecedor, String dataPagamento, String dividaAtual) {
                this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};

                this.setLayout(null);
                this.setSize(800, 100); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.BLUE);

                Componentes(nomeFornecedor, dataPagamento, dividaAtual);
                Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
                NomeFornecedor = nomeFornecedor;
                Divida_atual = dividaAtual;
            }

            Painel_Topo() {
                this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};
            }

            public String RetornaNomeFornecedor() {
                return NomeFornecedor;
            }

            public void Componentes(String nomeFornecedor, String dataPagamento, String dividaAtual) {

                Aux_totalpago = Calculos.PagaValorTotalPagamentosEfetuados(nomeFornecedor, ListaPagamentos);
             
                
                JlnomeForn.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlnomeForn.setText(nomeFornecedor);
                JlnomeForn.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JlnomeForn.setBackground(Color.BLUE);
                JlnomeForn.setLocation(20, 20);//
                add(JlnomeForn);

                float Divida_Inicial = (Converte.StringEmFloat(dividaAtual) + Aux_totalpago);
                JlvalorDividaInicial.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorDividaInicial.setText("Divida Inicial R$ " + Converte.FloatEmString(Divida_Inicial));
                JlvalorDividaInicial.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorDividaInicial.setLocation(300, 20);//
                add(JlvalorDividaInicial);

                JlvalorDividaAtual.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorDividaAtual.setText("Divida Atual R$" + dividaAtual);
                JlvalorDividaAtual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorDividaAtual.setLocation(20, 40);//
                add(JlvalorDividaAtual);

                JlvalorPagamentoEfetuados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorPagamentoEfetuados.setText("Total Pago R$" + Aux_totalpago);
                JlvalorPagamentoEfetuados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorPagamentoEfetuados.setLocation(300, 40);//
                JlvalorPagamentoEfetuados.setBackground(Color.red);
                add(JlvalorPagamentoEfetuados);

                JldataLimitePagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataLimitePagamento.setText("Data Limite Pagamento: " + dataPagamento);
                JldataLimitePagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataLimitePagamento.setLocation(20, 60);//
                add(JldataLimitePagamento);

                JltotalPagamentosArmazenados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JltotalPagamentosArmazenados.setText("Total Armazenado R$ " + Aux_totalArmazenado);
                JltotalPagamentosArmazenados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JltotalPagamentosArmazenados.setLocation(300, 60);//
                add(JltotalPagamentosArmazenados);

                JlquantidadeDiasRestantes.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                //JlquantidadeDiasRestantes.setText(String.valueOf("Qtd Dias Restantes " + RetornaQuantidadeDiasRestante(nome)));
                JlquantidadeDiasRestantes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlquantidadeDiasRestantes.setLocation(250, 60);//
                add(JlquantidadeDiasRestantes);

                JCopcoes = new JComboBox(opcoes);
                JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JCopcoes.setSize(200, 20);
                JCopcoes.setLocation(500, 25);
                JCopcoes.addItemListener(
                        new ItemListener() {

                            @Override
                            public void itemStateChanged(ItemEvent e) {
                                if (e.getStateChange() == ItemEvent.SELECTED) {

                                    //seleciona a opção visualizar produtos comprados
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {
                                        DeixaTodosPainelFalso();
                                        Painel_Visualizar_Produtos.setVisible(true);
                                        Painel_Visualizar_Produtos.CriaTabelaProdutos(Calculos.PegaprodutosParaCadaFornecedor(NomeFornecedor));
                                    }

                                    //seleciona a opcao efetuar um pagamento
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Pagamento")) {
                                        DeixaTodosPainelFalso();
                                        Painel_EfetuarPagamento.setVisible(true);

                                        DeixaPainelVerdadeiro();
                                    }
                                    //ARMAZENA UM PAGAMENTO PARA UMA NOTA
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Armazenamento")) {
                                        DeixaTodosPainelFalso();
                                        Painel_EfetuarArmazenamento.setVisible(true);
                                        Painel_EfetuarArmazenamento.Componentes();

                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Armazenamento")) {
                                        //ArmazenarPagamento(nome);
                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Armazenamento")) {
                                        DeixaTodosPainelFalso();
                                        Painel_visualizar_Armazenamentos.setVisible(true);
                                        Painel_visualizar_Armazenamentos.CriaTabelaVizualizarArmazenamentos(Calculos.PegaArmazenamentoEfetuados(NomeFornecedor));

                                    }
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Divida")) {

                                    }
                                    //seleciona a opcao para visualizar os pagamentos
                                    if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Pagamento")) {
                                        DeixaTodosPainelFalso();
                                        Painel_Visualizar_Pagamentos.setVisible(true);
                                        Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos.pegaPagamentoEfetuados(NomeFornecedor));
                                        // Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos_ExibePagamentosCompraNota.PegaTodosPagamentosExistentesnoArquivo());

                                    }

                                }
                            }
                        });
                add(JCopcoes);

            }

        }
        /*ESSA CLASSE ESTÁ AQUI PROVISORIAMENTE ATE QUENDO EU APREDER A FECHAR O FRAME 
         DE OUTRA CLASSE*/

        public final class Painel_EfetuarPagamento extends JPanel {

            private final Datas Datas = new Datas();
            String dia = Datas.retornaData();
            Mascara_Data Mascara_Data = new Mascara_Data();

            JLabel JldiaPagamento = new JLabel();
            JLabel JldataPagamento = new JLabel();
            JLabel JlvalorPagamento = new JLabel();
            JLabel JlquemRecebeu = new JLabel();

            JTextField JtdiaPagamento = new JTextField();
            private final String opcoes[] = {"Segunda-feira", "Terca-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira ", "Sabado", "Domingo"};
            JFormattedTextField JtdatPagamento = new JFormattedTextField(Mascara_Data.Mascara("##/##/####"));
            JNumberFormatField JtvalorPagamento = new JNumberFormatField(new DecimalFormat("0.00"));
            JTextField JtquemRecebeu = new JTextField();
            JButton Botao_salvar, Botao_cancelar;
            AcaoBotaoSalvar AcaoBotaoSalvar = new AcaoBotaoSalvar();
            ArrayList<COMPRA_NOTA> ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
           
            ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
            PAGAMENTO_NOTA PAGAMENTO_NOTA = new PAGAMENTO_NOTA();
            String Aux_nomeFornecedor;
           
            ARQUIVO_ESTOQUE_COMPRA ArquivoProdutoCompra = new ARQUIVO_ESTOQUE_COMPRA();
           
            Converte Converte = new Converte();
            AcaoFocoData AcaoFocoData = new AcaoFocoData();

            public Painel_EfetuarPagamento(String NomeFornecedor) {

                this.setLayout(null);
                this.setSize(800, 200); // larg, alt
                this.setLocation(0, 90);
                this.setBackground(Color.WHITE);

                Aux_nomeFornecedor = NomeFornecedor;
                Componentes();

            }

            public void Componentes() {

                JldiaPagamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
                JldiaPagamento.setText("Dia Pagamento");
                JldiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldiaPagamento.setLocation(20, 20);
                add(JldiaPagamento);

                JtdiaPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdiaPagamento.setSize(200, 25);
                JtdiaPagamento.setLocation(20, 50);
                JtdiaPagamento.setText(Datas.CapturaDiaDaSemana(dia));
                JtdiaPagamento.setEditable(false);
                add(JtdiaPagamento);

                JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataPagamento.setText("Data pagamento");
                JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataPagamento.setLocation(260, 20);
                add(JldataPagamento);

                JtdatPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtdatPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtdatPagamento.setSize(200, 25);
                JtdatPagamento.setLocation(260, 50);
                JtdatPagamento.setText(dia);
                JtdatPagamento.addFocusListener(AcaoFocoData);
                add(JtdatPagamento);

                JlvalorPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorPagamento.setText("Valor pagamento");
                JlvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorPagamento.setLocation(500, 20);
                add(JlvalorPagamento);

                JtvalorPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtvalorPagamento.setSize(200, 25);
                JtvalorPagamento.setLocation(500, 50);
                add(JtvalorPagamento);

                JlquemRecebeu.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlquemRecebeu.setText("Quem Recebeu Pagamento");
                JlquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlquemRecebeu.setLocation(20, 90);
                add(JlquemRecebeu);

                JtquemRecebeu.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
                JtquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JtquemRecebeu.setSize(400, 25);
                JtquemRecebeu.setLocation(20, 120);
                add(JtquemRecebeu);

                //**Jbutton Salvar**//
                Botao_salvar = new JButton();
                Botao_salvar.setText("SALVAR");
                Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_salvar.setSize(90, 25);
                Botao_salvar.addActionListener(AcaoBotaoSalvar);
                Botao_salvar.addKeyListener(AcaoBotaoSalvar);
                Botao_salvar.setLocation(500, 120);
                add(Botao_salvar);

                Botao_cancelar = new JButton();
                Botao_cancelar.setText("CANCELAR");
                Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_cancelar.setSize(100, 25);
                //Botao_cancelar.addActionListener(this);
                Botao_cancelar.setLocation(600, 120);
                add(Botao_cancelar);

            }

            public class AcaoFocoData implements FocusListener {

                @Override
                public void focusGained(FocusEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void focusLost(FocusEvent e) {
                    JtdiaPagamento.setText(Datas.CapturaDiaDaSemana(JtdatPagamento.getText()));
                    JtvalorPagamento.requestFocus();
                }

            }

            public PAGAMENTO_NOTA CapturaDadosDigitados() {
                PAGAMENTO_NOTA.setFornecedor(Aux_nomeFornecedor);
                PAGAMENTO_NOTA.setDiaRecebeu(JtdiaPagamento.getText());
                PAGAMENTO_NOTA.setDataRecebeu(JtdatPagamento.getText());
                PAGAMENTO_NOTA.setNomeRecebeu(JtquemRecebeu.getText());
                PAGAMENTO_NOTA.setValorRecebeu(Converte.StringEmFloat(JtvalorPagamento.getText()));

                return PAGAMENTO_NOTA;

            }

            public float AtualizaNovoValorDivida() {
                float ValorDivida = compra_nota.getValorDivida();
                float ValorPago = Converte.StringEmFloat(JtvalorPagamento.getText());

                compra_nota.setValorDivida(ValorDivida - ValorPago);

                return (ValorDivida - ValorPago);
            }

            public float retornaDividaAtual() {
                float ValorDivida = compra_nota.getValorDivida();
                return ValorDivida;
            }

            public void ArquivarPagamentos(ArrayList<PAGAMENTO_NOTA> ListaPagamentos) throws IOException {
                ARQUIVO_PAGAMENTO_NOTA.Armazena(ListaPagamentos);
            }

            public void ArquivarCompraNota(ArrayList<COMPRA_NOTA> ListaCompraNota) throws IOException {

                ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);

            }
            
            public void ArquivarProdutosComprados(ArrayList<PRODUTO_COMPRA> ListaProdutosComprados) throws IOException {

                ARQUIVO_ESTOQUE_COMPRA.Armazena(ListaProdutosComprados);

            }

            public class AcaoBotaoSalvar implements ActionListener, KeyListener {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (e.getSource() == Botao_salvar) {
                        
                         
                        
                          COMPRA_NOTA compra_nota = new COMPRA_NOTA(); 
                          ArrayList<PRODUTO_COMPRA> ListaProdutoComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();
                         
                        if (JtdatPagamento.getText().isEmpty() || JlvalorPagamento.getText().isEmpty()) {
                        
                        } else {
                            
                            for (int i = 0; i < ListaCompraNota.size(); i++) {
                                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
                                if (compra_nota.getNomeFornecedor().equalsIgnoreCase(Aux_nomeFornecedor)) {
                                    if (Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")) >= compra_nota.getValorDivida()) {
                                        for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                                           PRODUTO_COMPRA produtoCompr = new PRODUTO_COMPRA();
                                            produtoCompr.setNome(compra_nota.getProdutoEstoque().get(j).getNomeProduto().replace("[", "").replace(",", "").trim());
                                            produtoCompr.setQuantidade(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                                            produtoCompr.setCodigo(compra_nota.getProdutoEstoque().get(j).getCodigoProduto());
                                            produtoCompr.setPrecoCompraTotal(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                                            produtoCompr.setPrecoCompraUnidade(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                                            produtoCompr.setDia(JtdatPagamento.getText());
                                            produtoCompr.setMes(Datas.retornaMes());
                                            produtoCompr.setAno(Datas.retornaAno());                                         
                                            ListaProdutoComprados.add(produtoCompr);
                                        }

                                        ListaCompraNota.remove(compra_nota);

                                    } else {
                                        compra_nota.setValorDivida(compra_nota.getValorDivida() - Converte.StringEmFloat(JtvalorPagamento.getText()));
                                        Aux_totalpago = Calculos.PagaValorTotalPagamentosEfetuados(Aux_nomeFornecedor, ListaPagamentos);
                                        float Divida_Inicial = (retornaDividaAtual() + Aux_totalpago);

                                        try {
                                            Comprovante_compraNota Comprovante_compraNota = new Comprovante_compraNota();
                                            Comprovante_compraNota.comprovante(Aux_nomeFornecedor, datas.retornaData_format(), JtdiaPagamento.getText(), JtvalorPagamento.getText(), JtquemRecebeu.getText(), Converte.FloatEmString(Divida_Inicial), Converte.FloatEmString(AtualizaNovoValorDivida()));
                                        } catch (DocumentException ex) {
                                            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (IOException ex) {
                                            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        
                                        ListaPagamentos.add(CapturaDadosDigitados());
                                       
                                       
                                    }
                                }
                            }
                            try {
                               
                                ArquivarPagamentos(ListaPagamentos);
                            } catch (IOException ex) {
                                Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                ArquivarCompraNota(ListaCompraNota);
                            } catch (IOException ex) {
                                Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                ArquivarProdutosComprados(ListaProdutoComprados);
                            } catch (IOException ex) {
                                Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                        }
                        FechaFrame();

                        criaTabela();
                        Verifica = false;
                    }

                }

                @Override
                public void keyTyped(KeyEvent e) {
                }

                @Override
                public void keyPressed(KeyEvent e) {
                }

                @Override
                public void keyReleased(KeyEvent e) {
                }

            }
        }

    }

}
