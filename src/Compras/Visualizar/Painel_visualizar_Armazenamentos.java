/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author carlos
 */
public class Painel_visualizar_Armazenamentos extends JPanel {
    TabelaModel modelo;
    JScrollPane JSarmazena;
    
    public Painel_visualizar_Armazenamentos() {
        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);

    }

    public void CriaTabelaVizualizarArmazenamentos(ArrayList<String[]>ListaArmazenamentos) {

        String[] colunas = new String[]{"Fornecedor", "Dia Arm.", "Data Arm", "Obs", "Valor Arm."};
        modelo = new TabelaModel(ListaArmazenamentos, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(100);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(50);

        JSarmazena = new JScrollPane(jtable);
        JSarmazena.setLocation(10, 10);
        JSarmazena.getVerticalScrollBar();
        JSarmazena.setSize(700, 150);
        JSarmazena.revalidate();
        add(JSarmazena);

    }

}
