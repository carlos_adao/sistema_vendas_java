/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import Auxiliares.Converte;
import Auxiliares.Mascara_Data;
import BancoDados.ARQUIVO_ARMAZENAR_COMPRA_NOTA;
import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Cadastro.Datas;
import Compras.ARMAZENAR_COMPRA_NOTA;
import Compras.COMPRA_NOTA;
import Compras.PAGAMENTO_NOTA;
import Interface_Alternavel.JNumberFormatField;
import Interface_Alternavel.PRODUTO_COMPRA;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

/**
 *
 * @author carlos
 */
public final class Frame_Movimentacao extends JFrame {

    Container container;

    Painel_Geral Painel_Geral = new Painel_Geral();
    Painel_Topo Painel_Topo;
    Painel_Visualizar_Produtos Painel_Visualizar_Produtos = new Painel_Visualizar_Produtos();
    Painel_EfetuarPagamento Painel_EfetuarPagamento;
    Painel_Visualizar_Pagamentos Painel_Visualizar_Pagamentos = new Painel_Visualizar_Pagamentos();
    Painel_EfetuarArmazenamento Painel_EfetuarArmazenamento;
    Painel_visualizar_Armazenamentos Painel_visualizar_Armazenamentos = new Painel_visualizar_Armazenamentos();
    TableModel TabelaModelo;

    //faz as operacoes de pegar produtos, valores, forncecedores
    Calculos_ExibePagamentosCompraNota Calculos = new Calculos_ExibePagamentosCompraNota();

    public Frame_Movimentacao(String nomeFornecedor, String dataPagamento, String dividaAtual) {

        Painel_Topo = new Painel_Topo(nomeFornecedor, dataPagamento, dividaAtual);
        Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
        Painel_EfetuarArmazenamento = new Painel_EfetuarArmazenamento(nomeFornecedor);
        container = getContentPane();
        this.setLayout(null);
        this.setSize(800, 300);
        this.setVisible(true);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(180, 380);

        //this.container.add(Painel_Geral);
        this.container.add(Painel_Topo);
        this.container.add(Painel_Visualizar_Produtos);
        this.container.add(Painel_EfetuarPagamento);
        this.container.add(Painel_Visualizar_Pagamentos);
        this.container.add(Painel_EfetuarArmazenamento);
        this.container.add(Painel_visualizar_Armazenamentos);
        DeixaTodosPainelFalso();
    }
    public void FechaFrame(){
      this.dispose();
    }

    public void DeixaTodosPainelFalso() {
        Painel_EfetuarPagamento.setVisible(false);
        Painel_Visualizar_Produtos.setVisible(false);
        Painel_Visualizar_Pagamentos.setVisible(false);
        Painel_EfetuarArmazenamento.setVisible(false);
        Painel_visualizar_Armazenamentos.setVisible(false);
    }

   

    public class Painel_Movimentacao extends JPanel {

        public Painel_Movimentacao() {
            this.setLayout(null);
            this.setSize(800, 200); // larg, alt
            this.setLocation(0, 90);
            this.setBackground(Color.YELLOW);

        }

    }

    public final class Painel_Topo extends JPanel {
        /*Componentes Graficos do Painel*/

        JLabel JlnomeForn = new JLabel();
         JLabel JlvalorDividaInicial = new JLabel();
        JLabel JlvalorDividaAtual = new JLabel();
        JLabel JlvalorPagamentosArmazenados = new JLabel();
        JLabel JlvalorPagamentoEfetuados = new JLabel();
        JLabel JldataLimitePagamento = new JLabel();
        JLabel JlquantidadeDiasRestantes = new JLabel();
        JLabel JltotalPagamentosArmazenados = new JLabel();
        JComboBox<String> JCopcoes;
        String opcoes[];

        /*Classes usadas para pegar as listas de armazenamentos e pagamentos*/
        ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
        ArrayList<ARMAZENAR_COMPRA_NOTA> ListaArmazenamentos = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();

        /*Classe que faz os calculos de total divida, quantidade de dias entre outros*/
        Calculos_ExibePagamentosCompraNota Calculos = new Calculos_ExibePagamentosCompraNota();

        /*PARA PASSAR O NOME DO FORNECEDOR PARA OUTRAS CLASSES*/
        String NomeFornecedor;

        //Frame_Movimentacao frame_Movimentacao = new Frame_Movimentacao();
        Painel_EfetuarPagamento Painel_EfetuarPagamento;

        public Painel_Topo(String nomeFornecedor, String dataPagamento, String dividaAtual) {
            this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};

            this.setLayout(null);
            this.setSize(800, 100); // larg, alt
            this.setLocation(0, 0);
            this.setBackground(Color.BLUE);

            Componentes(nomeFornecedor, dataPagamento, dividaAtual);
            Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
            NomeFornecedor = nomeFornecedor;

        }

        Painel_Topo() {
            this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};
        }

        public String RetornaNomeFornecedor() {
            return NomeFornecedor;
        }

        public void Componentes(String nomeFornecedor, String dataPagamento, String dividaAtual) {

            float Aux_totalpago = Calculos.PagaValorTotalPagamentosEfetuados(nomeFornecedor, ListaPagamentos);
            float Aux_totalArmazenado = Calculos.PagaValorTotalArmazenadoEfetuados(nomeFornecedor, ListaArmazenamentos);

            JlnomeForn.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlnomeForn.setText(nomeFornecedor);
            JlnomeForn.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
            JlnomeForn.setBackground(Color.BLUE);
            JlnomeForn.setLocation(20, 20);//
            add(JlnomeForn);
            
            
            JlvalorDividaInicial.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlvalorDividaInicial.setText("Divida Inicial R$ " + (dividaAtual + Aux_totalpago));
            JlvalorDividaInicial.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlvalorDividaInicial.setLocation(300, 20);//
            add(JlvalorDividaInicial);

            JlvalorDividaAtual.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlvalorDividaAtual.setText("Divida Atual R$" + dividaAtual);
            JlvalorDividaAtual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlvalorDividaAtual.setLocation(20, 40);//
            add(JlvalorDividaAtual);

            JlvalorPagamentoEfetuados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlvalorPagamentoEfetuados.setText("Total Pago R$" + Aux_totalpago);
            JlvalorPagamentoEfetuados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlvalorPagamentoEfetuados.setLocation(300, 40);//
            JlvalorPagamentoEfetuados.setBackground(Color.red);
            add(JlvalorPagamentoEfetuados);

            JldataLimitePagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JldataLimitePagamento.setText("Data Limite Pagamento: " + dataPagamento);
            JldataLimitePagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JldataLimitePagamento.setLocation(20, 60);//
            add(JldataLimitePagamento);

            JltotalPagamentosArmazenados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JltotalPagamentosArmazenados.setText("Total Armazenado R$ " + Aux_totalArmazenado);
            JltotalPagamentosArmazenados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JltotalPagamentosArmazenados.setLocation(300, 60);//
            add(JltotalPagamentosArmazenados);

            JlquantidadeDiasRestantes.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            //JlquantidadeDiasRestantes.setText(String.valueOf("Qtd Dias Restantes " + RetornaQuantidadeDiasRestante(nome)));
            JlquantidadeDiasRestantes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlquantidadeDiasRestantes.setLocation(250, 60);//
            add(JlquantidadeDiasRestantes);

            JCopcoes = new JComboBox(opcoes);
            JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
            JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JCopcoes.setSize(200, 20);
            JCopcoes.setLocation(500, 25);
            JCopcoes.addItemListener(
                    new ItemListener() {

                        @Override
                        public void itemStateChanged(ItemEvent e) {
                            if (e.getStateChange() == ItemEvent.SELECTED) {

                                //seleciona a opção visualizar produtos comprados
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {
                                    DeixaTodosPainelFalso();
                                    Painel_Visualizar_Produtos.setVisible(true);
                                    Painel_Visualizar_Produtos.CriaTabelaProdutos(Calculos.PegaprodutosParaCadaFornecedor(NomeFornecedor));
                                }

                                //seleciona a opcao efetuar um pagamento
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Pagamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_EfetuarPagamento.setVisible(true);
                                     JOptionPane.showMessageDialog(null, "Efetuar pagamento");

                                }
                                //ARMAZENA UM PAGAMENTO PARA UMA NOTA
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Armazenamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_EfetuarArmazenamento.setVisible(true);
                                    Painel_EfetuarArmazenamento.Componentes();

                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Armazenamento")) {
                                    //ArmazenarPagamento(nome);
                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Armazenamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_visualizar_Armazenamentos.setVisible(true);
                                    Painel_visualizar_Armazenamentos.CriaTabelaVizualizarArmazenamentos(Calculos.PegaArmazenamentoEfetuados(NomeFornecedor));

                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Divida")) {

                                }
                                //seleciona a opcao para visualizar os pagamentos
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Pagamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_Visualizar_Pagamentos.setVisible(true);
                                    Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos.pegaPagamentoEfetuados(NomeFornecedor));
                                   // Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos_ExibePagamentosCompraNota.PegaTodosPagamentosExistentesnoArquivo());

                                }

                            }
                        }
                    });
            add(JCopcoes);

        }

    }
    /*ESSA CLASSE ESTÁ AQUI PROVISORIAMENTE ATE QUENDO EU APREDER A FECHAR O FRAME 
     DE OUTRA CLASSE*/
    
    public final class Painel_EfetuarPagamento extends JPanel {
    /*Manipulacao de mascara de data*/

    Mascara_Data Mascara_Data = new Mascara_Data();

    /*Componentes graficos Label */
    JLabel JldiaPagamento = new JLabel();
    JLabel JldataPagamento = new JLabel();
    JLabel JlvalorPagamento = new JLabel();
    JLabel JlquemRecebeu = new JLabel();

    /*Componentes graficos Textefield */
    JComboBox<String> JCdiaPagamento;
    private final String opcoes[] = {"Segunda-feira", "Terca-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira ", "Sabado", "Domingo"};

    JFormattedTextField JtdatPagamento = new JFormattedTextField(Mascara_Data.Mascara("##/##/####"));
    JNumberFormatField JtvalorPagamento = new JNumberFormatField(new DecimalFormat("0.00"));
    JTextField JtquemRecebeu = new JTextField();

    /*Componentes graficos botoes */
    JButton Botao_salvar, Botao_cancelar;

    /*Classe que execulta as acoes dos botoes salvar e cancelar*/
    AcaoBotaoSalvar AcaoBotaoSalvar = new AcaoBotaoSalvar();

    /*Classe que instancia um produco compra nota para fazer as manipulacoes*/
    ArrayList<COMPRA_NOTA> ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
    COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    /*----------------------------------------------------*/

    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
    PAGAMENTO_NOTA PAGAMENTO_NOTA = new PAGAMENTO_NOTA();

    /*
     *Classe que instancia painel topo para pegar o nome do fornecedor
     */
    //Painel_Topo Painel_Topo = new Painel_Topo();
    String Aux_nomeFornecedor;// = Painel_Topo.RetornaNomeFornecedor();
    /*----------------------------------------------------*/

    /*
     * CLASSE COMPRA PARA AQUIVA A COMPRA NOTA QUE FOI PAGA
     */
    PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
    ARQUIVO_ESTOQUE_COMPRA ArquivoProdutoCompra = new ARQUIVO_ESTOQUE_COMPRA();
    ArrayList<PRODUTO_COMPRA> ListaProdutoComprados = new ArrayList<>();
    /*----------------------------------------------------*/

    /*
     *Classe data para trabalhar com a data do sistema
     */
     private final Datas Datas = new Datas();
     String dia = Datas.retornaData();
    /*----------------------------------------------------*/

    /*
     *Classe usada para fazer convercao de tipos diferentes
     */
    Converte Converte = new Converte();
    /*----------------------------------------------------*/

    public Painel_EfetuarPagamento(String NomeFornecedor) {

        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);

        Aux_nomeFornecedor = NomeFornecedor;
        Componentes();
       
    }

    public void Componentes() {

        JldiaPagamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
        JldiaPagamento.setText("Dia Pagamento");
        JldiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldiaPagamento.setLocation(20, 20);
        add(JldiaPagamento);

        JCdiaPagamento = new JComboBox(opcoes);
        JCdiaPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCdiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCdiaPagamento.setSize(200, 20);
        JCdiaPagamento.setLocation(20, 50);
        add(JCdiaPagamento);

        JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldataPagamento.setText("Data pagamento");
        JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldataPagamento.setLocation(260, 20);
        add(JldataPagamento);

        JtdatPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdatPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdatPagamento.setSize(200, 25);
        JtdatPagamento.setLocation(260, 50);
        JtdatPagamento.setText(dia);
        JOptionPane.showMessageDialog(null, dia);
        //add(JtdatPagamento);

        JlvalorPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorPagamento.setText("Valor pagamento");
        JlvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorPagamento.setLocation(500, 20);
        add(JlvalorPagamento);

        JtvalorPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorPagamento.setSize(200, 25);
        JtvalorPagamento.setLocation(500, 50);
        add(JtvalorPagamento);

        JlquemRecebeu.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlquemRecebeu.setText("Quem Recebeu Pagamento");
        JlquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlquemRecebeu.setLocation(20, 90);
        add(JlquemRecebeu);

        JtquemRecebeu.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtquemRecebeu.setSize(400, 25);
        JtquemRecebeu.setLocation(20, 120);
        add(JtquemRecebeu);

        //**Jbutton Salvar**//
        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.addActionListener(AcaoBotaoSalvar);
        Botao_salvar.addKeyListener(AcaoBotaoSalvar);
        Botao_salvar.setLocation(500, 120);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        //Botao_cancelar.addActionListener(this);
        Botao_cancelar.setLocation(600, 120);
        add(Botao_cancelar);

    }

    public PAGAMENTO_NOTA CapturaDadosDigitados() {
        PAGAMENTO_NOTA.setFornecedor(Aux_nomeFornecedor);
        PAGAMENTO_NOTA.setDiaRecebeu(JCdiaPagamento.getSelectedItem().toString());
        PAGAMENTO_NOTA.setDataRecebeu(JtdatPagamento.getText());
        PAGAMENTO_NOTA.setNomeRecebeu(JtquemRecebeu.getText());
        PAGAMENTO_NOTA.setValorRecebeu(Converte.StringEmFloat(JtvalorPagamento.getText()));

        return PAGAMENTO_NOTA;

    }

    public void AtualizaNovoValorDivida() {
        float ValorDivida = compra_nota.getValorDivida();
        float ValorPago = Converte.StringEmFloat(JtvalorPagamento.getText());

        compra_nota.setValorDivida(ValorDivida - ValorPago);
    }

    public void ArquivarPagamentos(ArrayList<PAGAMENTO_NOTA> ListaPagamentos) throws IOException {
        ARQUIVO_PAGAMENTO_NOTA.Armazena(ListaPagamentos);     
    }

    public void ArquivarCompraNota(ArrayList<COMPRA_NOTA> ListaCompraNota) throws IOException {
        
        ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
      
    }

    public class AcaoBotaoSalvar implements ActionListener, KeyListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {

                if (JtdatPagamento.getText().isEmpty() || JlvalorPagamento.getText().isEmpty()) {
                    JCdiaPagamento.requestFocus();
                } else {
                    for (int i = 0; i < ListaCompraNota.size(); i++) {
                        compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
                        if (compra_nota.getNomeFornecedor().equalsIgnoreCase(Aux_nomeFornecedor)) {
                            if (Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")) >= compra_nota.getValorDivida()) {
                                for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                                    produtoCompra.setNome(compra_nota.getProdutoEstoque().get(j).getNomeProduto().replace("[", "").replace(",", "").trim());
                                    produtoCompra.setQuantidade(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                                    produtoCompra.setCodigo(compra_nota.getProdutoEstoque().get(j).getCodigoProduto());
                                    produtoCompra.setPrecoCompraTotal(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                                    produtoCompra.setPrecoCompraUnidade(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                                    produtoCompra.setDia(Datas.retornaData());
                                    produtoCompra.setMes(Datas.retornaMes());
                                    produtoCompra.setAno(Datas.retornaAno());
                                    ListaProdutoComprados.add(produtoCompra);
                                }

                                ListaCompraNota.remove(compra_nota);

                            } else {
                                
                                ListaPagamentos.add(CapturaDadosDigitados());
                                AtualizaNovoValorDivida();
                            }
                        }
                    }
                    try {
                        ArquivarPagamentos(ListaPagamentos);
                    } catch (IOException ex) {
                        Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        ArquivarCompraNota(ListaCompraNota);
                    } catch (IOException ex) {
                        Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                FechaFrame();
            }

        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

    }
}


}
