/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_ESTOQUE;
import Cadastro.Datas;
import Compras.Visualizar.TabelaModel;
import Compras_Em_Nota.Compra_Nota;
import Compras_Em_Nota.Fornecedor;
import Compras_Em_Nota.Nota;
import Interface_Alternavel.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public final class P_CompraNota extends JPanel {

    boolean test = false; //sentinela usada para inpedir a multipla ação do mause na tabela  
    int n = 0; // variavel usada como sentinela na classe ação jt data 
    int ver = 0;
    float valorDivida = 0;
    boolean Verificacao = false;
    boolean cadastrado = false;//usado na ação do txt codigo
    boolean produtoCadastrado = false;
    JLabel Jlnome = new JLabel();
    JLabel Jlproduto = new JLabel();
    JTextField Jtnome = new JTextField();
    JTextField Jtproduto = new JTextField();
    JLabel Jlcodigo = new JLabel();
    JTextField Jtcodigo = new JtextFieldSomenteNumeros();
    JLabel Jlquantidade = new JLabel();
    JTextField Jtquantidade = new JtextFieldSomenteNumeros();
    JLabel JlprecoCompraTotal = new JLabel();
    JNumberFormatField JtprecoCompraTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JlprecoVendaUnidade = new JLabel();
    JNumberFormatField JtprecoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JlprecoCompraUnidade = new JLabel();
    JNumberFormatField JtprecoCompraUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JllucroUnidade = new JLabel();
    JNumberFormatField JtlucroUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JllucroTotal = new JLabel();
    JNumberFormatField JtlucroTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JldataPagamento = new JLabel();
    JFormattedTextField JtdataPagamento = new JFormattedTextField(Mascara("##/##/####"));
    //JTextField JtdataPagamento  = new JTextField();
    JLabel JlvalorDivida = new JLabel();
    JNumberFormatField JtvalorDivida = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JldinheiroEntrada = new JLabel();
    JNumberFormatField JtdinheiroEntrada = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JldinheiroDia = new JLabel();
    JNumberFormatField JtdinheiroDia = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel Jltelefone = new JLabel();
    JFormattedTextField Jttelefone = new JFormattedTextField(Mascara("(##)####-####"));
    JLabel JlvalorDividaExibir = new JLabel();
    JNumberFormatField JtvalorDividaExibir = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JlLucroExibir = new JLabel();
    JNumberFormatField JtLucroExibir = new JNumberFormatField(new DecimalFormat("0.00"));
    //JTextField Jttelefone = new JTextField();
    private JButton salvar_produto, Finaliaza_compra, nao_produto, sim_produto, cancelar_compra, voltar;
    //USANDO O AUTO-COMPLETE COMBO BOX
    private JComboBox cBox_fornecedor;
    private JComboBox cBox_produto;
    private JTextField Tf_fonecedor;
    private JTextField Tf_produto;
    private static P_CompraNota P_CompraNota = new P_CompraNota();
    private static ArrayList<COMPRA_NOTA> ListaCompraNota = new ArrayList<>();
    private static ARQUIVO_COMPRA_NOTA arquivoNotas = new ARQUIVO_COMPRA_NOTA();
    private static COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    //TecladoPara_CompraNota TecladoPara_CompraNota = new TecladoPara_CompraNota();
    private String searchFor;
    private long lap;
    int contador = 0;
    AcaoBotao acao_botao = new AcaoBotao();
    ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaprodEstoque_Aguardando = new ArrayList<>();
    PRODUTO_ESTOQUE produto_Estoque = new PRODUTO_ESTOQUE();//AUXILIAR PARA PERCORRER O ARQUIVO DE PRODUTOS NO ESTOQUE
    PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();
    PRODUTO_ESTOQUE produtoEstoqueMaiorCodigo = new PRODUTO_ESTOQUE();//para armazenar o produto esoque que tiver o maior codigo
    ARQUIVO_ESTOQUE aquivo_estoque = new ARQUIVO_ESTOQUE();
    ArrayList<PRODUTO_ESTOQUE> ListaprodutoEstoque = new ArrayList<>();
    ArrayList<PRODUTO_ESTOQUE> Listaprodutos = new ArrayList<>();
    PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO produtoEstoqueConfirmado = new PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO();//produto ja foi confirmado 
    ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutosConfimados = new ArrayList<>();//UMA LISTA DE PODUTOS CONFIRMADOS MAS FALTA SALVAR E COLOCAR NO ESTOQUE
    AcaoFoco acaoFoco = new AcaoFoco();
    AcaoFocoProduto acaoFocoProduto = new AcaoFocoProduto();
    AcaoFocoCodigo acaoFocoCodigo = new AcaoFocoCodigo();
    AcaoQuantidade acaoQuantidade = new AcaoQuantidade();
    AcaoPrecoCompraTotal acaoPrecoCompraTotal = new AcaoPrecoCompraTotal();
    AcaoFocoPrecoVendaUnidade acaoFocoPrecoVendaUnidade = new AcaoFocoPrecoVendaUnidade();
    AcaoDataEfetuaPagamento acaoDataEfetuaPagamento = new AcaoDataEfetuaPagamento();
    AcaoDinheiroEntrada acaoDinheiroEntrada = new AcaoDinheiroEntrada();
    AcaoBotaoSimP_ConfirmaProduto acaoBotaoSimP_ConfirmaProduto = new AcaoBotaoSimP_ConfirmaProduto(); // ACÃO AO PRECIONAR O BOTÃO SIM DO PAINEL COMPRA NOVO PRODUTO
    Datas data = new Datas();
    CalculaDiferencaEntreDatas calculaDiferencaEntreDatas = new CalculaDiferencaEntreDatas();
    ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();  //ISTANCIA UM ARRAY COM VARIOS PRODUTO DA CLASSE COMPRA
    //AutoCompleteCombo AutoCompleteCombo = new AutoCompleteCombo();
    /*CLASSES DE ACÃO TECLADO E FOCO*/
    Teclado teclado = new Teclado();
    TecladoFornecedor TecladoFornecedor = new TecladoFornecedor();
    TecladoProduto TecladoProduto = new TecladoProduto();
    TecladoPrecoCompraTotal TecladoPrecoCompraTotal = new TecladoPrecoCompraTotal();
    TecladoNao TecladoNao = new TecladoNao();
    DataEfetuarPagamento TecladoDataEfetuarPagamento = new DataEfetuarPagamento();
    TecladoDinheiroEntrada TecladoDinheiroEntrada = new TecladoDinheiroEntrada();
    TecladoTelefone TecladoTelefone = new TecladoTelefone();
    TecladoPrecoCompraUnidade TecladoPrecoCompraUnidade = new TecladoPrecoCompraUnidade();
    int armazena_codigo = 0;
    ARQUIVO_ESTOQUE arquivo;
    private P_confirmaProduto p_confirmaProduto; //painel usado para saber se quer cadastrar outro produto na compra em nota

    //componentes que seram usados na tabela para exibir os produtos que estão sendo comprados 
    JTable jtable = new JTable();
    TabelaModel modelo = new TabelaModel();
    JScrollPane JSprodutosComprados;
    boolean verifica = true; //faz a verificação pra checar se o painel sim já foi utilizado anteriomente
    Teclado_F1 Teclado_Enter_volta = new Teclado_F1();//usado para retorna pra compra outro produto o pra finalizar
    //----------------------------------------------------------------------------------------
    JRadioButton JrCompra_unid, JrCompra_tot;
    String CompraUnidade = "Uni";
    String CompraTotal = "Tot";
    int Compra_uni_tot = 0;// variavel ultilizada para fazer o contro do desco por unidade ou total
    float Valor_total_compra = 0;
    float Lucro_total_compra = 0;

    Converte converte = new Converte();

    public P_CompraNota() {
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

        JSprodutosComprados = new JScrollPane(jtable);

        Jlnome.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlnome.setText("Nome do fornecedor");
        Jlnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlnome.setLocation(10, 40);//
        add(Jlnome);

        componente();

        Jlproduto.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlproduto.setText("Nome Mercadoria");
        Jlproduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlproduto.setLocation(10, 110);//
        add(Jlproduto);

        cBox_produto = new AutoCompleteProduto(pegaTodosNomesProdutos());
        cBox_produto.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_produto.addKeyListener(teclado);
        cBox_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        cBox_produto.setSize(250, 25);
        cBox_produto.setLocation(10, 145);
        cBox_produto.setEditable(true);
        cBox_produto.addFocusListener(acaoFocoProduto);
        add(cBox_produto);//

        Jlcodigo.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlcodigo.setText("Código do produto");
        Jlcodigo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlcodigo.setLocation(280, 40);
        add(Jlcodigo);

        Jtcodigo.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        Jtcodigo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jtcodigo.setSize(250, 25);
        Jtcodigo.setLocation(280, 75);
        Jtcodigo.addKeyListener(teclado);
        Jtcodigo.addFocusListener(acaoFocoCodigo);
        add(Jtcodigo);//

        Jlquantidade.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlquantidade.setText("Quantidade");
        Jlquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlquantidade.setLocation(10, 180);
        add(Jlquantidade);

        Jtquantidade.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        Jtquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jtquantidade.setSize(250, 25);
        Jtquantidade.setLocation(10, 215);
        Jtquantidade.addKeyListener(teclado);
        Jtquantidade.addFocusListener(acaoQuantidade);
        add(Jtquantidade);//

        JlprecoCompraTotal.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlprecoCompraTotal.setText("Preço de compra Total");
        JlprecoCompraTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlprecoCompraTotal.setLocation(280, 110);
        add(JlprecoCompraTotal);

        JtprecoCompraTotal.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtprecoCompraTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtprecoCompraTotal.setSize(200, 25);
        JtprecoCompraTotal.setLocation(280, 145);
        JtprecoCompraTotal.addKeyListener(TecladoPrecoCompraTotal);
        JtprecoCompraTotal.addFocusListener(acaoPrecoCompraTotal);
        add(JtprecoCompraTotal);//

        RadioListener myListener = new RadioListener();
        JrCompra_tot = new JRadioButton("t");
        JrCompra_tot.setSize(21, 25);
        JrCompra_tot.setLocation(505, 145);
        JrCompra_tot.setSelected(true);
        JrCompra_tot.setMnemonic('b');
        add(JrCompra_tot);

        JlprecoVendaUnidade.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlprecoVendaUnidade.setText("Preço de Venda Unidade");
        JlprecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlprecoVendaUnidade.setLocation(10, 250);
        add(JlprecoVendaUnidade);

        JtprecoVendaUnidade.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtprecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtprecoVendaUnidade.setSize(250, 25);
        JtprecoVendaUnidade.setLocation(10, 285);
        JtprecoVendaUnidade.addKeyListener(teclado);
        add(JtprecoVendaUnidade);//

        JlprecoCompraUnidade.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlprecoCompraUnidade.setText("Preço de Compra Unidade");
        JlprecoCompraUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlprecoCompraUnidade.setLocation(280, 180);
        add(JlprecoCompraUnidade);

        JtprecoCompraUnidade.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtprecoCompraUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtprecoCompraUnidade.setSize(200, 25);
        JtprecoCompraUnidade.setLocation(280, 215);
        JtprecoCompraUnidade.addKeyListener(TecladoPrecoCompraUnidade);
        JtprecoCompraUnidade.setEditable(false);
        add(JtprecoCompraUnidade);

        JrCompra_unid = new JRadioButton("u");
        JrCompra_unid.setSize(21, 25);
        JrCompra_unid.setLocation(505, 215);
        JrCompra_unid.setMnemonic('c');
        add(JrCompra_unid);

        JllucroUnidade.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JllucroUnidade.setText("Lucro Unidade");
        JllucroUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JllucroUnidade.setLocation(10, 315);
        add(JllucroUnidade);

        JtlucroUnidade.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtlucroUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtlucroUnidade.setSize(250, 25);
        JtlucroUnidade.setLocation(10, 350);
        JtlucroUnidade.setEditable(false);
        add(JtlucroUnidade);

        JllucroTotal.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JllucroTotal.setText("Lucro Total");
        JllucroTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JllucroTotal.setLocation(280, 250);
        add(JllucroTotal);

        JtlucroTotal.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtlucroTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtlucroTotal.setSize(250, 25);
        JtlucroTotal.setLocation(280, 285);
        JtlucroTotal.addKeyListener(teclado);
        JtlucroTotal.setEditable(false);
        add(JtlucroTotal);

        JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldataPagamento.setText("Data de Efetuar pagamento");
        JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldataPagamento.setLocation(550, 40);
        add(JldataPagamento);

        JtdataPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdataPagamento.setSize(250, 25);
        JtdataPagamento.setLocation(550, 75);
        JtdataPagamento.addFocusListener(acaoDataEfetuaPagamento);
        JtdataPagamento.addKeyListener(TecladoDataEfetuarPagamento);
        JtdataPagamento.addKeyListener(teclado);
        add(JtdataPagamento);//

        JlvalorDivida.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorDivida.setText("Valor total da divida");
        JlvalorDivida.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorDivida.setLocation(550, 110);
        add(JlvalorDivida);

        JtvalorDivida.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorDivida.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorDivida.setSize(250, 25);
        JtvalorDivida.setLocation(550, 145);
        JtvalorDivida.setEditable(false);
        add(JtvalorDivida);//

        JldinheiroEntrada.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldinheiroEntrada.setText("Dinheiro de entrada");
        JldinheiroEntrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldinheiroEntrada.setLocation(550, 180);
        add(JldinheiroEntrada);

        JtdinheiroEntrada.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdinheiroEntrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdinheiroEntrada.setSize(250, 25);
        JtdinheiroEntrada.setLocation(550, 215);
        JtdinheiroEntrada.addFocusListener(acaoDinheiroEntrada);
        JtdinheiroEntrada.addKeyListener(TecladoDinheiroEntrada);
        add(JtdinheiroEntrada);//

        JldinheiroDia.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldinheiroDia.setText("Dinheiro por dia");
        JldinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldinheiroDia.setLocation(550, 250);
        add(JldinheiroDia);

        JtdinheiroDia.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdinheiroDia.setSize(250, 25);
        JtdinheiroDia.setLocation(550, 285);
        JtdinheiroDia.setEditable(false);
        add(JtdinheiroDia);

        Jltelefone.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jltelefone.setText("Telefône contato");
        Jltelefone.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jltelefone.setLocation(550, 315);
        add(Jltelefone);

        Jttelefone.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        Jttelefone.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jttelefone.setSize(250, 25);
        Jttelefone.setLocation(550, 350);
        Jttelefone.addKeyListener(TecladoTelefone);
        add(Jttelefone);

        JlvalorDividaExibir.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorDividaExibir.setText("Valor da Compra");
        JlvalorDividaExibir.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorDividaExibir.setLocation(560, 350);
        add(JlvalorDividaExibir);

        JtvalorDividaExibir.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorDividaExibir.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorDividaExibir.setSize(150, 40);
        JtvalorDividaExibir.setLocation(560, 380);
        JtvalorDividaExibir.setEditable(false);
        add(JtvalorDividaExibir);

        JlLucroExibir.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlLucroExibir.setText("Lucro da Compra");
        JlLucroExibir.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlLucroExibir.setLocation(800, 350);
        add(JlLucroExibir);

        JtLucroExibir.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtLucroExibir.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtLucroExibir.setSize(150, 40);
        JtLucroExibir.setLocation(800, 380);
        JtLucroExibir.setEditable(false);
        add(JtLucroExibir);

        Finaliaza_compra = new JButton();
        Finaliaza_compra.setText("FINALIZAR");
        Finaliaza_compra.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Finaliaza_compra.setSize(150, 25);
        Finaliaza_compra.addActionListener(acao_botao);
        Finaliaza_compra.setLocation(820, 270);
        add(Finaliaza_compra);

        cancelar_compra = new JButton();
        cancelar_compra.setText("CANCELAR");
        cancelar_compra.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        cancelar_compra.setSize(150, 25);
        cancelar_compra.addActionListener(acao_botao);
        cancelar_compra.setLocation(820, 300);
        add(cancelar_compra);

        voltar = new JButton();
        voltar.setText("VOLTAR");
        voltar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        voltar.setSize(150, 25);
        voltar.addKeyListener(Teclado_Enter_volta);
        voltar.setLocation(820, 350);
        add(voltar);

        Finaliaza_compra.setVisible(false);
        cancelar_compra.setVisible(false);
        voltar.setVisible(false);
        deixaComponentesIvisiveis();

        ButtonGroup grupoBotao = new ButtonGroup();
        grupoBotao.add(JrCompra_tot);
        grupoBotao.add(JrCompra_unid);
        JrCompra_tot.addActionListener(myListener);
        JrCompra_unid.addActionListener(myListener);

    }

    public void deixaValor_lucro_invisiveis() {
        JlLucroExibir.setVisible(false);
        JtLucroExibir.setVisible(false);
        JlvalorDividaExibir.setVisible(false);
        JtvalorDividaExibir.setVisible(false);
    }

    class RadioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            if (e.getActionCommand().equalsIgnoreCase("t")) {
                Compra_uni_tot = 0;
                JtprecoCompraUnidade.setEditable(false);
                JtprecoCompraTotal.setEditable(true);
            } else {
                JtprecoCompraTotal.setEditable(false);
                JtprecoCompraUnidade.setEditable(true);
                Compra_uni_tot = 1;
            }
        }
    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Qtd", "Cod", "Descrição", "PC uni ", "PV uni  ", "PC Tot", "L Uni", "L Tot"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        if (!test) {
            jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
            jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
            jtable.getColumnModel().getColumn(7).setPreferredWidth(20);
            jtable.addMouseListener(new MouseAdapter() {
                private int linha;
                private String opcoes[] = new String[]{"Atualizar", "Excluir"};

                @Override
                public void mouseClicked(MouseEvent e) {

                    if ((e.getClickCount() == 2)) {

                        linha = jtable.getSelectedRow();
                        String codigoProduto = String.valueOf(jtable.getValueAt(linha, 1));

                        for (int i = 0; i < ListaProdutosConfimados.size(); i++) {
                            if (ListaProdutosConfimados.get(i).getCodigoProduto().equalsIgnoreCase(codigoProduto)) {
                                // JOptionPane.showMessageDialog(null,"PRODUTO CONFIRMADO" + ListaProdutosConfimados.get(i));

                                float DividaExibirAtual = converte.StringEmFloat(JtvalorDividaExibir.getText());
                                // JOptionPane.showMessageDialog(null,"Divida exibir atual" + DividaExibirAtual);
                                float DividaProduto = ListaProdutosConfimados.get(i).getPrecoCompra_Geral();
                                // JOptionPane.showMessageDialog(null,"Divida produto" + DividaProduto);
                                float LucroExibeAtual = converte.StringEmFloat(JtLucroExibir.getText());
                                // JOptionPane.showMessageDialog(null,"Lucro exibir atual" + LucroExibeAtual);
                                float LucroProduto = ListaProdutosConfimados.get(i).getLucroGeral();
                                // JOptionPane.showMessageDialog(null,"Lucro produto" + LucroProduto);
                                JtvalorDividaExibir.setText(converte.FloatEmString(DividaExibirAtual - DividaProduto));
                                JtLucroExibir.setText(converte.FloatEmString(LucroExibeAtual - LucroProduto));
                                ListaProdutosConfimados.remove(ListaProdutosConfimados.get(i));
                                Tabela(RetornaValoresParaExibirAtabela());
                            }

                        }

                    }

                }
            });
            test = true;
        }
        JSprodutosComprados.setLocation(550, 60);
        JSprodutosComprados.getVerticalScrollBar();
        JSprodutosComprados.setSize(450, 250);
        JSprodutosComprados.revalidate();
        add(JSprodutosComprados);

    }

    public ArrayList<String[]> RetornaValoresParaExibirAtabela() {
        PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO produtoCompra;
        String quantidade, codigo, nome, precoCompraUni, precoCompraTotal, precoVendaUnidade, lucroUnidade, lucroGeral;
        String[] produtos;
        ArrayList<String[]> dados = new ArrayList<>();

        for (int i = 0; i < ListaProdutosConfimados.size(); i++) {
            produtoCompra = (PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO) ListaProdutosConfimados.get(i);

            quantidade = converte.InteiroEmString(produtoCompra.getQuantidadeProduto());
            codigo = produtoCompra.getCodigoProduto();
            nome = produtoCompra.getNomeProduto();
            precoCompraUni = converte.FloatEmString(produtoCompra.getPrecoCompra_unidade());
            precoCompraTotal = converte.FloatEmString(produtoCompra.getPrecoCompra_Geral());
            precoVendaUnidade = converte.FloatEmString(produtoCompra.getPrecoVenda_unidade());
            lucroUnidade = converte.FloatEmString(produtoCompra.getLucroUnidade());
            lucroGeral = converte.FloatEmString(produtoCompra.getLucroGeral());

            produtos = new String[]{quantidade, codigo, nome, precoCompraUni, precoVendaUnidade, precoCompraTotal, lucroUnidade, lucroGeral};

            dados.add(produtos);
        }

        return dados;
    }

    public void componente() {

        cBox_fornecedor = new AutoCompleteFornecedor(pegaTodosNomesFornecedores());
        cBox_fornecedor.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_fornecedor.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        cBox_fornecedor.setSize(250, 25);
        cBox_fornecedor.setLocation(10, 75);
        cBox_fornecedor.addFocusListener(acaoFoco);
        cBox_fornecedor.addKeyListener(TecladoFornecedor);
        cBox_fornecedor.setEditable(true);
        add(cBox_fornecedor);

    }

    //deixa os componentes invisiveis para que a tabela seja exibida '
    public void deixaComponentesIvisiveis() {
        JldataPagamento.setVisible(false);
        JtdataPagamento.setVisible(false);
        JlvalorDivida.setVisible(false);
        JtvalorDivida.setVisible(false);
        JldinheiroEntrada.setVisible(false);
        JtdinheiroEntrada.setVisible(false);
        JldinheiroDia.setVisible(false);
        JtdinheiroDia.setVisible(false);
        Jltelefone.setVisible(false);
        Jttelefone.setVisible(false);
        JSprodutosComprados.setVisible(true);

    }

    //deixa os componentes visiveis para exibir os campos
    public void deixaComponentesvisiveis() {
        JldataPagamento.setVisible(true);
        JtdataPagamento.setVisible(true);
        JlvalorDivida.setVisible(true);
        JtvalorDivida.setVisible(true);
        JldinheiroEntrada.setVisible(true);
        JtdinheiroEntrada.setVisible(true);
        JldinheiroDia.setVisible(true);
        JtdinheiroDia.setVisible(true);
        Jltelefone.setVisible(true);
        Jttelefone.setVisible(true);
        Finaliaza_compra.setVisible(true);
        cancelar_compra.setVisible(true);
        JSprodutosComprados.setVisible(false);

    }

    /*PEGA TODOS OS NOMES DOS PRODUTOS PARA O AUTO COMPLETE*/
    public String[] pegaTodosNomesProdutos() {
        int i, j = 0;

        String[] names = {""};
        ArrayList<String> Sentinela = new ArrayList<>();
        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        names = new String[ListaProduto_Estoque.size()];
        Sentinela.add("");
        for (i = 1; i < ListaProduto_Estoque.size(); i++) {

            produtoEstoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
            Sentinela.add(produtoEstoque.getNomeProduto());
        }

        names = Sentinela.toArray(names);
        Arrays.sort(names, 0, ListaProduto_Estoque.size());

        return (names);
    }

    /*PEGA TODOS OS NOMES DOS FORNECEDORES PARA O AUTO COMPLETE*/
    public String[] pegaTodosNomesFornecedores() {
        int i;

        String[] names = {""};
        ArrayList<String> Sentinela = new ArrayList<>();

        ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
        names = new String[ListaCompraNota.size()];
        Sentinela.add("");

        for (i = 0; i < ListaCompraNota.size(); i++) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
            Sentinela.add(compra_nota.getNomeFornecedor());
        }

        names = Sentinela.toArray(names);
        Arrays.sort(names, 0, ListaCompraNota.size());

        return (names);
    }

    //RETORNO A COMPRA NOTA DE UM FORNECEDOR CADASTRADO
    public COMPRA_NOTA RetornaCompraFornecedor(String nome) {
        ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
        for (int i = 0; i < ListaCompraNota.size(); i++) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
            if (compra_nota.getNomeFornecedor().equalsIgnoreCase(nome)) {
                return compra_nota;
            }

        }
        return null;

    }

    //PARA RETORNAR OS PRODUTOS DE UM FORNECEDOR QUE JA TENHA UMA COMPRA NOTA CADASTRADA  
    public ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> RetornaProdutosFornecedor(ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutosNota,
            ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutosConfirmados) {

        // JOptionPane.showMessageDialog(null,"Lista do estoque" + ListaProdutosNota);
        for (int i = 0; i < ListaProdutosConfirmados.size(); i++) {
            ListaProdutosNota.add(ListaProdutosConfirmados.get(i));

        }
      
        return ListaProdutosNota;
    }

    //retorna o codigo do produto
    public String RetornaCodigoProduto(String name) {
        for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
            produtoEstoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
            if (name.equalsIgnoreCase(produtoEstoque.getNomeProduto())) {
                return produtoEstoque.getCodigoProduto();
            }
        }

        return "";
    }

    /* metodo usado apenas para colocar o foco no txt correto*/
    public void DirecionaCusorFornecedor() {
        cBox_produto.requestFocus();

    }

    //metodo usado para pegar os produtos que foram confimados
    public void ArmazenaProdutosConfirmados() {
        String nomeProduto = Tf_produto.getText(),
                codigoProduto = Jtcodigo.getText();
        int quantidadeProduto = Integer.parseInt(Jtquantidade.getText());
        float precoCompra_unidade = Float.parseFloat(JtprecoCompraUnidade.getText().replace(",", ".")),
                precoVenda_unidade = Float.parseFloat(JtprecoVendaUnidade.getText().replace(",", ".")),
                precoCompra_Geral = Float.parseFloat(JtprecoCompraTotal.getText().replace(",", ".")),
                LucroUnidade = Float.parseFloat(JtlucroUnidade.getText().replace(",", ".")),
                LucroGeral = Float.parseFloat(JtlucroTotal.getText().replace(",", "."));

        produtoEstoqueConfirmado = new PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO(nomeProduto, codigoProduto, quantidadeProduto, precoCompra_unidade, precoVenda_unidade, precoCompra_Geral, LucroUnidade, LucroGeral);

        ListaProdutosConfimados.add(produtoEstoqueConfirmado);

        Valor_total_compra = Valor_total_compra + Float.parseFloat(JtprecoCompraTotal.getText().replace(",", "."));
        Lucro_total_compra = Lucro_total_compra + Float.parseFloat(JtlucroTotal.getText().replace(",", "."));

        JtvalorDividaExibir.setText(converte.FloatEmString(Valor_total_compra));
        JtLucroExibir.setText(converte.FloatEmString(Lucro_total_compra));
        Tabela(RetornaValoresParaExibirAtabela());//adiona os produtos na tabela
    }


    /*COLOCA NO ESTOQUE OS PRODUTOS QUE FORAM COMFIRMADO QUANDO BOTAO FINALIZAR NOTA E PRECIONADO*/
    public void ColocaListasProdutosEstoque() {
        ListaprodutoEstoque = ARQUIVO_ESTOQUE.retorna();

        boolean verifica = false;//fica verdadeira se o produto existir no estoque
        int contador = 0;

        for (PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO ListaProdutosConfimado : ListaProdutosConfimados) {
            produtoEstoqueConfirmado = (PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO) ListaProdutosConfimado;
            verifica = false;
            for (PRODUTO_ESTOQUE ListaprodutoEstoque1 : ListaprodutoEstoque) {
                produtoEstoque = (PRODUTO_ESTOQUE) ListaprodutoEstoque1;
                if (produtoEstoqueConfirmado.getCodigoProduto().equalsIgnoreCase(produtoEstoque.getCodigoProduto())) {
                    contador++;
                    verifica = true;
                    int quantidade = produtoEstoqueConfirmado.getQuantidadeProduto() + produtoEstoque.getQuantidadeProduto();
                    float precoVendaUnid = produtoEstoqueConfirmado.getPrecoVenda_unidade();
                    float precoCompraGeral = produtoEstoqueConfirmado.getPrecoCompra_Geral() + produtoEstoque.getPrecoCompra_Geral();
                    float precoCompaUnid = precoCompraGeral / quantidade;
                    float lucroUnid = precoVendaUnid - precoCompaUnid;
                    float lucroTotal = quantidade * lucroUnid;

                    produtoEstoque.setQuantidadeProduto(quantidade);
                    produtoEstoque.setPrecoCompra_Geral(precoCompraGeral);
                    produtoEstoque.setPrecoCompra_unidade(precoCompaUnid);
                    produtoEstoque.setPrecoVenda_unidade(precoVendaUnid);
                    produtoEstoque.setLucroUnidade(lucroUnid);
                    produtoEstoque.setLucroGeral(lucroTotal);
                    break;
                }
            }
            if (!verifica) {
                produtoEstoque = new PRODUTO_ESTOQUE(produtoEstoqueConfirmado.getNomeProduto(),
                        produtoEstoqueConfirmado.getCodigoProduto(),
                        produtoEstoqueConfirmado.getQuantidadeProduto(),
                        produtoEstoqueConfirmado.getPrecoCompra_unidade(),
                        produtoEstoqueConfirmado.getPrecoVenda_unidade(),
                        produtoEstoqueConfirmado.getPrecoCompra_Geral(),
                        produtoEstoqueConfirmado.getLucroUnidade(),
                        produtoEstoqueConfirmado.getLucroGeral());

                ListaprodutoEstoque.add(produtoEstoque);

            }
        }

        try {

            ARQUIVO_ESTOQUE.Armazena(ListaprodutoEstoque);

        } catch (IOException ex) {
            Logger.getLogger(AlternavelCadastroProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //metodo para retornas a divida dos produtos que ja foram confirmados
    public float RetornaDividaConfirmada(ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutos) {
        PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO produtoConfirmado = new PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO();
        float ValorDivida = 0;

        for (PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO ListaProduto : ListaProdutos) {
            produtoConfirmado = (PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO) ListaProduto;
            ValorDivida += produtoConfirmado.getPrecoCompra_Geral();
        }
        return ValorDivida;
    }

    public void limpa() {

        Tf_fonecedor.setText(null);
        Tf_produto.setText(null);
        Jtcodigo.setText(null);
        JtdataPagamento.setText(null);
        JtdinheiroDia.setText(null);
        JtlucroTotal.setText(null);
        JtlucroUnidade.setText(null);
        JtprecoCompraTotal.setText(null);
        JtprecoCompraUnidade.setText(null);
        JtprecoVendaUnidade.setText(null);
        Jtproduto.setText(null);
        Jtquantidade.setText(null);
        JtdinheiroEntrada.setText(null);
        Jltelefone.setText(null);
        Jttelefone.setText(null);
        JtvalorDivida.setText(null);
        JtvalorDividaExibir.setText(null);
        JtLucroExibir.setText(null);

        Tf_fonecedor.requestFocus();

    }

    /*metodo para redirecionar o foco para o botão finalizar depois de perder o foco ou pressionar enter
     calcula a nova divida apartir do dinheiro de entrada*/
    public void RedirecionaFocoFinalizarCompra() {
        String dia = data.retornaData();
        float dinheiroEntrada, precoCompraTotal, dividaTotal;
        int quantidadeDias = calculaDiferencaEntreDatas.calcula(dia, JtdataPagamento.getText());

        dinheiroEntrada = Float.parseFloat(JtdinheiroEntrada.getText().replace(",", "."));
        precoCompraTotal = Float.parseFloat(JtvalorDivida.getText().replace(",", "."));
        dividaTotal = precoCompraTotal - dinheiroEntrada;

        JtvalorDivida.setText(AlternavelCadastroProduto.converterFloatString(dividaTotal));
        JtdinheiroDia.setText(AlternavelCadastroProduto.converterFloatString(dividaTotal / quantidadeDias));

        Jttelefone.requestFocus();
    }

    //retorna de compra unidade
    public float RetornaOprecoCompraUnidade() {

        //  jt  
        return 0;
    }

    //retorna o valor do lucro unidade
    public float retornaLucroUnidade() {

        return Float.parseFloat(JtprecoVendaUnidade.getText().replace(",", ".")) - Float.parseFloat(JtprecoCompraUnidade.getText().replace(",", "."));
    }

    //coloca o lucro por unidada no txt 
    public void ColocaLucroUnidadeEmTxt() {
        JtlucroUnidade.setText(AlternavelCadastroProduto.converterFloatString(retornaLucroUnidade()));
    }

    // metodo para colocar o lucro total no txt
    public void ColocaLucroTotalEmTxt() {
        JtlucroTotal.setText(AlternavelCadastroProduto.converterFloatString((Integer.parseInt(Jtquantidade.getText())) * (retornaLucroUnidade())));
    }

    public MaskFormatter Mascara(String Mascara) {

        MaskFormatter F_Mascara = new MaskFormatter();
        try {
            F_Mascara.setMask(Mascara); //Atribui a mascara  
            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
        } catch (Exception excecao) {
            excecao.printStackTrace();
        }
        return F_Mascara;
    }
    //RETORNA O TOTAL DE DINHEIRO POR DIA 

    //CLASSE PARA IMPLEMENTAR O AUTO-COMPLETE DO FORNECEDOR
    public class AutoCompleteFornecedor extends JComboBox implements JComboBox.KeySelectionManager {

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        //Auto Completa o txt do fornecedor
        public AutoCompleteFornecedor(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                Tf_fonecedor = (JTextField) getEditor().getEditorComponent();

                if (Tf_fonecedor != null) {
                    Tf_fonecedor.addKeyListener(TecladoFornecedor);
                    Tf_fonecedor.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    Tf_fonecedor.setDocument(new AutoCompleteFornecedor.CBDocument());
                    addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {

                            Tf_fonecedor = (JTextField) getEditor().getEditorComponent();
                            Tf_fonecedor.setSize(230, 25);
                            String text = Tf_fonecedor.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    Tf_fonecedor.setText(current);
                                    Tf_fonecedor.setSelectionStart(text.length());
                                    Tf_fonecedor.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    //CLASSE PARA IMPLEMENTAR O AUTO-COMPLETE DO PRODUTO
    public class AutoCompleteProduto extends JComboBox implements JComboBox.KeySelectionManager {

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        //Auto Completa o Tf_produto
        public AutoCompleteProduto(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                Tf_produto = (JTextField) getEditor().getEditorComponent();

                if (Tf_produto != null) {
                    Tf_produto.addKeyListener(TecladoProduto);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    Tf_produto.setDocument(new AutoCompleteProduto.CBDocument());
                    addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {

                            Tf_produto = (JTextField) getEditor().getEditorComponent();
                            Tf_produto.setSize(230, 25);
                            String text = Tf_produto.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    Tf_produto.setText(current);
                                    Tf_produto.setSelectionStart(text.length());
                                    Tf_produto.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }
    
    /*FINALIZA A COMPRA QUANDO PRECIONAMOS O BOTÃO FINAIZAR*/

    public class AcaoBotao implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == cancelar_compra) {
                limpa();
            }

            if (e.getSource() == Finaliaza_compra) {
                String[] names = pegaTodosNomesFornecedores();
                boolean flagEncontrouProd = false;
                ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
                ColocaListasProdutosEstoque();
                COMPRA_NOTA compra_nota = new COMPRA_NOTA();
                Compra_Nota compra_em_Nota;
              
                for (int i = 0; i < names.length; i++) {
                    if (names[i].equalsIgnoreCase(Tf_fonecedor.getText())) {

                        flagEncontrouProd = true;
                        
                        Fornecedor fornecedor = null;
                        
                   // Nota nota = new Nota(0, 700.00,"31/08/1026", (RetornaProdutosFornecedor(compra_nota.getProdutoEstoque(), ListaProdutosConfimados)));
    int numeroNota;
    float valorNota;
    String dataLimitePagamento;
    ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutos = new ArrayList<>();     
                        
                        //compra_em_Nota = new Compra_Nota(0, fornecedor , valorDivida,);
                        
                        
                        compra_nota = RetornaCompraFornecedor(Tf_fonecedor.getText());
                        compra_nota.setProdutoEstoque(RetornaProdutosFornecedor(compra_nota.getProdutoEstoque(), ListaProdutosConfimados));
                        compra_nota.setDataEfetuarPagamento(JtdataPagamento.getText());
                        compra_nota.setDinheiroDia(Float.parseFloat(JtdinheiroDia.getText().replace(",", ".")) + compra_nota.getDinheiroDia());
                        compra_nota.setValorDivida(Float.parseFloat(JtvalorDivida.getText().replace(",", ".")) + compra_nota.getValorDivida());
                        compra_nota.setDinheiroEntrada(Float.parseFloat(JtdinheiroEntrada.getText().replace(",", ".")) + compra_nota.getDinheiroEntrada());

                    }

                }
                if (!flagEncontrouProd) {

                    compra_nota.setNomeFornecedor(Tf_fonecedor.getText());
                    compra_nota.setProdutoEstoque(ListaProdutosConfimados);
                    compra_nota.setDataInicoDivida(data.retornaData());
                    compra_nota.setDataEfetuarPagamento(JtdataPagamento.getText());
                    compra_nota.setDinheiroDia(Float.parseFloat(JtdinheiroDia.getText().replace(",", ".")));
                    compra_nota.setValorDivida(Float.parseFloat(JtvalorDivida.getText().replace(",", ".")));
                    compra_nota.setDinheiroEntrada(Float.parseFloat(JtdinheiroEntrada.getText().replace(",", ".")));
                    compra_nota.setTelefone(Jttelefone.getText());
                    ListaCompraNota.add(compra_nota);
                }

                try {
                    ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
                } catch (IOException ex) {
                    Logger.getLogger(P_CompraNota.class.getName()).log(Level.SEVERE, null, ex);
                }

                limpa();

            }

        }
    }

    public class AcaoFoco implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            Jtproduto.requestFocus();

        }
    }

    public class AcaoFocoProduto implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }

    public class AcaoFocoCodigo implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            boolean estaVasio = false;
            boolean controlaIf = false;//Somente para controlar o if que entra ou não no produto cadastrado (if 01)

            Jtquantidade.requestFocus();

        }
    }

    public class AcaoQuantidade implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            produtoCadastrado = false;

            //JtprecoCompraTotal.requestFocus();
        }
    }

    public class AcaoPrecoCompraTotal implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

        }
    }

    public class AcaoFocoPrecoVendaUnidade implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            if (Float.parseFloat(JtprecoVendaUnidade.getText().replace(",", ".")) == (float) 0.00 && JtdataPagamento.getText().equalsIgnoreCase("  /  /    ")) {
                JOptionPane.showMessageDialog(null, "Digite o preço da unidade do produto!");

                JtprecoVendaUnidade.requestFocus();
            } else if (JtdataPagamento.getText().equalsIgnoreCase("  /  /    ")) {

                int quantidadeProduto = Integer.parseInt(Jtquantidade.getText());
                float precoCompraUnidade = Float.parseFloat(JtprecoCompraUnidade.getText().replace(",", "."));
                float precoVendaUnidade = Float.parseFloat(JtprecoVendaUnidade.getText().replace(",", "."));

                JtlucroUnidade.setText(AlternavelCadastroProduto.converterFloatString(precoVendaUnidade - precoCompraUnidade));
                JtlucroTotal.setText(AlternavelCadastroProduto.converterFloatString((precoVendaUnidade - precoCompraUnidade) * quantidadeProduto));

                p_confirmaProduto = new P_confirmaProduto();

            }

        }
    }

    public class AcaoDataEfetuaPagamento implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            String dia = data.retornaData();

            if (JtdataPagamento.getText().equalsIgnoreCase("  /  /    ") && n != 0) {
                JOptionPane.showMessageDialog(null, "Digite a data de efetuar o pagamento!");

                JtdataPagamento.requestFocus();

            } else {
                if (!JtdataPagamento.getText().equalsIgnoreCase("  /  /    ")) {
                    int quantidadeDias = CalculaDiferencaEntreDatas.calcula(dia, JtdataPagamento.getText());

                    if (quantidadeDias < 1 || quantidadeDias > 581) {
                        JOptionPane.showMessageDialog(null, "Data invalida!\nDigite outra data.");
                        JtdataPagamento.setText(null);
                        JtdataPagamento.requestFocus();
                    } else {
                        float precoCompraTotal = Float.parseFloat(JtvalorDivida.getText().replace(",", "."));

                        JtdinheiroDia.setText(AlternavelCadastroProduto.converterFloatString(precoCompraTotal / quantidadeDias));

                        JtdinheiroEntrada.requestFocus();

                    }
                }
            }
        }
    }

    /* Foco do campo txt JtDinheiroEntrada redireciona o foco para Jttelefole
     alem de chamar o metodo RedirecionaFocoFinalizarCompra() que calcula o novo valor da divida*/
    public class AcaoDinheiroEntrada implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            RedirecionaFocoFinalizarCompra();
        }
    }

    public class P_confirmaProduto extends JFrame {

        private final Container container;
        private PainelConfirmaProduto painelConfirmaProduto = new PainelConfirmaProduto();

        public P_confirmaProduto() {

            container = getContentPane();

            this.container.add(painelConfirmaProduto);
            this.setLayout(null);
            this.setSize(300, 200); // alt, larg
            this.setBackground(Color.BLUE);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(490, 250);
        }
    }

    public class PainelConfirmaProduto extends JPanel {

        JLabel Jlmenssagem = new JLabel();//label usada na tela de cadastrar novo produto 
        //boolean O_botaoSimPress = false;

        public PainelConfirmaProduto() {

            this.setLayout(null);
            this.setSize(300, 200); // larg, alt // larg, alt
            this.setLocation(0, 0);
            this.setBackground(Color.white);

            Jlmenssagem.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            Jlmenssagem.setText("Deseja Comprar Outro Produto");
            Jlmenssagem.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Jlmenssagem.setLocation(20, 10);//
            add(Jlmenssagem);

            sim_produto = new JButton();//botao usado caso o cliente queira comprar outro produto
            sim_produto.setText("SIM");
            sim_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            sim_produto.setSize(100, 25);
            sim_produto.addActionListener(acao_botao);
            sim_produto.addActionListener(acaoBotaoSimP_ConfirmaProduto);
            sim_produto.addKeyListener(acaoBotaoSimP_ConfirmaProduto);
            sim_produto.setLocation(20, 75);
            add(sim_produto);

            nao_produto = new JButton();//botao usado caso nao queira comprar outro produto
            nao_produto.setText("NAO");
            nao_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            nao_produto.setSize(100, 25);
            nao_produto.addActionListener(acao_botao);
            nao_produto.addKeyListener(TecladoNao);
            nao_produto.addActionListener(acaoBotaoSimP_ConfirmaProduto);
            nao_produto.setLocation(130, 75);
            add(nao_produto);

            sim_produto.requestFocus();

            // metodo para armazenar os produtos que ja foram confirmados
        }
    }

    public class AcaoBotaoSimP_ConfirmaProduto implements ActionListener, KeyListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == sim_produto) {
                JOptionPane.showMessageDialog(null, "PRODUTO SIM");
                //chama os metodos para colocar os lucros nos campos de texto
                ColocaLucroUnidadeEmTxt();
                ColocaLucroTotalEmTxt();

                //Armazenas os produtos previamentes confirmados
                ArmazenaProdutosConfirmados();

                Tf_produto.requestFocus();

                LimpaProduto();
                deixaComponentesIvisiveis();
                p_confirmaProduto.dispose();
                voltar.setVisible(false);

            }

            if (e.getSource() == nao_produto) {

                JOptionPane.showMessageDialog(null, "PRODUTO NAO");
                ColocaLucroUnidadeEmTxt();
                ColocaLucroTotalEmTxt();

                //Armazenas os produtos previamentes confirmados
                ArmazenaProdutosConfirmados();

                JtdataPagamento.requestFocus();
                p_confirmaProduto.dispose();
                LimpaProduto();
                voltar.setVisible(true);
                //recebe a lista de produtos confirmados & retorna o valor da divida temporario

                JtvalorDivida.setText(AlternavelCadastroProduto.converterFloatString(RetornaDividaConfirmada(ListaProdutosConfimados)));

            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                //chama os metodos para colocar os lucros nos campos de texto
                if (verifica) {
                    ColocaLucroUnidadeEmTxt();
                    ColocaLucroTotalEmTxt();

                    //Armazenas os produtos previamentes confirmados
                    ArmazenaProdutosConfirmados();

                    Tf_produto.requestFocus();

                    LimpaProduto();
                    deixaComponentesIvisiveis();
                }
                
                p_confirmaProduto.dispose();
                voltar.setVisible(false);

            }
            if (e.getKeyCode() == 39) {
                nao_produto.requestFocus();

            }
        }
    }

    //classe de teclas para painel confirma produto quando precionar botão nao
    public class TecladoNao implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                //chama os metodos para colocar os lucros nos campos de texto
                ColocaLucroUnidadeEmTxt();
                ColocaLucroTotalEmTxt();

                //Armazenas os produtos previamentes confirmados
                ArmazenaProdutosConfirmados();

                JtdataPagamento.requestFocus();
                p_confirmaProduto.dispose();
                deixaComponentesvisiveis();
                JtdataPagamento.requestFocus();
                LimpaProduto();
                voltar.setVisible(true);
                deixaValor_lucro_invisiveis();
                //recebe a lista de produtos confirmados & retorna o valor da divida temporario
                JtvalorDivida.setText(AlternavelCadastroProduto.converterFloatString(RetornaDividaConfirmada(ListaProdutosConfimados)));
            }
            if (e.getKeyCode() == 37) {
                sim_produto.requestFocus();
            }

        }
    }

    public void LimpaProduto() {
        Tf_produto.setText(null);
        Jtcodigo.setText(null);
        Jtproduto.setText(null);
        Jtquantidade.setText(null);
        JtprecoCompraTotal.setText(null);
        JtprecoCompraUnidade.setText(null);
        JtprecoVendaUnidade.setText(null);
        JtlucroUnidade.setText(null);
        JtlucroTotal.setText(null);

    }

    public class Teclado_F1 implements KeyListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if ((e.getKeyCode() == 10)) {
                p_confirmaProduto = new P_confirmaProduto();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /*TECLADO DO TXT FORNECEDO E TXT PRODUTO 
     ENCONTRA O PRODUTO NO AUTO COMPLETE E COLOCA O CODIGO NO TXT CASO 
     O PRODUTO ESTEJA CADASTRADO CASO CONTRARIO FORNECE UM NOVO CODIGO*/
    public class TecladoFornecedor implements KeyListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            if ((e.getKeyCode() == 10)) {
                ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
                String codigo;
                int Codigo = 0;

                if (Tf_produto.getText().equalsIgnoreCase("")) {
                    cBox_produto.requestFocus();
                }
                if (!cBox_produto.getSelectedItem().toString().equalsIgnoreCase("")) {

                    if (RetornaCodigoProduto(cBox_produto.getSelectedItem().toString()).equalsIgnoreCase("")) {
                        codigo = ListaProduto_Estoque.get(ListaProduto_Estoque.size() - 1).getCodigoProduto();
                        Codigo = Integer.parseInt(codigo);
                        Codigo++;
                        Jtcodigo.setText(String.valueOf(Codigo));

                    } else {
                        Jtcodigo.setText(RetornaCodigoProduto(cBox_produto.getSelectedItem().toString()));
                    }
                    Jtquantidade.requestFocus();
                }

            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }
    /*TECLADO DO TXTPRODUTO FORNECE UM NOVO CODIGO*/

    public class TecladoProduto implements KeyListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            if ((e.getKeyCode() == 10)) {
                ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
                String codigo;
                int Codigo = 0;

                if (!cBox_produto.getSelectedItem().toString().equalsIgnoreCase("")) {

                    if (RetornaCodigoProduto(cBox_produto.getSelectedItem().toString()).equalsIgnoreCase("") && ListaProdutosConfimados.size() == 0) {
                        codigo = ListaProduto_Estoque.get(ListaProduto_Estoque.size() - 1).getCodigoProduto();
                        Codigo = Integer.parseInt(codigo);
                        Codigo++;
                        Jtcodigo.setText(String.valueOf(Codigo));
                        armazena_codigo = Codigo;
                    } else if (RetornaCodigoProduto(cBox_produto.getSelectedItem().toString()).equalsIgnoreCase("") && ListaProdutosConfimados.size() != 0) {
                        if (armazena_codigo == 0) {
                            codigo = ListaProduto_Estoque.get(ListaProduto_Estoque.size() - 1).getCodigoProduto();
                            Codigo = Integer.parseInt(codigo);
                            Codigo++;
                            Jtcodigo.setText(String.valueOf(Codigo));
                            armazena_codigo = Codigo;

                        } else {
                            armazena_codigo++;
                            Jtcodigo.setText(String.valueOf(armazena_codigo));
                        }
                    } else {
                        Jtcodigo.setText(RetornaCodigoProduto(cBox_produto.getSelectedItem().toString()));

                    }
                    Jtquantidade.requestFocus();
                }

            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

    public class Teclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {
                p_confirmaProduto = new P_confirmaProduto();
            }

            if (e.getKeyCode() == 10) {

                if (JtprecoCompraTotal.getText().equalsIgnoreCase("0,00") && Compra_uni_tot == 0) {
                    JtprecoCompraTotal.requestFocus();

                } else if (JtprecoCompraUnidade.getText().equalsIgnoreCase("0,00") && Compra_uni_tot == 1) {

                    JtprecoCompraUnidade.requestFocus();
                } else {
                    ColocaLucroUnidadeEmTxt();
                    ColocaLucroTotalEmTxt();
                    p_confirmaProduto = new P_confirmaProduto();

                }

            }
        }
    }

    //classe de teclado e ação de foco para redirecionar o cursor 
    public class TecladoPrecoCompraTotal implements KeyListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                int quantidadeProduto = converte.StringEmInteiro(Jtquantidade.getText());
                float precoCompraTotal = converte.StringEmFloat(JtprecoCompraTotal.getText());

                JtprecoCompraUnidade.setText(AlternavelCadastroProduto.converterFloatString(precoCompraTotal / quantidadeProduto));

                JtprecoVendaUnidade.requestFocus();

            }

        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

    public class TecladoPrecoCompraUnidade implements KeyListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            if (e.getKeyCode() == 10) {

                int quantidadeProduto = converte.StringEmInteiro(Jtquantidade.getText());
                float precoCompraUnidade = converte.StringEmFloat(JtprecoCompraUnidade.getText());

                JtprecoCompraTotal.setText(converte.FloatEmString(precoCompraUnidade * quantidadeProduto));

                JtprecoVendaUnidade.requestFocus();

            }

        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

    //classe para maipular a acão do teclado na data
    public class DataEfetuarPagamento implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                String dia = data.retornaData();

                if (JtdataPagamento.getText().equalsIgnoreCase("  /  /    ") && n != 0) {
                    JOptionPane.showMessageDialog(null, "Digite a data de efetuar o pagamento!");

                    JtdataPagamento.requestFocus();

                } else {
                    int quantidadeDias = CalculaDiferencaEntreDatas.calcula(dia, JtdataPagamento.getText());

                    if (quantidadeDias < 1 || quantidadeDias > 581) {
                        JOptionPane.showMessageDialog(null, "Data invalida!\nDigite outra data.");
                        JtdataPagamento.setText(null);
                        JtdataPagamento.requestFocus();
                    } else {
                        float precoCompraTotal = Float.parseFloat(JtvalorDivida.getText().replace(",", "."));

                        JtdinheiroDia.setText(AlternavelCadastroProduto.converterFloatString(precoCompraTotal / quantidadeDias));

                        JtdinheiroEntrada.requestFocus();

                    }
                }

            }
        }
    }

    public class TecladoDinheiroEntrada implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                Jttelefone.requestFocus();
            }

        }
    }
    //DIRECIONA O FOCO DE JTTELEFONE PARA O BOTÃO FINALIZAR COMPRA

    public class TecladoTelefone implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                Finaliaza_compra.requestFocus();
            }
        }
    }
}
