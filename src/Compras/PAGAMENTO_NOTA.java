/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;

/**
 *
 * @author User
 */
public class PAGAMENTO_NOTA implements Comparable<Object>{
    String Fornecedor;
    String nomeRecebeu;
    String diaRecebeu;
    String dataRecebeu;
    float valorRecebeu;


    public PAGAMENTO_NOTA(String Fornecedor, String diaRecebeu, String dataRecebeu, String nomeRecebeu, float valorRecebeu) {
        this.Fornecedor = Fornecedor;
        this.nomeRecebeu = nomeRecebeu;
        this.diaRecebeu = diaRecebeu;
        this.dataRecebeu = dataRecebeu;
        this.valorRecebeu = valorRecebeu;
    }

    public String getFornecedor() {
        return Fornecedor;
    }

    public void setFornecedor(String Fornecedor) {
        this.Fornecedor = Fornecedor;
    }

    public PAGAMENTO_NOTA() {
       
    }

    public String getDataRecebeu() {
        return dataRecebeu;
    }

    public void setDataRecebeu(String dataRecebeu) {
        this.dataRecebeu = dataRecebeu;
    }

    public String getDiaRecebeu() {
        return diaRecebeu;
    }

    public void setDiaRecebeu(String diaRecebeu) {
        this.diaRecebeu = diaRecebeu;
    }

    public String getNomeRecebeu() {
        return nomeRecebeu;
    }

    public void setNomeRecebeu(String nomeRecebeu) {
        this.nomeRecebeu = nomeRecebeu;
    }

    public float getValorRecebeu() {
        return valorRecebeu;
    }

    public void setValorRecebeu(float valorRecebeu) {
        this.valorRecebeu = valorRecebeu;
    }

    @Override
    public String toString() {
        return  Fornecedor+ ";" + diaRecebeu + ";" + dataRecebeu + ";" +nomeRecebeu + ";"+ valorRecebeu + ";" + "\n";
    }
     @Override
    public int compareTo(Object o) {
        PAGAMENTO_NOTA outro =(PAGAMENTO_NOTA)o;
        
        return this.dataRecebeu.compareToIgnoreCase(outro.dataRecebeu);
    }
    
}
