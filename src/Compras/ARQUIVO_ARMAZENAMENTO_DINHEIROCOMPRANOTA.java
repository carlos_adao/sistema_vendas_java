/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;

import BancoDados.ARQUIVO_ARMAZENA_DINHEIRO;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class ARQUIVO_ARMAZENAMENTO_DINHEIROCOMPRANOTA {
    private static int primaryKey;
    private static String formecedor;
    private static String diaArmazenamento;
    private static String dataArmazenamento;
    private static String obcervacao;
    private static float valorArmazenado;
    private static ARMAZENAMENTO_DINHEIROCOMPRANOTA armazena_dinheiro  = new ARMAZENAMENTO_DINHEIROCOMPRANOTA();
    private static File file = new File("Arquivo Armazenar Dinheiro.csv");
    private static File arq = new File("Arquivo Armazenar Dinheiro.txt");
 
     public static void Armazena(ArrayList<ARMAZENAMENTO_DINHEIROCOMPRANOTA> Produtos) throws IOException {
         
         if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);

        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Produtos.size();
        for (i = 0; i < n; i++) {
            gravarArq.printf("%s%n", Produtos.get(i));
            bw.write(Produtos.get(i).toString());

        }
        gravarArq.close();
        bw.close();
        fw.close();

        System.out.println("gravado com sucesso!!!");
    }
     
     public static ArrayList<ARMAZENAMENTO_DINHEIROCOMPRANOTA> retorna(){
          ArrayList<ARMAZENAMENTO_DINHEIROCOMPRANOTA> ListaPagamentosArmazenados = new ArrayList<>();
          
           try{
        
        FileReader fr = new FileReader(file);
        BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
     
        String linha = lerArq.readLine();
      
        int j = 0;
        
        while(linha != null){
           
                String[] produto = linha.split(";");
                
               // primaryKey = new Integer.parseInt();
                formecedor = new String(produto[0]);
                diaArmazenamento = new String(produto[1]);
                dataArmazenamento = new String(produto[2]);
                obcervacao = new String(produto[3]);
                valorArmazenado = Float.parseFloat(produto[4]);

               // armazena_dinheiro = new ARQUIVO_ARMAZENA_DINHEIRO(formecedor, diaArmazenamento, dataArmazenamento, obcervacao,valorArmazenado);
                
                linha = lerArq.readLine();

                //ListaPagamentosArmazenados.add(armazenaPagamento);

            }

            fr.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
            ListaPagamentosArmazenados = null;
        }
        return ListaPagamentosArmazenados;
    }
}
