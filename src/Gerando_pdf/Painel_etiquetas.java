/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerando_pdf;

import Auxiliares.Converte;
import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_ESTOQUE;
import Compras.Visualizar.TabelaModel;
import Interface_Alternavel.AlternavelCadastroProduto;
import Interface_Alternavel.JtextFieldSomenteNumeros;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import com.itextpdf.text.DocumentException;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public final class Painel_etiquetas extends JPanel {

    /*COMPONENTES GRAFICOS*/
    JLabel Jlnome_produto = new JLabel();

    JLabel Jlquantidade_produto = new JLabel();
    JtextFieldSomenteNumeros Jtquantidade_produto = new JtextFieldSomenteNumeros();

    JTextField tf;
    JComboBox comboBox;

    JLabel Jlnome_tabela = new JLabel();

    TabelaModel modelo = new TabelaModel();
    JTable jtable = new JTable();
    JScrollPane JStabelaEtiquetas = new JScrollPane(jtable);
    JButton Botao_salvar;

    private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
    private ArrayList<String> listaNomesProdutos = new ArrayList<>();
    private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
    private ArrayList<Etiqueta> ListaEtiquetas = new ArrayList<>();
    Etiqueta Etiqueta = new Etiqueta();

    AlternavelPesquisa AlternavelPesquisa = new AlternavelPesquisa();

    /*CLASSE DE AÇÃO*/
    teclado_QuantMerc teclado_QuantMerc = new teclado_QuantMerc();
    tecladoExclusivo tecladoExclusivo = new tecladoExclusivo();
    EnterPrecionadoBtSalvar EnterPrecionadoBtSalvar = new EnterPrecionadoBtSalvar();

    /*classe para fazer converção de valores*/
    Converte convert = new Converte();

    /*classe para fazer converção de valores*/
    Etiquetas Etiquetas;

    public Painel_etiquetas() throws DocumentException, IOException {
        this.Etiquetas = new Etiquetas();
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

        CriaTabela(TransformaValorParaTabela());

        comboBox = new AlternavelPesquisa(pegaTodosNomesProdutos());
        comboBox.setEditable(true);
        comboBox.setBounds(50, 50, 100, 25);
        comboBox.setMaximumRowCount(10);
        comboBox.setSize(250, 25);
        comboBox.setLocation(20, 55);
        add(comboBox);
        new JavaApplication4(comboBox);

        Jlnome_produto.setBounds(new Rectangle(168, 135, 400, 30));
        Jlnome_produto.setText("Nome da mercadoria");
        Jlnome_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Jlnome_produto.setLocation(20, 20);//
        add(Jlnome_produto);

        Jlquantidade_produto.setBounds(new Rectangle(168, 135, 400, 30));
        Jlquantidade_produto.setText("Quantidade da mercadoria");
        Jlquantidade_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Jlquantidade_produto.setLocation(20, 80);//
        add(Jlquantidade_produto);

        Jtquantidade_produto.setBounds(new Rectangle(200, 135, 120, 17));
        Jtquantidade_produto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        Jtquantidade_produto.setSize(250, 25);
        Jtquantidade_produto.setLocation(20, 115);
        Jtquantidade_produto.addKeyListener(teclado_QuantMerc);
        add(Jtquantidade_produto);

        Jlnome_tabela.setBounds(new Rectangle(168, 135, 400, 30));
        Jlnome_tabela.setText("Tabela de Etiquetas");
        Jlnome_tabela.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Jlnome_tabela.setLocation(500, 20);//
        add(Jlnome_tabela);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR PDF");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(200, 25);
        Botao_salvar.addActionListener(EnterPrecionadoBtSalvar);
        Botao_salvar.addKeyListener(EnterPrecionadoBtSalvar);
        Botao_salvar.setLocation(30, 300);
        add(Botao_salvar);

    }

    public void CriaTabela(ArrayList<String[]> ListaEtiquetas) {

        String[] colunas = new String[]{"Quant", "Nome"};
        modelo.setLinhas(ListaEtiquetas);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(1);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(15);

        JStabelaEtiquetas.setLocation(500, 55);
        JStabelaEtiquetas.getVerticalScrollBar();
        JStabelaEtiquetas.setSize(200, 300);
        JStabelaEtiquetas.revalidate();
        add(JStabelaEtiquetas);

    }

    public String[] pegaTodosNomesProdutos() {
        int i, j = 0;
        String test;
        String[] names = {""};
        ArrayList<String> Sentinela = new ArrayList<>();

        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
        names = new String[ListaProduto_Estoque.size()];
        Sentinela.add("");

        for (i = 0; i < ListaProduto_Estoque.size(); i++) {

            Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
            Sentinela.add(Produto_Estoque.getNomeProduto());
        }

        names = Sentinela.toArray(names);
        Arrays.sort(names, 0, ListaProduto_Estoque.size());

        return (names);
    }

    /*TRASFORMA OS VALORES DA LISTA DE OBJETO PARA STRING*/
    public ArrayList<String[]> TransformaValorParaTabela() {

        Etiqueta Etiqueta = new Etiqueta();
        ArrayList<String[]> Local_dados = new ArrayList<>();

        for (int i = 0; i < ListaEtiquetas.size(); i++) {
            Etiqueta = (Etiqueta) ListaEtiquetas.get(i);
            Local_dados.add(TransformaDadoEmString(Etiqueta));
        }
        return Local_dados;
    }

    public String[] TransformaDadoEmString(Etiqueta Etiqueta) {

        String nome = Etiqueta.getNome();
        String quantidade = convert.InteiroEmString(Etiqueta.getQuantidade());

        String[] etiqueta = new String[]{quantidade, nome};

        return etiqueta;

    }

    public class teclado_QuantMerc implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyPressed(KeyEvent e) {
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                Etiqueta Etiqueta = new Etiqueta();
                Etiqueta.setNome(tf.getText());
                Etiqueta.setCodigo(retornaProdutoEstoque(tf.getText()).getCodigoProduto());
                Etiqueta.setPreco(convert.FloatEmString(retornaProdutoEstoque(tf.getText()).getPrecoVenda_unidade()));
                Etiqueta.setQuantidade(convert.StringEmInteiro(Jtquantidade_produto.getText()));

                ListaEtiquetas.add(Etiqueta);

                CriaTabela(TransformaValorParaTabela());

                Jtquantidade_produto.setText(null);

                comboBox.requestFocus();
                tf.requestFocus();

            }
        }

    }

    public class tecladoExclusivo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {

                Jtquantidade_produto.requestFocus();
            }
        }

    }

    public class EnterPrecionadoBtSalvar implements ActionListener, KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

            if (e.getKeyCode() == 10) {

                try {

                    Etiquetas.CriarEtiquetas(ListaEtiquetas);
                    ListaEtiquetas.clear();
                    CriaTabela(TransformaValorParaTabela());
                    JOptionPane.showMessageDialog(null,"Etiquetas criadas!" );
                } catch (DocumentException ex) {
                    Logger.getLogger(Painel_etiquetas.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Painel_etiquetas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {
                try {
                    Etiquetas.CriarEtiquetas(ListaEtiquetas);
                    ListaEtiquetas.clear();
                    CriaTabela(TransformaValorParaTabela());
                    JOptionPane.showMessageDialog(null,"Etiquetas criadas!" );

                } catch (DocumentException ex) {
                    Logger.getLogger(Painel_etiquetas.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Painel_etiquetas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    ///PAINEL DE PESQUISA///
    public class AlternavelPesquisa extends JComboBox implements JComboBox.KeySelectionManager {

        private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
        private ArrayList<String> listaNomesProdutos = new ArrayList<>();
        private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
        private String searchFor;
        private long lap;
        int contador = 0;
        JFrame f = new JFrame("AutoCompleteComboBox");

        private AlternavelPesquisa() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AlternavelPesquisa(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);
            tf = null;
            if (getEditor() != null) {
                tf = (JTextField) getEditor().getEditorComponent();

                if (tf != null) {
                    //para fazer apenas uma verificação de enter precionado
                    tf.setDocument(new AlternavelPesquisa.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            tf = (JTextField) getEditor().getEditorComponent();
                            tf.setSize(230, 25);
                            String text = tf.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    tf.setText(current);
                                    tf.setSelectionStart(text.length());
                                    tf.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }
            tf.addKeyListener(tecladoExclusivo);
            tf.setText(null);
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {

                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();

                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }

        String RetornaNome() {

            return tf.getText();
        }

        public void FechaPainel() {
            f.dispose();

        }
    }

    public PRODUTO_ESTOQUE retornaProdutoEstoque(String nome) {
        ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
        PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();

        for (PRODUTO_ESTOQUE ListaProduto_Estoque1 : ListaProduto_Estoque) {
            Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque1;
            if (Produto_Estoque.getNomeProduto().equalsIgnoreCase(nome)) {
                return Produto_Estoque;

            }
        }
        return null;
    }

}
