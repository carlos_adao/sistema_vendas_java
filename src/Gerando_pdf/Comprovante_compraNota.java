/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerando_pdf;

import Auxiliares.Converte;
import Interface_Alternavel.PRODUTO_VENDA;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class Comprovante_compraNota {

    PdfPTable table = new PdfPTable(new float[]{0.1f, 0.1f, 0.4f, 0.2f, 0.2f});
    Converte convert = new Converte();
    PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA();

    Font fontexto = FontFactory.getFont(FontFactory.TIMES_ITALIC, 9);
    Font f = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);
    Font f_titulo = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.BOLD);
    Font f_subtitulo = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    Font fonteTest = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    public Comprovante_compraNota() throws DocumentException, IOException {
   
    }

    public void AdcionaNaTabela(String elemento) {
        table.addCell(new Phrase(elemento, f));

    }

    public static void main(String[] args) throws DocumentException, IOException {

        Comprovante_compraNota Comprovante_compraNota = new Comprovante_compraNota();
    }

    public void comprovante(String nomeFornecedor, String datapagamento, String diapagamento, String valorpagamento, String quemrebeu, String valortotal, String valoratual ) throws DocumentException, IOException {
        
        try {

            Document doc = new Document(com.itextpdf.text.PageSize.A4, 10, 10, 10, 10);
            OutputStream os = new FileOutputStream("Pagamento"+nomeFornecedor+" "+datapagamento+".pdf");
            PdfWriter.getInstance(doc, os);
            doc.open();

            Paragraph p = new Paragraph("BOX DO JOSE ALBERTINO ARTES & ARTESANATO", f_titulo);
            p.setAlignment(Element.ALIGN_LEFT);

            Paragraph p1 = new Paragraph("Central de Abastecimanto - Pavilhao B - Box 310 & 311", f_subtitulo);
            p1.setAlignment(Element.ALIGN_LEFT);

            Paragraph p2 = new Paragraph("Malhado - Ilhéus - Bahia", f_subtitulo);
            p2.setAlignment(Element.ALIGN_LEFT);

            Paragraph p3 = new Paragraph("facebook.com/BoxdoJoseAlbertino                            tel. (73)9116-7137 / 8106-6027", f_subtitulo);
            p3.setAlignment(Element.ALIGN_LEFT);
            p3.setSpacingAfter(20);

            Paragraph p4 = new Paragraph("***********COMPROVANTE PAGAMENTO COMPRA NOTA***********", f_subtitulo);
            p4.setAlignment(Element.ALIGN_CENTER);
            p4.setSpacingAfter(20);

            table.setWidthPercentage(100.0f);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);

            AdcionaNaTabela("Cod");
            AdcionaNaTabela("quant");
            AdcionaNaTabela("descrição");
            AdcionaNaTabela("p.venda unid");
            AdcionaNaTabela("p.venda total");

            float soma = (float) 0.0;
            /*for (int i = 0; i < L_ProdutoVenda_aguardandoPagamento.size(); i++) {

                produtoVenda = (PRODUTO_VENDA) L_ProdutoVenda_aguardandoPagamento.get(i);
                soma += produtoVenda.getSubTotal();
                AdcionaNaTabela(produtoVenda.getCodigoProduto());
                AdcionaNaTabela(convert.InteiroEmString(produtoVenda.getQuantidadeProduto()));
                AdcionaNaTabela(produtoVenda.getNomeProduto());
                AdcionaNaTabela(convert.FloatEmString(produtoVenda.getValor_Venda()));
                AdcionaNaTabela(convert.FloatEmString(produtoVenda.getSubTotal()));
            }*/

            Paragraph p5 = new Paragraph(" Pagamento realizado no dia "+datapagamento+", "+diapagamento+" no valor de R$ "+valorpagamento+" referente à compra em nota de mercadorias no valor total de R$: "+valortotal+" do fornecedor: "+ nomeFornecedor+". Valor restante da nota R$ "+valoratual, f_subtitulo);
            p5.setSpacingAfter(30);
            Paragraph p6 = new Paragraph("----------------------------------------------                                                            ----------------------------------------");
           Paragraph p7 = new Paragraph("                "+quemrebeu+"                                                                                            "+"Carlos Adão");
            // p4.setAlignment(Element.ALIGN_RIGHT);
            Rectangle rect;

            doc.add(p);
            doc.add(p1);
            doc.add(p2);
            doc.add(p3);
            //doc.add(table);
            doc.add(p4);
            doc.add(p5);
            doc.add(p6);
            doc.add(p7);
         
            doc.add(rect = new Rectangle(200, 200));

            if (doc != null) {
                //fechamento do documento
                doc.close();
            }
            if (os != null) {
                //fechamento da stream de saída
                os.close();
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(testando_pdf.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}
