/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerando_pdf;

import Auxiliares.Converte;
import com.itextpdf.text.Chunk;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Etiquetas {

    PdfPTable table = new PdfPTable(new float[]{0.25f, 0.25f, 0.25f, 0.25f});
    Converte convert = new Converte();
    String enderecos = "\n\nCarlos Adão (73)9104-6949\nfacebook.com/boxdojosealbertino\nboxdojosealbertino@hotmail.com";
    int cont = 0;

    public Etiquetas() throws DocumentException, IOException {

    }


    public static void main(String[] args) throws DocumentException, IOException {

        Etiquetas Etiquetas = new Etiquetas();
    }

    public void CriarEtiquetas(ArrayList<Etiqueta> ListaEtiquetas) throws DocumentException, IOException {
        int gusrda  = 0;
        Etiqueta Etiqueta = new Etiqueta();
        cont++;
        try {

            Document doc = new Document(com.itextpdf.text.PageSize.A4, 0, 0, 0, 0);
            OutputStream os = new FileOutputStream("Etiquetas.pdf");
            PdfWriter.getInstance(doc, os);
            doc.open();

            Color preto = new Color(1, 1, 1);
            Font fontexto = FontFactory.getFont(FontFactory.TIMES_ITALIC, 9);
            Font f = new Font(FontFamily.COURIER, 10, Font.BOLD);
            Font f_codPreco = new Font(FontFamily.TIMES_ROMAN, 16, Font.BOLD);
            Font fonteTest = new Font(FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

            for (int i = 0; i < ListaEtiquetas.size(); i++) {

                Etiqueta = (Etiqueta) ListaEtiquetas.get(i);
                
                for (int j = 1; j <= Etiqueta.getQuantidade(); j++) {
            
                    Phrase tickt = new Phrase();
                    Chunk nome_Produto = new Chunk(Etiqueta.getNome(), f);
                    Chunk cod_produto = new Chunk("\n           Cod "+ Etiqueta.getCodigo(), f_codPreco);
                    Chunk preco_produto = new Chunk("\n                R$ "+Etiqueta.getPreco(), f_codPreco);
                    Chunk endereco = new Chunk(enderecos, fonteTest);
                    tickt.add(nome_Produto);
                    tickt.add(cod_produto);
                    tickt.add(preco_produto);
                    tickt.add(endereco);
                   
                  
                    table.addCell(tickt);
                  
                
                }
               
            }

            table.setWidthPercentage(100.0f);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            doc.add(table);

            if (doc != null) {
                //fechamento do documento
                doc.close();
            }
            if (os != null) {
                //fechamento da stream de saída
                os.close();
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(testando_pdf.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
