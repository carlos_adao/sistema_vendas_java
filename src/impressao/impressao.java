/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impressao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JOptionPane;

public class impressao {

    String textoimp = "o que vai ser impresso /n /r /f";
// /n/r para novas linhas e /f para fim da pagina

    private SimpleDoc documentoTexto;
    private PrintService impressora;
    private DocFlavor.INPUT_STREAM docFlavor;

    {  
          
        InputStream prin = new ByteArrayInputStream(textoimp.getBytes());
        docFlavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        documentoTexto = new SimpleDoc(prin, docFlavor, null);
        impressora = PrintServiceLookup.lookupDefaultPrintService(); // pega a //impressora padrao
        JOptionPane.showMessageDialog(null,impressora);
        PrintRequestAttributeSet printerAttributes = new HashPrintRequestAttributeSet();
        printerAttributes.add(new JobName("Impressao", null));
        printerAttributes.add(OrientationRequested.PORTRAIT);
        printerAttributes.add(MediaSizeName.ISO_A4); // informa o tipo de folha
        DocPrintJob printJob = impressora.createPrintJob();
        try {
            printJob.print(documentoTexto, (PrintRequestAttributeSet) printerAttributes); //tenta imprimir
        } catch (PrintException e) {
            JOptionPane.showMessageDialog(null, "Não foi possível realizar a impressão !!", "Erro", JOptionPane.ERROR_MESSAGE); // mostra //mensagem de erro
        }
        try {
            prin.close();
        } catch (IOException ex) {
            Logger.getLogger(impressao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public static void main(String[] args) {
     
      
  
    }
}
