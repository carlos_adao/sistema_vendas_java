/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Cadastro;

/**
 *
 * @author carlos
 */
public class CadastroProduto {
    
    private String codigo_produto;
    private String nome_produto;
    private float preco_produtoUni;
    private int quantidade_produto;
    private float preco_produtoGer;

    
    
    public CadastroProduto(String codigo_produto, String nome_produto, float preco_produtoUni, float preco_produtoGer,int quantidade_produto) {
        this.codigo_produto = codigo_produto;
        this.nome_produto = nome_produto;
        this.preco_produtoUni = preco_produtoUni;
        this.preco_produtoGer = preco_produtoGer;
        this.quantidade_produto = quantidade_produto;
    }

    public CadastroProduto() {
        
    }

    public String getCodigo_produto() {
        return codigo_produto;
    }

    public void setCodigo_produto(String codigo_produto) {
        this.codigo_produto = codigo_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public void setPreco_produtoGer(float preco_produtoGer) {
        this.preco_produtoGer = preco_produtoGer;
    }

    public void setPreco_produtoUni(float preco_produtoUni) {
        this.preco_produtoUni = preco_produtoUni;
    }

    public void setQuantidade_produto(int quantidade_produto) {
        this.quantidade_produto = quantidade_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public float getPreco_produtoGer() {
        return preco_produtoGer;
    }

    public float getPreco_produtoUni() {
        return preco_produtoUni;
    }

    public int getQuantidade_produto() {
        return quantidade_produto;
    }

    
    @Override
    public String toString() {
        return(codigo_produto+ ";" + nome_produto +";"+ preco_produtoUni + ";"+preco_produtoGer +";" + quantidade_produto+";");
    }
   
    
    
}