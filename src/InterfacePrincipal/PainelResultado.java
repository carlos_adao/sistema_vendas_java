/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacePrincipal;

import Auxiliares.Converte;
import Interface_Alternavel.JNumberFormatField;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.text.DecimalFormat;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author carlos
 */
public class PainelResultado extends JPanel{
    JLabel JLsubTotal = new JLabel();
    JLabel JLtotal = new JLabel();
    private JNumberFormatField JTsubTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JTtotal = new JNumberFormatField(new DecimalFormat("0.00"));
    Converte convert = new Converte();
   
    public PainelResultado(){
      setLayout(null);
      setSize(180, 270); // larg, al
      this.setLocation(600, 30);
      this.setBackground(Color.white);
      this.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
      
        
        JLsubTotal.setBounds(new Rectangle(168, 135, 400, 30));
        JLsubTotal.setText("Sub Total");
        JLsubTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        JLsubTotal.setLocation(10, 20);
        add(JLsubTotal);
        
        
        JTsubTotal.setBounds(new Rectangle(200, 135, 120, 17));
        JTsubTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTsubTotal.setSize(160, 58);
        JTsubTotal.setLocation(10, 70);//
        add(JTsubTotal);
        
        
        JLtotal.setBounds(new Rectangle(168, 135, 400, 50));
        JLtotal.setText("Total");
        JLtotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 50));
        JLtotal.setLocation(10, 130);
        add(JLtotal);
        
        
        JTtotal.setBounds(new Rectangle(200, 135, 120, 17));
        JTtotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTtotal.setSize(160, 50);
        JTtotal.setLocation(10,180 );//
        add(JTtotal);
        
    }
   public void ColocaNoSubTotal(String Valor){
     JTsubTotal.setText(Valor);
   } 
   
   public void ColocarNoTotal(String Total){
     JTtotal.setText(Total);
   }
    public float RetornaTotal() {
        return convert.StringEmFloat(JTtotal.getText());
    }

    public void LimpaTxt() {
        JTsubTotal.setText(null);
    JTtotal.setText(null);
   
   }
}
