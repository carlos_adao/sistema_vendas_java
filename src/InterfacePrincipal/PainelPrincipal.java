
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacePrincipal;

import Auxiliares.Converte;
import BancoDados.bdProdutos;
import Auxiliares.Datas;
import Compras.P_CompraNota;
import Compras.Visualizar.P_CompraNotasVisualizar;
import Entradas.Calculos_Entradas;
import Gastos.Calculos;
import Gastos.P_Gastos;
import Gastos.P_exibeGastosMes;
import Gastos.p_ExibeGastos;
import Gerando_pdf.Painel_etiquetas;
import InterfaceProduto.PainelTopo;
import Interface_Alternavel.*;
import com.itextpdf.text.DocumentException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.*;

public final class PainelPrincipal extends JFrame {

    JRadioButtonMenuItem caixaItem[], caixa[];
    JRadioButtonMenuItem comprasItem[], Compras[];
    JRadioButtonMenuItem estoqueItem[], estoque[];
    JRadioButtonMenuItem gastosItem[], gastos[];
    JRadioButtonMenuItem consutaItem[], consuta[];
    JRadioButtonMenuItem visualizarItem[], visualizar[];
    private ButtonGroup comprasGroup, estoqueGroup, caixaGroup, consutaGroup, visualizarGroup, gastosGrup;
    JButton JbCancela, Jbsalvar;
    private int largura;
    private int altura;
    private bdProdutos bdpod = new bdProdutos();
    private Container container;
    private AlternavelCadastroProduto Alternavel = new AlternavelCadastroProduto();
    private AlternavelEst AlternavelEst = new AlternavelEst();
    static Alternavel_Caixa_Consuta AlternavelCaixaConsuta = new Alternavel_Caixa_Consuta();
    static PainelAlternavelVisualizacao painel_alternavel_visualizacao = new PainelAlternavelVisualizacao();//painel para visualizacão de dados 
    static PainelAltenavelCaixa AlternavelCaixa = new PainelAltenavelCaixa();
    private PainelPricipal Principal = new PainelPricipal();
    private PainelTopo Topo = new PainelTopo();
    private Roda rodape = new Roda();
    private Alternavel_Compras_Consuta Alternavel_Compras = new Alternavel_Compras_Consuta();
    private P_CompraNotasVisualizar p_compraNotasVisualizar = new P_CompraNotasVisualizar();
    // private PainelVisualizacaoCompraNota P_VisualizacaoCompraNota = new PainelVisualizacaoCompraNota();
    private p_ExibeGastos p_exibeGastos = new p_ExibeGastos();
    private P_exibeGastosMes P_exibe_gastosMes = new P_exibeGastosMes();
    private Alternavel_Caixa_ConsutaMes AlternavelCaixaConsutaMes = new Alternavel_Caixa_ConsutaMes();
    private Alternavel_Caixa_ConsutaAno AlternavelCaixaConsutaAno = new Alternavel_Caixa_ConsutaAno();
    private Alternavel_Adc_ProdEstoque Alternavel_Adc_ProdEstoque = new Alternavel_Adc_ProdEstoque();
    private Datas datas = new Datas();
    private P_CompraNota p_compraNota = new P_CompraNota();
    private P_Gastos painel_gastos = new P_Gastos();
    private final Painel_etiquetas Painel_etiquetas;
    JRadioButton JR_dia;
    Calculos Calculos = new Calculos();
    Converte convert = new Converte();

    int Selec_dt_ms_an;

    public PainelPrincipal() throws DocumentException, IOException {
        this.Painel_etiquetas = new Painel_etiquetas();

        InicializaMenu();

        container = getContentPane();
        this.setTitle("Box Do José Albertino");
        this.setLayout(null);
        this.largura = 1024;
        this.altura = 726;
        this.setSize(largura, altura);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize(); // Captura a Dimentao Atual da Tela do PC
        this.setLocation((TamTela.width - largura) / 2, (TamTela.height - altura) / 2);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // DEVE FICA NAR JANELA PRINCIPAL

        this.container.add(Principal);
        this.Principal.add(painel_gastos);
        this.Principal.add(p_compraNota);
        this.Principal.add(AlternavelEst);
        this.Principal.add(Topo);
        this.Principal.add(Alternavel);
        this.Principal.add(AlternavelCaixaConsuta);
        this.Principal.add(AlternavelCaixaConsutaMes);
        this.Principal.add(AlternavelCaixaConsutaAno);
        this.Principal.add(painel_alternavel_visualizacao);
        this.Principal.add(AlternavelCaixa);
        this.Principal.add(Alternavel_Compras);
        this.Principal.add(p_compraNotasVisualizar);
        this.Principal.add(p_exibeGastos);
        this.Principal.add(P_exibe_gastosMes);
        this.Principal.add(Alternavel_Adc_ProdEstoque);
        this.Principal.add(Painel_etiquetas);
        this.Principal.add(rodape);

        Principal.addKeyListener(AlternavelEst.teclado);
        painel_gastos.addKeyListener(AlternavelEst.teclado);
        p_compraNota.addKeyListener(AlternavelEst.teclado);
        AlternavelEst.addKeyListener(AlternavelEst.teclado);
        Topo.addKeyListener(AlternavelEst.teclado);
        Alternavel.addKeyListener(AlternavelEst.teclado);
        AlternavelCaixaConsuta.addKeyListener(AlternavelEst.teclado);
        AlternavelCaixa.addKeyListener(AlternavelEst.teclado);
        Alternavel_Compras.addKeyListener(AlternavelEst.teclado);
        p_compraNotasVisualizar.addKeyListener(AlternavelEst.teclado);
        p_exibeGastos.addKeyListener(AlternavelEst.teclado);
        P_exibe_gastosMes.addKeyListener(AlternavelEst.teclado);
        AlternavelCaixaConsutaMes.addKeyListener(AlternavelEst.teclado);
        AlternavelCaixaConsutaAno.addKeyListener(AlternavelEst.teclado);

        Alternavel_Adc_ProdEstoque.setVisible(false);
        p_compraNotasVisualizar.setVisible(false);
        p_exibeGastos.setVisible(false);
        p_compraNotasVisualizar.setVisible(false);
        Alternavel_Compras.setVisible(false);
        painel_gastos.setVisible(false);
        P_exibe_gastosMes.setVisible(false);
        p_compraNota.setVisible(false);
        painel_alternavel_visualizacao.setVisible(false);
        AlternavelCaixaConsutaMes.setVisible(false);
        AlternavelCaixaConsutaAno.setVisible(false);
        Painel_etiquetas.setVisible(false);

    }

    private class PainelPricipal extends JPanel { // Primeiro Painel

        public PainelPricipal() {
            this.setLayout(null);
            this.setSize(1024, 726); // larg, alt
            this.setLocation(0, 0);
            

            AlternavelCaixaConsuta.setVisible(false);
            AlternavelCaixa.setVisible(false);
            Alternavel.setVisible(false);
            AlternavelEst.setVisible(false);

            PrimeiroDiaMes(2015, 01);

        }
    }

    public void CapturaEntrada() {
        Calculos_Entradas Calculos_Entradas = new Calculos_Entradas();
        Calculos_Entradas.RetornaTodasEntradas(datas.retornaData(), 1);
        float entrada = convert.StringEmFloat(Calculos_Entradas.RetornaEntradas());

        if (entrada > 0) {
        } else {

        }
    }

    public void PrimeiroDiaMes(Integer Ano, Integer Mes) {
        Calendar cal = new GregorianCalendar(Ano, Mes - 1, 1);

    }

    public void InicializaMenu() {
        JMenuBar bar = new JMenuBar();
        setJMenuBar(bar);

        JMenu caixa = new JMenu("  Caixa            ");
        caixa.setMnemonic('z');
        caixa.setBackground(Color.BLACK);
        caixa.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));

        String opcoesCaixa[] = {"Abrir Caixa             ", "Fechar Caixa               "};

        JMenu caixaMenu = new JMenu("Opcões                ");
        caixaMenu.setMnemonic('y');
        caixaMenu.setBackground(Color.BLACK);
        caixaMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        caixaItem = new JRadioButtonMenuItem[opcoesCaixa.length];
        caixaGroup = new ButtonGroup();
        AcaoCaixa acaoCaixa = new AcaoCaixa();

        for (int j = 0; j < opcoesCaixa.length; j++) {
            caixaItem[j] = new JRadioButtonMenuItem(opcoesCaixa[j]);
            caixaMenu.add(caixaItem[j]);
            caixaGroup.add(caixaItem[j]);
            caixaItem[j].addActionListener(acaoCaixa);

        }

        String Consutar[] = {"VENDAS DIA   ", "VENDAS MES   ", "VENDAS ANO   ", "BALANCO"};

        JMenu ConsutarMenu = new JMenu("Visualizar");
        ConsutarMenu.setMnemonic('k');
        ConsutarMenu.setBackground(Color.BLACK);
        ConsutarMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        consutaItem = new JRadioButtonMenuItem[Consutar.length];
        consutaGroup = new ButtonGroup();
        AcaoConsutar acaoConsutar = new AcaoConsutar();

        for (int k = 0; k < Consutar.length; k++) {
            consutaItem[k] = new JRadioButtonMenuItem(Consutar[k]);
            ConsutarMenu.add(consutaItem[k]);
            consutaGroup.add(consutaItem[k]);
            consutaItem[k].addActionListener(acaoConsutar);
        }

        JMenu test = new JMenu("                                                                                                                                                                                                            ");
        JMenu formatMenu = new JMenu("Compras          ");
        formatMenu.setMnemonic('r');
        formatMenu.setBackground(Color.BLACK);
        formatMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));

        String Compras[] = {"Em Dinheiro               ", "Em Cheque               ", "Em Nota               "};
        JMenu CompraMenu = new JMenu("Iniciar                    ");
        CompraMenu.setMnemonic('c');
        CompraMenu.setBackground(Color.BLACK);
        CompraMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        comprasItem = new JRadioButtonMenuItem[Compras.length];
        comprasGroup = new ButtonGroup();
        ItemHandler itemHandler = new ItemHandler();

        for (int i = 0; i < Compras.length; i++) {
            comprasItem[i] = new JRadioButtonMenuItem(Compras[i]);
            CompraMenu.add(comprasItem[i]);
            comprasGroup.add(comprasItem[i]);
            comprasItem[i].addActionListener(itemHandler);
        }

        String[] visualizar = {"Compra dia", "Compras mes  ", "Compras ano  ", "Compras Cheque aberto  ", "Compras nota aberto   "};
        JMenu visualizarMenu = new JMenu("Visualizar          ");
        visualizarMenu.setMnemonic('l');
        visualizarMenu.setBackground(Color.BLACK);
        visualizarMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));

        visualizarItem = new JRadioButtonMenuItem[visualizar.length];
        visualizarGroup = new ButtonGroup();
        AcaoVisualizar acaoVisualizar = new AcaoVisualizar();

        for (int i = 0; i < visualizar.length; i++) {
            visualizarItem[i] = new JRadioButtonMenuItem(visualizar[i]);
            visualizarMenu.add(visualizarItem[i]);
            visualizarGroup.add(visualizarItem[i]);
            visualizarItem[i].addActionListener(acaoVisualizar);
        }

        JMenu estoqueMenu = new JMenu("Estoque         ");
        estoqueMenu.setMnemonic('r');
        estoqueMenu.setBackground(Color.BLACK);
        estoqueMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));

        String estoque[] = {"Visualizar               ", "Edita               "};
        JMenu EstoqueMenu = new JMenu("Opcões                    ");
        EstoqueMenu.setMnemonic('c');
        EstoqueMenu.setBackground(Color.BLACK);
        EstoqueMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        estoqueItem = new JRadioButtonMenuItem[estoque.length];
        estoqueGroup = new ButtonGroup();
        AcaoEstoque acaoEstoque = new AcaoEstoque();

        for (int i = 0; i < estoque.length; i++) {
            estoqueItem[i] = new JRadioButtonMenuItem(estoque[i]);
            EstoqueMenu.add(estoqueItem[i]);
            estoqueGroup.add(estoqueItem[i]);
            estoqueItem[i].addActionListener(acaoEstoque);

        }

        //MENU GASTOS
        JMenu gastosMenu = new JMenu("Gastos         ");
        gastosMenu.setMnemonic('f');
        gastosMenu.setBackground(Color.BLACK);
        gastosMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));

        String gastos[] = {"CADASTRAR                ", "VISUALIZAR DIA               ", "VISUALIZAR MES               ", "VISUALIZAR ANO               ", "VISUALIZAR POR USUARIO               "};
        JMenu GastosMenu = new JMenu("Opcões                    ");
        GastosMenu.setMnemonic('d');
        GastosMenu.setBackground(Color.BLACK);
        GastosMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        gastosItem = new JRadioButtonMenuItem[gastos.length];
        gastosGrup = new ButtonGroup();
        AcaoGastos acaoGastos = new AcaoGastos();

        for (int j = 0; j < gastos.length; j++) {
            gastosItem[j] = new JRadioButtonMenuItem(gastos[j]);
            GastosMenu.add(gastosItem[j]);
            gastosGrup.add(gastosItem[j]);
            gastosItem[j].addActionListener(acaoGastos);
        }

        caixa.add(caixaMenu);
        caixa.add(ConsutarMenu);
        formatMenu.add(CompraMenu);
        formatMenu.add(visualizarMenu);
        estoqueMenu.add(EstoqueMenu);
        gastosMenu.add(GastosMenu);

        bar.add(caixa);
        bar.add(formatMenu);
        bar.add(estoqueMenu);
        bar.add(gastosMenu);
        bar.add(test);

    }

    class AcaoGastos implements ActionListener {

        Datas Data = new Datas();

        @Override
        /*
         * MENU DE GASTOS
         */
        public void actionPerformed(ActionEvent e) {
            int j = 0;
            for (int i = 0; i < gastosItem.length; i++) {

                if (gastosItem[0].isSelected()) {
                    Alternavel.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(true);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                }

                if (gastosItem[1].isSelected() && j == 0) {

                    Alternavel.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Selec_dt_ms_an = 1;
                    p_exibeGastos.Funcionamento(Selec_dt_ms_an, datas.retornaData(), datas.CapturaDiaDaSemana(datas.retornaData()), Calculos.DiasMesVendas());
                    p_exibeGastos.setVisible(true);

                }

                if (gastosItem[2].isSelected() && j == 0) {
                    Alternavel.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Selec_dt_ms_an = 2;
                    p_exibeGastos.Funcionamento(Selec_dt_ms_an, datas.retornaMes(), datas.CapturaNomeMesAno(datas.retornaMesInteiro()), Calculos.MesAnoVendas());
                    p_exibeGastos.setVisible(true);
                }
                if (gastosItem[3].isSelected() && j == 0) {
                    Alternavel.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Selec_dt_ms_an = 3;
                    p_exibeGastos.Funcionamento(Selec_dt_ms_an, datas.retornaAno(), datas.retornaAno(), Calculos.RetornaAnosVendas());
                    p_exibeGastos.setVisible(true);
                }

                j++;
            }

        }
    }

    class ItemHandler implements ActionListener {

        @Override
        /*
         * MENU COMPRAS
         */
        public void actionPerformed(ActionEvent e) {
            int j = 0;
            for (int i = 0; i < comprasItem.length; i++) {
                if (comprasItem[0].isSelected() && j == 0) {
                    Alternavel.setVisible(true);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);
                }
                if (comprasItem[2].isSelected() && j == 0) {
                    //
                    Alternavel.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    painel_gastos.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    p_compraNota.setVisible(true);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);
                }
                if (comprasItem[2].isSelected() && j == 0) {
                }
                j++;
            }
        }
    }

    class AcaoVisualizar implements ActionListener {

        @Override
        /*
         * ALTERNAVEL COMRA VISUALIZAR
         */
        public void actionPerformed(ActionEvent e) {
            int j = 0;
            for (int i = 0; i < visualizarItem.length; i++) {
                //VIZUALIZA COMPRA DIA 
                if (visualizarItem[0].isSelected() && j == 0) {
                    Alternavel_Compras.setVisible(true);
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Selec_dt_ms_an = 1;
                    Alternavel_Compras.Funcionamento(Selec_dt_ms_an, datas.retornaData(), datas.CapturaDiaDaSemana(datas.retornaData()), Calculos.DiasMesVendas());
                    Alternavel_Compras.setVisible(true);
                }
                //VIZUALIZA COMPRA MÊS
                if (visualizarItem[1].isSelected() && j == 0) {
                    Alternavel_Compras.setVisible(true);
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);
                    Selec_dt_ms_an = 2;
                    Alternavel_Compras.Funcionamento(Selec_dt_ms_an, datas.retornaMes(), datas.CapturaNomeMesAno(datas.retornaMesInteiro()), Calculos.MesAnoVendas());
                    Alternavel_Compras.setVisible(true);
                }
                //VIZUALIZA COMPRA ANO
                if (visualizarItem[2].isSelected() && j == 0) {
                    Alternavel_Compras.setVisible(true);
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Compras.setVisible(true);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Alternavel_Compras.Funcionamento(Selec_dt_ms_an, datas.retornaAno(), datas.retornaAno(), Calculos.RetornaAnosVendas());
                    Alternavel_Compras.setVisible(true);

                }

                //VIZUALIZA COMPRA EM NOTAS
                if (visualizarItem[4].isSelected() && j == 0) {//DEIXA VISIVEL O PAINEL COMPRAS NOTAS AGUARDANDO PAGAMENTO
                    Alternavel_Compras.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    p_compraNotasVisualizar.criaTabela();
                    p_compraNotasVisualizar.setVisible(true);

                }
                j++;
            }
        }
    }

    class AcaoEstoque implements ActionListener {

        @Override
        /*
         * MENU ESTOQUE
         */
        public void actionPerformed(ActionEvent e) {
           
            int j = 0;
            for (int i = 0; i < estoqueItem.length; i++) {
                if (estoqueItem[0].isSelected() && j == 0) {
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    painel_gastos.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    AlternavelEst.remove();
                    AlternavelEst.criaTabela();
                    AlternavelEst.setVisible(true);

                }
                //DEIXA VISIVEL O PAINEL DE ADICIONAR PRODUTO AO ESTOQUE       
                if (estoqueItem[1].isSelected() && j == 0) {
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    painel_gastos.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Painel_etiquetas.setVisible(true);
                    

                    //Alternavel_Adc_ProdEstoque.setVisible(true);
                }
                j++;

            }
        }
    }

    class AcaoCaixa implements ActionListener {

        @Override
        /*
         * MENU CAIXA
         */
        public void actionPerformed(ActionEvent e) {
            int j = 0;
            for (int i = 0; i < caixaItem.length; i++) {

                if (caixaItem[0].isSelected() && j == 0) {

                    AlternavelCaixaConsuta.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    AlternavelCaixa.setVisible(true);
                    AlternavelCaixa.ArmazenaDinheiroEntrada();
                    AlternavelCaixa.AtualizaListaProdutosAutoComplete();
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                }
                j++;
            }
        }
    }

    class AcaoConsutar implements ActionListener {

        @Override
        /*
         * MENU CAIXA CONSUTA
         */
        public void actionPerformed(ActionEvent e) {
            int j = 0;
            for (int i = 0; i < consutaItem.length; i++) {
                if (consutaItem[0].isSelected() && j == 0) {
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);
                    Selec_dt_ms_an = 1;
                    AlternavelCaixaConsuta.Funcionamento(Selec_dt_ms_an, datas.retornaData(), datas.CapturaDiaDaSemana(datas.retornaData()), Calculos.DiasMesVendas());

                    AlternavelCaixaConsuta.setVisible(true);

                }

                if (consutaItem[1].isSelected() && j == 0) {

                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    AlternavelCaixaConsutaAno.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    Selec_dt_ms_an = 2;

                    AlternavelCaixaConsuta.Funcionamento(Selec_dt_ms_an, datas.retornaMes(), datas.CapturaNomeMesAno(datas.retornaMesInteiro()), Calculos.MesAnoVendas());

                    AlternavelCaixaConsuta.setVisible(true);

                }

                if (consutaItem[2].isSelected() && j == 0) {

                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    painel_alternavel_visualizacao.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    AlternavelCaixaConsuta.Funcionamento(Selec_dt_ms_an, datas.retornaAno(), datas.retornaAno(), Calculos.RetornaAnosVendas());
                    AlternavelCaixaConsuta.setVisible(true);

                }

                if (consutaItem[3].isSelected() && j == 0) {
                    AlternavelCaixa.setVisible(false);
                    Alternavel.setVisible(false);
                    AlternavelCaixaConsuta.setVisible(false);
                    AlternavelEst.setVisible(false);
                    Alternavel_Compras.setVisible(false);
                    painel_gastos.setVisible(false);
                    p_compraNota.setVisible(false);
                    p_compraNotasVisualizar.setVisible(false);
                    p_exibeGastos.setVisible(false);
                    P_exibe_gastosMes.setVisible(false);
                    AlternavelCaixaConsutaMes.setVisible(false);
                    Alternavel_Adc_ProdEstoque.setVisible(false);
                    Painel_etiquetas.setVisible(false);

                    painel_alternavel_visualizacao.FormataDadosParaTabela();
                    painel_alternavel_visualizacao.setVisible(true);

                }
                j++;
            }
        }
    }
}
