/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entradas;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_ENTRADAS;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author carlos
 */
public class Calculos_Entradas {

    ArrayList<ENTRADAS> ListaEntradas_saidas = new ArrayList<>();
    ENTRADAS Entrada = new ENTRADAS();
    float tot_entrada = 0;
    float tot_saida = 0;
    Converte convert = new Converte();

    public void SalvaArquivo(){
         //JOptionPane.showInputDialog("Digite o valor de entrada");
        try {
            ARQUIVO_ENTRADAS.Armazena(ListaEntradas_saidas);
        } catch (IOException ex) {
            Logger.getLogger(Calculos_Entradas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void RetornaTodasEntradas(String consutar, int opcao) {
        tot_entrada = 0;
        tot_saida = 0;
        ListaEntradas_saidas = ARQUIVO_ENTRADAS.retorna();

        if (ListaEntradas_saidas.size() > 0) {
            
            for (int i = 0; i < ListaEntradas_saidas.size(); i++) {
                Entrada = (ENTRADAS) ListaEntradas_saidas.get(i);

                if (opcao == 1) {
                    
                    if (Entrada.getDia().equalsIgnoreCase(consutar)) {
                        
                        tot_entrada =  Entrada.getValorEntrada();
                        tot_saida =  Entrada.getValorSaida();
                    }
                }
            }

        }
    }
   
    public String RetornaEntradas(){
        
        return convert.FloatEmString(tot_entrada);
    }
    
    public String RetornaSaida(){
    
      return convert.FloatEmString(tot_saida);
    }
}
