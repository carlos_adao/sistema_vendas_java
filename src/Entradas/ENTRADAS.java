/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entradas;

/**
 *
 * @author carlos
 */
public class ENTRADAS {
     int primaryKey;
     float valorEntrada, valorSaida;
     String dia;
     String mes;
     String ano;
     boolean Verf_valorEntrada, Verf_valorSaida;

    public ENTRADAS(int primaryKey, float valorEntrada, float valorSaida, String dia, String mes, String ano, boolean Verf_valorEntrada, boolean Verf_valorSaida) {
        this.primaryKey = primaryKey;
        this.valorEntrada = valorEntrada;
        this.valorSaida = valorSaida;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.Verf_valorEntrada = Verf_valorEntrada;
        this.Verf_valorSaida = Verf_valorSaida;
    }
     

   public ENTRADAS() {
      
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public float getValorEntrada() {
        return valorEntrada;
    }

    public void setValorEntrada(float valorEntrada) {
        this.valorEntrada = valorEntrada;
    }

    public float getValorSaida() {
        return valorSaida;
    }

    public void setValorSaida(float valorSaida) {
        this.valorSaida = valorSaida;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public boolean isVerf_valorEntrada() {
        return Verf_valorEntrada;
    }

    public void setVerf_valorEntrada(boolean Verf_valorEntrada) {
        this.Verf_valorEntrada = Verf_valorEntrada;
    }

    public boolean isVerf_valorSaida() {
        return Verf_valorSaida;
    }

    public void setVerf_valorSaida(boolean Verf_valorSaida) {
        this.Verf_valorSaida = Verf_valorSaida;
    }

   
    @Override
    public String toString() {
        return ";" + primaryKey + ";" + valorEntrada + ";" + valorSaida + ";" + dia + ";" + mes + ";" + ano +";"+ Verf_valorEntrada+";" + Verf_valorSaida+";"+"\n";
    }

   
    
}
