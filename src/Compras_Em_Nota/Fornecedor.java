
package Compras_Em_Nota;

public class Fornecedor {
    int codigoFornecedor;
    String nomeFonecedor, Telefone;

    public Fornecedor(int codigoFornecedor, String nomeFonecedor, String Telefone) {
        this.codigoFornecedor = codigoFornecedor;
        this.nomeFonecedor = nomeFonecedor;
        this.Telefone = Telefone;
    }

    public int getCodigoFornecedor() {
        return codigoFornecedor;
    }

    public void setCodigoFornecedor(int codigoFornecedor) {
        this.codigoFornecedor = codigoFornecedor;
    }

    public String getNomeFonecedor() {
        return nomeFonecedor;
    }

    public void setNomeFonecedor(String nomeFonecedor) {
        this.nomeFonecedor = nomeFonecedor;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    @Override
    public String toString() {
        return codigoFornecedor + "<" + nomeFonecedor + "<" + Telefone + "<" + "<<" ;
    }
    
}
