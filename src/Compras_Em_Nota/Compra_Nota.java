
package Compras_Em_Nota;

import java.util.ArrayList;

public class Compra_Nota {
    int codigoCompraNota;
    Fornecedor fornecedor;
    float valorTotal;
    ArrayList<Nota> ListaNotas;

    public Compra_Nota(int codigoCompraNota, Fornecedor fornecedo, float valorTotal, ArrayList<Nota> ListaNotas) {
        this.codigoCompraNota = codigoCompraNota;
        this.fornecedor = fornecedo;
        this.valorTotal = valorTotal;
        this.ListaNotas = ListaNotas;
    }

    public int getCodigoCompraNota() {
        return codigoCompraNota;
    }

    public void setCodigoCompraNota(int codigoCompraNota) {
        this.codigoCompraNota = codigoCompraNota;
    }

    public Fornecedor getFornecedo() {
        return fornecedor;
    }

    public void setFornecedo(Fornecedor fornecedo) {
        this.fornecedor = fornecedo;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public ArrayList<Nota> getListaNotas() {
        return ListaNotas;
    }

    public void setListaNotas(ArrayList<Nota> ListaNotas) {
        this.ListaNotas = ListaNotas;
    }

    @Override
    public String toString() {
        return  codigoCompraNota + ":" + fornecedor + ":" + valorTotal + ":" + ListaNotas + ":"+"\n";
    }
    
    
}
