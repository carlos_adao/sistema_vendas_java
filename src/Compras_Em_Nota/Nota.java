
package Compras_Em_Nota;


import Interface_Alternavel.PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO;
import java.util.ArrayList;

public class Nota {
    int numeroNota;
    float valorNota;
    String dataLimitePagamento;
    ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutos = new ArrayList<>();            

    public Nota(int numeroNota, float valorNota, String dataLimitePagamento) {
        this.numeroNota = numeroNota;
        this.valorNota = valorNota;
        this.dataLimitePagamento = dataLimitePagamento;
    }

    public int getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(int numeroNota) {
        this.numeroNota = numeroNota;
    }

    public float getValorNota() {
        return valorNota;
    }

    public void setValorNota(float valorNota) {
        this.valorNota = valorNota;
    }

    public String getDataLimitePagamento() {
        return dataLimitePagamento;
    }

    public void setDataLimitePagamento(String dataLimitePagamento) {
        this.dataLimitePagamento = dataLimitePagamento;
    }

    public ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> getListaProdutos() {
        return ListaProdutos;
    }

    public void setListaProdutos(ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutos) {
        this.ListaProdutos = ListaProdutos;
    }

    @Override
    public String toString() {
        return numeroNota + "*" + valorNota + "*" + dataLimitePagamento + "*" + ListaProdutos + "*" + "**";
    }
    
}
