/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_vendas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class TableTest {
    
    

    public static void main(String[] args) {
       TableTest TableTest = new TableTest();
        TableTest.Criar();
    }
    public void Criar() {

        Object[] columns = new Object[]{"Num", "BR", "EN"};
        JTable tbl = new JTable(
                new Object[][]{
                    {5, "Cinco", "Five"},
                    {7, "Sete", "Seven"},
                    {2, "Dois", "Two"},
                    {0, "Zero", "Zero"},},
                columns
        );

        TableCellRenderer renderer = new LineSelectionTableCellRenderer();

        for (int c = 0; c < tbl.getColumnCount(); c++) {
            tbl.setDefaultRenderer(tbl.getColumnClass(c), renderer);
        }

        JFrame dlg = new JFrame("Teste");
        Container c = dlg.getContentPane();
        c.add(tbl, BorderLayout.CENTER);
        dlg.setSize(300, 300);
        dlg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dlg.setVisible(true);
    
    }


    public class LineSelectionTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(
                JTable table,
                Object value,
                boolean isSelected,
                boolean hasFocus,
                int row,
                int column) {
            Component result = super.getTableCellRendererComponent(
                    table,
                    value,
                    isSelected,
                    hasFocus,
                    row,
                    column
            );

            if (isSelected) {
                result.setFont(new Font("arial", Font.BOLD, 12));
                result.setForeground(Color.white);
                result.setBackground(Color.blue);
            } else {
                result.setFont(new Font("arial", Font.PLAIN, 12));
                result.setForeground(Color.gray);
                result.setBackground(Color.black);
            }
            return result;
        }

    }
}
