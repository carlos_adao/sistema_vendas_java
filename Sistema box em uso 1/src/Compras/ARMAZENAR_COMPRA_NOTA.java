/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;

/**
 *
 * @author User
 */
public class ARMAZENAR_COMPRA_NOTA {
    String formecedor;
    String diaArmazenamento;
    String dataArmazenamento;
    String obcervacao;
    float valorArmazenado;

    public ARMAZENAR_COMPRA_NOTA(String formecedor, String diaArmazenamento, String dataArmazenamento, String obcervacao, float valorArmazenado) {
        this.formecedor = formecedor;
        this.diaArmazenamento = diaArmazenamento;
        this.dataArmazenamento = dataArmazenamento;
        this.obcervacao = obcervacao;
        this.valorArmazenado = valorArmazenado;
    }

    public ARMAZENAR_COMPRA_NOTA() {
      
    }

    public String getDataArmazenamento() {
        return dataArmazenamento;
    }

    public void setDataArmazenamento(String dataArmazenamento) {
        this.dataArmazenamento = dataArmazenamento;
    }

    public String getDiaArmazenamento() {
        return diaArmazenamento;
    }

    public void setDiaArmazenamento(String diaArmazenamento) {
        this.diaArmazenamento = diaArmazenamento;
    }

    public String getFormecedor() {
        return formecedor;
    }

    public void setFormecedor(String formecedor) {
        this.formecedor = formecedor;
    }

    public String getObcervacao() {
        return obcervacao;
    }

    public void setObcervacao(String obcervacao) {
        this.obcervacao = obcervacao;
    }

    public float getValorArmazenado() {
        return valorArmazenado;
    }

    public void setValorArmazenado(float valorArmazenado) {
        this.valorArmazenado = valorArmazenado;
    }

    @Override
    public String toString() {
        return formecedor + ";" + diaArmazenamento + ";" + dataArmazenamento + ";" + obcervacao + ";" + valorArmazenado + ";" + "\n";
    }
    
    
}
