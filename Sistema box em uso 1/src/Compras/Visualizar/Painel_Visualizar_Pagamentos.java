/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Compras.PAGAMENTO_NOTA;
import Interface_Alternavel.AlternavelCadastroProduto;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.ListSelectionModel;

/**
 *
 * @author carlos
 */
public final class Painel_Visualizar_Pagamentos extends JPanel {

    TabelaModel modelo;
    JScrollPane JSpagamentosEfetuados;
    RemoverPagamentosDuplicados RemoverPagamentosDuplicados = new RemoverPagamentosDuplicados();
    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();

    Painel_Visualizar_Pagamentos() {

        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);

    }

    public void CriaTabelaExibePagamentos(ArrayList<String[]> ListaPagamentosEfetuados) {
        RemoverPagamentosDuplicados.RemoverPagamentos();

        String[] colunas = new String[]{"Fornecedor", "Dia Pag.", "Data Pag", "Q.recebeu", "Valor pag."};
        modelo = new TabelaModel(ListaPagamentosEfetuados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(100);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtable.addMouseListener(new MouseAdapter() {

            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 2) {

                    linha = jtable.getSelectedRow();

                    // removePagamento( jtable.getSelectedRow());                 
                }
            }
        });

        JSpagamentosEfetuados = new JScrollPane(jtable);
        JSpagamentosEfetuados.setLocation(10, 10);
        JSpagamentosEfetuados.getVerticalScrollBar();
        JSpagamentosEfetuados.setSize(700, 150);
        JSpagamentosEfetuados.revalidate();
        add(JSpagamentosEfetuados);

    }

    public void removePagamento(int posicao) {
        ListaPagamentos.remove(ListaPagamentos.get(posicao));

        try {
            ARQUIVO_PAGAMENTO_NOTA.Armazena(ListaPagamentos);
            JOptionPane.showMessageDialog(null, "Pagamento removido com Sucesso!!!");
        } catch (IOException ex) {
            Logger.getLogger(P_CompraNotasVisualizar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
