package Compras.Visualizar;

import Auxiliares.Converte;
import Auxiliares.Mascara_Data;
import BancoDados.ARQUIVO_ARMAZENAR_COMPRA_NOTA;
import Compras.ARMAZENAR_COMPRA_NOTA;
import Interface_Alternavel.ARMAZENA_DINHEIRO;
import Interface_Alternavel.JNumberFormatField;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author carlos
 */
public class Painel_EfetuarArmazenamento extends JPanel {

    /*Instancias da classe visualizar Armazenamentos nota*/
    ARMAZENAR_COMPRA_NOTA ArmazenarPagamentos = new ARMAZENAR_COMPRA_NOTA();

    /*---------------------------------------------------------------*/
    Mascara_Data Mascara_Data = new Mascara_Data();

    JLabel JldiaArmazenamento = new JLabel();
    JComboBox<String> JCdiaArmazenamento;
    private final String opcoes[] = {"Segunda-feira", "Terca-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira ", "Sabado", "Domingo"};
    JLabel JldataArmazenamento = new JLabel();
    JFormattedTextField JtdataArmazenamento = new JFormattedTextField(Mascara_Data.Mascara("##/##/####"));
    JLabel JlvalorArmazanamento = new JLabel();
    JNumberFormatField JtvalorArmazanamento = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel JlObcervacao = new JLabel();
    JTextField JtObcervacao = new JTextField();

    JButton Botao_salvar, Botao_cancelar;

    String Aux_nomeFornecedor;

    Converte Converte = new Converte();

    Acao_BotaoSalvarArmazenamento Acao_BotaoSalvarArmazenamento = new Acao_BotaoSalvarArmazenamento();

    public Painel_EfetuarArmazenamento(String nomeFornecedor) {
        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);
        Aux_nomeFornecedor = nomeFornecedor;
    }

    public void Componentes() {

        JldiaArmazenamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
        JldiaArmazenamento.setText("Dia do Armazenamento");
        JldiaArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldiaArmazenamento.setLocation(20, 20);
        add(JldiaArmazenamento);

        JCdiaArmazenamento = new JComboBox(opcoes);
        JCdiaArmazenamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCdiaArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCdiaArmazenamento.setSize(200, 20);
        JCdiaArmazenamento.setLocation(20, 50);
        add(JCdiaArmazenamento);

        JldataArmazenamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldataArmazenamento.setText("Data Armazenamento");
        JldataArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldataArmazenamento.setLocation(260, 20);
        add(JldataArmazenamento);

        JtdataArmazenamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdataArmazenamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdataArmazenamento.setSize(200, 25);
        JtdataArmazenamento.setLocation(260, 50);
        add(JtdataArmazenamento);

        JlvalorArmazanamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorArmazanamento.setText("Valor do Armazenamento");
        JlvalorArmazanamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorArmazanamento.setLocation(500, 20);
        add(JlvalorArmazanamento);

        JtvalorArmazanamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorArmazanamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorArmazanamento.setSize(200, 25);
        JtvalorArmazanamento.setLocation(500, 50);
        add(JtvalorArmazanamento);

        JlObcervacao.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlObcervacao.setText("Obcervacao");
        JlObcervacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlObcervacao.setLocation(20, 90);
        add(JlObcervacao);

        JtObcervacao.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtObcervacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtObcervacao.setSize(400, 25);
        JtObcervacao.setLocation(20, 120);
        add(JtObcervacao);

        //**Jbutton Salvar**//
        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.addActionListener(Acao_BotaoSalvarArmazenamento);
        // Botao_salvar.addKeyListener(AcaoBotaoSalvar);
        Botao_salvar.setLocation(500, 120);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        //Botao_cancelar.addActionListener(this);
        Botao_cancelar.setLocation(600, 120);
        add(Botao_cancelar);

    }

    public boolean VerificaSeDadosForamDigitados() {
        return !JtdataArmazenamento.getText().isEmpty() && !JtvalorArmazanamento.getText().isEmpty()
                && !JtObcervacao.getText().isEmpty();
    }

    public ARMAZENAR_COMPRA_NOTA CapturaDadosDigitados() {
        ArmazenarPagamentos.setFormecedor(Aux_nomeFornecedor);
        ArmazenarPagamentos.setDiaArmazenamento(JCdiaArmazenamento.getSelectedItem().toString());
        ArmazenarPagamentos.setDataArmazenamento(JtdataArmazenamento.getText());
        ArmazenarPagamentos.setObcervacao(JtObcervacao.getText());
        ArmazenarPagamentos.setValorArmazenado(Converte.StringEmFloat(JtvalorArmazanamento.getText()));

        return ArmazenarPagamentos;
    }
    public void LimpaCampos(){
     JtdataArmazenamento.setText(null);
     JtObcervacao.setText(null);
     JtvalorArmazanamento.setText(null);
    
    }

    public void ArquivaRArmazenamento(ArrayList<ARMAZENAR_COMPRA_NOTA> ListaarmazenarPagamento) throws IOException {
        ARQUIVO_ARMAZENAR_COMPRA_NOTA.Armazena(ListaarmazenarPagamento);
    }

    public class Acao_BotaoSalvarArmazenamento implements ActionListener, KeyListener {

        ArrayList<ARMAZENAR_COMPRA_NOTA> ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == Botao_salvar) {
                if (VerificaSeDadosForamDigitados()) {

                    ListaarmazenarPagamento.add(CapturaDadosDigitados());

                    try {
                        ArquivaRArmazenamento(ListaarmazenarPagamento);
                    } catch (IOException ex) {
                        Logger.getLogger(Painel_EfetuarArmazenamento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  LimpaCampos();    
                }
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

    }
}
