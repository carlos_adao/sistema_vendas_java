/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author carlos
 */
public class Painel_Visualizar_Produtos extends JPanel {
   //  JSprodutos;
    
    public Painel_Visualizar_Produtos() {
        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);

    }

    public void CriaTabelaProdutos(ArrayList<String[]> ListaProdutosComprados) {
        String[] colunas = new String[]{"Quant prod.", "Código", "Nome Prod.", "Compra uni.", "Compra Ger.", "Venda uni.", "Lucro uni.", "Lucro Ger."};
        TabelaModel modelo = new TabelaModel(ListaProdutosComprados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(15);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(15);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(100);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(30);

        TableColumn coluna = null;

        add(jtable);

        JScrollPane JSprodutos = new JScrollPane(jtable);
        JSprodutos.setLocation(10, 20);//lado outro & cima baixo
        JSprodutos.setSize(700, 150);
        add(JSprodutos);
    }
}
