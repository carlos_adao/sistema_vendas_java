/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import Auxiliares.Converte;
import Auxiliares.Mascara_Data;
import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Cadastro.Datas;
import Compras.COMPRA_NOTA;
import Compras.PAGAMENTO_NOTA;
import Interface_Alternavel.JNumberFormatField;
import Interface_Alternavel.PRODUTO_COMPRA;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author carlos
 */
public final class Painel_EfetuarPagamento extends JPanel {
    /*Manipulacao de mascara de data*/

    Mascara_Data Mascara_Data = new Mascara_Data();

    /*Componentes graficos Label */
    JLabel JldiaPagamento = new JLabel();
    JLabel JldataPagamento = new JLabel();
    JLabel JlvalorPagamento = new JLabel();
    JLabel JlquemRecebeu = new JLabel();

    /*Componentes graficos Textefield */
    JComboBox<String> JCdiaPagamento;
    private final String opcoes[] = {"Segunda-feira", "Terca-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira ", "Sabado", "Domingo"};

    JFormattedTextField JtdatPagamento = new JFormattedTextField(Mascara_Data.Mascara("##/##/####"));
    JNumberFormatField JtvalorPagamento = new JNumberFormatField(new DecimalFormat("0.00"));
    JTextField JtquemRecebeu = new JTextField();

    /*Componentes graficos botoes */
    JButton Botao_salvar, Botao_cancelar;

    /*Classe que execulta as acoes dos botoes salvar e cancelar*/
    AcaoBotaoSalvar AcaoBotaoSalvar = new AcaoBotaoSalvar();

    /*Classe que instancia um produco compra nota para fazer as manipulacoes*/
    ArrayList<COMPRA_NOTA> ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
    COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    /*----------------------------------------------------*/

    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
    PAGAMENTO_NOTA PAGAMENTO_NOTA = new PAGAMENTO_NOTA();

    /*
     *Classe que instancia painel topo para pegar o nome do fornecedor
     */
    //Painel_Topo Painel_Topo = new Painel_Topo();
    String Aux_nomeFornecedor;// = Painel_Topo.RetornaNomeFornecedor();
    /*----------------------------------------------------*/

    /*
     * CLASSE COMPRA PARA AQUIVA A COMPRA NOTA QUE FOI PAGA
     */
    PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
    ARQUIVO_ESTOQUE_COMPRA ArquivoProdutoCompra = new ARQUIVO_ESTOQUE_COMPRA();
    ArrayList<PRODUTO_COMPRA> ListaProdutoComprados = new ArrayList<>();
    /*----------------------------------------------------*/

    /*
     *Classe data para trabalhar com a data do sistema
     */
    Datas Datas = new Datas();
    /*----------------------------------------------------*/

    /*
     *Classe usada para fazer convercao de tipos diferentes
     */
    Converte Converte = new Converte();
    /*----------------------------------------------------*/

    public Painel_EfetuarPagamento(String NomeFornecedor) {

        this.setLayout(null);
        this.setSize(800, 200); // larg, alt
        this.setLocation(0, 90);
        this.setBackground(Color.WHITE);

        Aux_nomeFornecedor = NomeFornecedor;
        Componentes();

    }

    public void Componentes() {

        JldiaPagamento.setBounds(new java.awt.Rectangle(168, 150, 500, 30));
        JldiaPagamento.setText("Dia Pagamento");
        JldiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldiaPagamento.setLocation(20, 20);
        add(JldiaPagamento);

        JCdiaPagamento = new JComboBox(opcoes);
        JCdiaPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCdiaPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCdiaPagamento.setSize(200, 20);
        JCdiaPagamento.setLocation(20, 50);
        add(JCdiaPagamento);

        JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldataPagamento.setText("Data pagamento");
        JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldataPagamento.setLocation(260, 20);
        add(JldataPagamento);

        JtdatPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdatPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdatPagamento.setSize(200, 25);
        JtdatPagamento.setLocation(260, 50);
        add(JtdatPagamento);

        JlvalorPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorPagamento.setText("Valor pagamento");
        JlvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorPagamento.setLocation(500, 20);
        add(JlvalorPagamento);

        JtvalorPagamento.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorPagamento.setSize(200, 25);
        JtvalorPagamento.setLocation(500, 50);
        add(JtvalorPagamento);

        JlquemRecebeu.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlquemRecebeu.setText("Quem Recebeu Pagamento");
        JlquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlquemRecebeu.setLocation(20, 90);
        add(JlquemRecebeu);

        JtquemRecebeu.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtquemRecebeu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtquemRecebeu.setSize(400, 25);
        JtquemRecebeu.setLocation(20, 120);
        add(JtquemRecebeu);

        //**Jbutton Salvar**//
        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.addActionListener(AcaoBotaoSalvar);
        Botao_salvar.addKeyListener(AcaoBotaoSalvar);
        Botao_salvar.setLocation(500, 120);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        //Botao_cancelar.addActionListener(this);
        Botao_cancelar.setLocation(600, 120);
        add(Botao_cancelar);

    }

    public PAGAMENTO_NOTA CapturaDadosDigitados() {
        PAGAMENTO_NOTA.setFornecedor(Aux_nomeFornecedor);
        PAGAMENTO_NOTA.setDiaRecebeu(JCdiaPagamento.getSelectedItem().toString());
        PAGAMENTO_NOTA.setDataRecebeu(JtdatPagamento.getText());
        PAGAMENTO_NOTA.setNomeRecebeu(JtquemRecebeu.getText());
        PAGAMENTO_NOTA.setValorRecebeu(Converte.StringEmFloat(JtvalorPagamento.getText()));

        return PAGAMENTO_NOTA;

    }

    public void AtualizaNovoValorDivida() {
        float ValorDivida = compra_nota.getValorDivida();
        float ValorPago = Converte.StringEmFloat(JtvalorPagamento.getText());

        compra_nota.setValorDivida(ValorDivida - ValorPago);
    }

    public void ArquivarPagamentos(ArrayList<PAGAMENTO_NOTA> ListaPagamentos) throws IOException {
        ARQUIVO_PAGAMENTO_NOTA.Armazena(ListaPagamentos);     
    }

    public void ArquivarCompraNota(ArrayList<COMPRA_NOTA> ListaCompraNota) throws IOException {
        
        ARQUIVO_COMPRA_NOTA.Armazena(ListaCompraNota);
      
    }

    public class AcaoBotaoSalvar implements ActionListener, KeyListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {

                if (JtdatPagamento.getText().isEmpty() || JlvalorPagamento.getText().isEmpty()) {
                    JCdiaPagamento.requestFocus();
                } else {
                    for (int i = 0; i < ListaCompraNota.size(); i++) {
                        compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
                        if (compra_nota.getNomeFornecedor().equalsIgnoreCase(Aux_nomeFornecedor)) {
                            if (Float.parseFloat(JtvalorPagamento.getText().replace(",", ".")) >= compra_nota.getValorDivida()) {
                                for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                                    produtoCompra.setNome(compra_nota.getProdutoEstoque().get(j).getNomeProduto().replace("[", "").replace(",", "").trim());
                                    produtoCompra.setQuantidade(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                                    produtoCompra.setCodigo(compra_nota.getProdutoEstoque().get(j).getCodigoProduto());
                                    produtoCompra.setPrecoCompraTotal(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                                    produtoCompra.setPrecoCompraUnidade(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                                    produtoCompra.setDia(Datas.retornaData());
                                    produtoCompra.setMes(Datas.retornaMes());
                                    produtoCompra.setAno(Datas.retornaAno());
                                    ListaProdutoComprados.add(produtoCompra);
                                }

                                ListaCompraNota.remove(compra_nota);

                            } else {
                                
                                ListaPagamentos.add(CapturaDadosDigitados());
                                AtualizaNovoValorDivida();
                            }
                        }
                    }
                    try {
                        ArquivarPagamentos(ListaPagamentos);
                    } catch (IOException ex) {
                        Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        ArquivarCompraNota(ListaCompraNota);
                    } catch (IOException ex) {
                        Logger.getLogger(Painel_EfetuarPagamento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

    }
}
