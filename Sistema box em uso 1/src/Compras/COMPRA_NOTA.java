/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;


import Interface_Alternavel.PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class COMPRA_NOTA{
    
     ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> produtoEstoque = new ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO>();   
     String nomeFornecedor;
     String dataEfetuarPagamento;
     String dataInicoDivida;
     float valorDivida;
     float dinheiroEntrada;
     float dinheiroDia;
     String telefone;

    public String getDataInicoDivida() {
        return dataInicoDivida;
    }

    public void setDataInicoDivida(String dataInicoDivida) {
        this.dataInicoDivida = dataInicoDivida;
    }

    public COMPRA_NOTA(String nomeFornecedor, String dataEfetuarPagamento, String dataInicoDivida, float valorDivida, float dinheiroEntrada, float dinheiroDia, String telefone) {
        this.nomeFornecedor = nomeFornecedor;
        this.dataEfetuarPagamento = dataEfetuarPagamento;
        this.dataInicoDivida = dataInicoDivida;
        this.valorDivida = valorDivida;
        this.dinheiroEntrada = dinheiroEntrada;
        this.dinheiroDia = dinheiroDia;
        this.telefone = telefone;
    }
     
    public String getDataEfetuarPagamento() {
        return dataEfetuarPagamento;
    }

    public void setDataEfetuarPagamento(String dataEfetuarPagamento) {
        this.dataEfetuarPagamento = dataEfetuarPagamento;
    }

    public float getDinheiroDia() {
        return dinheiroDia;
    }

    public void setDinheiroDia(float dinheiroDia) {
        this.dinheiroDia = dinheiroDia;
    }

    public float getDinheiroEntrada() {
        return dinheiroEntrada;
    }

    public COMPRA_NOTA() {
    }

    public void setDinheiroEntrada(float dinheiroEntrada) {
        this.dinheiroEntrada = dinheiroEntrada;
    }

    public String getNomeFornecedor() {
        return nomeFornecedor;
    }

    public void setNomeFornecedor(String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }

    public ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> getProdutoEstoque() {
        return produtoEstoque;
    }

    public void setProdutoEstoque(ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> produtoEstoque) {
        this.produtoEstoque = produtoEstoque;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public float getValorDivida() {
        return valorDivida;
    }

    public void setValorDivida(float valorDivida) {
        this.valorDivida = valorDivida;
    }

    @Override
    public String toString() {
        return nomeFornecedor + ":"+ dataEfetuarPagamento + ":" + valorDivida + ":"+ dataInicoDivida +":"
                + dinheiroEntrada + ":" + dinheiroDia + ":" + telefone + ":"+ produtoEstoque + ":" + "\n";
    }
    
}
