/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

public class Alternavel_Caixa_ConsutaAno extends JPanel {
    
    private JScrollPane JSprodutosVendidos; 
    ArrayList<String[]> dados = new ArrayList<>();
    String quantidade, codigo, nome, precoVenda, subTotal, desconto, Total, precoCompra;
    JLabel JLtotalVenda = new JLabel();
    JLabel JLlucroVenda = new JLabel();
    JButton Botao_Voltar = new JButton();
    JNumberFormatField JTtotalVenda = new JNumberFormatField(new DecimalFormat("0.00"));
    JNumberFormatField JTlucroVenda = new JNumberFormatField(new DecimalFormat("0.00"));
    private JLabel JlData = new JLabel();
    private JTextField JtData = new JTextField();
    private JLabel Jldia = new JLabel();
    private JLabel Jlmes = new JLabel();
    private JLabel Jlano = new JLabel();
    JLabel JlgastosBox = new JLabel();
    JNumberFormatField JtgastosBox = new JNumberFormatField(new DecimalFormat("0.00"));
    private float lucroTotal = 0;
    private String lucros;
    private PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA();
    private ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = new ArrayList<>();

    public Alternavel_Caixa_ConsutaAno() {
        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
    }
    
     public void LimpaTabela() {
        
        dados.clear();
        Tabela(dados);
    }

    public void componentesVisiveis() {
        JLtotalVenda.setVisible(true);
        JTtotalVenda.setVisible(true);
        JLlucroVenda.setVisible(true);
        JTlucroVenda.setVisible(true);
        Botao_Voltar.setVisible(true);
        JlgastosBox.setVisible(true);
        JtgastosBox.setVisible(true);
        JlData.setVisible(false);
        JtData.setVisible(false);
    }

    public void iniciaizaTxt() {

        JLtotalVenda.setBounds(new Rectangle(168, 135, 400, 50));
        JLtotalVenda.setText("Valor total das vendas");
        JLtotalVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLtotalVenda.setLocation(800, 40);
        JLtotalVenda.setVisible(false);
        add(JLtotalVenda);

        JTtotalVenda.setBounds(new Rectangle(200, 135, 120, 17));
        JTtotalVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTtotalVenda.setSize(120, 50);
        JTtotalVenda.setLocation(850, 80);//
        JTtotalVenda.setVisible(false);
        add(JTtotalVenda);

        JLlucroVenda.setBounds(new Rectangle(168, 135, 400, 50));
        JLlucroVenda.setText("Lucro Vendas");
        JLlucroVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLlucroVenda.setLocation(830, 120);
        JLlucroVenda.setVisible(false);
        add(JLlucroVenda);

        JTlucroVenda.setBounds(new Rectangle(200, 135, 120, 17));
        JTlucroVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTlucroVenda.setSize(120, 50);
        JTlucroVenda.setLocation(850, 160);//
        JTlucroVenda.setVisible(false);
        add(JTlucroVenda);

        JlgastosBox.setBounds(new Rectangle(168, 135, 400, 50));
        JlgastosBox.setText("Gastos");
        JlgastosBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JlgastosBox.setLocation(850, 210);
        JlgastosBox.setVisible(false);
        add(JlgastosBox);

        JtgastosBox.setBounds(new Rectangle(200, 135, 120, 17));
        JtgastosBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JtgastosBox.setSize(120, 50);
        JtgastosBox.setLocation(850, 250);//
        JtgastosBox.setVisible(false);
        add(JtgastosBox);

        Botao_Voltar = new JButton();
        Botao_Voltar.setText("VOLTAR");
        Botao_Voltar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Voltar.setSize(100, 25);
        //Botao_Voltar.addActionListener(btn_voltar);
        Botao_Voltar.setLocation(850, 340);
        Botao_Voltar.setVisible(false);
        add(Botao_Voltar);

    }

    public void criaTabela(String data) {

        int j = 0;
        float tot_vendas = 0;
        lucroTotal = 0;

        iniciaizaTxt();

        ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
        Collections.sort(ListaProdutosVendidos);

        if (ListaProdutosVendidos.size() > 0) {
          
            for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);

                if (produtoVenda.getAno().equalsIgnoreCase(data)) {

                    componentesVisiveis();

                    quantidade = String.valueOf(produtoVenda.getQuantidadeProduto());
                    codigo = produtoVenda.getCodigoProduto();
                    nome = produtoVenda.getNomeProduto();
                    precoVenda = AlternavelCadastroProduto.converterFloatString(produtoVenda.getValor_Venda());
                    subTotal = AlternavelCadastroProduto.converterFloatString(produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto());
                    desconto = AlternavelCadastroProduto.converterFloatString(produtoVenda.getDescontoVenda());
                    lucros = AlternavelCadastroProduto.converterFloatString(produtoVenda.getLucroVenda());

                    String[] produtos = new String[]{quantidade, codigo, nome, precoCompra, precoVenda, desconto, subTotal, lucros, produtoVenda.getData()};
                    dados.add(produtos);

                    tot_vendas = tot_vendas + produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto();
                    lucroTotal = lucroTotal + produtoVenda.getLucroVenda();

                    Jldia.setVisible(false);
                    Jlmes.setVisible(false);
                    Jlano.setVisible(true);
                    Jlano.setBounds(new Rectangle(168, 135, 400, 30));
                    Jlano.setText("Vendas do Ano " + produtoVenda.getAno());
                    Jlano.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
                    Jlano.setLocation(180, 20);
                    add(Jlano);

                    JtData.setText(null);

                }

            }

            Alternavel_Caixa_Consuta Caixa_Consuta = new Alternavel_Caixa_Consuta();
            
            JTtotalVenda.setText(AlternavelCadastroProduto.converterFloatString(tot_vendas));
            JTlucroVenda.setText(AlternavelCadastroProduto.converterFloatString(lucroTotal));
            JtgastosBox.setText(AlternavelCadastroProduto.converterFloatString(Caixa_Consuta.retornaGastos(data)));
            
            Tabela(dados);
        }
    }
    public void Tabela(ArrayList<String[]> dados) {
        for(int i =0;i < dados.size();i++){
           
    }
        String[] colunas = new String[]{"Quantidade", "Codigo", "Descrição", "preço Compra", "Preço venda", "Desconto", "Sub Total", "Lucro", "Data"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setEditingRow(modelo.linhas.size());
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(8).setPreferredWidth(20);
        
     

        if (modelo.linhas.size()> 0 && modelo.linhas.size() < 10) {
            TableColumn coluna = null;
            jtable.setLocation(1, 100);
            jtable.setSize(800, 250);
            jtable.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
            add(jtable);
        } else {

            JSprodutosVendidos = new JScrollPane(jtable);
            JSprodutosVendidos.setLocation(1, 60);
            JSprodutosVendidos.getVerticalScrollBar();
            JSprodutosVendidos.setSize(800, 250);
            JSprodutosVendidos.revalidate();
            add(JSprodutosVendidos);
        }
    }

    public final class TabelaModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private TabelaModel(ArrayList dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return colunas[column];
        }
    }

    public void remove() {

        dados.clear();
        Tabela(dados);
    }

    public class botaoVoltarprecioncado implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JSprodutosVendidos.setVisible(false);
            JLtotalVenda.setVisible(false);
            JTtotalVenda.setVisible(false);
            JLlucroVenda.setVisible(false);
            JTlucroVenda.setVisible(false);
            Botao_Voltar.setVisible(false);
            JlgastosBox.setVisible(false);
            JtgastosBox.setVisible(false);
            JlData.setVisible(true);
            JtData.setVisible(true);

        }
    }

    private class EnterPrecionadoJTxtData implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {

            if (e.getKeyCode() == 10) {
                if (JtData.getText() != null) {
                    float TotalVendas = 0;
                    float lucroTotal = 0;

                    remove();
                   

                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }
    
 }
