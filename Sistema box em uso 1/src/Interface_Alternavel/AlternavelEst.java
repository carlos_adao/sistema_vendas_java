/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE;
import Cadastro.Datas;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author carlos
 */
public final class AlternavelEst extends JPanel{

    private JScrollPane JSprodutosEstoque;
    private ARQUIVO_ESTOQUE ArquivoEstoque = new ARQUIVO_ESTOQUE();
    private ArrayList<PRODUTO_ESTOQUE> ListaProdutosEstoque = new ArrayList<>();
    PRODUTO_ESTOQUE produtoCompra = new PRODUTO_ESTOQUE();
    private Datas Data = new Datas();
    private JButton Botao_Pequisar, Botao_Editar, Botao_Remover;
    String data;
    String dataVenda;
    ArrayList<String[]> dados = new ArrayList<>();
    String quantidade, codigo, nome, precoVenda, precoCompra, precoCompraGeral, LucroUnidade, LucroGeral;
    AcaoBotao acao_botao = new AcaoBotao();
    Ordena OrdenaEstoque = new Ordena();
    PainelRemoveEstoque PainelRemoveEstoque; 
    public Teclado teclado = new Teclado();
    
    

    public AlternavelEst() {
        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
        
        
        inicializarBotoes();
    }

    public void remove() {
        
      
        dados.clear();
        Tabela(dados);
    }
    
   
    public ArrayList<String[]> criaTabela() {
        
      
        ListaProdutosEstoque = ARQUIVO_ESTOQUE.retorna();
        
       
        
        Collections.sort(ListaProdutosEstoque);

        
        if (ListaProdutosEstoque.size() > 0) {
            data = Data.retornaData();

            for (int i = 0; i < ListaProdutosEstoque.size(); i++) {
                produtoCompra = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(i);



                quantidade = String.valueOf(produtoCompra.getQuantidadeProduto());
                codigo = produtoCompra.getCodigoProduto();
                nome = produtoCompra.getNomeProduto();
                precoVenda = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoVenda_unidade());
                precoCompra = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoCompra_unidade());
                precoCompraGeral = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoCompra_Geral());
                LucroUnidade = AlternavelCadastroProduto.converterFloatString(produtoCompra.getLucroUnidade());
                LucroGeral = AlternavelCadastroProduto.converterFloatString(produtoCompra.getLucroGeral());


                String[] produtos = new String[]{quantidade, codigo, nome, precoCompra, precoVenda, precoCompraGeral, LucroUnidade, LucroGeral};
                dados.add(produtos);
            }

            Tabela(dados);
        }
        return dados;
    }

    public void Tabela(ArrayList<String[]> dados) {
         
   
        String[] colunas = new String[]{"Quantidade", "Codigo", "Descrição", "Preço Compra", "Preço venda", "Preço geral", "Lucro Uni", "Lucro Geral"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);

        jtable.addMouseListener(new MouseAdapter() {
            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                        linha = jtable.getSelectedRow();
                        String quantidadeProduto = String.valueOf(jtable.getValueAt(linha, 0));
                        String codigoProduto = String.valueOf(jtable.getValueAt(linha, 1));
                        String nomeProduto = String.valueOf(jtable.getValueAt(linha, 2));
                        String precoVenda = String.valueOf(jtable.getValueAt(linha, 4));
                        
                       ListaProdutosEstoque = ArquivoEstoque.retorna();

                    
                    for (int i = 0; i < ListaProdutosEstoque.size(); i++) {
                        produtoCompra = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(i);
                        if(nomeProduto.equalsIgnoreCase(produtoCompra.getNomeProduto())) {
                            PainelRemoveEstoque painel_remove_estoque = new PainelRemoveEstoque(quantidadeProduto, codigoProduto, nomeProduto, precoVenda, i);
                        }
                    }
                }
            }
        });



        TableColumn coluna = null;
        
        add(jtable);
        jtable.clearSelection();
        
        jtable.addKeyListener(teclado);
        
        JSprodutosEstoque = new JScrollPane(jtable);
        //JSprodutosVendidos.setLocation(1, 145);
        JSprodutosEstoque.setSize(900, 400);
        JSprodutosEstoque.addKeyListener(teclado);
        add(JSprodutosEstoque);
        
      
        
      

    }
    public void atualiza(){
      remove();
      criaTabela();
      dados.clear();
      Tabela(dados);
    }
    
    

    public class TabelaModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private TabelaModel(ArrayList dados, String[] colunas) {
            
            
            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        public String getColumnName(int column) {
            return colunas[column];
        }
    }
 


    public void inicializarBotoes() {


        Botao_Pequisar = new JButton();
        Botao_Pequisar.setText("PESQUISAR");
        Botao_Pequisar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Pequisar.setSize(100, 25);
        //Botao_Pequisar.addKeyListener(enterPrecionadoBtSalvar);
        Botao_Pequisar.setLocation(1200, 10);
        add(Botao_Pequisar);

        Botao_Editar = new JButton();
        Botao_Editar.setText("EDITAR");
        Botao_Editar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Editar.setSize(100, 25);
        Botao_Editar.setLocation(1200, 50);
        add(Botao_Editar);

        Botao_Remover = new JButton();
        Botao_Remover.setText("REMOVER");
        Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Remover.setSize(100, 25);
        Botao_Remover.addActionListener(acao_botao);
        Botao_Remover.setLocation(1200, 90);
        add(Botao_Remover);
    }

    public class AcaoBotao implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_Remover) {
               
            }
        }
    }
  
    public class PainelRemoveEstoque extends JFrame {

        private int largura;
        private int altura;
        private Container container;
        P_remove p_remove;

        public PainelRemoveEstoque(String quantidadeProduto, String codigoProduto, String nomeProduto, String precoVenda, int Localizacao) {

            p_remove = new P_remove(quantidadeProduto, codigoProduto, nomeProduto, precoVenda, Localizacao);

            container = getContentPane();

            this.container.add(p_remove);
            this.setLayout(null);
            this.setSize(500, 450); // alt, larg
            this.largura = -125;
            this.altura = 300;
            this.setBackground(Color.BLUE);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(20, 230);

        }

        public class P_remove extends JPanel {

            private JButton Botao_Salvar, Botao_Cancelar;
            private String str1 = null;
            private JLabel JLcodigoProduto = new JLabel();
            private JLabel JLnome = new JLabel();
            private JTextField JTnome = new JTextField();
            private JTextField JTcodigoProduto = new JTextField();
            private JLabel JLprecoVenda = new JLabel();
            private JLabel JLquantidade = new JLabel();
            private JTextField JTquantidade = new JTextField();
            private JNumberFormatField JTprecoVenda = new JNumberFormatField(new DecimalFormat("0.00"));
            private JScrollPane JSprodutosEstoque = new JScrollPane();
            private ARQUIVO_ESTOQUE arquivo_estoque = new ARQUIVO_ESTOQUE();
            private ArrayList<String> dados = new ArrayList<>();
            private PainelAltenavelCaixa painel_alternavel_caixa = new PainelAltenavelCaixa();
            private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<PRODUTO_ESTOQUE>();//UMA LISTA DE PRODUTOS DO MEU ESTOQUE
            private PRODUTO_ESTOQUE produto_estoque = new PRODUTO_ESTOQUE();
            AcaoBotaoSalvar acaoBotaoSalvar = new AcaoBotaoSalvar();
            AcaoBotaoRemover acao_botao_remover = new AcaoBotaoRemover();

            public P_remove(String quantidadeProduto, String codigoProduto, String nomeProduto, String precoVenda, int Localizacao) {
                this.setLayout(null);
                this.setSize(500, 400); // larg, alt
                this.setLocation(0, 50);
                this.setBackground(Color.white);

                ListaProduto_Estoque = arquivo_estoque.retorna();



                JLcodigoProduto.setBounds(new Rectangle(168, 135, 400, 50));
                JLcodigoProduto.setText("Digite o novo código do produto");
                JLcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLcodigoProduto.setLocation(10, 0);
                add(JLcodigoProduto);

                JTcodigoProduto.setBounds(new Rectangle(200, 135, 120, 17));
                JTcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTcodigoProduto.setSize(160, 25);
                JTcodigoProduto.setLocation(10, 40);//
                JTcodigoProduto.setText(codigoProduto);
                // JTcodigoProduto.addKeyListener(acao);
                add(JTcodigoProduto);

                JLnome.setBounds(new Rectangle(168, 135, 400, 50));
                JLnome.setText("Digite o novo nome do produto");
                JLnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLnome.setLocation(10, 80);
                add(JLnome);

                JTnome.setBounds(new Rectangle(200, 135, 120, 17));
                JTnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTnome.setSize(160, 25);
                JTnome.setLocation(10, 120);//
                //JTnome.addKeyListener(acao);
                JTnome.setText(nomeProduto);
                add(JTnome);

                JLquantidade.setBounds(new Rectangle(168, 135, 400, 50));
                JLquantidade.setText("Digite a nova quantidade do produto");
                JLquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLquantidade.setLocation(10, 160);
                add(JLquantidade);

                JTquantidade.setBounds(new Rectangle(200, 135, 120, 17));
                JTquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTquantidade.setSize(160, 25);
                JTquantidade.setLocation(10, 200);//
                //JTquantidade.addKeyListener(acao);
                JTquantidade.setText(quantidadeProduto);
                add(JTquantidade);

                JLprecoVenda.setBounds(new Rectangle(168, 135, 400, 50));
                JLprecoVenda.setText("Digite o novo Preço de venda do produto");
                JLprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JLprecoVenda.setLocation(10, 240);
                add(JLprecoVenda);

                JTprecoVenda.setBounds(new Rectangle(200, 135, 120, 17));
                JTprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JTprecoVenda.setSize(160, 25);
                JTprecoVenda.setLocation(10, 280);//
                JTprecoVenda.setText(precoVenda);
                //JTprecoVenda.addKeyListener(acao);
                add(JTprecoVenda);



                Botao_Salvar = new JButton();
                Botao_Salvar.setText("SALVAR");
                Botao_Salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Salvar.setSize(100, 25);
                Botao_Salvar.addActionListener(acaoBotaoSalvar);
                Botao_Salvar.setLocation(50, 320);
                add(Botao_Salvar);

                Botao_Cancelar = new JButton();
                Botao_Cancelar.setText("CANCELAR");
                Botao_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Cancelar.setSize(100, 25);
                Botao_Cancelar.setLocation(200, 320);
                add(Botao_Cancelar);

                Botao_Remover = new JButton();
                Botao_Remover.setText("REMOVER");
                Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                Botao_Remover.setSize(100, 25);
                Botao_Remover.setLocation(350, 320);
                Botao_Remover.addActionListener(acao_botao_remover);
                add(Botao_Remover);

                acaoBotaoSalvar.pegarLocalizacao(Localizacao);

            }

            public class AcaoBotaoSalvar implements ActionListener {

                int Loc;

                public int pegarLocalizacao(int localizacao) {
                    Loc = localizacao;

                    return Loc;

                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    ListaProduto_Estoque.get(Loc).setNomeProduto(JTnome.getText());
                    ListaProduto_Estoque.get(Loc).setCodigoProduto(JTcodigoProduto.getText());
                    ListaProduto_Estoque.get(Loc).setQuantidadeProduto(Integer.parseInt(JTquantidade.getText()));
                    ListaProduto_Estoque.get(Loc).setPrecoVenda_unidade(Float.parseFloat(JTprecoVenda.getText().replace(",", ".")));
                    ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(Integer.parseInt(JTquantidade.getText()) * ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
                    ListaProduto_Estoque.get(Loc).setLucroUnidade(Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
                    float lucroUnidade = (Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
                    ListaProduto_Estoque.get(Loc).setLucroGeral(lucroUnidade * Integer.parseInt(JTquantidade.getText()));

                    if (Integer.parseInt(JTquantidade.getText()) == 0) {
                        float FLAG = (float) 0.00;
                        ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(FLAG);
                        ListaProduto_Estoque.get(Loc).setPrecoCompra_unidade(FLAG);
                        ListaProduto_Estoque.get(Loc).setLucroGeral(FLAG);
                        ListaProduto_Estoque.get(Loc).setLucroUnidade(FLAG);
                    }


                    try {
                        arquivo_estoque.Armazena(ListaProduto_Estoque);
                    } catch (IOException ex) {
                        Logger.getLogger(EditaEstoque.P_remove.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    atualiza();
                   
                    criaTabela();
                    PainelRemoveEstoque.this.dispose();

                }
            }

            public void limpaTxt() {
                JTnome.setText(null);
                JTcodigoProduto.setText(null);
                JTprecoVenda.setText(null);
                JTquantidade.setText(null);


            }

    
    
        public class AcaoBotaoRemover implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                try {
                    arquivo_estoque.Armazena(ListaProduto_Estoque);
                } catch (IOException ex) {
                    Logger.getLogger(EditaEstoque.P_remove.class.getName()).log(Level.SEVERE, null, ex);
                }

                atualiza();
                criaTabela();
                PainelRemoveEstoque.this.dispose();
            }
        }
    }
}
    public class Teclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {
            }
        }
    }
}
