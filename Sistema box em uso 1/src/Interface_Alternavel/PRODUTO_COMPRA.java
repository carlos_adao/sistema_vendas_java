/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

/**
 *
 * @author carlos
 */
public class PRODUTO_COMPRA {
   
     int quantidade;
     String codigo;
     String nome;
     float precoCompraUnidade;
     float precoCompraTotal;
     String dia;
     String mes;
     String ano;

    public PRODUTO_COMPRA(int quantidade, String codigo, String nome, float precoCompraUnidade, float precoCompraTotal, String dia, String mes, String ano) {
        this.quantidade = quantidade;
        this.codigo = codigo;
        this.nome = nome;
        this.precoCompraUnidade = precoCompraUnidade;
        this.precoCompraTotal = precoCompraTotal;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

   public PRODUTO_COMPRA() {
       
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getPrecoCompraTotal() {
        return precoCompraTotal;
    }

    public void setPrecoCompraTotal(float precoCompraTotal) {
        this.precoCompraTotal = precoCompraTotal;
    }

    public float getPrecoCompraUnidade() {
        return precoCompraUnidade;
    }

    public void setPrecoCompraUnidade(float precoCompraUnidade) {
        this.precoCompraUnidade = precoCompraUnidade;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String toString() {
        return (quantidade + ";" + codigo + ";" + nome + ";" + precoCompraUnidade + ";" 
                + precoCompraTotal + ";" + dia + ";" + mes + ";" + ano + ";"+"\n");
    }
    
    
}
