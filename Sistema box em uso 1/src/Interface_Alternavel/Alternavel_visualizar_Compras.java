/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import Cadastro.Datas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author carlos
 */
public class Alternavel_visualizar_Compras extends JPanel {
    
    private JScrollPane JSprodutosComprados;
    private ARQUIVO_ESTOQUE_COMPRA ArquivoCompras = new ARQUIVO_ESTOQUE_COMPRA();
    private ArrayList<PRODUTO_COMPRA> ListaProdutosComprados = new ArrayList<>();
    private PRODUTO_COMPRA produtoCompra = new PRODUTO_COMPRA();
    private Datas Data = new Datas();
    private String dia;
    private String mes;
    private String ano;
    private String total;
    private String dataArquivo;
    private String Data_Txt;
    private float totalF = 0;
    private ArrayList<String[]> dados = new ArrayList<>();
    String quantidade,codigo,nome,precoCompraUni,precoCompraTotal;
    JLabel JLtotalCompras = new JLabel();
    JNumberFormatField JTtotalComprs = new JNumberFormatField(new DecimalFormat("0.00")) ;
    private JLabel JlData = new JLabel();
    private JLabel Jldia = new JLabel();
    private JLabel Jlmes = new JLabel();
    private JLabel Jlano = new JLabel();
    private JTextField JtData = new JTextField(dia);
    private JRadioButton JR_dia, JR_mes, JR_ano;
    private ButtonGroup radioGroup = new ButtonGroup();
    JTable jtable;
    TabelaModel modelo = new TabelaModel(); 
    
    public Alternavel_visualizar_Compras() {
        this.setLayout(null);
        this.setSize(1336, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
        jtable = new JTable();
        JSprodutosComprados = new JScrollPane(jtable);
        
        JSprodutosComprados.setLocation(1, 60);
        JSprodutosComprados.getVerticalScrollBar();
        JSprodutosComprados.setSize(800, 250);
        JSprodutosComprados.revalidate();
        add(JSprodutosComprados);
    }
     
      public  void criaTabela(int idt) {//adt é um indentificador para seber se queremos ver as compras do dia mes ou ano
        dados = new ArrayList<>();
        ListaProdutosComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();
        
       // Collections.sort(ListaProdutosComprados);
      
        if (ListaProdutosComprados.size() > 0) {
        float tot_Comps = 0;
       
            for (int i = 0; i < ListaProdutosComprados.size(); i++){
                produtoCompra = (PRODUTO_COMPRA) ListaProdutosComprados.get(i);
          
                if (produtoCompra.getDia().equalsIgnoreCase(Data.retornaData())&& idt == 1){
                     tot_Comps += produtoCompra.getPrecoCompraTotal();
                     RetornaProduto(produtoCompra);
                     AcoesParoDia("Compras do Dia ",Data.retornaData());
                     dados.add(RetornaProduto(produtoCompra));
                    
                }
                if (produtoCompra.getMes().equalsIgnoreCase(Data.retornaMes()) && idt == 2){
                     tot_Comps += produtoCompra.getPrecoCompraTotal();
                     RetornaProduto(produtoCompra);
                     AcoesParoDia("Compras do Mês ",Data.retornaDia());
                     dados.add(RetornaProduto(produtoCompra));
                     
                }
                if (produtoCompra.getAno().equalsIgnoreCase(Data.retornaAno()) && idt == 3){
                   tot_Comps += produtoCompra.getPrecoCompraTotal();
                   RetornaProduto(produtoCompra);
                   AcoesParoDia("Compras do Mês ",Data.retornaDia());
                   dados.add(RetornaProduto(produtoCompra));
                
                }
            }
            ConstroiTxt(tot_Comps);
            Tabela(dados);
          
        }
    }

    
    public void Tabela( ArrayList<String[]> dados){
       
       String[] colunas = new String[] { "Quantidade", "Codigo", "Descrição", "Preco uni ", "Preco Tot ","Data"};    
       modelo.setLinhas(dados);
       modelo.setColunas(colunas);
       jtable.setModel(modelo); 
       jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
       jtable.setLocation(1, 145);
       jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
       jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
       jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
       jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
       jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
       jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
       jtable.getColumnModel().getColumn(5).setPreferredWidth(20);        
        
    }

    //FAZ O CALCULO DAS COMPRAS CASO SEJA EM FORMA DE DIA 
    public void AcoesParoDia(String text, String data) {
        Jldia.setVisible(true);
        Jldia.setBounds(new Rectangle(168, 135, 400, 30));
        Jldia.setText(text + data);
        Jldia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Jldia.setLocation(180, 20);
        add(Jldia);
    }
    //COLOCA VALORES NO OBJETO PRODUTO

    public String[] RetornaProduto(PRODUTO_COMPRA ProdutoCompra) {
        String[] produto;
        quantidade = String.valueOf(produtoCompra.getQuantidade());
        codigo = produtoCompra.getCodigo();
        nome = produtoCompra.getNome();
        precoCompraUni = AlternavelCadastroProduto.converterFloatString(produtoCompra.getPrecoCompraUnidade());
        precoCompraTotal = AlternavelCadastroProduto.converterFloatString((produtoCompra.getPrecoCompraTotal()));
        dia = (produtoCompra.getDia());

          return produto = new String[]{quantidade, codigo, nome, precoCompraUni, precoCompraTotal, dia};
    }
  
    public void ConstroiTxt(float TotalCompras){
       
        JLtotalCompras.setBounds(new Rectangle(168, 135, 400, 50));
        JLtotalCompras.setText("Valor total Compras");
        JLtotalCompras.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLtotalCompras.setLocation(800, 40);
        add(JLtotalCompras);

        JTtotalComprs.setBounds(new Rectangle(200, 135, 120, 17));
        JTtotalComprs.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTtotalComprs.setSize(120, 50);
        JTtotalComprs.setLocation(850, 80);//
        JTtotalComprs.setText(AlternavelCadastroProduto.converterFloatString(TotalCompras));
        add(JTtotalComprs);

    }

    public class TabelaModel extends AbstractTableModel{  
  
    private ArrayList linhas = null;  
    private String [] colunas = null;
    
    //Construtor da classe SimpleTableModel 
    private TabelaModel(ArrayList dados, String[] colunas) {
    setLinhas(dados);  
    setColunas(colunas);  

    }

        private TabelaModel() {
           
        }

        
    public String[] getColunas() {return colunas;}  
    public ArrayList getLinhas() {return linhas;}  
    public void setColunas(String[] strings) {colunas = strings;}  
    public void setLinhas(ArrayList list) {linhas = list;}

        @Override
        public int getRowCount() {
            return getLinhas().size();  
  
        }

        @Override
        public int getColumnCount() {
           return getColunas().length;  
  
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
           // Obtem a linha, que é uma String []  
           String [] linha = (String [])getLinhas().get(rowIndex);  
          // Retorna o objeto que esta na coluna  
        return linha[columnIndex];  
        }
        
     public String getColumnName(int column){  
      return colunas[column];  
     }
     
     
  }

  public void remove(){
   totalF = 0;
   dados.clear();
   Tabela(dados);
  }
  
  public void coloca(ArrayList<String[]> dados){
   ArrayList<String[]> Dados = dados;
      Tabela(Dados);
      
  }
       
  
}
