/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.Converte;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JOptionPane;

/**
 *
 * @author Convidado
 */
public class Calculos_ExibeVendas {

    float lucroTotal = 0;
    float tot_vendas = 0;
    String quantidade, codigo, nome, precoVenda, subTotal, desconto, lucro, diaVenda;// variaveis usadas para passa dados para tabela
    private ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = new ArrayList<>(); //carrega para memoria a lista de produtos vendidos
    private PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA(); //instancia um produto do tipo venda para fazer manipulações
    ArrayList<String[]> dados = new ArrayList<>();// usado para enviar a lista de vendas em forma de String
    Converte converte = new Converte();//isntancia a classe para realizar converção

    /*retorna todas as vendas da consulta solicitada*/
    public ArrayList<String[]> RetornaValoresParaExibirVendas(String Consutar) {// consutar pode ser o dia o mes ou o ano de acordo com o que deseja visualizar
        tot_vendas = 0;
        lucroTotal = 0;
        dados.clear();
        ListaProdutosVendidos.clear();
        ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
    
        Collections.sort(ListaProdutosVendidos);

        if (ListaProdutosVendidos.size() > 0) {

            for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);

                if (produtoVenda.getData().equalsIgnoreCase(Consutar)) {

                    quantidade = String.valueOf(produtoVenda.getQuantidadeProduto());
                    codigo = produtoVenda.getCodigoProduto();
                    nome = produtoVenda.getNomeProduto();
                    precoVenda = AlternavelCadastroProduto.converterFloatString(produtoVenda.getValor_Venda());
                    subTotal = AlternavelCadastroProduto.converterFloatString((produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto()));
                    desconto = AlternavelCadastroProduto.converterFloatString(produtoVenda.getDescontoVenda());
                    lucro = AlternavelCadastroProduto.converterFloatString(produtoVenda.getLucroVenda());
                    diaVenda = produtoVenda.getData();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoVenda, desconto, subTotal, lucro, diaVenda};
                    dados.add(produtos);

                    tot_vendas = tot_vendas + produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto();
                    lucroTotal = lucroTotal + produtoVenda.getLucroVenda();

                }

                if (produtoVenda.getMes().equalsIgnoreCase(Consutar)) {

                    quantidade = String.valueOf(produtoVenda.getQuantidadeProduto());
                    codigo = produtoVenda.getCodigoProduto();
                    nome = produtoVenda.getNomeProduto();
                    precoVenda = AlternavelCadastroProduto.converterFloatString(produtoVenda.getValor_Venda());
                    subTotal = AlternavelCadastroProduto.converterFloatString((produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto()));
                    desconto = AlternavelCadastroProduto.converterFloatString(produtoVenda.getDescontoVenda());
                    lucro = AlternavelCadastroProduto.converterFloatString(produtoVenda.getLucroVenda());
                    diaVenda = produtoVenda.getData();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoVenda, desconto, subTotal, lucro, diaVenda};
                    dados.add(produtos);

                    tot_vendas = tot_vendas + produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto();
                    lucroTotal = lucroTotal + produtoVenda.getLucroVenda();

                }
                if (produtoVenda.getAno().equalsIgnoreCase(Consutar)) {

                    quantidade = String.valueOf(produtoVenda.getQuantidadeProduto());
                    codigo = produtoVenda.getCodigoProduto();
                    nome = produtoVenda.getNomeProduto();
                    precoVenda = AlternavelCadastroProduto.converterFloatString(produtoVenda.getValor_Venda());
                    subTotal = AlternavelCadastroProduto.converterFloatString((produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto()));
                    desconto = AlternavelCadastroProduto.converterFloatString(produtoVenda.getDescontoVenda());
                    lucro = AlternavelCadastroProduto.converterFloatString(produtoVenda.getLucroVenda());
                    diaVenda = produtoVenda.getData();

                    String[] produtos = new String[]{quantidade, codigo, nome, precoVenda, desconto, subTotal, lucro, diaVenda};
                    dados.add(produtos);

                    tot_vendas = tot_vendas + produtoVenda.getValor_Venda() * produtoVenda.getQuantidadeProduto();
                    lucroTotal = lucroTotal + produtoVenda.getLucroVenda();

                }
            }
            
            return dados;
        }
        return null;
    }

    public String RetornaTotalVendas() {   
       return converte.FloatEmString(tot_vendas);
    }

    public String RetornaTotalLucro() {
        return converte.FloatEmString(lucroTotal);
    }
}
