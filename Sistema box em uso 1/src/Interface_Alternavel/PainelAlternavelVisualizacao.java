/*
 * nesse painel terá a visualiação dos dados do sistema 
 * como vendas, compras, compras notas, gastos 
 */
package Interface_Alternavel;
import BancoDados.ARQUIVO_ESTOQUE_COMPRA;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import Gastos.BOX;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;


public final class PainelAlternavelVisualizacao extends JPanel {
    JButton Botao_Atualizar = new JButton(); 
    private JScrollPane JSbalancoDia;
    private JScrollPane JSbalancoMes;
    private JScrollPane JSbalancoAno;
    private BALANCO balaco = new BALANCO();
    private BOX gastos = new BOX();
    private ArrayList<PRODUTO_VENDA>ListaProdutosVendidos = new ArrayList<>();
    private ArrayList<PRODUTO_COMPRA>ListaProdutosComprados = new ArrayList<>();
    private ArrayList<BOX>ListaGastos = new ArrayList<>();
    private ArrayList<String[]> ListaBalaco = new ArrayList<>();
    
    public PainelAlternavelVisualizacao() {
        this.setLayout(null);
        this.setSize(1024, 465); // larg, alt
        this.setLocation(0, 160);
        this.setBackground(Color.WHITE);
        
        inicialiacao();
        TabelaDia(ListaBalaco);
        TabelaMes(ListaBalaco);
        TabelaAno(ListaBalaco);
        capturaDados();

    }
   
    public void inicialiacao() {
         
        Botao_Atualizar.setText("ATUALIZAR");
        Botao_Atualizar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Atualizar.setSize(200, 20);
        //Botao_Atualizar.addActionListener(btn_voltar);
        Botao_Atualizar.setLocation(780, 30);
        add(Botao_Atualizar);
        
            

    }
    public ArrayList<String[]> capturaDados(){
          
          ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
          ListaProdutosComprados = ARQUIVO_ESTOQUE_COMPRA.retorna();
    //      ListaGastos = ARQUIVO_GASTOS.retorna();
        
          
          for(int i = 0; i < ListaGastos.size();i++){
           gastos = (BOX)ListaGastos.get(i);
          
          
          }
          
    return null;
    }
    
    public void TabelaDia(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"DATA","ENTRADA", "VENDAS", "COMPRAS", "COMPRAS NOTA", "G CARLOS ", "G ELIANE", "G BOX", "SAIDA S", "SAIDA C"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(8).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(9).setPreferredWidth(20);

        TableColumn coluna = null;

        add(jtable);

        JSbalancoDia = new JScrollPane(jtable);
        JSbalancoDia.setLocation(1, 60);
        JSbalancoDia.setSize(880, 150);
        JSbalancoDia.setVisible(true);
        add(JSbalancoDia);
    }
    
    public void TabelaMes(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"MES","VENDAS", "COMPRAS", "DIVIDA", "G CARLOS ", "G ELIANE", "G BOX"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
       
        TableColumn coluna = null;

        add(jtable);

        JSbalancoMes = new JScrollPane(jtable);
        JSbalancoMes.setLocation(1, 250);
        JSbalancoMes.setSize(880, 100);
        JSbalancoMes.setVisible(true);
        add(JSbalancoMes);
    }
    
    public void TabelaAno(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"ANO","VENDAS", "COMPRAS", "DIVIDA", "G CARLOS ", "G ELIANE", "G BOX"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
       
        TableColumn coluna = null;

        add(jtable);

        JSbalancoAno = new JScrollPane(jtable);
        JSbalancoAno.setLocation(1, 400);
        JSbalancoAno.setSize(880, 50);
        JSbalancoAno.setVisible(true);
        add(JSbalancoAno);
    }
    
    
     public final class TabelaModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private TabelaModel(ArrayList dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return colunas[column];
        }
    }

    
}
