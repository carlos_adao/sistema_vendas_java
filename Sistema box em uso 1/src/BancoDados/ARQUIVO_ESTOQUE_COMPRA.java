/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Interface_Alternavel.PRODUTO_COMPRA;
import java.io.*;
import java.util.ArrayList;


/**
 *
 * @author carlos
 */
public class ARQUIVO_ESTOQUE_COMPRA{
    
   private static PRODUTO_COMPRA produtos;
   private static int quantidade;
   private static String codigo;
   private static String nome;
   private static String dia;
   private static String mes ;
   private static String Ano ;
   private static String linha;
   private static File file = new File("Arquivo de Produtos Compra.csv"); 
   private static File arq = new File("Arquivo de Compras.txt");
   private static float precoCompra_unidade,precoCompra_Total;
   private static int cont = 0;

     public static void Armazena(ArrayList<PRODUTO_COMPRA> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

       if(!file.exists()){
          file.createNewFile();
        }
        
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        
        FileWriter fxTxt = new FileWriter(arq);
        PrintWriter bwTxt = new PrintWriter(fxTxt);
        
      
        for (int i = 0; i < Produtos.size(); i++) {
           bw.write(Produtos.get(i).toString());
           bwTxt.printf("%s%n", Produtos.get(i));
        }
        
         bw.close();
         fw.close();
        System.out.println("gravado com sucesso!!!");
        
    }
    
    
    public static ArrayList<PRODUTO_COMPRA> retorna(){ // retorna o dados do arquivo para a classe produto 
          ArrayList<PRODUTO_COMPRA> ListaProdutos = new ArrayList<PRODUTO_COMPRA>();       
       
       try{
        
          
           FileReader fr = new FileReader(file);
           BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
           
        
       
           
          
           int i = 0;
           String linha = lerArq.readLine();
           while(linha != null){
             
              String[] produto = linha.split(";");
             
              quantidade = Integer.parseInt(produto[0]);
              codigo = new String( produto[1]);
              nome = new String(produto[2]);
              precoCompra_unidade = Float.parseFloat(produto[3]);
              precoCompra_Total = Float.parseFloat(produto[4]);
              dia = new String( produto[5]);
              mes = new String( produto[6]);
              Ano = new String( produto[7]);
                 
              produtos = new PRODUTO_COMPRA(quantidade, codigo, nome, precoCompra_unidade, 
                                            precoCompra_Total, dia, mes, Ano);
              linha = lerArq.readLine(); 
              ListaProdutos.add(produtos);
             i++;
            
            }
        
          
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
      
        return ListaProdutos;
    } 
}
