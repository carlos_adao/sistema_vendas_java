/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;
import Gastos.BOX;
import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class ARQUIVO_GASTOS extends BOX{
    static BOX box;
    static float  quanto;
    static String nome, comQue, observacao, dia, mes, ano;
    
    
    private static File file = new File("Arquivo Gastos.csv");
    private static File arq = new File("Arquivo  Gastos.txt"); 
    
    public static void Armazena(ArrayList<BOX> Gastos) throws IOException {
    
       if(!file.exists()){
          file.createNewFile();
        }  
       
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        
        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Gastos.size();
        for (i = 0; i < n; i++) {
            gravarArq.printf("%s%n", Gastos.get(i));
            bw.write(Gastos.get(i).toString());

        }
        gravarArq.close();
        bw.close();
        fw.close();
        
        System.out.println("gastos arquivado!!!");
    }
    
   public static ArrayList<BOX> retorna(){ // retorna o dados do arquivo para a classe gastos 
          ArrayList<BOX> Listagastos = new ArrayList<BOX>();       
       
       try{
        
        FileReader fr = new FileReader(file);
        BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
        
        
        String linha = lerArq.readLine();
        
        int j = 0;
        int cont = 0;
        while(linha != null){
            cont++;
            
            String[] gastos = linha.split(";");     
            
          
                  
                  nome = new String(gastos[0]);
                  quanto = Float.parseFloat( gastos[1]);
                  comQue = new String(gastos[2]);
                  observacao = new String(gastos[3]);
                  dia = new String(gastos[4]);
                  mes = new String(gastos[5]);
                  ano = new String(gastos[6]);
                         
                  box = new BOX (nome, quanto, comQue, observacao,dia, mes, ano);
             
                  linha = lerArq.readLine();
             
                  Listagastos.add(box); 
             }
        
        fr.close();
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
        return Listagastos;
    }
 
}
