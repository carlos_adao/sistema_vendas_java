/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Compras.COMPRA_NOTA;
import Interface_Alternavel.PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO;
import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class ARQUIVO_COMPRA_NOTA {

    private static COMPRA_NOTA compra_nota;
    private static PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO Prod_Est_Aguar_Pag = new PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO();
    private static String nomeFornecedor;
    private static int quantidadeProduto ;
    private static float valorDivida, dinheiroEntrada, dinheiroDia;
    private static String dataInicioDivida;
    private static String dataPagamento;
    private static String telefone;
    private static String produto;
    private static String nomeProduto;
    private static String codigoProduto;
    private static float precoCompra_unidade, precoVenda_unidade, precoCompra_Geral, LucroUnidade, LucroGeral;
    private static File file = new File("Arquivo compras aguardado pagamento.csv");
    private static File arq = new File("Arquivo compras aguardado pagamento.txt");

    public static void Armazena(ArrayList<COMPRA_NOTA> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);

        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Produtos.size();
        for (i = 0; i < n; i++) {
            gravarArq.printf("%s%n", Produtos.get(i));
         
            bw.write(Produtos.get(i).toString());

        }
        gravarArq.close();
        bw.close();
        fw.close();

        System.out.println("gravado com sucesso!!!");
    }

    public static ArrayList<COMPRA_NOTA> retorna() { // RETORNA UM OBJETO DO TIPO PRODUTO NOTAS 
        ArrayList<COMPRA_NOTA> ListaCompraNotas = new ArrayList<>();
        ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ListaProdutos = new ArrayList<>();
        ArrayList<String> test = new ArrayList<>();


        try {

            FileReader fr = new FileReader(file);
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));


            String linha = lerArq.readLine();

            int j = 0;

            while (linha != null) {

                String[] produtos;
                String[] CompraNotas = linha.split(":");

                nomeFornecedor = new String(CompraNotas[0]);
                dataPagamento = new String(CompraNotas[1]);
                valorDivida = Float.parseFloat(CompraNotas[2]);
                dataInicioDivida = new String(CompraNotas[3]);
                dinheiroEntrada = Float.parseFloat(CompraNotas[4]);
                dinheiroDia = Float.parseFloat(CompraNotas[5]);
                telefone = new String(CompraNotas[6]);
                produtos = new String(CompraNotas[7]).split("  ");

                String[] p;

                int tam = produtos.length - 1;


                for (int i = 0; i < tam; i++) {

                    p = produtos[i].split(";");

                    nomeProduto = new String(p[0]);
                    codigoProduto = new String(p[1]);
                    quantidadeProduto = Integer.parseInt(p[2]);
                    precoCompra_unidade = Float.parseFloat(p[3]);
                    precoVenda_unidade = Float.parseFloat(p[4]);
                    precoCompra_Geral = Float.parseFloat(p[5]);
                    LucroUnidade = Float.parseFloat(p[6]);
                    LucroGeral = Float.parseFloat(p[7]);


                    Prod_Est_Aguar_Pag = new PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO(nomeProduto, codigoProduto, quantidadeProduto, precoCompra_unidade,
                            precoVenda_unidade, precoCompra_Geral, LucroUnidade, LucroGeral);

                    ListaProdutos.add(Prod_Est_Aguar_Pag);
                }  
                compra_nota = new COMPRA_NOTA();

                compra_nota.setNomeFornecedor(nomeFornecedor);
                compra_nota.setDataEfetuarPagamento(dataPagamento);
                compra_nota.setValorDivida(valorDivida);
                compra_nota.setDataInicoDivida(dataInicioDivida);
                compra_nota.setDinheiroEntrada(dinheiroEntrada);
                compra_nota.setDinheiroDia(dinheiroDia);
                compra_nota.setTelefone(telefone);
                compra_nota.setProdutoEstoque(ListaProdutos);

                linha = lerArq.readLine();

                ListaCompraNotas.add(compra_nota);

                ListaProdutos = new ArrayList<PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO>();

             

            }
          

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
        }

        return ListaCompraNotas;
    }
}
