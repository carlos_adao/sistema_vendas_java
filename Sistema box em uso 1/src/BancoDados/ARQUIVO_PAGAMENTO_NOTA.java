/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Compras.PAGAMENTO_NOTA;
import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class ARQUIVO_PAGAMENTO_NOTA {
    
    private static String fornecedor;
    private static String nomeRecebeu;
    private static String diaRecebeu;
    private static String dataRecebeu;
    private static float valorRecebeu;
    
    /*CLASSE QUE GERARÁ O OBJETO PAGAMENTO*/
    private static PAGAMENTO_NOTA PagamentoNota;
    
    
    /* INTANCIA DA CLASSE QUE MANIPULATARÁ O ARQUIVO*/
    private static File file = new File("Arquivo pagamento nota.csv");
    private static File arq = new File("Arquivo pagamento nota.txt");

    public static void Armazena(ArrayList<PAGAMENTO_NOTA> pagamentoNotas) throws IOException {

        if (!file.exists()) {
            file.createNewFile();
        }

        try (FileWriter fw = new FileWriter(file); BufferedWriter bw = new BufferedWriter(fw)) {

            FileWriter fxTxt = new FileWriter(arq);
            PrintWriter bwTxt = new PrintWriter(fxTxt);

            for (int i = 0; i < pagamentoNotas.size(); i++) {
                bw.write(pagamentoNotas.get(i).toString());
                bwTxt.printf("%s%n", pagamentoNotas.get(i));
            }
        }
        System.out.println("gravado com sucesso!!!");
    }

    public static ArrayList<PAGAMENTO_NOTA> retorna() { // retorna o dados do arquivo para a classe produto 
        ArrayList<PAGAMENTO_NOTA> ListaPagamentoNota = new ArrayList<>();


        try {

            FileReader fr = new FileReader(file);
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));

            String linha = lerArq.readLine();

            int j = 0;

            while (linha != null) {

             
                String[] pagamento = linha.split(";");
                
                fornecedor  = pagamento[0];
                diaRecebeu = pagamento[1];
                dataRecebeu = pagamento[2];
                nomeRecebeu = pagamento[3];
                valorRecebeu = Float.parseFloat(pagamento[4]);

                PagamentoNota = new PAGAMENTO_NOTA(fornecedor, diaRecebeu, dataRecebeu, nomeRecebeu, valorRecebeu);
                
                ListaPagamentoNota.add(PagamentoNota);
                linha = lerArq.readLine();
            }
            fr.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
            ListaPagamentoNota = null;
        }
        return ListaPagamentoNota;

    }
}
