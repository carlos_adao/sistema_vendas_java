/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Interface_Alternavel.PRODUTO_VENDA;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class ARQUIVO_ESTOQUE_VENDA {
    
   private static PRODUTO_VENDA produtos;
   private static String nomeProduto;
   private static String codigoProduto;
   private static String data;
   private static String Ano ;
   private static String mes ;
   private static String linha;
   private static int quantidadeProduto;
   private static float precoVenda_unidade,LucroVenda,descontoVenda,subTotal, valor_Venda;
   private static File file = new File("Arquivo de Produtos Vendas.csv"); 
   private static File arq = new File("Arquivo de Produtos Estoque Vendas.txt");
   private static int cont = 0;

    public static void Armazena(ArrayList<PRODUTO_VENDA> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        
        if(!file.exists()){
          file.createNewFile();
        }
        
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        
        FileWriter fxTxt = new FileWriter(arq);
        PrintWriter bwTxt = new PrintWriter(fxTxt);
        
        for (int i = 0; i < Produtos.size(); i++) {
           bw.write(Produtos.get(i).toString());
           bwTxt.printf("%s%n", Produtos.get(i));
        }
         bw.close();
         fw.close();
        System.out.println("gravado com sucesso!!!");
        
        
    }
    
    public static ArrayList<PRODUTO_VENDA> retorna(){ // retorna o dados do arquivo para a classe produto 
          ArrayList<PRODUTO_VENDA> ListaProdutos = new ArrayList<PRODUTO_VENDA>();       
          
          
       try{
        
           if(!file.exists()){
             file.createNewFile();
           }
           
           FileReader fr = new FileReader(file);
           BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
           
           String linha = lerArq.readLine();
        
       
           while(linha != null){
              
              String[] produto = linha.split(";");
              
              
              nomeProduto = new String(produto[0]);
              codigoProduto = new String( produto[1]);
              quantidadeProduto = Integer.parseInt(produto[2]);
              precoVenda_unidade = Float.parseFloat(produto[3]);
              descontoVenda = Float.parseFloat(produto[4]);
              subTotal = Float.parseFloat(produto[5]);
              LucroVenda = Float.parseFloat(produto[6]);
              valor_Venda = Float.parseFloat(produto[7]);
              data = new String( produto[8]);
              mes = new String( produto[9]);
              Ano = new String( produto[10]);
                
             
              
              produtos = new PRODUTO_VENDA(nomeProduto, codigoProduto, quantidadeProduto, precoVenda_unidade, 
                                           descontoVenda, subTotal, LucroVenda, valor_Venda, data, mes, Ano);
             
             ListaProdutos.add(produtos);  
             linha = lerArq.readLine();
       }
        
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
        return ListaProdutos;
    }

    
}
