/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_vendas;
import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_ESTOQUE;
import Compras.COMPRA_NOTA;
import Compras.P_CompraNota;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class AutoCompleteCombo extends JComboBox implements JComboBox.KeySelectionManager {

    private static ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
    private static ArrayList<String> listaNomesProdutos = new ArrayList<>();
    private static PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
    JTextField tf ;
    
    //ISTANCIANDO A CLASSE COMPRA NOTAS PARA RETORNA TODO OS FORNECEDORES
    private static P_CompraNota P_CompraNota = new P_CompraNota();
    private static ArrayList<COMPRA_NOTA> ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
    private static ARQUIVO_COMPRA_NOTA arquivoNotas = new ARQUIVO_COMPRA_NOTA();
    private static COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    TecladoPara_CompraNota TecladoPara_CompraNota = new TecladoPara_CompraNota();
    
    
    private String searchFor;
    private long lap;
    int contador = 0;

    public AutoCompleteCombo() {
    
    }

    public class CBDocument extends PlainDocument {

        public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
            if (str == null) {
                return;
            }
            super.insertString(offset, str, a);
            if (!isPopupVisible() && str.length() != 0) {
                fireActionEvent();
            }
        }
    }

    public AutoCompleteCombo(Object[] items) {
        super(items);
        lap = new java.util.Date().getTime();
        setKeySelectionManager(this);
 

        if (getEditor() != null) {
            tf = (JTextField) getEditor().getEditorComponent();
          

            if (tf != null) {
                tf.addKeyListener(TecladoPara_CompraNota);
              //para fazer apenas uma verificação de enter precionado
                tf.setDocument(new CBDocument());
                addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent evt) {

                        JTextField tf = (JTextField) getEditor().getEditorComponent();
                        tf.setSize(230, 25);
                        String text = tf.getText();
                        
                        
                        //JOptionPane.showMessageDialog(null, text);
                        ComboBoxModel aModel = getModel();
                       
                        String current;

                        for (int i = 0; i < aModel.getSize(); i++) {
                            Object d = aModel.getElementAt(i);

                            if(d != null){
                                current = d.toString();
                                
                            }else{
                                current = "";
                            }
                           
                            if (current.toLowerCase().startsWith(text.toLowerCase())) {
                                
                                if(current.toLowerCase().equalsIgnoreCase(text.toLowerCase())&& contador == 0){
                                  
                                  contador++;
                                }
                                tf.setText(current);
                                tf.setSelectionStart(text.length());
                                tf.setSelectionEnd(current.length());

                                break;
                            }
                            
                        }
                       
                    }
                });
            }
        }
        
    }

    @Override
    public int selectionForKey(char aKey, ComboBoxModel aModel) {

        long now = new java.util.Date().getTime();
        if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
            searchFor = searchFor.substring(0, searchFor.length() - 1);

        } else {
            //	System.out.println(lap);
            // Kam nie hier vorbei.
            if (lap + 1000 < now) {
                searchFor = "" + aKey;
            } else {
                searchFor = searchFor + aKey;
            }
        }
        lap = now;
        String current;

        for (int i = 0; i < aModel.getSize(); i++) {
            current = aModel.getElementAt(i).toString().toLowerCase();
            if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                return i;
            }
        }

        return -1;
    }

    public static String[] pegaTodosNomesProdutos() {
        int i, j = 0;
      
        String[] names = {""};
        ArrayList<String> Sentinela = new ArrayList<>();

        
        names = new String[ListaProduto_Estoque.size()];
        Sentinela.add("");
        for (i = 1; i < ListaProduto_Estoque.size(); i++) {
            
            Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
            Sentinela.add(Produto_Estoque.getNomeProduto());
        }
    
        names = Sentinela.toArray(names);
        Arrays.sort(names, 0, ListaProduto_Estoque.size());

        return (names);
    }
    //retorna o codigo do produto
    public String RetornaCodigoProduto(String name){
       for(int i = 0; i < ListaProduto_Estoque.size();i++){
         Produto_Estoque = (PRODUTO_ESTOQUE)ListaProduto_Estoque.get(i);
         if(name.equalsIgnoreCase(Produto_Estoque.getNomeProduto())){
           return Produto_Estoque.getCodigoProduto();
         } 
       } 
       
    return "";
    }
    
     public static String[] pegaTodosNomesFornecedores() {
       int i;
       
       String[] names = {""};
       ArrayList<String> Sentinela = new ArrayList<>();
     
       ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
       names = new String[ListaCompraNota.size()];
       Sentinela.add("");
       
       for(i = 0; i < ListaCompraNota.size(); i++){
         compra_nota = (COMPRA_NOTA)ListaCompraNota.get(i);
         Sentinela.add(compra_nota.getNomeFornecedor());
       }
       
       names = Sentinela.toArray(names);
       Arrays.sort(names, 0, ListaCompraNota.size());
       
       return(names);
     }
    
    
        static void createAndShowGUI_2() {
            JFrame f = new JFrame("AutoCompleteComboBox");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setSize(265, 200);
            Container cp = f.getContentPane();
            cp.setLayout(null);
            pegaTodosNomesProdutos();

           
            Locale[] locales = Locale.getAvailableLocales();//
            JComboBox cBox = new AutoCompleteCombo(pegaTodosNomesProdutos());
            cBox.setBounds(50, 50, 100, 25);
            cBox.setMaximumRowCount( 20 ); 
            cBox.setSize(250, 25);
            cBox.setLocation(0, 0);
            cBox.setEditable(true);
            cp.add(cBox);
            f.setVisible(true);

        }

        public static void main(String arg[]) {
          createAndShowGUI_2();
          //createAndShowGUI_1();
        }
        
        // classe para capturar toda a ação de teclado que envolver a classe compra nota 
        public class TecladoPara_CompraNota implements KeyListener, ActionListener{

        @Override
        public void keyTyped(KeyEvent e) {
            
        }

        @Override
        public void keyPressed(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
          if(e.getKeyCode() == 10){
           ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
           
           for(int i = 0; i < ListaCompraNota.size();i++){
             compra_nota = (COMPRA_NOTA)ListaCompraNota.get(i);
               if(tf.getText().equalsIgnoreCase(compra_nota.getNomeFornecedor())){
                 // JOptionPane.showMessageDialog(null, "O Fornecedor Tem uma venda Aberta!");
               }
           
           }
           P_CompraNota.DirecionaCusorFornecedor();
          }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
           
        }
        
        
        
        }
    }

