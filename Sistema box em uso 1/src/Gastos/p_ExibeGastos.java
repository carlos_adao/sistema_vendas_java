/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_GASTOS;
import Cadastro.Datas;
import Compras.Visualizar.TabelaModel;
import Interface_Alternavel.AlternavelCadastroProduto;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Convidado
 */
public class p_ExibeGastos extends JPanel {

    /*
     * parte de instancia da classe gasto
     */
    private BOX gastos = new BOX();
    private final P_Gastos p_gastos = new P_Gastos();
    private final ARQUIVO_GASTOS arquivoGastos = new ARQUIVO_GASTOS();
    private ArrayList<BOX> ListaGastos;
    private OrdenacaoArrayList ordenacaoArrayList = new OrdenacaoArrayList();

    ArrayList<String[]> dados = new ArrayList<>();
    ArrayList<String[]> dadosOrdenados = new ArrayList<>();
    JLabel dataTitulo = new JLabel();
    JLabel JLgastoBox = new JLabel();
    JTextField JTgastoBox = new JTextField();
    JLabel JLgastoCarlos = new JLabel();
    JTextField JTgastoCarlos = new JTextField();
    JLabel JLgastoEliane = new JLabel();
    JTextField JTgastoEliane = new JTextField();
    float gastoBox = 0, gastoCarlos = 0, gastoEliane = 0;
    P_FrameEditaGastos p_frameEditaGastos;
    int posicao = 0; //variavel para guardar a posicao do gasto na lista de gastos 
    Datas data = new Datas();

    Calculos Calculos = new Calculos();

    TabelaModel modelo;
    JTable jtable = new JTable();
    JScrollPane JStabelaGastos = new JScrollPane(jtable);
    JComboBox comboBox;
    JTextField tf;

    public p_ExibeGastos() {
        this.ListaGastos = new ArrayList<>();
        this.modelo = new TabelaModel();
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

    }

    public void iniciaLabel(String dia) {

        dataTitulo.setBounds(new Rectangle(168, 135, 400, 30));
        dataTitulo.setText(" Gastos do dia ");
        dataTitulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 15));
        dataTitulo.setLocation(7, 10);
        add(dataTitulo);
        
        comboBox = new AltoCompete(Calculos.RetornaTodosDiasMes(data.retornaMes()));
        comboBox.setEditable(true);
        comboBox.setBounds(50, 50, 100, 25);
        comboBox.setMaximumRowCount(20);
        comboBox.setSize(180, 25);
        comboBox.setLocation(10, 36);
        add(comboBox);
        JavaApplication4 javaApplication4 = new JavaApplication4(comboBox);

        JLgastoBox.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoBox.setText("Gasto Box");
        JLgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoBox.setLocation(850, 40);
        add(JLgastoBox);

        JTgastoBox.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoBox.setSize(120, 50);
        JTgastoBox.setLocation(850, 80);//
        add(JTgastoBox);

        JLgastoCarlos.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoCarlos.setText("Gasto Carlos");
        JLgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoCarlos.setLocation(850, 120);
        add(JLgastoCarlos);

        JTgastoCarlos.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoCarlos.setSize(120, 50);
        JTgastoCarlos.setLocation(850, 160);//
        add(JTgastoCarlos);

        JLgastoEliane.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoEliane.setText("Gastos Eliane");
        JLgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoEliane.setLocation(850, 210);
        add(JLgastoEliane);

        JTgastoEliane.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoEliane.setSize(120, 50);
        JTgastoEliane.setLocation(850, 250);
        add(JTgastoEliane);

    }

    public void remove() {
        dados.clear();

        Tabela(dados);
    }

    public void criaTabela(String dia) {

        gastoBox = 0;
        gastoCarlos = 0;
        gastoEliane = 0;

        iniciaLabel(dia);

        ListaGastos = arquivoGastos.retorna();

        if (ListaGastos.size() > 0) {

            for (int i = 0; i < ListaGastos.size(); i++) {
                gastos = (BOX) ListaGastos.get(i);

                if (gastos.getDia().equalsIgnoreCase(dia)) {

                    String quemGastou = gastos.getNome();
                    String comOque = gastos.getComQue();
                    String quantoGastou = AlternavelCadastroProduto.converterFloatString(gastos.getQuanto());
                    String data = gastos.getDia();

                    String[] ListaGastos = new String[]{quemGastou, comOque, quantoGastou, data};
                    dados.add(ListaGastos);

                    if (quemGastou.equalsIgnoreCase("Box")) {
                        gastoBox = gastoBox + gastos.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Carlos")) {
                        gastoCarlos = gastoCarlos + gastos.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Eliane")) {
                        gastoEliane = gastoEliane + gastos.getQuanto();

                    }

                }
            }

            Tabela(dados);
            JTgastoBox.setText(AlternavelCadastroProduto.converterFloatString(gastoBox));
            JTgastoCarlos.setText(AlternavelCadastroProduto.converterFloatString(gastoCarlos));
            JTgastoEliane.setText(AlternavelCadastroProduto.converterFloatString(gastoEliane));
        }
    }

    public void Tabela(ArrayList<String[]> dados) {
      
        String[] colunas = new String[]{"Quem gastou", "Com o que", "Quanto", "Dia"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(200);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.setAutoCreateRowSorter(true);

        jtable.addMouseListener(new MouseAdapter() {

            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    linha = jtable.getSelectedRow();
                    String quemGastou = String.valueOf(jtable.getValueAt(linha, 0));
                    String comQueGastou = String.valueOf(jtable.getValueAt(linha, 1));
                    String quantoGastou = String.valueOf(jtable.getValueAt(linha, 2));

                    String emQueData = String.valueOf(jtable.getValueAt(linha, 3));

                    ListaGastos = arquivoGastos.retorna();
                    gastos = (BOX) ListaGastos.get(linha);

                    for (int i = 0; i < ListaGastos.size(); i++) {
                        gastos = (BOX) ListaGastos.get(i);
                        if (quemGastou.equalsIgnoreCase(gastos.getNome()) && comQueGastou.equalsIgnoreCase(gastos.getComQue()) && quantoGastou.equalsIgnoreCase(AlternavelCadastroProduto.converterFloatString(gastos.getQuanto()))) {
                            posicao = i;
                            p_frameEditaGastos = new P_FrameEditaGastos(quemGastou, comQueGastou, quantoGastou, emQueData, posicao);
                        }
                    }
                }
            }
        });

        JStabelaGastos.setLocation(10, 80);
        JStabelaGastos.setSize(800, 250);
        JStabelaGastos.getVerticalScrollBar();
        add(JStabelaGastos);

    }

    public class AltoCompete extends JComboBox implements JComboBox.KeySelectionManager {

        private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
        private ArrayList<String> listaNomesProdutos = new ArrayList<>();
        private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
        private String searchFor;
        private long lap;
        int contador = 0;
        JFrame f = new JFrame("AutoCompleteComboBox");
        Calculos Calculos = new Calculos();
        JComboBox comboBox;
        JTextField tf;
        AcaoTF AcaoTF = new AcaoTF();

        AltoCompete() {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AltoCompete(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);
            tf = null;
           
            if (getEditor() != null) {
                tf = (JTextField) getEditor().getEditorComponent();

                if (tf != null) {
                    //para fazer apenas uma verificação de enter precionado
                    tf.setDocument(new CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            tf = (JTextField) getEditor().getEditorComponent();
                            tf.setSize(230, 25);
                            String text = tf.getText();

                            //JOptionPane.showMessageDialog(null, text);
                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {
                                        //JOptionPane.showMessageDialog(null, "prescionado");
                                        contador++;
                                    }
                                    tf.setText(current);
                                    tf.setSelectionStart(text.length());
                                    tf.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }
            tf.addKeyListener(AcaoTF);
            tf.addActionListener(AcaoTF);
            tf.setText(null);
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();

                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }

        String RetornaNome() {

            return tf.getText();
        }

        /**
         *
         * @param dias
         */
        public void Atualiza(String[] dias) {
            this.setModel(new DefaultComboBoxModel<>(dias));
        }

        public class AcaoTF implements KeyListener, ActionListener {

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == 10) {

                    dados.clear();
                   
                    jtable.setModel(null);
                    criaTabela(tf.getText());

                }
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == tf) {
                    dados.clear();
                    Tabela(dados);

                    criaTabela(tf.getText());
                }
            }
        }

    }

}
