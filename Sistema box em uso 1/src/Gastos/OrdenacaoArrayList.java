/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import BancoDados.ARQUIVO_GASTOS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JOptionPane;

/**
 *
 * @author Convidado
 */
public class OrdenacaoArrayList extends BOX{

    private ArrayList<BOX> ListaGastos = new ArrayList<BOX>();
    ArrayList<String[]> dados = new ArrayList<>();
    private BOX gastos = new BOX();
    private ARQUIVO_GASTOS arquivoGastos = new ARQUIVO_GASTOS();
    
        
    public ArrayList<String[]> retornaOrdenado() {
        
        
        ListaGastos = arquivoGastos.retorna();
        
        Collections.sort(ListaGastos);
               
        for (int i = 0; i < ListaGastos.size(); i++) {
            gastos = (BOX) ListaGastos.get(i);

            String nome = gastos.getNome();
            String quanto = String.valueOf(gastos.getQuanto());
            String com_o_Que = gastos.getComQue();
            String dia  = gastos.getDia();
            
            String[] gastos_S = new String[]{nome, com_o_Que, quanto, dia};
            dados.add(gastos_S);
     
        }

        return dados;
    }
}