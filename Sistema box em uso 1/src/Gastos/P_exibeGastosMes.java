/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import BancoDados.ARQUIVO_GASTOS;
import Interface_Alternavel.AlternavelCadastroProduto;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author User
 */
public final class P_exibeGastosMes extends JPanel {

    /* parte de instancia da classe gasto*/
    private BOX gastos = new BOX();
    private P_Gastos p_gastos = new P_Gastos();
    private ARQUIVO_GASTOS arquivoGastos = new ARQUIVO_GASTOS();
    ArrayList<String[]> dados = new ArrayList<>();
    private ArrayList<BOX> ListaGastos = new ArrayList<>();
    private OrdenacaoArrayList ordenacaoArrayList = new OrdenacaoArrayList();
    public static JScrollPane JStabelaGastos;
    ArrayList<String[]> dadosOrdenados = new ArrayList<>();
    String quemGastou, comOque, quantoGastou, data;
    JTable jtable = new JTable();
    JLabel dataTitulo = new JLabel();
    JLabel JLgastoBox = new JLabel();
    JTextField JTgastoBox = new JTextField();
    JLabel JLgastoCarlos = new JLabel();
    JTextField JTgastoCarlos = new JTextField();
    JLabel JLgastoEliane = new JLabel();
    JTextField JTgastoEliane = new JTextField();
    float gastoBox = 0, gastoCarlos = 0, gastoEliane = 0;
    P_FrameEditaGastos p_frameEditaGastos;
    int posicao = 0;
    JComboBox<String> JCopcoes;//combo que seleciona a forma de visualização
    String opcoes[];// vetor usado na opção de visualização do mês 

    public P_exibeGastosMes() {
        this.opcoes = new String[]{"Visualizar por mês", "Visualizar por Categoria"};
        this.setLayout(null);
        this.setSize(1336, 465);
        this.setLocation(0, 160);
        this.setBackground(Color.white);

    }

    public void iniciaLabel(String dia) {

        dataTitulo.setBounds(new Rectangle(168, 135, 400, 30));
        dataTitulo.setText(" Gastos do mês" + dia);
        dataTitulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        dataTitulo.setLocation(250, 40);
        add(dataTitulo);

        JLgastoBox.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoBox.setText("Gasto Box");
        JLgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoBox.setLocation(850, 40);
        add(JLgastoBox);

        JTgastoBox.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoBox.setSize(120, 50);
        JTgastoBox.setLocation(850, 80);//
        add(JTgastoBox);

        JLgastoCarlos.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoCarlos.setText("Gasto Carlos");
        JLgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoCarlos.setLocation(850, 120);
        add(JLgastoCarlos);

        JTgastoCarlos.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoCarlos.setSize(120, 50);
        JTgastoCarlos.setLocation(850, 160);//
        add(JTgastoCarlos);

        JLgastoEliane.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoEliane.setText("Gastos Eliane");
        JLgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoEliane.setLocation(850, 210);
        add(JLgastoEliane);

        JTgastoEliane.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoEliane.setSize(120, 50);
        JTgastoEliane.setLocation(850, 250);
        add(JTgastoEliane);

        JCopcoes = new JComboBox(opcoes);
        JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCopcoes.setSize(200, 20);
        JCopcoes.setLocation(10, 36);
        JCopcoes.addItemListener(
                new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {

                            //seleciona a opção de visualizar pelo mês 
                            if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar por mês")) {

                            }

                            //seleciona a opcao de visualizar por categorias
                            if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar por Categoria")) {

                            }
                           
                        }
                    }
                });
        add(JCopcoes);

    }

    public void tabelaVazia() {

        for (int t = 0; t < 30; t++) {
            String[] istaGastos = new String[]{"test", "test", "test"};
            dados.add(istaGastos);
        }

        Tabela(dados);

    }

    public void criaTabelaMes(String mes) {

        gastoBox = 0;
        gastoCarlos = 0;
        gastoEliane = 0;

        iniciaLabel(mes);

        ListaGastos = arquivoGastos.retorna();
        Collections.sort(ListaGastos);

        if (ListaGastos.size() > 0) {

            for (int i = 0; i < ListaGastos.size(); i++) {
                gastos = (BOX) ListaGastos.get(i);

                if (gastos.getMes().equalsIgnoreCase(mes)) {

                    String quemGastou = gastos.getNome();
                    String comOque = gastos.getComQue();
                    String quantoGastou = AlternavelCadastroProduto.converterFloatString(gastos.getQuanto());
                    String dat = gastos.getDia();

                    String[] ListaGastos = new String[]{quemGastou, comOque, quantoGastou, dat};
                    dados.add(ListaGastos);

                    if (quemGastou.equalsIgnoreCase("Box")) {
                        gastoBox = gastoBox + gastos.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Carlos")) {
                        gastoCarlos = gastoCarlos + gastos.getQuanto();

                    }
                    if (quemGastou.equalsIgnoreCase("Eliane")) {
                        gastoEliane = gastoEliane + gastos.getQuanto();

                    }

                }
            }

            Tabela(dados);
            JTgastoBox.setText(AlternavelCadastroProduto.converterFloatString(gastoBox));
            JTgastoCarlos.setText(AlternavelCadastroProduto.converterFloatString(gastoCarlos));
            JTgastoEliane.setText(AlternavelCadastroProduto.converterFloatString(gastoEliane));

        }
    }

    public void remove() {
        dados.clear();

        Tabela(dados);
    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Quem gastou", "Com o que", "Quanto", "data"};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        jtable = new JTable(modelo);
        //JOptionPane.showMessageDialog(null,"Modelo"+ modelo.linhas.size());
        jtable.setValueAt(modelo, 0, 0);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.addMouseListener(new MouseAdapter() {

            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    linha = jtable.getSelectedRow();
                    String quemGastou = String.valueOf(jtable.getValueAt(linha, 0));
                    String comQueGastou = String.valueOf(jtable.getValueAt(linha, 1));
                    String quantoGastou = String.valueOf(jtable.getValueAt(linha, 2));

                    String emQueData = String.valueOf(jtable.getValueAt(linha, 3));

                    ListaGastos = arquivoGastos.retorna();
                    gastos = (BOX) ListaGastos.get(linha);

                    for (int i = 0; i < ListaGastos.size(); i++) {
                        gastos = (BOX) ListaGastos.get(i);
                        if (quemGastou.equalsIgnoreCase(gastos.getNome()) && comQueGastou.equalsIgnoreCase(gastos.getComQue()) && quantoGastou.equalsIgnoreCase(AlternavelCadastroProduto.converterFloatString(gastos.getQuanto()))) {
                            int posicao = i;
                            p_frameEditaGastos = new P_FrameEditaGastos(quemGastou, comQueGastou, quantoGastou, emQueData, posicao);
                        }
                    }
                }
            }
        });
        add(jtable);

        if (modelo.linhas.size() > 0 && modelo.linhas.size() < 18) {
            TableColumn coluna = null;
            jtable.setLocation(1, 100);
            jtable.setSize(800, 250);
            jtable.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
            add(jtable);

        }
        if (modelo.linhas.size() >= 18) {

            JStabelaGastos = new JScrollPane(jtable);
            JStabelaGastos.setLocation(10, 80);
            JStabelaGastos.setSize(800, 250);
            JStabelaGastos.getVerticalScrollBar();
            add(JStabelaGastos);

        }

    }

    public final class TabelaModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private TabelaModel(ArrayList dados, String[] colunas) {

            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {

            return linhas;

        }

        public void setColunas(String[] strings) {

            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String [] 

            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return colunas[column];
        }
    }

}
