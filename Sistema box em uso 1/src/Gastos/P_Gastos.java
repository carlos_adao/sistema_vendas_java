/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_GASTOS;
import Cadastro.Datas;
import Compras.P_CompraNota;
import Interface_Alternavel.JNumberFormatField;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public final class P_Gastos extends JPanel {

    JLabel Jlnome = new JLabel();
    JTextField Jtnome = new JTextField();
    JLabel JlcomQue = new JLabel();
    JTextField JtcomQue = new JTextField();
    private JComboBox cBox_comQue;
    JLabel Jlquanto = new JLabel();
    JNumberFormatField Jtquanto = new JNumberFormatField(new DecimalFormat("0.00"));
    JLabel Jlobservacao = new JLabel();
    JTextArea Jtobservacao = new JTextArea();
    JScrollPane Jsobservacao = new JScrollPane();
    JComboBox<String> JCnome = new JComboBox<>();
    private String nomes[] = {"Box", "Carlos", "Eliane"};
    JButton Botao_salvar, Botao_cancelar;
    private Acao acao = new Acao();
    BOX box = new BOX();
    P_CompraNota PcompraNota = new P_CompraNota();
    private String searchFor;
    private long lap;
    int contador = 0;
    ArrayList<BOX> Listagastos = new ArrayList<>();
    ARQUIVO_GASTOS arquivo_gastos = new ARQUIVO_GASTOS();
    JFormattedTextField JTdataManual = new JFormattedTextField(Mascara("##/##/####"));
    JFormattedTextField JTmesManual = new JFormattedTextField(Mascara("##"));
    JFormattedTextField JTanoManual = new JFormattedTextField(Mascara("####"));
    private Datas data = new Datas();
    String Data = data.retornaData();
    String mes = data.retornaMes();
    String ano = data.retornaAno();
    
    AutoCompleteProduto AutoCompleteProduto = new AutoCompleteProduto(pegaTodosNomes());
    Calculos Calculos = new Calculos();
    
    TecladoComQue tecladoComque = new TecladoComQue();
    
    
    TecladoBotao TecladoBotao = new TecladoBotao();
    TecladoQuanto tecladoQuanto = new TecladoQuanto();

    public P_Gastos() {
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);




        Jlnome.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlnome.setText("Quem gastou?");
        Jlnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlnome.setLocation(10, 40);//
        add(Jlnome);

        JCnome = new JComboBox(nomes);
        JCnome.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCnome.setSize(250, 25);
        JCnome.setLocation(10, 75);
        add(JCnome);
        
        
        JlcomQue.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlcomQue.setText("Com o que?");
        JlcomQue.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlcomQue.setLocation(280, 40);
        add(JlcomQue);
        
        cBox_comQue = AutoCompleteProduto;
        cBox_comQue.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_comQue.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        cBox_comQue.setSize(250, 25);
        cBox_comQue.setLocation(280, 75);
        cBox_comQue.setEditable(true);
        cBox_comQue.addKeyListener(tecladoComque);
        cBox_comQue.addFocusListener(tecladoComque);
        add(cBox_comQue);//

       

        Jlquanto.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlquanto.setText("Quanto?");
        Jlquanto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlquanto.setLocation(550, 40);
        add(Jlquanto);

        Jtquanto.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        Jtquanto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jtquanto.setSize(250, 25);
        Jtquanto.setLocation(550, 75);
        Jtquanto.addKeyListener(tecladoQuanto);
        add(Jtquanto);

        Jlobservacao.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        Jlobservacao.setText("Observação");
        Jlobservacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Jlobservacao.setLocation(10, 110);
        add(Jlobservacao);

        Jsobservacao = new JScrollPane(Jtobservacao);
        Jsobservacao.setSize(790, 100);
        Jsobservacao.setLocation(10, 140);
        add(Jsobservacao);

        JTdataManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTdataManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTdataManual.setSize(100, 25);
        JTdataManual.setLocation(0, 438);//
        JTdataManual.setText(Data);
        add(JTdataManual);

        JTmesManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTmesManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTmesManual.setSize(30, 25);
        JTmesManual.setText(mes);
        JTmesManual.setLocation(100, 438);//
        add(JTmesManual);

        JTanoManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTanoManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTanoManual.setSize(60, 25);
        JTanoManual.setText(ano);
        JTanoManual.setLocation(130, 438);//
        add(JTanoManual);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.addActionListener(acao);
        Botao_salvar.setLocation(100, 400);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        Botao_salvar.addKeyListener(TecladoBotao);
        Botao_cancelar.addActionListener(acao);
        Botao_cancelar.setLocation(200, 400);
        add(Botao_cancelar);

    }

    public void limpa_txt() {
        Jtnome.setText(null);
        JtcomQue.setText(null);
        Jtquanto.setText(null);
        Jtobservacao.setText(null);
        Jtnome.requestFocus();
    }

    public MaskFormatter Mascara(String Mascara) {

        MaskFormatter F_Mascara = new MaskFormatter();
        try {
            F_Mascara.setMask(Mascara); //Atribui a mascara  
            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
        } catch (Exception excecao) {
            excecao.printStackTrace();
        }
        return F_Mascara;
    }

    /*
     * PEGA TODOS OS NOMES DOS PRODUTOS PARA O AUTO COMPLETE
     */
    public String[] pegaTodosNomes() {
        int i, j = 0;

        String[] names = {""};
        ArrayList<String> Sentinela = new ArrayList<>();
        Listagastos = ARQUIVO_GASTOS.retorna();

        names = new String[Listagastos.size()];
        Sentinela.add("");
        for (i = 1; i < Listagastos.size(); i++) {

            box = (BOX) Listagastos.get(i);
            Sentinela.add(box.getComQue());
        }

        names = Sentinela.toArray(names);
        Arrays.sort(names, 0, Listagastos.size());

        return (names);
    }

    public float enviaGastos(String data) {
        float gastos = 0;


        Listagastos = arquivo_gastos.retorna();
        Collections.sort(Listagastos);

        for (int i = 0; i < Listagastos.size(); i++) {
            box = (BOX) Listagastos.get(i);

            if (box.getDia().equalsIgnoreCase(data)) {

                gastos = gastos + box.getQuanto();

            }
        }

        return gastos;
    }

    //CLASSE PARA IMPLEMENTAR O AUTO-COMPLETE DO PRODUTO
    public class AutoCompleteProduto extends JComboBox implements JComboBox.KeySelectionManager {

        private AutoCompleteProduto() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        //Auto Completa o Tf_produto
        public AutoCompleteProduto(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtcomQue = (JTextField) getEditor().getEditorComponent();

                if (JtcomQue != null) {
                    JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtcomQue.setDocument(new AutoCompleteProduto.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtcomQue = (JTextField) getEditor().getEditorComponent();
                            JtcomQue.setSize(230, 25);
                            String text = JtcomQue.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtcomQue.setText(current);
                                    JtcomQue.setSelectionStart(text.length());
                                    JtcomQue.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }
     public void AtualizaListaProdutosAutoComplete(){
     String[]nomes = pegaTodosNomes();
        AutoCompleteProduto.setModel(new DefaultComboBoxModel<>(nomes));
      
    }

    public class TecladoComQue implements KeyListener, FocusListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                Jtquanto.requestFocus();
             
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }

    public class TecladoQuanto implements KeyListener, FocusListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                Botao_salvar.requestFocus();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }

    public class TecladoBotao implements KeyListener, FocusListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                if (JtcomQue.getText().isEmpty() || Float.parseFloat(Jtquanto.getText().replace(",", ".")) == (float) 0.0) {
                    JtcomQue.requestFocus();
                    JOptionPane.showMessageDialog(null, "preencha os campos vazios");
                } else {
                    Listagastos = arquivo_gastos.retorna();

                    box.setNome(JCnome.getSelectedItem().toString());
                    box.setComQue(JtcomQue.getText());
                    box.setQuanto(Float.parseFloat(Jtquanto.getText().replace(",", ".")));
                    box.setObservacao(Jtobservacao.getText().replace("\n", "&"));
                    box.setDia(JTdataManual.getText());
                    box.setMes(JTmesManual.getText());
                    box.setAno(JTanoManual.getText());


                    Listagastos.add(box);

                    try {
                        arquivo_gastos.Armazena(Listagastos);
                    } catch (IOException ex) {
                        Logger.getLogger(P_Gastos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   
                    limpa_txt();
                    cBox_comQue.requestFocus();
                }
                
                 AtualizaListaProdutosAutoComplete();
               
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }

    public class Acao implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_cancelar) {

                Listagastos = arquivo_gastos.retorna();

                for (int i = 0; i < Listagastos.size(); i++) {
                    box = (BOX) Listagastos.get(i);


                }

                limpa_txt();
            }

            if (e.getSource() == Botao_salvar) {


                if (JtcomQue.getText().isEmpty() || Float.parseFloat(Jtquanto.getText().replace(",", ".")) == (float) 0.0) {
                    JtcomQue.requestFocus();
                    JOptionPane.showMessageDialog(null, "preencha os campos vazios");
                } else {
                    Listagastos = arquivo_gastos.retorna();

                    box.setNome(JCnome.getSelectedItem().toString());
                    box.setComQue(JtcomQue.getText());
                    box.setQuanto(Float.parseFloat(Jtquanto.getText().replace(",", ".")));
                    box.setObservacao(Jtobservacao.getText().replace("\n", "&"));
                    box.setDia(JTdataManual.getText());
                    box.setMes(JTmesManual.getText());
                    box.setAno(JTanoManual.getText());


                    Listagastos.add(box);

                    try {
                        arquivo_gastos.Armazena(Listagastos);
                    } catch (IOException ex) {
                        Logger.getLogger(P_Gastos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    limpa_txt();
                    cBox_comQue.requestFocus();
                }
                
                 AtualizaListaProdutosAutoComplete();
            }
        }
    }
}
