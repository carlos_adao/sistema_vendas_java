/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import BancoDados.ARQUIVO_GASTOS;
import Interface_Alternavel.PRODUTO_VENDA;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import Auxiliares.Datas;
import javax.swing.JOptionPane;

public class Calculos {

    Set<String> Lgastos = new HashSet<>();
    ArrayList<BOX> Listagastos = ARQUIVO_GASTOS.retorna();
    ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = ARQUIVO_ESTOQUE_VENDA.retorna();
    PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA();
    Set<String> LprodutosVendidos = new HashSet<>();
    BOX gastos = new BOX();
    String[] dias = new String[Listagastos.size()];
    String hoje;
    String[] Dias = {""};
    String[] Meses = {""};
    String[] Anos = {""};
    String[] Dia_s_repeticao = {""};
    ArrayList<String> Sentinela = new ArrayList<>();
    Datas Datas = new Datas();

    public Calculos() {

        this.Dias = new String[]{hoje};
    }

    //Obtem todos os dias de um determinado mes sem repetições

    public String[] RetornaTodosDiasMes(String mes) {

        for (int i = 0; i < Listagastos.size(); i++) {
            gastos = (BOX) Listagastos.get(i);
            if (gastos.getMes().equalsIgnoreCase(mes)) {
                Sentinela.add(gastos.getDia());

            }
        }
        Dias = Sentinela.toArray(Dias);

        for (String Sentinela1 : Sentinela) {
            Lgastos.add(Sentinela1);
        }

        Dia_s_repeticao = Lgastos.toArray(Dia_s_repeticao);
        Arrays.sort(Dia_s_repeticao, Collections.reverseOrder());

        return Dia_s_repeticao;
    }

    /*[VENDAS] dado uma lista de objetos retorna todos os dias do mes que hover registros */
    public String[] DiasMesVendas() {
        Dias = new String[0];
        Dias = Datas.RetornaDias_Mes().toArray(Dias);
      
        Arrays.sort(Dias, Collections.reverseOrder());

        return Dias;
       
    }

    public String[] MesAnoVendas() {
       
        Meses = Datas.RetornaMes_Ano().toArray(Meses);
        Arrays.sort(Meses, Collections.reverseOrder());

        return Meses;

    }
     public String[] RetornaAnosVendas() {
      
        Anos = Datas.RetornaAnos().toArray(Anos);
        Arrays.sort(Anos, Collections.reverseOrder());

        return Anos;

    }
}
