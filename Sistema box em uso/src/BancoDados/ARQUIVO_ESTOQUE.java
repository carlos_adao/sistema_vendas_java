package BancoDados;

import Interface_Alternavel.PRODUTO_ESTOQUE;

import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author carlos
 */
public class ARQUIVO_ESTOQUE {
   private static PRODUTO_ESTOQUE produtos;
   private static String nomeProduto;
   private static String codigoProduto;
   private static int quantidadeProduto;
   private static float precoCompra_unidade,precoVenda_unidade,precoCompra_Geral,LucroUnidade,LucroGeral;
   private static File file = new File("Arquivo de Produtos Estoque.csv");
   private static File arq = new File("Arquivo de Produtos Estoque.txt"); 
   
   public static void Armazena(ArrayList<PRODUTO_ESTOQUE> Produtos) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        
        if(!file.exists()){
          file.createNewFile();
        }
        
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        
        FileWriter fwTxt = new FileWriter(arq);
        PrintWriter gravarArq = new PrintWriter(arq);

        int i, n = Produtos.size();
        for (i = 0; i < n; i++) {
            
            gravarArq.printf("%s%n", Produtos.get(i));
            bw.write(Produtos.get(i).toString());

        }
        
        gravarArq.close();
        bw.close();
        fw.close();
      
        System.out.println("gravado com sucesso!!!");
    }
    
    public static ArrayList<PRODUTO_ESTOQUE> retorna(){ // retorna o dados do arquivo para a classe produto 
          ArrayList<PRODUTO_ESTOQUE> ListaProdutos = new ArrayList<PRODUTO_ESTOQUE>();       
       
       try{
        
        FileReader fr = new FileReader(file);
        BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8")); 
        
        String linha = lerArq.readLine();
        //JOptionPane.showMessageDialog(null, linha);
        
        //JOptionPane.showMessageDialog(null, linha+"\n");
        int j = 0;
        
        while(linha != null){
            
            //JOptionPane.showMessageDialog(null,"Aqui e a linha:"+ linha+"\n");
            //JOptionPane.showMessageDialog(null, ListaProdutos.size());
            String[] produto = linha.split(";");     
            
            
                  
                 nomeProduto = new String(produto[0]);
                 
                 codigoProduto = new String( produto[1]);
                 try{
                 quantidadeProduto = Integer.parseInt(produto[2]);
                 precoCompra_unidade = Float.parseFloat(produto[3]);
                 precoVenda_unidade = Float.parseFloat(produto[4]);
                 precoCompra_Geral = Float.parseFloat(produto[5]);
                 LucroUnidade = Float.parseFloat(produto[6]);
                 LucroGeral = Float.parseFloat(produto[7]);
                 
          
             produtos = new PRODUTO_ESTOQUE (nomeProduto, codigoProduto, quantidadeProduto, precoCompra_unidade, 
                                            precoVenda_unidade, precoCompra_Geral, LucroUnidade, LucroGeral);
             
             //2JOptionPane.showMessageDialog(null, linha);    
             }catch(NumberFormatException nu){}
             linha = lerArq.readLine();
             
             ListaProdutos.add(produtos); 
             
            
             
     
       }
        
        fr.close();
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
        return ListaProdutos;
    }

}
