/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDados;

import Cadastro.CadastroProduto;
import java.io.*;
import java.util.ArrayList;

public class ArquivoProdutos {  
    static CadastroProduto produtos; 
    static String codigoProduto = null,nomeProduto = null;  
    static float precoUnidade = 0,precoTotal = 0;
    static int quantidade = 0; 
     
    public static void Armazena(ArrayList<CadastroProduto> ListaProdutos)throws IOException {  //passa da classe produtos para o arquivo lista de produtos 
            
      
        FileWriter arq = new FileWriter("Arquivo de Produtos.txt");     
        PrintWriter gravarArq = new PrintWriter(arq);
        
        int i, n = ListaProdutos.size(); 
        for(i= 0; i < n; i++){
          gravarArq.printf("%s%n", ListaProdutos.get(i));
        
        }
       gravarArq.close(); 
       
       System.out.println("gravado com sucesso!!!");
    }
    
    public static ArrayList<CadastroProduto> retorna(){ // retorna o dados do arquivo para a classe produto 
        ArrayList<CadastroProduto> ListaProdutos = new ArrayList<CadastroProduto>();
       
        try{
        
        FileReader arq = new FileReader("Arquivo de Produtos.txt");
        BufferedReader lerArq = new BufferedReader(arq); 
        
        String linha = lerArq.readLine();
        
       while(linha!= null){
              String[] produto = linha.split(";");     
             
              for(int i = 0; i<5; i++){
                 codigoProduto = ( produto[0]);
                 nomeProduto = (produto[1]);
                 precoUnidade = Float.parseFloat(produto[2]);
                 precoTotal = Float.parseFloat(produto[3]);
                 quantidade = Integer.parseInt(produto[4]);
             }
             produtos = new CadastroProduto(codigoProduto,nomeProduto,precoUnidade,precoTotal,quantidade);
             ListaProdutos.add(produtos);  
             linha = lerArq.readLine();
       }
        
        arq.close();
        
        }catch(IOException e){
             System.err.printf("Erro na abertura do arquivo: %s.",e.getMessage());
       }
        return ListaProdutos;
    }   


}   
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /* String caminho = "C:\teste2.txt";  
            try (FileWriter escrita = new FileWriter(caminho)) {
                BufferedWriter escritor = new BufferedWriter(escrita);
           
           for(int i=0;i< ListaProdutos.size();i++){  
                    escritor.write(ListaProdutos.get(i).toString());  
                    escritor.newLine();  
                }  
                  
                escritor.flush();  
                escritor.close();
            }
       }catch (Exception e) {  
          System.out.println("Erro ao criar arquivo!!");  
    */   
  

