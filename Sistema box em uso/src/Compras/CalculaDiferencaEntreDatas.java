/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Convidado
 */
public class CalculaDiferencaEntreDatas {
    
    public static int calcula(String dia1, String dia2) {
         
        DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        Date data3 = null;
        Date data4 = null;
        long m1 = 0;
        long m2 = 0;
        try {
            data3 = (Date) fmt.parse(dia1);
            data4 = (Date) fmt.parse(dia2);
        } catch (Exception tyretrt) {
       
        }
        
        Calendar data1 = new GregorianCalendar();
        data1.setTime(data3);
        Calendar data2 = new GregorianCalendar();
        data2.setTime(data4);
        
        m1 = data1.getTimeInMillis();
        m2 = data2.getTimeInMillis();
        
        return (int) ((m2 - m1) / (24 * 60 * 60 * 1000));
    }
}
