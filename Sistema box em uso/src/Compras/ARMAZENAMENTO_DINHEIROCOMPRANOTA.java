package Compras;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carlos
 */
public class ARMAZENAMENTO_DINHEIROCOMPRANOTA {
    int primaryKey;
    String formecedor;
    String diaArmazenamento;
    String dataArmazenamento;
    String obcervacao;
    float valorArmazenado;

    public ARMAZENAMENTO_DINHEIROCOMPRANOTA(int primaryKey, String formecedor, String diaArmazenamento, String dataArmazenamento, String obcervacao, float valorArmazenado) {
        this.primaryKey = primaryKey;
        this.formecedor = formecedor;
        this.diaArmazenamento = diaArmazenamento;
        this.dataArmazenamento = dataArmazenamento;
        this.obcervacao = obcervacao;
        this.valorArmazenado = valorArmazenado;
    }

    ARMAZENAMENTO_DINHEIROCOMPRANOTA() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getFormecedor() {
        return formecedor;
    }

    public void setFormecedor(String formecedor) {
        this.formecedor = formecedor;
    }

    public String getDiaArmazenamento() {
        return diaArmazenamento;
    }

    public void setDiaArmazenamento(String diaArmazenamento) {
        this.diaArmazenamento = diaArmazenamento;
    }

    public String getDataArmazenamento() {
        return dataArmazenamento;
    }

    public void setDataArmazenamento(String dataArmazenamento) {
        this.dataArmazenamento = dataArmazenamento;
    }

    public String getObcervacao() {
        return obcervacao;
    }

    public void setObcervacao(String obcervacao) {
        this.obcervacao = obcervacao;
    }

    public float getValorArmazenado() {
        return valorArmazenado;
    }

    public void setValorArmazenado(float valorArmazenado) {
        this.valorArmazenado = valorArmazenado;
    }
    
     @Override
    public String toString() {
        return primaryKey + ";" + formecedor + ";" + diaArmazenamento + ";" + dataArmazenamento + ";" + obcervacao + ";" + valorArmazenado + ";" + "\n";
    }
    
}
