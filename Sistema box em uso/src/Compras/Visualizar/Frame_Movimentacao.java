/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import BancoDados.ARQUIVO_ARMAZENAR_COMPRA_NOTA;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Compras.ARMAZENAR_COMPRA_NOTA;
import Compras.PAGAMENTO_NOTA;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.TableModel;

/**
 *
 * @author carlos
 */
public final class Frame_Movimentacao extends JFrame {

    Container container;

    Painel_Geral Painel_Geral = new Painel_Geral();
    Painel_Topo Painel_Topo;
    Painel_Visualizar_Produtos Painel_Visualizar_Produtos = new Painel_Visualizar_Produtos();
    Painel_EfetuarPagamento Painel_EfetuarPagamento;
    Painel_Visualizar_Pagamentos Painel_Visualizar_Pagamentos = new Painel_Visualizar_Pagamentos();
    Painel_EfetuarArmazenamento Painel_EfetuarArmazenamento;
    Painel_visualizar_Armazenamentos Painel_visualizar_Armazenamentos = new Painel_visualizar_Armazenamentos();
    TableModel TabelaModelo;

    //faz as operacoes de pegar produtos, valores, forncecedores
    Calculos Calculos = new Calculos();

    public Frame_Movimentacao(String nomeFornecedor, String dataPagamento, String dividaAtual) {

        Painel_Topo = new Painel_Topo(nomeFornecedor, dataPagamento, dividaAtual);
        Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
        Painel_EfetuarArmazenamento = new Painel_EfetuarArmazenamento(nomeFornecedor);
        container = getContentPane();
        this.setLayout(null);
        this.setSize(800, 300);
        this.setVisible(true);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(180, 380);

        //this.container.add(Painel_Geral);
        this.container.add(Painel_Topo);
        this.container.add(Painel_Visualizar_Produtos);
        this.container.add(Painel_EfetuarPagamento);
        this.container.add(Painel_Visualizar_Pagamentos);
        this.container.add(Painel_EfetuarArmazenamento);
        this.container.add(Painel_visualizar_Armazenamentos);
        DeixaTodosPainelFalso();
    }
    public void FechaFrame(){
      this.dispose();
    }

    public void DeixaTodosPainelFalso() {
        Painel_EfetuarPagamento.setVisible(false);
        Painel_Visualizar_Produtos.setVisible(false);
        Painel_Visualizar_Pagamentos.setVisible(false);
        Painel_EfetuarArmazenamento.setVisible(false);
        Painel_visualizar_Armazenamentos.setVisible(false);
    }

    public void DeixaPainelVerdadeiro() {
        Painel_EfetuarPagamento.setVisible(true);
    }

    public class Painel_Movimentacao extends JPanel {

        public Painel_Movimentacao() {
            this.setLayout(null);
            this.setSize(800, 200); // larg, alt
            this.setLocation(0, 90);
            this.setBackground(Color.YELLOW);

        }

    }

    public final class Painel_Topo extends JPanel {
        /*Componentes Graficos do Painel*/

        JLabel JlnomeForn = new JLabel();
        JLabel JlvalorDividaAtual = new JLabel();
        JLabel JlvalorPagamentosArmazenados = new JLabel();
        JLabel JlvalorPagamentoEfetuados = new JLabel();
        JLabel JldataLimitePagamento = new JLabel();
        JLabel JlquantidadeDiasRestantes = new JLabel();
        JLabel JltotalPagamentosArmazenados = new JLabel();
        JComboBox<String> JCopcoes;
        String opcoes[];

        /*Classes usadas para pegar as listas de armazenamentos e pagamentos*/
        ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
        ArrayList<ARMAZENAR_COMPRA_NOTA> ListaArmazenamentos = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();

        /*Classe que faz os calculos de total divida, quantidade de dias entre outros*/
        Calculos Calculos = new Calculos();

        /*PARA PASSAR O NOME DO FORNECEDOR PARA OUTRAS CLASSES*/
        String NomeFornecedor;

        //Frame_Movimentacao frame_Movimentacao = new Frame_Movimentacao();
        Painel_EfetuarPagamento Painel_EfetuarPagamento;

        public Painel_Topo(String nomeFornecedor, String dataPagamento, String dividaAtual) {
            this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};

            this.setLayout(null);
            this.setSize(800, 100); // larg, alt
            this.setLocation(0, 0);
            this.setBackground(Color.BLUE);

            Componentes(nomeFornecedor, dataPagamento, dividaAtual);
            Painel_EfetuarPagamento = new Painel_EfetuarPagamento(nomeFornecedor);
            NomeFornecedor = nomeFornecedor;

        }

        Painel_Topo() {
            this.opcoes = new String[]{"Selecione Opcao", "Visualizar Produtos", "Efetua Pagamento", "Visualizar Pagamento", "Efetua Armazenamento", "Remover Armazenarmento ", "Visualizar Armazenamento", "Remover Divida"};
        }

        public String RetornaNomeFornecedor() {
            return NomeFornecedor;
        }

        public void Componentes(String nomeFornecedor, String dataPagamento, String dividaAtual) {

            float Aux_totalpago = Calculos.PagaValorTotalPagamentosEfetuados(nomeFornecedor, ListaPagamentos);
            float Aux_totalArmazenado = Calculos.PagaValorTotalArmazenadoEfetuados(nomeFornecedor, ListaArmazenamentos);

            JlnomeForn.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlnomeForn.setText(nomeFornecedor);
            JlnomeForn.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
            JlnomeForn.setBackground(Color.BLUE);
            JlnomeForn.setLocation(20, 20);//
            add(JlnomeForn);

            JlvalorDividaAtual.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlvalorDividaAtual.setText("Divida Atual R$" + dividaAtual);
            JlvalorDividaAtual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlvalorDividaAtual.setLocation(20, 40);//
            add(JlvalorDividaAtual);

            JlvalorPagamentoEfetuados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JlvalorPagamentoEfetuados.setText("Total Pago R$" + Aux_totalpago);
            JlvalorPagamentoEfetuados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlvalorPagamentoEfetuados.setLocation(300, 40);//
            JlvalorPagamentoEfetuados.setBackground(Color.red);
            add(JlvalorPagamentoEfetuados);

            JldataLimitePagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JldataLimitePagamento.setText("Data Limite Pagamento: " + dataPagamento);
            JldataLimitePagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JldataLimitePagamento.setLocation(20, 60);//
            add(JldataLimitePagamento);

            JltotalPagamentosArmazenados.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            JltotalPagamentosArmazenados.setText("Total Armazenado R$ " + Aux_totalArmazenado);
            JltotalPagamentosArmazenados.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JltotalPagamentosArmazenados.setLocation(300, 60);//
            add(JltotalPagamentosArmazenados);

            JlquantidadeDiasRestantes.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
            //JlquantidadeDiasRestantes.setText(String.valueOf("Qtd Dias Restantes " + RetornaQuantidadeDiasRestante(nome)));
            JlquantidadeDiasRestantes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JlquantidadeDiasRestantes.setLocation(250, 60);//
            add(JlquantidadeDiasRestantes);

            JCopcoes = new JComboBox(opcoes);
            JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
            JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JCopcoes.setSize(200, 20);
            JCopcoes.setLocation(10, 36);
            JCopcoes.addItemListener(
                    new ItemListener() {

                        @Override
                        public void itemStateChanged(ItemEvent e) {
                            if (e.getStateChange() == ItemEvent.SELECTED) {

                                //seleciona a opção visualizar produtos comprados
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {
                                    DeixaTodosPainelFalso();
                                    Painel_Visualizar_Produtos.setVisible(true);
                                    Painel_Visualizar_Produtos.CriaTabelaProdutos(Calculos.PegaprodutosParaCadaFornecedor(NomeFornecedor));
                                }

                                //seleciona a opcao efetuar um pagamento
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Pagamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_EfetuarPagamento.setVisible(true);

                                    DeixaPainelVerdadeiro();
                                }
                                //ARMAZENA UM PAGAMENTO PARA UMA NOTA
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Efetua Armazenamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_EfetuarArmazenamento.setVisible(true);
                                    Painel_EfetuarArmazenamento.Componentes();

                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Armazenamento")) {
                                    //ArmazenarPagamento(nome);
                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Armazenamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_visualizar_Armazenamentos.setVisible(true);
                                    Painel_visualizar_Armazenamentos.CriaTabelaVizualizarArmazenamentos(Calculos.PegaArmazenamentoEfetuados(NomeFornecedor));

                                }
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Remover Divida")) {

                                }
                                //seleciona a opcao para visualizar os pagamentos
                                if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Pagamento")) {
                                    DeixaTodosPainelFalso();
                                    Painel_Visualizar_Pagamentos.setVisible(true);
                                    Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos.pegaPagamentoEfetuados(NomeFornecedor));
                                   // Painel_Visualizar_Pagamentos.CriaTabelaExibePagamentos(Calculos.PegaTodosPagamentosExistentesnoArquivo());

                                }

                            }
                        }
                    });
            add(JCopcoes);

        }

    }

}
