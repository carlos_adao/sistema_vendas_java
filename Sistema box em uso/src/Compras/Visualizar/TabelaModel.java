/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author carlos
 */
public class TabelaModel extends AbstractTableModel{
    
        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        public TabelaModel(ArrayList <String[]> dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

    public TabelaModel() {
      
    }
        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        public int getRowCount() {
            return getLinhas().size();

        }

        public int getColumnCount() {

            return getColunas().length;

        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        public String getColumnName(int column) {
            return colunas[column];
        }

}
