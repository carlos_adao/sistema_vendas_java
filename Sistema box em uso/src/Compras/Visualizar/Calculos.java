/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compras.Visualizar;

import BancoDados.ARQUIVO_ARMAZENAR_COMPRA_NOTA;
import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_PAGAMENTO_NOTA;
import Compras.ARMAZENAR_COMPRA_NOTA;
import Compras.COMPRA_NOTA;
import Compras.PAGAMENTO_NOTA;
import Interface_Alternavel.AlternavelCadastroProduto;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class Calculos {

    PAGAMENTO_NOTA PagamentosEfetuados;
    ARMAZENAR_COMPRA_NOTA ArmazenamentosEfetuados;
    float Valor_TotalPagamentosEfetuados = 0;

    /*Classe que instancia um produco compra nota para fazer as manipulacoes*/
    ArrayList<COMPRA_NOTA> ListaCompraNota = ARQUIVO_COMPRA_NOTA.retorna();
    COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    /*----------------------------------------------------*/

    /*Instancias da classe visualizar pagamentos*/
    ArrayList<PAGAMENTO_NOTA> ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();
    PAGAMENTO_NOTA PagamentoNota = new PAGAMENTO_NOTA();
    TabelaModel modelo;
    /*----------------------------------------------------*/

    /*Instancias da classe visualizar Armazenamentos nota*/
    ArrayList<ARMAZENAR_COMPRA_NOTA> ListaarmazenarPagamento = ARQUIVO_ARMAZENAR_COMPRA_NOTA.retorna();
    ARMAZENAR_COMPRA_NOTA ArmazenarPagamentos = new ARMAZENAR_COMPRA_NOTA();
    /*---------------------------------------------------------------*/

    public float PagaValorTotalPagamentosEfetuados(String nomeFornecedor, ArrayList<PAGAMENTO_NOTA> ListaPagamentosEfetuados) {

        for (PAGAMENTO_NOTA ListaPagamentosEfetuado : ListaPagamentosEfetuados) {
            PagamentosEfetuados = (PAGAMENTO_NOTA) ListaPagamentosEfetuado;
            if (PagamentosEfetuados.getFornecedor().equalsIgnoreCase(nomeFornecedor)) {
                Valor_TotalPagamentosEfetuados += PagamentosEfetuados.getValorRecebeu();
            }
        }

        return Valor_TotalPagamentosEfetuados;
    }

    float PagaValorTotalArmazenadoEfetuados(String nomeFornecedor, ArrayList<ARMAZENAR_COMPRA_NOTA> ListaArmazenamentos) {

        float ValorTotalArmazenado = 0;

        for (ARMAZENAR_COMPRA_NOTA ListaArmazenamento : ListaArmazenamentos) {
            ArmazenamentosEfetuados = (ARMAZENAR_COMPRA_NOTA) ListaArmazenamento;
            if (ArmazenamentosEfetuados.getFormecedor().equalsIgnoreCase(nomeFornecedor)) {
                ValorTotalArmazenado += ArmazenamentosEfetuados.getValorArmazenado();
            }
        }

        return ValorTotalArmazenado;
    }

    public ArrayList<String[]> PegaprodutosParaCadaFornecedor(String nomeFornecedor) {

        ArrayList<String[]> produtosAgardandoPagamendo = new ArrayList<>();
        String[] produtos;
        String nomeProduto, codigoProduto, quantidadeProdutoS, precoCompra_unidadeS, precoVenda_unidadeS, precoCompra_GeralS, LucroUnidadeS, LucroGeralS;

        for (COMPRA_NOTA ListaCompraNota1 : ListaCompraNota) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota1;
            if (nomeFornecedor.equalsIgnoreCase(compra_nota.getNomeFornecedor())) {

                for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                    nomeProduto = compra_nota.getProdutoEstoque().get(j).nomeProduto.replace("[", "").replace(",", "").trim();
                    codigoProduto = compra_nota.getProdutoEstoque().get(j).codigoProduto;
                    quantidadeProdutoS = String.valueOf(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                    precoCompra_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                    precoVenda_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoVenda_unidade());
                    precoCompra_GeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                    LucroUnidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroUnidade());
                    LucroGeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroGeral());

                    produtos = new String[]{quantidadeProdutoS, codigoProduto, nomeProduto, precoCompra_unidadeS, precoCompra_GeralS, precoVenda_unidadeS, LucroUnidadeS, LucroGeralS};

                    produtosAgardandoPagamendo.add(produtos);

                }

            }
        }

        return produtosAgardandoPagamendo;
    }

    public ArrayList<String[]> pegaPagamentoEfetuados(String name) {
        String[] pagamentos;
        ArrayList<String[]> ListaPagamentosEfetuados = new ArrayList<>();
        ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();

        for (PAGAMENTO_NOTA ListaPagamento : ListaPagamentos) {
            PagamentoNota = (PAGAMENTO_NOTA) ListaPagamento;
            if (name.equalsIgnoreCase(PagamentoNota.getFornecedor())) {
                pagamentos = new String[]{PagamentoNota.getFornecedor(),
                    PagamentoNota.getDiaRecebeu(),
                    PagamentoNota.getDataRecebeu(),
                    PagamentoNota.getNomeRecebeu(),
                    AlternavelCadastroProduto.converterFloatString(PagamentoNota.getValorRecebeu())};

                ListaPagamentosEfetuados.add(pagamentos);
            }
        }

        return ListaPagamentosEfetuados;
    }

    public ArrayList<String[]> PegaTodosPagamentosExistentesnoArquivo() {
        String[] pagamentos;
        ArrayList<String[]> ListaPagamentosEfetuados = new ArrayList<>();
        ListaPagamentos = ARQUIVO_PAGAMENTO_NOTA.retorna();

        for (PAGAMENTO_NOTA ListaPagamento : ListaPagamentos) {
            PagamentoNota = (PAGAMENTO_NOTA) ListaPagamento;

            pagamentos = new String[]{PagamentoNota.getFornecedor(),
                PagamentoNota.getDiaRecebeu(),
                PagamentoNota.getDataRecebeu(),
                PagamentoNota.getNomeRecebeu(),
                AlternavelCadastroProduto.converterFloatString(PagamentoNota.getValorRecebeu())};

            ListaPagamentosEfetuados.add(pagamentos);

        }
           return ListaPagamentosEfetuados;
    }

    /*Pega uma lista de armazenamentos efetuados para uma determinada nota*/
    public ArrayList<String[]> PegaArmazenamentoEfetuados(String name) {

        String[] armazenamentos;
        ArrayList<String[]> ListaArmazenamentos = new ArrayList<>();

        for (ARMAZENAR_COMPRA_NOTA ListaarmazenarPagamento1 : ListaarmazenarPagamento) {
            ArmazenarPagamentos = (ARMAZENAR_COMPRA_NOTA) ListaarmazenarPagamento1;
            if (name.equalsIgnoreCase(ArmazenarPagamentos.getFormecedor())) {
                armazenamentos = new String[]{ArmazenarPagamentos.getFormecedor(),
                    ArmazenarPagamentos.getDiaArmazenamento(),
                    ArmazenarPagamentos.getDataArmazenamento(),
                    ArmazenarPagamentos.getObcervacao(),
                    AlternavelCadastroProduto.converterFloatString(ArmazenarPagamentos.getValorArmazenado())};

                ListaArmazenamentos.add(armazenamentos);
            }
        }

        return ListaArmazenamentos;
    }

    /*public ArrayList<ARMAZENAR_COMPRA_NOTA> RemoveUmArmazenamento(String NomeFornecedor, String DataArmazenamento, Float ValorArmazenamento) {
     ListaarmazenarPagamento      
     return null;
     }*/
}
