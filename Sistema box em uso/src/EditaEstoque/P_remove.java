/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EditaEstoque;

import BancoDados.ARQUIVO_ESTOQUE;
import Interface_Alternavel.AlternavelCadastroProduto;
import Interface_Alternavel.AlternavelEst;
import Interface_Alternavel.JNumberFormatField;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import Interface_Alternavel.PainelAltenavelCaixa;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author carlos
 */
public class P_remove extends JPanel {

    private JButton Botao_Salvar, Botao_Cancelar;
    private String str1 = null;
    private JLabel JLcodigoProduto = new JLabel();
    private JLabel JLnome = new JLabel();
    private JTextField JTnome = new JTextField();
    private JTextField JTcodigoProduto = new JTextField();
    private JLabel JLprecoVenda = new JLabel();
    private JLabel JLquantidade = new JLabel();
    private JTextField JTquantidade = new JTextField();
    private JNumberFormatField JTprecoVenda = new JNumberFormatField(new DecimalFormat("0.00"));
    private JScrollPane JSprodutosEstoque = new JScrollPane();
     
    private ARQUIVO_ESTOQUE arquivo_estoque = new ARQUIVO_ESTOQUE();
    private ArrayList<String> dados = new ArrayList<>();
    private PainelAltenavelCaixa painel_alternavel_caixa = new PainelAltenavelCaixa();
    private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<PRODUTO_ESTOQUE>();//UMA LISTA DE PRODUTOS DO MEU ESTOQUE
    private PRODUTO_ESTOQUE produto_estoque = new PRODUTO_ESTOQUE();
    AcaoBotaoSalvar acaoBotaoSalvar = new AcaoBotaoSalvar();
  

    public P_remove(String quantidadeProduto, String codigoProduto, String nomeProduto, String precoVenda, int Localizacao) {
        this.setLayout(null);
        this.setSize(500, 400); // larg, alt
        this.setLocation(0, 50);
        this.setBackground(Color.white);
        
        ListaProduto_Estoque = arquivo_estoque.retorna();
        
        

        JLcodigoProduto.setBounds(new Rectangle(168, 135, 400, 50));
        JLcodigoProduto.setText("Digite o novo código do produto");
        JLcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLcodigoProduto.setLocation(10, 0);
        add(JLcodigoProduto);

        JTcodigoProduto.setBounds(new Rectangle(200, 135, 120, 17));
        JTcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTcodigoProduto.setSize(160, 25);
        JTcodigoProduto.setLocation(10, 40);//
        JTcodigoProduto.setText(codigoProduto);
        // JTcodigoProduto.addKeyListener(acao);
        add(JTcodigoProduto);

        JLnome.setBounds(new Rectangle(168, 135, 400, 50));
        JLnome.setText("Digite o novo nome do produto");
        JLnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLnome.setLocation(10, 80);
        add(JLnome);

        JTnome.setBounds(new Rectangle(200, 135, 120, 17));
        JTnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTnome.setSize(160, 25);
        JTnome.setLocation(10, 120);//
        //JTnome.addKeyListener(acao);
        JTnome.setText(nomeProduto);
        add(JTnome);

        JLquantidade.setBounds(new Rectangle(168, 135, 400, 50));
        JLquantidade.setText("Digite a nova quantidade do produto");
        JLquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLquantidade.setLocation(10, 160);
        add(JLquantidade);

        JTquantidade.setBounds(new Rectangle(200, 135, 120, 17));
        JTquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTquantidade.setSize(160, 25);
        JTquantidade.setLocation(10, 200);//
        //JTquantidade.addKeyListener(acao);
        JTquantidade.setText(quantidadeProduto);
        add(JTquantidade);

        JLprecoVenda.setBounds(new Rectangle(168, 135, 400, 50));
        JLprecoVenda.setText("Digite o novo Preço de venda do produto");
        JLprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLprecoVenda.setLocation(10, 240);
        add(JLprecoVenda);

        JTprecoVenda.setBounds(new Rectangle(200, 135, 120, 17));
        JTprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTprecoVenda.setSize(160, 25);
        JTprecoVenda.setLocation(10, 280);//
        JTprecoVenda.setText(precoVenda);
        //JTprecoVenda.addKeyListener(acao);
        add(JTprecoVenda);
        
        acaoBotaoSalvar.pegarLocalizacao(Localizacao);
        
        Botao_Salvar = new JButton();
        Botao_Salvar.setText("SALVAR");
        Botao_Salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Salvar.setSize(100, 25);
        Botao_Salvar.addActionListener(acaoBotaoSalvar);
        Botao_Salvar.setLocation(50, 320);
        add(Botao_Salvar);

        Botao_Cancelar = new JButton();
        Botao_Cancelar.setText("CANCELAR");
        Botao_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Cancelar.setSize(100, 25);
        Botao_Cancelar.setLocation(200, 320);
        add(Botao_Cancelar);

    }

    public class AcaoBotaoSalvar implements ActionListener{
        int Loc;

        public void pegarLocalizacao(int localizacao) {
            Loc = localizacao;

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            
            ListaProduto_Estoque.get(Loc).setNomeProduto(JTnome.getText());
            ListaProduto_Estoque.get(Loc).setCodigoProduto(JTcodigoProduto.getText());
            ListaProduto_Estoque.get(Loc).setQuantidadeProduto(Integer.parseInt(JTquantidade.getText()));
            ListaProduto_Estoque.get(Loc).setPrecoVenda_unidade(Float.parseFloat(JTprecoVenda.getText().replace(",", ".")));
            ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(Integer.parseInt(JTquantidade.getText())*ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
            ListaProduto_Estoque.get(Loc).setLucroUnidade(Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
            float lucroUnidade = (Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - ListaProduto_Estoque.get(Loc).getPrecoCompra_unidade());
            ListaProduto_Estoque.get(Loc).setLucroGeral( lucroUnidade * Integer.parseInt(JTquantidade.getText()));
            
            if (Integer.parseInt(JTquantidade.getText()) == 0) {
                float FLAG = (float) 0.00;                
                ListaProduto_Estoque.get(Loc).setPrecoCompra_Geral(FLAG);
                ListaProduto_Estoque.get(Loc).setPrecoCompra_unidade(FLAG);
                ListaProduto_Estoque.get(Loc).setLucroGeral(FLAG);
                ListaProduto_Estoque.get(Loc).setLucroUnidade(FLAG);
                
                
            }
            
            
            try {
                arquivo_estoque.Armazena(ListaProduto_Estoque);
            } catch (IOException ex) {
                Logger.getLogger(P_remove.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            limpaTxt();
            AlternavelEst AlternavelEst = new AlternavelEst();
          
            AlternavelEst.remove();
            AlternavelEst.revalidate();
            AlternavelEst.repaint();
      
          }
   }
    
    public void limpaTxt(){
         JTnome.setText(null);
         JTcodigoProduto.setText(null);
         JTprecoVenda.setText(null);
         JTquantidade.setText(null);
    
    
    }
    
    public static ArrayList<String> retornarOcorrencias(ArrayList<String> lista, String word) {
        ArrayList<String> retorno = new ArrayList<String>();

        for (String elemento : lista) {
            if (elemento.startsWith(word)) {
                retorno.add(elemento);
            }
        }
        Collections.sort(retorno);
        return retorno;
    }

   
    
    public ArrayList<String[]> criaTabela(PRODUTO_ESTOQUE produto, ArrayList dados) {

        ListaProduto_Estoque = arquivo_estoque.retorna();





        for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
            produto_estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);



            String quantidade = String.valueOf(produto_estoque.getQuantidadeProduto());
            String codigo = produto_estoque.getCodigoProduto();
            String nome = produto_estoque.getNomeProduto();
            String precoVenda = AlternavelCadastroProduto.converterFloatString(produto_estoque.getPrecoVenda_unidade());
            String precoCompra = AlternavelCadastroProduto.converterFloatString(produto_estoque.getPrecoCompra_unidade());
            String precoCompraGeral = AlternavelCadastroProduto.converterFloatString(produto_estoque.getPrecoCompra_Geral());
            String LucroUnidade = AlternavelCadastroProduto.converterFloatString(produto_estoque.getLucroUnidade());
            String LucroGeral = AlternavelCadastroProduto.converterFloatString(produto_estoque.getLucroGeral());


            String[] produtos = new String[]{quantidade, codigo, nome, precoCompra, precoVenda, precoCompraGeral, LucroUnidade, LucroGeral};
            dados.add(produtos);


        }

        Tabela(dados);

        return dados;
    }

    public void Tabela(ArrayList<String[]> dados) {


        String[] colunas = new String[]{"Quantidade", "Codigo", "Descrição", "Preço Compra", "Preço venda", "Preço geral", "Lucro Uni", "Lucro Geral"};
        SimpleTableModel modelo = new SimpleTableModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(20);

        TableColumn coluna = null;

        add(jtable);

        JSprodutosEstoque = new JScrollPane(jtable);
        //JSprodutosVendidos.setLocation(1, 145);
        JSprodutosEstoque.setSize(700, 200);
        add(JSprodutosEstoque);

    }

    public class SimpleTableModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private SimpleTableModel(ArrayList dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna
            return linha[columnIndex];
        }

        public String getColumnName(int column) {
            return colunas[column];
        }
    }

    

}
