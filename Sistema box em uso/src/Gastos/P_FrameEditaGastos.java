/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_GASTOS;
import Cadastro.Datas;
import Interface_Alternavel.JNumberFormatField;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import Interface_Alternavel.PainelAltenavelCaixa;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.xml.crypto.Data;

/**
 *
 * @author User
 */
public class P_FrameEditaGastos extends JFrame {

    static void DISPOSE_ON_CLOSE() {
        P_FrameEditaGastos.DISPOSE_ON_CLOSE();
    }

    private int largura;
    private int altura;
    private Container container;
    P_EditaGastos p_editaGastos;
     
    public P_FrameEditaGastos(String quemGastou, String comQueGastou, String quantoGastou, String emQueData, int posicao) {

        container = getContentPane();
        p_editaGastos = new P_EditaGastos(quemGastou,comQueGastou,quantoGastou, emQueData, posicao);
        
       
        
        this.container.add(p_editaGastos);
        this.setLayout(null);
        this.setSize(500, 450); // alt, larg
        this.largura = -125;
        this.altura = 300;
        this.setBackground(Color.BLUE);
        this.setVisible(true);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(20, 230);

    }

    P_FrameEditaGastos() {
       
    }

    public final class P_EditaGastos extends JPanel {
    
    private JButton Botao_Salvar, Botao_Cancelar, Botao_Remover;
    private String str1 = null;
    private JLabel JLqueGastou= new JLabel();
    private JLabel JLcomQue = new JLabel();
    private JTextField JTcomQue = new JTextField();
    private JTextField JTqueGastou = new JTextField();
    private JLabel JLquanto = new JLabel();
    private JLabel JLemQueData = new JLabel();
    private JTextField JTemQueData = new JTextField();
    private JNumberFormatField JTquanto = new JNumberFormatField(new DecimalFormat("0.00"));
    private JScrollPane JSprodutosEstoque = new JScrollPane();
    private ARQUIVO_ESTOQUE arquivo_estoque = new ARQUIVO_ESTOQUE();
    private ArrayList<String> dados = new ArrayList<>();
    private PainelAltenavelCaixa painel_alternavel_caixa = new PainelAltenavelCaixa();
    private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<PRODUTO_ESTOQUE>();//UMA LISTA DE PRODUTOS DO MEU ESTOQUE
    private PRODUTO_ESTOQUE produto_estoque = new PRODUTO_ESTOQUE();
    private P_FrameEditaGastos p_FrameEditaGastos = new P_FrameEditaGastos();
    private p_ExibeGastos p_exibeGastos = new p_ExibeGastos();
    private Datas datas = new Datas();
    
    /*COMBOBOX*/
    private JComboBox<String> JCnome = new JComboBox<>();
    private String nomes[] = {"Box","Carlos","Eliane"};
   
    /*CLASSE PARA MANUZEAR A LISTA DE GASTO QUE TENHO ARQUIVADA*/
    private BOX gastos = new BOX();
    private ArrayList<BOX> ListaGastos = new ArrayList<>();
    
    private JLabel JLposicao = new JLabel(); //label usada para armazenar e passa para a classe botao salvar a posicao do gasto 
   
    /*CLASSE QUE REALIZA AS ACOES DOS BOTOES SALVAR CANCELAR E REMOVER*/
    private P_EditaGastos.AcaoBotoes acaoBotoes = new P_EditaGastos.AcaoBotoes();
            
    public P_EditaGastos(String nome, String descricao, String valor, String data, int posicao) {
        this.setLayout(null);
        this.setSize(500, 400); // larg, alt
        this.setLocation(0, 50);
        this.setBackground(Color.white);

        ListaProduto_Estoque = arquivo_estoque.retorna();
        
        JLposicao.setText(String.valueOf(posicao));

        JLqueGastou.setBounds(new Rectangle(168, 135, 400, 50));
        JLqueGastou.setText("Quem Gastou?");
        JLqueGastou.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLqueGastou.setLocation(10, 0);
        add(JLqueGastou);
        
        JCnome = new JComboBox(OrdemCombo(nome));
        JCnome.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JCnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JCnome.setSize(250, 25);
        JCnome.setLocation(10, 40);//
        add(JCnome);

        JLcomQue.setBounds(new Rectangle(168, 135, 400, 50));
        JLcomQue.setText("Com o que Gastou?");
        JLcomQue.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLcomQue.setLocation(10, 80);
        add(JLcomQue);

        JTcomQue.setBounds(new Rectangle(200, 135, 120, 17));
        JTcomQue.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTcomQue.setSize(160, 25);
        JTcomQue.setLocation(10, 120);//
        //JTnome.addKeyListener(acao);
        JTcomQue.setText( descricao);
        add(JTcomQue);

        JLquanto.setBounds(new Rectangle(168, 135, 400, 50));
        JLquanto.setText("Quanto Gastou?");
        JLquanto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLquanto.setLocation(10, 160);
        add(JLquanto);

        JTquanto.setBounds(new Rectangle(200, 135, 120, 17));
        JTquanto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTquanto.setSize(160, 25);
        JTquanto.setLocation(10, 200);//
        JTquanto.setText(valor);
        //JTprecoVenda.addKeyListener(acao);
        add(JTquanto);
        
        
        JLemQueData.setBounds(new Rectangle(168, 135, 400, 50));
        JLemQueData.setText("Em que Data?");
        JLemQueData.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JLemQueData.setLocation(10, 240);
        add(JLemQueData);

        JTemQueData.setBounds(new Rectangle(200, 135, 120, 17));
        JTemQueData.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTemQueData.setSize(160, 25);
        JTemQueData.setLocation(10, 280);//
        //JTquantidade.addKeyListener(acao);
        JTemQueData.setText(data);
        add(JTemQueData);

        Botao_Salvar = new JButton();
        Botao_Salvar.setText("SALVAR");
        Botao_Salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Salvar.setSize(100, 25);
        Botao_Salvar.addActionListener(acaoBotoes);
        Botao_Salvar.setLocation(50, 320);
        add(Botao_Salvar);

        Botao_Cancelar = new JButton();
        Botao_Cancelar.setText("CANCELAR");
        Botao_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Cancelar.setSize(100, 25);
        Botao_Cancelar.setLocation(200, 320);
        add(Botao_Cancelar);

        Botao_Remover = new JButton();
        Botao_Remover.setText("REMOVER");
        Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_Remover.setSize(100, 25);
        Botao_Remover.setLocation(350, 320);
        Botao_Remover.addActionListener(acaoBotoes);
        add(Botao_Remover);

        //acaoBotaoSalvar.pegarLocalizacao(Localizacao);  
    }
    
    public String[] OrdemCombo(String nome) {
        String st1, st2, st3;

        if (nome.equalsIgnoreCase("Box")) {
            st1 = "Box";
            st2 = "Carlos";
            st3 = "Eliane";

        } else if (nome.equalsIgnoreCase("Carlos")) {
            st1 = "Carlos";
            st2 = "Box";
            st3 = "Eliane";

        } else {
            st1 = "Eliane";
            st2 = "Box";
            st3 = "Carlos";
        }
        String nomes[] = {st1, st2, st3};

        return nomes;
    }
    
    
    public class AcaoBotoes implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == Botao_Salvar) {
               ListaGastos = ARQUIVO_GASTOS.retorna();
                
             
                /* VERIFICA SE NÃO TEM NENHUM CAMPO VAIZO */
                if ((JTcomQue.getText().isEmpty() || JTquanto.getText().isEmpty() || JTemQueData.getText().isEmpty())) {
                    JOptionPane.showMessageDialog(null, "Existe campos vazios!");
                } else {
                    
                    int posi = Integer.parseInt(JLposicao.getText());
                    ListaGastos.get(posi).setNome(JCnome.getSelectedItem().toString());
                    ListaGastos.get(posi).setComQue(JTcomQue.getText());
                    ListaGastos.get(posi).setQuanto(Float.parseFloat(JTquanto.getText().replace(",", ".")));
                    ListaGastos.get(posi).setDia(JTemQueData.getText());
                   
                    try {
                        ARQUIVO_GASTOS.Armazena(ListaGastos);
                    } catch (IOException ex) {
                        Logger.getLogger(P_FrameEditaGastos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    P_FrameEditaGastos.this.dispose();
                   

                   
         
                    }
                }
                if (e.getSource() == Botao_Remover) {
                    ListaGastos = ARQUIVO_GASTOS.retorna();
                    int posi = Integer.parseInt(JLposicao.getText());
                   
                    JOptionPane.showMessageDialog(null, ListaGastos.size());
                    ListaGastos.remove(ListaGastos.get(posi));
                    JOptionPane.showMessageDialog(null, ListaGastos.size());
                    P_FrameEditaGastos.this.dispose();
                  
                    try {
                        ARQUIVO_GASTOS.Armazena(ListaGastos);
                    } catch (IOException ex) {
                        Logger.getLogger(P_FrameEditaGastos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
}

}
