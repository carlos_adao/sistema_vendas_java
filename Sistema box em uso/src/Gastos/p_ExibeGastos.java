/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

import Auxiliares.Converte;
import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_GASTOS;
import Auxiliares.Datas;
import Compras.Visualizar.TabelaModel;
import Interface_Alternavel.AlternavelCadastroProduto;
import Interface_Alternavel.PRODUTO_ESTOQUE;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.*;
import static javax.swing.JComponent.TOOL_TIP_TEXT_KEY;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Convidado
 */
public class p_ExibeGastos extends JPanel {

    /*
     * parte de instancia da classe gasto
     */
    private BOX gastos = new BOX();
    private final P_Gastos p_gastos = new P_Gastos();
    private final ARQUIVO_GASTOS arquivoGastos = new ARQUIVO_GASTOS();
    private ArrayList<BOX> ListaGastos;
    private OrdenacaoArrayList ordenacaoArrayList = new OrdenacaoArrayList();

    ArrayList<String[]> dados = new ArrayList<>();
    ArrayList<String[]> dadosOrdenados = new ArrayList<>();
    JLabel dataTitulo = new JLabel();
    JLabel Jldia = new JLabel();
    JLabel JLgastoBox = new JLabel();
    JTextField JTgastoBox = new JTextField();
    JLabel JLgastoCarlos = new JLabel();
    JTextField JTgastoCarlos = new JTextField();
    JLabel JLgastoEliane = new JLabel();
    JTextField JTgastoEliane = new JTextField();
    float gastoBox = 0, gastoCarlos = 0, gastoEliane = 0;
    P_FrameEditaGastos p_frameEditaGastos;
    int posicao = 0; //variavel para guardar a posicao do gasto na lista de gastos 
    Datas datas = new Datas();

    Calculos Calculos = new Calculos();

    TabelaModel modelo;
    JTable jtable = new JTable();
    JScrollPane JStabelaGastos = new JScrollPane(jtable);
    JComboBox<String> JCopcoes;
    boolean ver = false;
    Calculos_ExibeGastos calculoExibeGastos = new Calculos_ExibeGastos();
    String dia_mes_ou_Ano;
    Converte convert = new Converte();

    public p_ExibeGastos() {
        this.ListaGastos = new ArrayList<>();
        this.modelo = new TabelaModel();
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

    }

    public void Funcionamento(int Selec_dt_ms_an, String QualData_Mes_Ano, String dataMesOuAno, String[] todosDias_Mes_Ano) {

        if (Selec_dt_ms_an == 1) {

            Tabela(calculoExibeGastos.RetornaValoresParaExiberGastos(QualData_Mes_Ano));
            iniciaizaComponetes("Gastos do dia", dataMesOuAno, calculoExibeGastos.RetornaGastosBox(), calculoExibeGastos.RetornaGastosCarlos(), calculoExibeGastos.RetornaGastosEliane(),
                    todosDias_Mes_Ano);
        } else if (Selec_dt_ms_an == 2) {

            Tabela(calculoExibeGastos.RetornaValoresParaExiberGastos(QualData_Mes_Ano));
            iniciaizaComponetes("Gastos do Mês", dataMesOuAno, calculoExibeGastos.RetornaGastosBox(), calculoExibeGastos.RetornaGastosCarlos(), calculoExibeGastos.RetornaGastosEliane(),
                    todosDias_Mes_Ano);

        } else {

            Tabela(calculoExibeGastos.RetornaValoresParaExiberGastos(QualData_Mes_Ano));
            iniciaizaComponetes("Gastos do Ano", dataMesOuAno, calculoExibeGastos.RetornaGastosBox(), calculoExibeGastos.RetornaGastosCarlos(), calculoExibeGastos.RetornaGastosEliane(),
                    todosDias_Mes_Ano);
        }

    }

    public void iniciaizaComponetes(String gastos_do_dia, String dataMesOuAno, String RetornaGastosBox, String RetornaGastosCarlos, String RetornaGastosEliane, String[] todosDias_Mes_Ano) {

        dataTitulo.setBounds(new Rectangle(168, 135, 400, 30));
        dataTitulo.setText(" Gastos do dia ");
        dataTitulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 15));
        dataTitulo.setLocation(7, 10);
        add(dataTitulo);
        if (!ver) {
            JCopcoes = new JComboBox(todosDias_Mes_Ano);

            JCopcoes.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
            JCopcoes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JCopcoes.setSize(150, 20);
            JCopcoes.setLocation(10, 36);
        } else {

            JCopcoes.setModel(new DefaultComboBoxModel<>(todosDias_Mes_Ano));
        }

        JCopcoes.addItemListener(
                new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {

                            Tabela(calculoExibeGastos.RetornaValoresParaExiberGastos(JCopcoes.getSelectedItem().toString()));
                            if (datas.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString()) != null) {
                                dia_mes_ou_Ano = datas.CapturaDiaDaSemana(JCopcoes.getSelectedItem().toString());
                            } else if (datas.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString())) != null) {
                                dia_mes_ou_Ano = datas.CapturaNomeMesAno(convert.StringEmInteiro(JCopcoes.getSelectedItem().toString()));
                            } else {
                                dia_mes_ou_Ano = JCopcoes.getSelectedItem().toString();
                            }
                            AtualizarValores(calculoExibeGastos.RetornaGastosBox(), calculoExibeGastos.RetornaGastosCarlos(), calculoExibeGastos.RetornaGastosEliane(), dia_mes_ou_Ano);

                            if (calculoExibeGastos.RetornaValoresParaExiberGastos(JCopcoes.getSelectedItem().toString()).isEmpty()) {
                                JOptionPane.showMessageDialog(null, "NÃO EXISTE VENDAS CADASTRA PARA ESSE DIA");
                            }
                            if (JCopcoes.getSelectedItem().toString().equalsIgnoreCase("Visualizar Produtos")) {

                            }

                        }
                    }

                });
        add(JCopcoes);

        Jldia.setVisible(true);
        Jldia.setBounds(new Rectangle(168, 135, 400, 30));
        Jldia.setText(dataMesOuAno);
        Jldia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 35));
        Jldia.setLocation(250, 20);
        add(Jldia);

        JLgastoBox.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoBox.setText("Gasto Box");
        JLgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoBox.setLocation(850, 40);
        add(JLgastoBox);

        JTgastoBox.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoBox.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoBox.setSize(120, 50);
        JTgastoBox.setLocation(850, 80);//
        JTgastoBox.setText(RetornaGastosBox);
        add(JTgastoBox);

        JLgastoCarlos.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoCarlos.setText("Gasto Carlos");
        JLgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoCarlos.setLocation(850, 120);
        add(JLgastoCarlos);

        JTgastoCarlos.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoCarlos.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoCarlos.setSize(120, 50);
        JTgastoCarlos.setLocation(850, 160);//
        JTgastoCarlos.setText(RetornaGastosCarlos);
        add(JTgastoCarlos);

        JLgastoEliane.setBounds(new Rectangle(168, 135, 400, 50));
        JLgastoEliane.setText("Gastos Eliane");
        JLgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JLgastoEliane.setLocation(850, 210);
        add(JLgastoEliane);

        JTgastoEliane.setBounds(new Rectangle(200, 135, 120, 17));
        JTgastoEliane.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        JTgastoEliane.setSize(120, 50);
        JTgastoEliane.setLocation(850, 250);
        JTgastoEliane.setText(RetornaGastosEliane);
        add(JTgastoEliane);

        ver = true;

    }

    public void Tabela(ArrayList<String[]> dados) {

        String[] colunas = new String[]{"Quem gastou", "Com o que", "Quanto", "Dia"};
        modelo.setLinhas(dados);
        modelo.setColunas(colunas);
        jtable.setModel(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(200);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.setAutoCreateRowSorter(true);

        jtable.addMouseListener(new MouseAdapter() {

            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    linha = jtable.getSelectedRow();
                    String quemGastou = String.valueOf(jtable.getValueAt(linha, 0));
                    String comQueGastou = String.valueOf(jtable.getValueAt(linha, 1));
                    String quantoGastou = String.valueOf(jtable.getValueAt(linha, 2));

                    String emQueData = String.valueOf(jtable.getValueAt(linha, 3));

                    ListaGastos = arquivoGastos.retorna();
                    gastos = (BOX) ListaGastos.get(linha);

                    for (int i = 0; i < ListaGastos.size(); i++) {
                        gastos = (BOX) ListaGastos.get(i);
                        if (quemGastou.equalsIgnoreCase(gastos.getNome()) && comQueGastou.equalsIgnoreCase(gastos.getComQue()) && quantoGastou.equalsIgnoreCase(AlternavelCadastroProduto.converterFloatString(gastos.getQuanto()))) {
                            posicao = i;
                            p_frameEditaGastos = new P_FrameEditaGastos(quemGastou, comQueGastou, quantoGastou, emQueData, posicao);
                        }
                    }
                }
            }
        });

        JStabelaGastos.setLocation(10, 80);
        JStabelaGastos.setSize(800, 250);
        JStabelaGastos.getVerticalScrollBar();
        add(JStabelaGastos);

    }

    private void AtualizarValores(String RetornaGastosBox, String RetornaGastosCarlos, String RetornaGastosEliane, String CapturaDiaDaSemana) {
        Jldia.setText(dia_mes_ou_Ano);
        JTgastoBox.setText(RetornaGastosBox);
        JTgastoCarlos.setText(RetornaGastosCarlos);
        JTgastoEliane.setText(RetornaGastosEliane);
    }

}
