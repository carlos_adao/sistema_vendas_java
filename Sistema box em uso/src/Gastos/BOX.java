/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gastos;

/**
 *
 * @author carlos
 */
public class BOX extends GASTOS implements Comparable<Object> {
    String nome;
    
    
    public BOX() {
    }
    

    public BOX(String nome) {
        this.nome = nome;
    }
  
   public BOX(String nome, float quanto, String comQue,String observacao, String dia, String mes, String ano) {
        super(quanto, comQue,observacao, dia, mes, ano);
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
     public String primeiroNome() {
        return  nome;
    }
     
  

    @Override
    public String toString() {
        return nome+";"+ getQuanto() + ";" + getComQue() +";"+ getObservacao()+ ";"+ getDia() + ";" + getMes()+";"+ getAno()+";" +"\n";
    }

    @Override
    public int compareTo(Object o) {
        BOX outro =(BOX)o;
        
        return this.dia.compareToIgnoreCase(outro.dia);
    }

   
    
    
    
}
