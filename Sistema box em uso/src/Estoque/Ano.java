/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Estoque;

import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class Ano {
    
    ArrayList<Mes> mes = new ArrayList<Mes>();
    public Ano(){
    
    
    }
    
 public class Mes{
   ArrayList<Dia> dia = new ArrayList<Dia>();
   public Mes(){
   
   }
 
 }
 public class Dia{
   ArrayList<Vendas> vendas = new ArrayList<Vendas>(); 
   ArrayList<Compras> compras = new ArrayList<Compras>();
 
 }
 public class Vendas{
    ArrayList<Produto> produtos = new ArrayList<Produto>();
     public Vendas(){ 
    
      
    }
 }
 
 public class Produto{ 
    int quantidade;
    String nomeProduto;
    float precoUnidade;
    float subTotal;
    float total;
    float desconto;
    float lucro;

        public Produto(int quantidade, String nomeProduto, float precoUnidade, float subTotal, float total, float desconto) {
            this.quantidade = quantidade;
            this.nomeProduto = nomeProduto;
            this.precoUnidade = precoUnidade;
            this.subTotal = subTotal;
            this.total = total;
            this.desconto = desconto;
            this.lucro = lucro;
        }

        public float getDesconto() {
            return desconto;
        }

        public void setDesconto(float desconto) {
            this.desconto = desconto;
        }

        public String getNomeProduto() {
            return nomeProduto;
        }

        public void setNomeProduto(String nomeProduto) {
            this.nomeProduto = nomeProduto;
        }

        public float getPrecoUnidade() {
            return precoUnidade;
        }

        public void setPrecoUnidade(float precoUnidade) {
            this.precoUnidade = precoUnidade;
        }

        public int getQuantidade() {
            return quantidade;
        }

        public void setQuantidade(int quantidade) {
            this.quantidade = quantidade;
        }

        public float getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(float subTotal) {
            this.subTotal = subTotal;
        }

        public float getTotal() {
            return total;
        }

        public void setTotal(float total) {
            this.total = total;
        }

        public float getLucro() {
            return lucro;
        }

        public void setLucro(float lucro) {
            this.lucro = lucro;
        }

        @Override
        public String toString() {
            return (quantidade + ";" + nomeProduto + ";" + precoUnidade + ";" + subTotal + ";" + total + ";" + desconto+ ";" + lucro);
        }
        
        
    
    
 }
 public class Compras{
 
 
 
 }
 
}
