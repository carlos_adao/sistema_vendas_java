/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import Cadastro.Datas;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author User
 */
public class FrameEditaVendas extends JFrame {

    private int largura;
    private int altura;
    private Container container;
    P_EditaVendas p_EditaVendas;

    public FrameEditaVendas(String nomeProduto, String codigoProduto, String precoVenda, String desconto, String quantidadeProduto, String data, String subTotal, String lucro, int i) {

        container = getContentPane();
        p_EditaVendas = new P_EditaVendas(nomeProduto, codigoProduto, precoVenda, desconto, quantidadeProduto, data, subTotal, lucro, i);
        this.container.add(p_EditaVendas);
        this.setLayout(null);
        this.setSize(500, 450); // alt, larg
        this.largura = -125;
        this.altura = 300;
        this.setBackground(Color.BLUE);
        this.setVisible(true);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(20, 230);


    }

    public final class P_EditaVendas extends JPanel {

        /*
         * COMPONENTES DE GUI GRAFICAS
         */
        private JButton Botao_Salvar, Botao_Cancelar, Botao_Remover;
        private JLabel JLcodigoProduto = new JLabel();
        private JLabel JLnome = new JLabel();
        private JTextField JTnome = new JTextField();
        private JTextField JTcodigoProduto = new JTextField();
        private JLabel JLprecoVenda = new JLabel();
        private JLabel JLquantidade = new JLabel();
        private JTextField JTquantidade = new JTextField();
        private JNumberFormatField JTprecoVenda = new JNumberFormatField(new DecimalFormat("0.00"));
        private JLabel JLdesconto = new JLabel();
        private JNumberFormatField JTdesconto = new JNumberFormatField(new DecimalFormat("0.00"));
        private JLabel JLdata = new JLabel();
        private JFormattedTextField JTdata = new JFormattedTextField(Mascara("##/##/####"));
        private JLabel JLsubTotal = new JLabel();
        private JNumberFormatField JTsubTotal = new JNumberFormatField(new DecimalFormat("0.00"));
        private JLabel JLlucro = new JLabel();
        private JNumberFormatField JTlucro = new JNumberFormatField(new DecimalFormat("0.00"));
        /*
         * CLASSES DE ACAO FOCO TEXTFIELDS
         */
        P_EditaVendas.AcaoFocoDesconto acaoFocoDesconto = new P_EditaVendas.AcaoFocoDesconto();
        P_EditaVendas.AcaoFocoQuantidade acaoFocoQuantidade = new P_EditaVendas.AcaoFocoQuantidade();
        P_EditaVendas.AcaofocoData acaofocoData = new P_EditaVendas.AcaofocoData();
        P_EditaVendas.AcaoBotoes acaoBotoes = new P_EditaVendas.AcaoBotoes();
        /*
         * VARIAVEIS PARA FAZER CALCULO NOVO PRODUTO VENDA
         */
        float precoCompraUnidade = 0;
        float precoVendaUnidade = 0;
        float subTotal = 0;
        float lucroTotal = 0;
        /*
         * CLASSES PARA PEGAR A LISTA DE PRODUTOS VENDIDOS DO ARQUIVO
         */
        private PRODUTO_VENDA produtoVenda = new PRODUTO_VENDA();
        private ARQUIVO_ESTOQUE_VENDA ArquivoVendas = new ARQUIVO_ESTOQUE_VENDA();
        private ArrayList<PRODUTO_VENDA> ListaProdutosVendidos = new ArrayList<>();
        /*
         * CLASSES PARA PEGAR A LISTA DE PRODUTOS DO MEU ESTOQUE PARA COLOCAR
         * NOVAMENTE QUE TEVE A VENDA CANCELADA NO ESTOQUE
         */
        private ArrayList<PRODUTO_ESTOQUE> ListaProdutosEstoque = new ArrayList<>();
        private ARQUIVO_ESTOQUE ArquivoEstoque = new ARQUIVO_ESTOQUE();
        private PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();
        
         /*
         * COMUNICAÇÃO COM CLASSE CAIXA CONSUTA PARA ATUALIZAR O PAINEL DE VENDAS DO DIA 
         */
        private Alternavel_Caixa_Consuta AlternavelCaixaConsuta = new Alternavel_Caixa_Consuta();
        
        /*
         * CLASSE PARA CAPTURA A DATA DO SISTEMA
         */
        private Datas datas = new Datas(); 


        /*
         * COMUNICACAO COM A CLASSE FRAMEEDITAVENDAS
         */
        public P_EditaVendas(String nomeProduto, String codigoProduto, String precoVenda, String desconto, String quantidadeProduto, String data, String subTotal, String lucro, int i) {


            this.setLayout(null);
            this.setSize(500, 400); // larg, alt
            this.setLocation(0, 50);
            this.setBackground(Color.white);

            Componentes(nomeProduto, codigoProduto, precoVenda, desconto, quantidadeProduto, data, subTotal, lucro);


        }

        public void Componentes(String nomeProduto, String codigoProduto, String precoVenda, String desconto, String quantidadeProduto, String data, String subTotal, String lucro) {


            JLnome.setBounds(new Rectangle(168, 135, 400, 50));
            JLnome.setText("Nome do Produto");
            JLnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLnome.setLocation(10, 0);
            add(JLnome);

            JTnome.setBounds(new Rectangle(200, 135, 120, 17));
            JTnome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTnome.setSize(230, 25);
            JTnome.setLocation(10, 40);//
            JTnome.setEditable(false);
            JTnome.setText(nomeProduto);
            add(JTnome);


            JLcodigoProduto.setBounds(new Rectangle(168, 135, 400, 50));
            JLcodigoProduto.setText("Codigo Produto");
            JLcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLcodigoProduto.setLocation(250, 0);
            add(JLcodigoProduto);

            JTcodigoProduto.setBounds(new Rectangle(200, 135, 120, 17));
            JTcodigoProduto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTcodigoProduto.setSize(230, 25);
            JTcodigoProduto.setLocation(250, 40);//
            JTcodigoProduto.setText(codigoProduto);
            JTcodigoProduto.setEditable(false);
            add(JTcodigoProduto);

            JLprecoVenda.setBounds(new Rectangle(168, 135, 400, 50));
            JLprecoVenda.setText("Preco de Venda");
            JLprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLprecoVenda.setLocation(10, 60);
            add(JLprecoVenda);

            JTprecoVenda.setBounds(new Rectangle(200, 135, 120, 17));
            JTprecoVenda.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTprecoVenda.setSize(230, 25);
            JTprecoVenda.setLocation(10, 100);//

            JTprecoVenda.setText(precoVenda);
            JTprecoVenda.setEditable(false);
            add(JTprecoVenda);

            JLdesconto.setBounds(new Rectangle(168, 135, 400, 50));
            JLdesconto.setText("Digite o novo Desconto");
            JLdesconto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLdesconto.setLocation(250, 60);
            add(JLdesconto);

            JTdesconto.setBounds(new Rectangle(200, 135, 120, 17));
            JTdesconto.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTdesconto.setSize(230, 25);
            JTdesconto.setLocation(250, 100);//
            JTdesconto.requestFocus();
            JTdesconto.setText(desconto);
            JTdesconto.addFocusListener(acaoFocoDesconto);
            //JTprecoVenda.addKeyListener(acao);
            add(JTdesconto);

            JLquantidade.setBounds(new Rectangle(168, 135, 400, 50));
            JLquantidade.setText("Digite a nova Quantidade");
            JLquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLquantidade.setLocation(10, 120);
            add(JLquantidade);

            JTquantidade.setBounds(new Rectangle(200, 135, 120, 17));
            JTquantidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTquantidade.setSize(230, 25);
            JTquantidade.setLocation(10, 160);//
            JTquantidade.setText(quantidadeProduto);
            JTquantidade.addFocusListener(acaoFocoQuantidade);
            add(JTquantidade);

            JLdata.setBounds(new Rectangle(168, 135, 400, 50));
            JLdata.setText("Digite a nova Data");
            JLdata.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLdata.setLocation(250, 120);
            add(JLdata);

            JTdata.setBounds(new Rectangle(200, 135, 120, 17));
            JTdata.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTdata.setSize(230, 25);
            JTdata.setLocation(250, 160);//
            JTdata.addFocusListener(acaofocoData);
            JTdata.setText(data);
            add(JTdata);

            JLsubTotal.setBounds(new Rectangle(168, 135, 400, 50));
            JLsubTotal.setText("Subtotal");
            JLsubTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLsubTotal.setLocation(10, 180);
            add(JLsubTotal);

            JTsubTotal.setBounds(new Rectangle(200, 135, 120, 17));
            JTsubTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTsubTotal.setSize(230, 25);
            JTsubTotal.setLocation(10, 220);//
            JTsubTotal.setEditable(false);
            //JTquantidade.addKeyListener(acao);
            JTsubTotal.setText(subTotal);
            add(JTsubTotal);

            JLlucro.setBounds(new Rectangle(168, 135, 400, 50));
            JLlucro.setText("Lucro");
            JLlucro.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JLlucro.setLocation(250, 180);
            add(JLlucro);

            JTlucro.setBounds(new Rectangle(200, 135, 120, 17));
            JTlucro.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            JTlucro.setSize(230, 25);
            JTlucro.setLocation(250, 220);//
            JTlucro.setEditable(false);
            //JTquantidade.addKeyListener(acao);
            JTlucro.setText(lucro);
            add(JTlucro);

            Botao_Salvar = new JButton();
            Botao_Salvar.setText("SALVAR");
            Botao_Salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            Botao_Salvar.setSize(100, 25);
            Botao_Salvar.addKeyListener(acaoBotoes);
            Botao_Salvar.addActionListener(acaoBotoes);
            Botao_Salvar.setLocation(50, 320);
            add(Botao_Salvar);

            Botao_Cancelar = new JButton();
            Botao_Cancelar.setText("CANCELAR");
            Botao_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            Botao_Cancelar.setSize(100, 25);
            Botao_Cancelar.setLocation(200, 320);
            add(Botao_Cancelar);

            Botao_Remover = new JButton();
            Botao_Remover.setText("REMOVER");
            Botao_Remover.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
            Botao_Remover.setSize(100, 25);
            Botao_Remover.setLocation(350, 320);
            // Botao_Remover.addActionListener(acao_botao_remover);
            add(Botao_Remover);
        }

        public MaskFormatter Mascara(String Mascara) {

            MaskFormatter F_Mascara = new MaskFormatter();
            try {
                F_Mascara.setMask(Mascara); //Atribui a mascara  
                F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
            } catch (Exception excecao) {
            }
            return F_Mascara;
        }

        public class AcaoBotoes implements ActionListener, KeyListener {

            @Override
            public void actionPerformed(ActionEvent e) {

                ListaProdutosEstoque = ArquivoEstoque.retorna();  
                ListaProdutosVendidos = ArquivoVendas.retorna();


                for (int i = 0; i < ListaProdutosVendidos.size(); i++) {

                    produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);

                    if (produtoVenda.getNomeProduto().equalsIgnoreCase(JTnome.getText()) && produtoVenda.getData().equalsIgnoreCase(JTdata.getText())) {


                        if (Integer.parseInt(JTquantidade.getText()) <= 0) {

                            for (int j = 0; j < ListaProdutosEstoque.size(); j++) {
                                produtoEstoque = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(j);

                                if (produtoVenda.getNomeProduto().equalsIgnoreCase(produtoEstoque.getNomeProduto())) {
                                    produtoEstoque.setPrecoCompra_Geral((produtoEstoque.getPrecoCompra_unidade()) * (produtoEstoque.getQuantidadeProduto() + produtoVenda.getQuantidadeProduto()));
                                    produtoEstoque.setLucroGeral((produtoEstoque.getLucroUnidade()) * (produtoEstoque.getQuantidadeProduto() + produtoVenda.getQuantidadeProduto()));
                                    produtoEstoque.setQuantidadeProduto(produtoEstoque.getQuantidadeProduto() + produtoVenda.getQuantidadeProduto());
                                    JOptionPane.showMessageDialog(null, "ATENÇÃO!!!" + "\n" + "O produto foi colocado novamente do estoque");
                                }

                            }

                            ListaProdutosVendidos.remove(produtoVenda);

                        }/* else if (Integer.parseInt(JTquantidade.getText()) <= produtoVenda.getQuantidadeProduto()) {
                            for (int j = 0; j < ListaProdutosEstoque.size(); j++) {
                                produtoEstoque = (PRODUTO_ESTOQUE) ListaProdutosEstoque.get(j);

                                if (produtoVenda.getNomeProduto().equalsIgnoreCase(produtoEstoque.getNomeProduto())) {
                                    produtoEstoque.setPrecoCompra_Geral((produtoEstoque.getPrecoCompra_unidade()) * (produtoVenda.getQuantidadeProduto() - Integer.parseInt(JTquantidade.getText()) + produtoEstoque.getQuantidadeProduto()));
                                    produtoEstoque.setLucroGeral((produtoEstoque.getLucroUnidade()) * (produtoVenda.getQuantidadeProduto() - Integer.parseInt(JTquantidade.getText()) + produtoEstoque.getQuantidadeProduto()));
                                    produtoEstoque.setQuantidadeProduto(produtoVenda.getQuantidadeProduto() - Integer.parseInt(JTquantidade.getText()) + produtoEstoque.getQuantidadeProduto());
                                    //produtoVenda.setQuantidadeProduto(produtoVenda.getQuantidadeProduto() - (produtoVenda.getQuantidadeProduto() - Integer.parseInt(JTquantidade.getText()))); 
                                    JOptionPane.showMessageDialog(null, produtoVenda.getQuantidadeProduto() - (produtoVenda.getQuantidadeProduto() - Integer.parseInt(JTquantidade.getText())));
                                            
                                }
                            }
                        }*/
                        try {
                            ArquivoEstoque.Armazena(ListaProdutosEstoque);
                        } catch (IOException ex) {
                            Logger.getLogger(FrameEditaVendas.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            ArquivoVendas.Armazena(ListaProdutosVendidos);
                        } catch (IOException ex) {
                            Logger.getLogger(FrameEditaVendas.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        //AlternavelCaixaConsuta.criaTabela(datas.retornaData());
                        FrameEditaVendas.this.dispose();
                    }
                }


            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
               
                ListaProdutosVendidos = ArquivoVendas.retorna();
                if (e.getKeyCode() == 10) {

                    for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                        produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);
                        if (produtoVenda.getNomeProduto().equalsIgnoreCase(JTnome.getText()) && produtoVenda.getData().equalsIgnoreCase(JTdata.getText())) {

                            if (Integer.parseInt(JTquantidade.getText()) <= 0) {
                                ListaProdutosVendidos.remove(produtoVenda);
                                try {
                                    ArquivoVendas.Armazena(ListaProdutosVendidos);
                                } catch (IOException ex) {
                                    Logger.getLogger(FrameEditaVendas.class.getName()).log(Level.SEVERE, null, ex);
                                }
                              

                            }
                        }
                    }

                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        }

        public class AcaoFocoDesconto implements FocusListener {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                float lucroUnidade = 0;
                float descontoArquivo = 0;
                ListaProdutosVendidos = ArquivoVendas.retorna();


                for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                    produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);
                    if (produtoVenda.getNomeProduto().equalsIgnoreCase(JTnome.getText()) && produtoVenda.getData().equalsIgnoreCase(JTdata.getText())) {

                        descontoArquivo = produtoVenda.getDescontoVenda();
                    }
                }

                if (descontoArquivo != Float.parseFloat(JTdesconto.getText().replace(",", "."))) {
                    //precoCompraUnidade = Stubtotal - LucroTotal / Quantidade 
                    precoCompraUnidade = ((Float.parseFloat(JTsubTotal.getText().replace(",", ".")) - Float.parseFloat(JTlucro.getText().replace(",", "."))) / Integer.parseInt(JTquantidade.getText()));
                    precoVendaUnidade = (Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - (Float.parseFloat(JTdesconto.getText().replace(",", ".")) / Integer.parseInt(JTquantidade.getText())));
                    subTotal = (precoVendaUnidade * Integer.parseInt(JTquantidade.getText()));
                    lucroTotal = ((precoVendaUnidade - precoCompraUnidade) * Integer.parseInt(JTquantidade.getText()));

                    //setando novos valores
                    JTprecoVenda.setText(AlternavelCadastroProduto.converterFloatString(precoVendaUnidade));
                    JTsubTotal.setText(AlternavelCadastroProduto.converterFloatString(subTotal));
                    JTlucro.setText(AlternavelCadastroProduto.converterFloatString(lucroTotal));

                    JTdesconto.setEditable(false);
                    JTdesconto.setFocusable(false);
                }
                JTdesconto.setEditable(false);
                JTdesconto.setFocusable(false);
            }
        }

        public class AcaoFocoQuantidade implements FocusListener {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                float lucroUnidade = 0;
                int quantidade = 0;
                ListaProdutosVendidos = ArquivoVendas.retorna();

                if (Integer.parseInt(JTquantidade.getText()) > 0) {

                    for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                        produtoVenda = (PRODUTO_VENDA) ListaProdutosVendidos.get(i);
                        if (produtoVenda.getNomeProduto().equalsIgnoreCase(JTnome.getText()) && produtoVenda.getData().equalsIgnoreCase(JTdata.getText())) {

                            lucroUnidade = produtoVenda.getLucroVenda() / produtoVenda.getQuantidadeProduto();
                            quantidade = produtoVenda.getQuantidadeProduto();

                        }
                    }

                    if (quantidade != Integer.parseInt(JTquantidade.getText())) {
                        precoVendaUnidade = (Float.parseFloat(JTprecoVenda.getText().replace(",", ".")) - (Float.parseFloat(JTdesconto.getText().replace(",", ".")) / Integer.parseInt(JTquantidade.getText())));
                        subTotal = (Integer.parseInt(JTquantidade.getText()) * Float.parseFloat(JTprecoVenda.getText().replace(",", ".")));
                        precoCompraUnidade = ((subTotal - Float.parseFloat(JTdesconto.getText().replace(",", "."))) / Integer.parseInt(JTquantidade.getText()));
                        lucroTotal = lucroUnidade * (Integer.parseInt(JTquantidade.getText()));

                        JTsubTotal.setText(AlternavelCadastroProduto.converterFloatString(subTotal));
                        JTlucro.setText(AlternavelCadastroProduto.converterFloatString(lucroTotal));
                    }
                }
                if (Integer.parseInt(JTquantidade.getText()) <= 0) {
                    /*
                     * for (int i = 0; i < ListaProdutosVendidos.size(); i++) {
                     * produtoVenda = (PRODUTO_VENDA)
                     * ListaProdutosVendidos.get(i); if
                     * (produtoVenda.getNomeProduto().equalsIgnoreCase(JTnome.getText())
                     * &&
                     * produtoVenda.getData().equalsIgnoreCase(JTdata.getText()))
                     * {
                     *
                     * ListaProdutosVendidos.remove(produtoVenda); } } try {
                     * ArquivoVendas.Armazena(ListaProdutosVendidos); } catch
                     * (IOException ex) {
                     *
                     * Logger.getLogger(P_EditaVendas.class.getName()).log(Level.SEVERE,
                     * null, ex); }
                     */
                    JTdesconto.setText("0,00");
                    JTlucro.setText("0,00");
                    JTsubTotal.setText("0,00");
                    JTprecoVenda.setText("0,00");
                    JTdata.setEditable(false);
                    JTdata.setFocusable(false);
                    JTquantidade.setEditable(false);
                    JTquantidade.setFocusable(false);
                    Botao_Salvar.requestFocus();
                }
                JTquantidade.setEditable(false);
                JTquantidade.setFocusable(false);

            }
        }

        public class AcaofocoData implements FocusListener {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                JTdata.setEditable(false);
                JTdata.setFocusable(false);
                Botao_Salvar.requestFocus();
            }
        }
    }
}
