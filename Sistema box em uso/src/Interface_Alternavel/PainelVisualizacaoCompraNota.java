/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import BancoDados.ARQUIVO_COMPRA_NOTA;
import BancoDados.ARQUIVO_ESTOQUE;
import Cadastro.Datas;
import Compras.COMPRA_NOTA;
import Compras.CalculaDiferencaEntreDatas;
import Compras.Visualizar.P_CompraNotasVisualizar;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author User
 */
public class PainelVisualizacaoCompraNota extends JPanel {

    ArrayList<String[]> dados = new ArrayList<String[]>();
    PRODUTO_ESTOQUE produtoEstoque = new PRODUTO_ESTOQUE();
    ARQUIVO_ESTOQUE aquivo_estoque = new ARQUIVO_ESTOQUE();
    ArrayList<PRODUTO_ESTOQUE> ListaprodutoEstoque = new ArrayList<PRODUTO_ESTOQUE>();
    ArrayList<PRODUTO_ESTOQUE> Listaprodutos = new ArrayList<PRODUTO_ESTOQUE>();
    COMPRA_NOTA compra_nota = new COMPRA_NOTA();
    ArrayList<COMPRA_NOTA> ListaCompraNota = new ArrayList<COMPRA_NOTA>();
    ARQUIVO_COMPRA_NOTA arquivoNotas = new ARQUIVO_COMPRA_NOTA();
    JLabel JlnomeFornecedor = new JLabel();
    JLabel JlvalorTotalDividas = new JLabel();
    JTextField JtvalorTotalDividas = new JTextField();
    JLabel JldinheiroDia = new JLabel();
    JTextField JtdinheiroDia = new JTextField();
    JScrollPane JSfornecedores;
    JScrollPane JSnotas;
    JScrollPane JSprodutos;
    JTextArea JTAfornecedores = new JTextArea();
    private JList JListFonecedores;//UMA LISTA PARA OS FORNECEDORES
    private DefaultListModel ListaModel = new DefaultListModel();//UMA LISTA(MODEL) PARA OS FORNECEDORES
    private DefaultListSelectionModel ListaModelSelection = new DefaultListSelectionModel();//USADA QUANDO CLICLA NO NOME NO TEXT AREA
    TabelaModel tabela_model = new TabelaModel();
    JTable jtable = new JTable();
    PFonecedorProduto P_FonecedorProduto;
    static String nomeFornecedor;
    Datas datas = new Datas();
    ArrayList< PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO> ProdutoEstoqueAguard = new ArrayList< PRODUTO_ESTOQUE_AGURDANDO_PAGAMENTO>();
    String nomeProduto;
    String codigoProduto;
    String quantidadeProduto;
    String precoCompra_unidade;
    String precoVenda_unidade;
    String precoCompra_Geral;
    String LucroUnidade;
    String LucroGeral;
    
    
    public PainelVisualizacaoCompraNota() {
   
        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.black);
        Componentes();  
        criaTabela();
      
  }
     public void LimpaTabela() {
        dados.clear();
        Tabela(dados);
    }
     
      public void Componentes(){//METODO PARA INICIAR OS COMPONENTES DO PAINEL 
       
        AdicionarClientesJLista();//CHAMADA DO METODO PARA ADICIONAR FORNECEDORES A LISTAMODEL
        
        
        JlnomeFornecedor.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlnomeFornecedor.setText("Nome Dos Fornecedores");
        JlnomeFornecedor.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlnomeFornecedor.setLocation(20, 20);//
        add(JlnomeFornecedor);
        
        JListFonecedores = new JList(ListaModel);
        JListFonecedores.setVisibleRowCount(10); // MAXIMO 10 NOMES NA LISTA
        JListFonecedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //ListaModelSelection.addListSelectionListener(this.AcaoLista);
        JListFonecedores.setSelectionModel(this.ListaModelSelection);
        //this.ScrollList = new JScrollPane(this.ListaProduto);
        
        
        JSfornecedores = new JScrollPane(JListFonecedores);
        JSfornecedores.setLocation(20, 50);//LADO OUTRO & CIMA BAIXO
        JSfornecedores.setSize(200, 400);//LARGURA & ALTURA
        //add(JSfornecedores);
        
        JlvalorTotalDividas.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JlvalorTotalDividas.setText("Divida Total");
        JlvalorTotalDividas.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JlvalorTotalDividas.setLocation(900, 20);//
        add(JlvalorTotalDividas);
        
        JtvalorTotalDividas.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtvalorTotalDividas.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtvalorTotalDividas.setSize(95, 35);
        JtvalorTotalDividas.setLocation(900, 45);
        JtvalorTotalDividas.setText(AlternavelCadastroProduto.converterFloatString(RetornaDividaTotal()));
       // JtvalorTotalDividas.addFocusListener(acaoFocoProduto);
        add(JtvalorTotalDividas);
    
        
        
        JldinheiroDia.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
        JldinheiroDia.setText("Dinheiro Dia");
        JldinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JldinheiroDia.setLocation(900, 100);//
        add(JldinheiroDia); 
        
        JtdinheiroDia.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        JtdinheiroDia.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtdinheiroDia.setSize(95, 35);
        JtdinheiroDia.setLocation(900, 125);
        JtdinheiroDia.setText(AlternavelCadastroProduto.converterFloatString(RetornaDinheiroTotalDia()));
       // JtvalorTotalDividas.addFocusListener(acaoFocoProduto);
        add(JtdinheiroDia);
        
    }
      
    public ArrayList<String[]> criaTabela() {
        Datas data = new Datas();
        String dia = data.retornaData(); 
        String nomeFornecedor;
        String valorDivida;
        String dinheiroDia;
        String dataPagamento;
        String dinheiroEntrada;
        String telefone;
        
         Componentes();
        
        ListaCompraNota = arquivoNotas.retorna();

        if (ListaCompraNota.size() > 0) {
            
         
            for (int j = 0; j < ListaCompraNota.size(); j++) {
                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(j); 
                
                if(compra_nota.getDataEfetuarPagamento().equalsIgnoreCase("null") && compra_nota.getTelefone().equalsIgnoreCase("null")){
                 ListaCompraNota.remove(compra_nota);
                
               }
            }
            
            for (int i = 0; i < ListaCompraNota.size(); i++) {
                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);
                
                JOptionPane.showMessageDialog(null, "dia sitema: "+dia+ "\n"+"dia arquino:" + compra_nota.getDataEfetuarPagamento());
                
                int quantidadeDias = CalculaDiferencaEntreDatas.calcula(dia, compra_nota.getDataEfetuarPagamento());
                nomeFornecedor = compra_nota.getNomeFornecedor();
                valorDivida = AlternavelCadastroProduto.converterFloatString(compra_nota.getValorDivida());
                dinheiroDia = AlternavelCadastroProduto.converterFloatString(compra_nota.getValorDivida()/ quantidadeDias);
                dinheiroEntrada = AlternavelCadastroProduto.converterFloatString(compra_nota.getDinheiroEntrada());
                dataPagamento = compra_nota.getDataEfetuarPagamento();
                telefone = compra_nota.getTelefone();
             
                
                String[] Nota = new String[]{nomeFornecedor, valorDivida, dinheiroDia, dinheiroEntrada, dataPagamento, telefone};
                dados.add(Nota);
            }
            Tabela(dados);
        }
        return dados;
     }
     
    public void Tabela(ArrayList<String[]> dados) {
        
       
        String[] colunas = new String[]{"Fornecedor", "Divida", "Dinheiro Dia", "Entrada", "Data Pag", "Telefône"};
       TabelaModel modelo = new TabelaModel(dados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(50);
        jtable.addMouseListener(new MouseAdapter() {
            private int linha;
            private String opcoes[] = new String[]{"Atualizar", "Excluir"};

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    
                        linha = jtable.getSelectedRow();
                        String nomeFornecedo = String.valueOf(jtable.getValueAt(linha, 0));
                        String dividaTotal = String.valueOf(jtable.getValueAt(linha, 1));
                        String dataPagamento = String.valueOf(jtable.getValueAt(linha, 4));
                        
                        P_FonecedorProduto = new PFonecedorProduto(nomeFornecedo, dividaTotal, dataPagamento);
         
         
                    }
                }
          
         });

 
            int  test = jtable.getSelectedRow();
        
        
        TableColumn coluna = null;

        //if (modelo.linhas.size() > 0 && modelo.linhas.size() < 10) {

            jtable.setLocation(20, 50);
            jtable.setSize(800, 250);
            jtable.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
            add(jtable);
        /*} else {

            add(jtable);

            JSnotas = new JScrollPane(jtable);
            JSnotas.setLocation(20, 50);
            JSnotas.setSize(800, 250);
            add(JSnotas);

        }*/
    }
    
    public class TabelaModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private TabelaModel(ArrayList dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

        private TabelaModel() {
           
        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        public String getColumnName(int column) {
            return colunas[column];
        }
    }
    
    public void AdicionarClientesJLista() {  //  ADICIONA FORNECEDORES NA LISTAMODEL

        this.ListaModel.clear();

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            try {
                this.ListaModel.addElement(arquivoNotas.retorna().get(i).getNomeFornecedor());
            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "Acesso a memoria indevida!");
                break;
            }
        }
    }

    public float RetornaDividaTotal() {
        float dividaTotal = 0;

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            dividaTotal += arquivoNotas.retorna().get(i).getValorDivida();

        }
        return dividaTotal;
    }
     
    public float RetornaDinheiroTotalDia() {
        float dinheiroTotalDia = 0;

        for (int i = 0; i < arquivoNotas.retorna().size(); i++) {
            dinheiroTotalDia += arquivoNotas.retorna().get(i).getDinheiroDia();

        }
        return dinheiroTotalDia;
    }
    public class PFonecedorProduto extends JFrame {
  
        private int largura;
        private int altura;
        private Container container;
        PainelFonrecedoProdutos Painel_FonrecedoProdutos;
        String nome;
        
        public PFonecedorProduto(String nome, String dividaTotal, String dataPagamento) {

            container = getContentPane();

            Painel_FonrecedoProdutos = new PainelFonrecedoProdutos(nome, dividaTotal, dataPagamento);

            this.container.add(Painel_FonrecedoProdutos);

            this.setSize(800, 300); // alt, larg
            this.largura = -125;
            this.altura = 300;
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(180, 380);


        }

        public class PainelFonrecedoProdutos extends JPanel {

            JLabel JlnomeForn = new JLabel();
            JLabel JlvalorDividaTotal = new JLabel();
            JLabel JldataPagamento = new JLabel();
            Datas datas = new Datas();
            String[] data;
            ArrayList<String[]> produtosAgardandoPagamendo = new ArrayList<String[]>();

            public PainelFonrecedoProdutos(String nome, String dividaTotal, String dataPagamento) {
                this.setLayout(null);
                this.setSize(800, 300); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                data = dataPagamento.split("/");

                int ano = Integer.parseInt(data[2]);
                int mes = Integer.parseInt(data[1]);
                int dia = Integer.parseInt(data[0]);



                //JOptionPane.showMessageDialog(null, retornarDiaSemana(dia, mes, ano));


                JlnomeForn.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlnomeForn.setText(nome);
                JlnomeForn.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JlnomeForn.setBackground(Color.BLUE);
                JlnomeForn.setLocation(20, 20);//
                add(JlnomeForn);

                JlvalorDividaTotal.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JlvalorDividaTotal.setText("Divida Total R$" + dividaTotal);
                JlvalorDividaTotal.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JlvalorDividaTotal.setLocation(20, 40);//
                add(JlvalorDividaTotal);

                JldataPagamento.setBounds(new java.awt.Rectangle(168, 135, 400, 30));
                JldataPagamento.setText("Data pagamento:" + dataPagamento);
                JldataPagamento.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
                JldataPagamento.setLocation(20, 60);//
                add(JldataPagamento);



                produtosAgardandoPagamendo = PegaprodutosParaCadaFornecedor(nome);
               

                String[] colunas = new String[]{"Quant prod.", "Código", "Nome Prod.", "Compra uni.", "Compra Ger.", "Venda uni.", "Lucro uni.", "Lucro Ger."};
                TabelaModel modelo = new TabelaModel(produtosAgardandoPagamendo, colunas);
                final JTable jtable = new JTable(modelo);
                jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                jtable.setLocation(1, 145);
                jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
                jtable.getColumnModel().getColumn(0).setPreferredWidth(15);
                jtable.getColumnModel().getColumn(1).setPreferredWidth(15);
                jtable.getColumnModel().getColumn(2).setPreferredWidth(100);
                jtable.getColumnModel().getColumn(3).setPreferredWidth(30);
                jtable.getColumnModel().getColumn(4).setPreferredWidth(30);
                jtable.getColumnModel().getColumn(5).setPreferredWidth(30);
                jtable.getColumnModel().getColumn(6).setPreferredWidth(30);
                jtable.getColumnModel().getColumn(7).setPreferredWidth(30);

                TableColumn coluna = null;

                add(jtable);

                JSprodutos = new JScrollPane(jtable);
                JSprodutos.setLocation(20, 100);//lado outro & cima baixo
                JSprodutos.setSize(700, 150);
                add( JSprodutos);
            }
        }
    }
    
    /*METODO QUE RETORNA UMA ARRAYLIST DE STRING CONTENDO OS PRODUTOS PARA 
     UM DETERMINADO FORNECEDOR DE ACORCO COM O PARAMETRO DE ENTRADA*/
    public ArrayList<String[]> PegaprodutosParaCadaFornecedor(String nomeFornecedor) {
        ListaCompraNota = arquivoNotas.retorna();
        ArrayList<String[]> produtosAgardandoPagamendo = new ArrayList<String[]>();//cria um arraylist para ser o retornar os produtos de cada cliente
        String[] produtos;
        String quantidadeProdutoS, precoCompra_unidadeS, precoVenda_unidadeS, precoCompra_GeralS, LucroUnidadeS, LucroGeralS;


        for (int i = 0; i < ListaCompraNota.size(); i++) {
            compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

            if (nomeFornecedor.equalsIgnoreCase(compra_nota.getNomeFornecedor())) {
                    
                for (int j = 0; j < compra_nota.getProdutoEstoque().size(); j++) {
                    nomeProduto = compra_nota.getProdutoEstoque().get(j).nomeProduto.replace("[", "").replace(",", "").trim();
                    codigoProduto = compra_nota.getProdutoEstoque().get(j).codigoProduto;
                    quantidadeProdutoS = String.valueOf(compra_nota.getProdutoEstoque().get(j).getQuantidadeProduto());
                    precoCompra_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_unidade());
                    precoVenda_unidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoVenda_unidade());
                    precoCompra_GeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getPrecoCompra_Geral());
                    LucroUnidadeS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroUnidade());
                    LucroGeralS = AlternavelCadastroProduto.converterFloatString(compra_nota.getProdutoEstoque().get(j).getLucroGeral());

                    produtos = new String[]{quantidadeProdutoS, codigoProduto, nomeProduto, precoCompra_unidadeS, precoCompra_GeralS, precoVenda_unidadeS, LucroUnidadeS, LucroGeralS};

                    produtosAgardandoPagamendo.add(produtos);

                }

            }
        }
        
        return produtosAgardandoPagamendo;
    }
    
      /*METODO QUE CRIA UMA TABLEA DE PRODUTOS PARA A VISUALIZAÇÃO
       NO PAINEL FONECEDOR PRODUTO*/
    public void TabelaProdutos(ArrayList<String[]> dados) {
       
        String[] colunas = new String[]{"Quant prod.", "Código", "Nome Prod.", "Compra uni.", "Compra Ger.", "Venda uni.", "Lucro uni.", "Lucro Ger."};
        TabelaModel modelo = new TabelaModel(dados, colunas);
        final JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        jtable.setLocation(1, 145);
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(5).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(6).setPreferredWidth(30);
        jtable.getColumnModel().getColumn(7).setPreferredWidth(30); 
         
        TableColumn coluna = null;
        
        add(jtable);

        JSprodutos = new JScrollPane(jtable);
        JSprodutos.setLocation(20, 50);
        JSprodutos.setSize(400, 400);
       
  }
    
    //retorna o dia da semana dada uma data  

    public String retornarDiaSemana(int ano, int mes, int dia) {

        Calendar calendario = new GregorianCalendar(ano, mes, dia);
        int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);
        
        return pesquisarDiaSemana(diaSemana);
    }

    //faz a pesquisa, dado um inteiro de 1 a 7  
    public String pesquisarDiaSemana(int _diaSemana) {
        String diaSemana = null;

        switch (_diaSemana) {

            case 1: {
                diaSemana = "Domingo";
                break;
            }
            case 2: {
                diaSemana = "Segunda";
                break;
            }
            case 3: {
                diaSemana = "Terça";
                break;
            }
            case 4: {
                diaSemana = "Quarta";
                break;
            }
            case 5: {
                diaSemana = "Quinta";
                break;
            }
            case 6: {
                diaSemana = "Sexta";
                break;
            }
            case 7: {
                diaSemana = "Sábado";
                break;
            }

        }
        return diaSemana;

    }

    /*public ArrayList<String[]> criaTabelaProdutos(String nomeforn) {
        ListaCompraNota = arquivoNotas.retorna();

        if (ListaCompraNota.size() > 0) {


            for (int i = 0; i < ListaCompraNota.size(); i++) {
                compra_nota = (COMPRA_NOTA) ListaCompraNota.get(i);

                nomeFornecedor = compra_nota.getNomeFornecedor();
               
                if(nomeforn.equalsIgnoreCase(nomeFornecedor)){
                  ProdutoEstoqueAguard = compra_nota.getProdutoEstoque();
                  
                  nomeProduto = ProdutoEstoqueAguard.
                }

                String[] Nota = new String[]{nomeFornecedor, valorDivida, dinheiroDia, dinheiroEntrada, dataPagamento, telefone};
                dados.add(Nota);
            }
            Tabela(dados);
        }
        return dados;
    }*/    
    
}
