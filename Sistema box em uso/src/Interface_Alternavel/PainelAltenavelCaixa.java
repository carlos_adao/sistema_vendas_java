/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Auxiliares.JavaApplication4;
import BancoDados.ARQUIVO_ESTOQUE;
import BancoDados.ARQUIVO_ESTOQUE_VENDA;
import BancoDados.ArquivoProdutos;
import Cadastro.CadastroProduto;
import Cadastro.Datas;
import InterfacePrincipal.PainelResultado;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public class PainelAltenavelCaixa extends JPanel {

    //private PDF_nota pdf_nota = new PDF_nota();  
    private PainelAltenavelCaixa.PainelDinheiroEntrada Entrada;
    PainelAltenavelCaixa.AlternavelPesquisa alternavelPesquisa = new PainelAltenavelCaixa.AlternavelPesquisa();
    JTextField tf;
    int vrifcaComp_prod = 0;
    int verificao = 0;
    int btFecharPress = 0;
    String nome, codigoArquivo, codigoText, precoVendaUnidadeS;
    float precoVendaUnidadeF;
    float Total = 0;
    private boolean checaQuantida = false;
    ArrayList<CadastroProduto> ListaProdutoCadastrados = new ArrayList<>();
    private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>(); //UMA LISTA DE PRODUTOS COMPRADOS E COLOCADOS NO ESTOQUE 
    private PRODUTO_ESTOQUE Produto_Compra = new PRODUTO_ESTOQUE();   //UM PRODUTO DA CLASSE COMPRA 
    private ARQUIVO_ESTOQUE ArquivoProduto_Estoque; //ARQUIVO PARA VERIFICAR OS PRODUTOS QUE ESTÃO NO ESTOQUE 
    private ARQUIVO_ESTOQUE arquivo;//ARQUIVO PARA ATUALIZAR PRODUTOS NO ESTOQUE;
    private PRODUTO_VENDA Produto_Venda = new PRODUTO_VENDA();
    private ArrayList<PRODUTO_VENDA> Lista_ProdutoVenda = new ArrayList<>();
    ARQUIVO_ESTOQUE_VENDA Arquivo_Estoque_venda = new ARQUIVO_ESTOQUE_VENDA();
    private CadastroProduto produto = new CadastroProduto();
    ArquivoProdutos BancoDadosProduto;
    private ArrayList<String> linha_pdf;
    JLabel JLnomeMercadoria = new JLabel();
    JLabel JLcodigoMercadoria = new JLabel();
    JLabel JLprecoVendaUnidade = new JLabel();
    JLabel JLquantidaMercadoria = new JLabel();
    JLabel JLsubtotalCompra = new JLabel();
    JLabel JLtotalCompra = new JLabel();
    JLabel JLsubTotal = new JLabel();
    JLabel JLtotal = new JLabel();
    JLabel JLdescontoVendaUnidade = new JLabel();
    JLabel JLdataDoSistema = new JLabel();
    JLabel JLdataManual = new JLabel();
    JFormattedTextField JTdataManual = new JFormattedTextField(Mascara("##/##/####"));
    JFormattedTextField JTmesManual = new JFormattedTextField(Mascara("##"));
    JFormattedTextField JTanoManual = new JFormattedTextField(Mascara("####"));
    JComboBox comboBox;
    JLabel JLhoraSistema = new JLabel();
    JTextField JTcodigo_mercadoria = new JtextFieldSomenteNumeros();
    JTextField JTnome_mercadoria;
    JTextField JtQuantidade_mercadoria;
    private DateFormat df = new SimpleDateFormat("hh:mm:ss");
    private JNumberFormatField JTsubTotal = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JTsubtotalCompra = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JTtotalCompra = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtPrecoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JTtotal = new JNumberFormatField(new DecimalFormat("0.00"));
    private JNumberFormatField JtDescontoVendaUnidade = new JNumberFormatField(new DecimalFormat("0.00"));//JTXT DESCONTO DO PRODUTO
    private JList JLTprodutosEstoque;
    private JList JLTnomeProdutosCadastrados;
    private JScrollPane JScprodutosEstoque2;
    private JScrollPane JScprodutosEstoque;
    private DefaultListModel ListaModelProdutoEstoque = new DefaultListModel();
    private DefaultListSelectionModel ListaModelSelection = new DefaultListSelectionModel();
    private DefaultTableModel TabelaModelProdutoEstoque = new DefaultTableModel();
    ArrayList<String> listaNomesProdutos = new ArrayList<>();
    public ArrayList<String> dados = new ArrayList<>();
    private Datas data = new Datas();//ISTANCIA A CLASSE DATA 
    String Data = data.retornaData();
    String mes = data.retornaMes();
    String ano = data.retornaAno();
    JLabel lblHora;
    PainelResultado P_resultado = new PainelResultado();
    PainelAltenavelCaixa.HandlerText handlerText = new PainelAltenavelCaixa.HandlerText();
    PainelAltenavelCaixa.FocoNome focoNome = new PainelAltenavelCaixa.FocoNome();
    PainelAltenavelCaixa.EnterPrecionadoJTxtCodigo EnterPrecionado = new PainelAltenavelCaixa.EnterPrecionadoJTxtCodigo();
    int verificacao;
    String str1 = null;
    AlternavelEst alternavelEst = new AlternavelEst();
    PainelAltenavelCaixa.Teclado teclado = new PainelAltenavelCaixa.Teclado();
    PainelAltenavelCaixa.TecladoExclusivo tecladoExclusivo = new PainelAltenavelCaixa.TecladoExclusivo();
    AlternavelPesquisa AlternavelPesquisa = new AlternavelPesquisa();

    public PainelAltenavelCaixa() {

        this.setLayout(null);
        this.setSize(1336, 465); // larg, al
        this.setLocation(0, 160);
        this.setBackground(Color.white);

        new Thread(new PainelAltenavelCaixa.AtualizadorDeHora()).start();
        String a = "a";

        //JOptionPane.showMessageDialog(null, BuscaProduto(a));
        add(P_resultado);

        P_resultado.setVisible(false);

        JLcodigoMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLcodigoMercadoria.setText("Cód da Merc");
        JLcodigoMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLcodigoMercadoria.setLocation(10, 40);//
        add(JLcodigoMercadoria);

        JTcodigo_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JTcodigo_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTcodigo_mercadoria.setSize(110, 25);
        JTcodigo_mercadoria.setLocation(10, 75);
        JTcodigo_mercadoria.addFocusListener(focoNome);
        JTcodigo_mercadoria.addKeyListener(EnterPrecionado);
        JTcodigo_mercadoria.addKeyListener(teclado);
        add(JTcodigo_mercadoria);//
        /*
         * JLabel nome da Mercadoria
         */
        JLnomeMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLnomeMercadoria.setText("Nome da mercadoria");
        JLnomeMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLnomeMercadoria.setLocation(10, 110);//

        add(JLnomeMercadoria);

        JTnome_mercadoria = new JTextField();
        JTnome_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JTnome_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTnome_mercadoria.setSize(250, 25);
        JTnome_mercadoria.setLocation(10, 140);//
        JTnome_mercadoria.addActionListener(handlerText);
        JTnome_mercadoria.addFocusListener(focoNome);
        JTnome_mercadoria.addKeyListener(teclado);
        JTnome_mercadoria.setEditable(false);
        add(JTnome_mercadoria);

        JLquantidaMercadoria = new JLabel();
        JLquantidaMercadoria.setBounds(new Rectangle(168, 135, 400, 30));
        JLquantidaMercadoria.setText("Quantidade");
        JLquantidaMercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLquantidaMercadoria.setLocation(152, 40);
        add(JLquantidaMercadoria);

        //** JTextField quantidade**//
        JtQuantidade_mercadoria = new JtextFieldSomenteNumeros();
        JtQuantidade_mercadoria.setBounds(new Rectangle(200, 135, 120, 17));
        JtQuantidade_mercadoria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtQuantidade_mercadoria.setSize(110, 25);
        JtQuantidade_mercadoria.setLocation(152, 75);
        JtQuantidade_mercadoria.addKeyListener(handlerText);
        JtQuantidade_mercadoria.addKeyListener(teclado);
        add(JtQuantidade_mercadoria);

        JLprecoVendaUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLprecoVendaUnidade.setText("preço de venda unid");
        JLprecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLprecoVendaUnidade.setLocation(279, 110);
        add(JLprecoVendaUnidade);

        JtPrecoVendaUnidade.setBounds(new Rectangle(200, 135, 120, 17));
        JtPrecoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtPrecoVendaUnidade.setSize(178, 25);
        JtPrecoVendaUnidade.setLocation(279, 140);
        JtPrecoVendaUnidade.setEditable(false);
        add(JtPrecoVendaUnidade);

        //LABEL E TXT DO DESCONTO DO PRODUTO
        JLdescontoVendaUnidade.setBounds(new Rectangle(168, 135, 400, 30));
        JLdescontoVendaUnidade.setText("desconto");
        JLdescontoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        JLdescontoVendaUnidade.setLocation(280, 40);
        add(JLdescontoVendaUnidade);

        JtDescontoVendaUnidade.setBounds(new Rectangle(100, 135, 120, 17));
        JtDescontoVendaUnidade.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JtDescontoVendaUnidade.setSize(178, 25);
        JtDescontoVendaUnidade.setLocation(280, 75);//
        JtDescontoVendaUnidade.addKeyListener(handlerText);
        JtDescontoVendaUnidade.addKeyListener(teclado);
        //JtDescontoVendaUnidade.setForeground(Color.BLUE);
        add(JtDescontoVendaUnidade);

        JLdataManual.setBounds(new Rectangle(168, 135, 400, 30));
        JLdataManual.setText("Digite a data");
        JLdataManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 10));
        JLdataManual.setLocation(10, 400);
        //add(JLdataManual);

        JTdataManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTdataManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTdataManual.setSize(100, 25);
        JTdataManual.setLocation(0, 438);//
        JTdataManual.addKeyListener(teclado);
        JTdataManual.setText(Data);
        add(JTdataManual);

        JTmesManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTmesManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTmesManual.setSize(30, 25);
        JTmesManual.setText(mes);
        JTmesManual.setLocation(100, 438);//
        JTmesManual.addKeyListener(teclado);
        add(JTmesManual);

        JTanoManual.setBounds(new Rectangle(100, 135, 120, 17));
        JTanoManual.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        JTanoManual.setSize(60, 25);
        JTanoManual.setText(ano);
        JTanoManual.setLocation(130, 438);//
        JTanoManual.addKeyListener(teclado);
        add(JTanoManual);

        JLdataDoSistema.setBounds(new Rectangle(168, 135, 400, 30));
        JLdataDoSistema.setText("Ilhéus, " + Data);
        JLdataDoSistema.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        JLdataDoSistema.setLocation(10, 438);
        //add(JLdataDoSistema);

        String hora = df.format(new Date());

        JLhoraSistema.setBounds(new Rectangle(168, 135, 400, 30));
        JLhoraSistema.setText(hora);
        JLhoraSistema.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        JLhoraSistema.setLocation(1250, 438);
        add(JLhoraSistema);

    }

    public void AtualizaListaProdutosAutoComplete() {
        String[] nomes = AlternavelPesquisa.pegaTodosNomesProdutos();
        AlternavelPesquisa.setModel(new DefaultComboBoxModel<>(nomes));
        // JOptionPane.showMessageDialog(null, "inicializa");
    }

    public MaskFormatter Mascara(String Mascara) {

        MaskFormatter F_Mascara = new MaskFormatter();
        try {
            F_Mascara.setMask(Mascara); //Atribui a mascara  
            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
        } catch (Exception excecao) {
            excecao.printStackTrace();
        }
        return F_Mascara;
    }

    public class AtualizadorDeHora implements Runnable {

        private SimpleDateFormat sdf = null;

        public AtualizadorDeHora() {
            sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        }

        @Override
        public void run() {
            System.out.println(sdf.format(new Date()));
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }

    private class HandlerText implements ActionListener, KeyListener, FocusListener {

        ArrayList<PRODUTO> Produto = new ArrayList<>();
        ArrayList<String> RetornoTabela = new ArrayList<>();
        //VARIAVEIS DA NOVA CLASSE PRODUTO VENDA
        String codigo;
        String nomeProduto;
        int quantidade;
        float precoUni;
        float subTot;

        @Override
        public void actionPerformed(ActionEvent e) {
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyChar() == 8) { // Para apagar caractere 1 a 1
                if (str1.length() > 0) {
                    str1 = str1.substring(0, str1.length() - 1);
                }
            } else {
                str1 += e.getKeyChar();
            }

            BuscaProduto(str1);

        }

        @Override
        public void keyPressed(KeyEvent e) {
            checaQuantida = false;
            boolean verificaVenda = true;

            if (e.getKeyCode() == 10) {

                if (!"".equals(JtQuantidade_mercadoria.getText())) {
                    PRODUTO_VENDA prod_venda;
                    Alternavel_Caixa_Consuta AlternavelCaixaConsuta = new Alternavel_Caixa_Consuta();
                    Lista_ProdutoVenda = ARQUIVO_ESTOQUE_VENDA.retorna();

                    ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

                    for (int i = 0; i < ListaProduto_Estoque.size(); i++) {

                        Produto_Compra = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
                        nome = Produto_Compra.getNomeProduto();
                        precoVendaUnidadeF = Produto_Compra.getPrecoVenda_unidade();
                        codigoText = JTcodigo_mercadoria.getText();
                        codigoArquivo = Produto_Compra.getCodigoProduto();
                        //precoVendaUnidadeS = AlternavelCadastroProduto.converterFloatString(precoVendaUnidadeF);
                        precoVendaUnidadeS = JtPrecoVendaUnidade.getText();

                        float lucroVenda = Produto_Compra.getLucroUnidade();
                        int quantidade = Integer.parseInt(JtQuantidade_mercadoria.getText());
                        float SubTotal_Variavel = quantidade * precoVendaUnidadeF;//FAZ O CALCULO DO SUBTOTAL

                        if (codigoArquivo.equalsIgnoreCase(codigoText)) {//COMPARA O CODIGO DO PRODUTO DO ARQUIVO COM O CODIGO QUE TÁ NO TXT                    

                            if (Produto_Compra.getQuantidadeProduto() >= quantidade) {

                                checaQuantida = true;

                                lucroVenda = (quantidade * lucroVenda) - Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", "."));//CALCULA O LUCRO DA VENDA DE CADA PRODUTO 
                                JTnome_mercadoria.setText(Produto_Compra.getNomeProduto()); //SETA O NOME DO PRODUTO 
                                JtPrecoVendaUnidade.setText(AlternavelCadastroProduto.converterFloatString(Produto_Compra.getPrecoVenda_unidade()));//SETA PRECO DE VENDA POR UNIDADE

                                dados = criaTabela(Produto_Compra, dados);

                                Produto_Venda.setNomeProduto(JTnome_mercadoria.getText());
                                Produto_Venda.setCodigoProduto(JTcodigo_mercadoria.getText());
                                Produto_Venda.setQuantidadeProduto(Integer.parseInt(JtQuantidade_mercadoria.getText()));
                                Produto_Venda.setLucroVenda((Produto_Compra.getLucroUnidade() - (Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", ".")) / Integer.parseInt(JtQuantidade_mercadoria.getText()))) * Integer.parseInt(JtQuantidade_mercadoria.getText()));
                                Produto_Venda.setDescontoVenda(Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", ".")));
                                Produto_Venda.setSubTotal(SubTotal_Variavel);
                                Produto_Venda.setValor_Venda(precoVendaUnidadeF - (Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", ".")) / Integer.parseInt(JtQuantidade_mercadoria.getText())));
                                Produto_Venda.setData(JTdataManual.getText());
                                Produto_Venda.setMes(JTmesManual.getText());
                                Produto_Venda.setAno(JTanoManual.getText());
                                //Produto_Venda.set;

                                if (Lista_ProdutoVenda.isEmpty()) {
                                    Lista_ProdutoVenda.add(Produto_Venda);
                                } else {
                                    for (int j = 0; j < Lista_ProdutoVenda.size(); j++) {
                                        prod_venda = (PRODUTO_VENDA) Lista_ProdutoVenda.get(j);

                                        String nomeSystema = JTnome_mercadoria.getText();
                                        String nomeArquivo = prod_venda.getNomeProduto();
                                        String dataSystema = Data;
                                        String dataArquivo = prod_venda.getData();

                                        //CASO O PRODUTO ESTEJA CADASTRADO O IF FAZ O INCREMENTO 
                                        if (nomeSystema.equalsIgnoreCase(nomeArquivo) && dataSystema.equalsIgnoreCase(dataArquivo)) {
                                            int quantSystema = Integer.parseInt(JtQuantidade_mercadoria.getText());
                                            int quantArquivo = prod_venda.getQuantidadeProduto();
                                            int quantTotal = quantSystema + quantArquivo;
                                            float lucroSystema = lucroVenda;
                                            float lucroArquivo = prod_venda.getLucroVenda();
                                            float descontoSystema = Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", "."));
                                            float descontoArquivo = prod_venda.getDescontoVenda();
                                            float subTotalSystema = SubTotal_Variavel - descontoSystema;
                                            float subTotalArquivo = prod_venda.getSubTotal();
                                            float somaSubTotal = subTotalSystema + subTotalArquivo;
                                            float valorVendaSystema = subTotalSystema + subTotalArquivo / quantTotal;
                                            float valorVendaArquivo = prod_venda.getValor_Venda();
                                            verificaVenda = false;

                                            prod_venda.setQuantidadeProduto(quantTotal);
                                            prod_venda.setLucroVenda(lucroSystema + lucroArquivo);
                                            prod_venda.setDescontoVenda(descontoSystema + descontoArquivo);
                                            prod_venda.setSubTotal(somaSubTotal);
                                            prod_venda.setValor_Venda(somaSubTotal / quantTotal);

                                        }//FECHANDO O IF
                                    }//FECHANDO O FOR 
                                }//FECHANDO O ELSE 

                                //SUBTRAINDO A VENDO DO ESTOQUE 
                                int Quant = (Produto_Compra.getQuantidadeProduto() - quantidade);
                                float ComUni = Produto_Compra.getPrecoCompra_unidade();
                                float LucroUni = Produto_Compra.getLucroUnidade();

                                Produto_Compra.setQuantidadeProduto(Quant);
                                Produto_Compra.setPrecoCompra_Geral(Quant * ComUni);
                                Produto_Compra.setLucroGeral(Quant * LucroUni);

                                //SE O PRODUTO NÃO ESTÁ CADASTRADO ADICIONA ELE A LISTA DE PRODUTOS 
                                if (verificaVenda) {
                                    Lista_ProdutoVenda.add(Produto_Venda);
                                }//FECHANDO O IF 

                                try {
                                    Arquivo_Estoque_venda.Armazena(Lista_ProdutoVenda);
                                } catch (IOException ex) {
                                    Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                try {
                                    ARQUIVO_ESTOQUE.Armazena(ListaProduto_Estoque);
                                } catch (IOException ex) {
                                    Logger.getLogger(PainelAltenavelCaixa.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }//FECHANDO O IF QUANTIDADE MENOR QUE QUANTIDADE ESTOQUE 
                            else if (Produto_Compra.getQuantidadeProduto() == 0) {
                                checaQuantida = true;
                                JOptionPane.showMessageDialog(null, "Produto em Falta");
                            } else {
                                JOptionPane.showMessageDialog(null, "Quantidade deve ser menor que: " + Produto_Compra.getQuantidadeProduto() + "Uni");
                                JtQuantidade_mercadoria.requestFocus();
                            }

                        }//FECHA O IF QUE VERIFICA SE O CODIGO DO PRODUTO NO SYSTEMA E IGUAL AO CODIGO DO PRODUTO NO ARQUIVO
                        else {
                            verificao++;
                        }

                    }//FECHANDO O FOR QUE PERCORRE A LISTA DE PRODUTOS NO ESTOQUE

                    if (checaQuantida) {
                        if (verificao == ListaProduto_Estoque.size()) {
                            JTnome_mercadoria.setText(null);
                            JtQuantidade_mercadoria.setText(null);
                            JtPrecoVendaUnidade.setText(null);
                            JtDescontoVendaUnidade.setText(null);
                            JOptionPane.showMessageDialog(null, "Produto não cadastrado");
                        }
                    }

                    if (checaQuantida) {
                        JTcodigo_mercadoria.setText(null);
                        JTnome_mercadoria.setText(null);
                        JtPrecoVendaUnidade.setText(null);
                        JtQuantidade_mercadoria.setText(null);
                        JtDescontoVendaUnidade.setText(null);
                        JTcodigo_mercadoria.requestFocus();

                        P_resultado.setVisible(true);
                    }

                    JtQuantidade_mercadoria.setText(null);
                }//FECHANDO IF
                else {
                    JOptionPane.showMessageDialog(null, "Digite a quantidade");
                }

            }//FECHA A OPCAO BOTÃO SALVAR 
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {

            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

            for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
                Produto_Compra = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);

                nome = Produto_Compra.getNomeProduto();
                precoVendaUnidadeF = Produto_Compra.getPrecoVenda_unidade();
                codigoText = JTcodigo_mercadoria.getText();
                codigoArquivo = Produto_Compra.getCodigoProduto();
                precoVendaUnidadeS = AlternavelCadastroProduto.converterFloatString(precoVendaUnidadeF);

                if (codigoArquivo.equalsIgnoreCase(codigoText)) {//COMPARA O CODIGO DO PRODUTO DO ARQUIVO COM O CODIGO QUE TÁ NO TXT 

                    JTnome_mercadoria.setText(Produto_Compra.getNomeProduto()); //SETA O NOME DO PRODUTO 
                    JtPrecoVendaUnidade.setText(AlternavelCadastroProduto.converterFloatString(Produto_Compra.getPrecoVenda_unidade()));//SETA PRECO DE VENDA POR UNIDADE

                    dados = criaTabela(Produto_Compra, dados);

                    //COLOCANDO VALORES NA CLASSE VENDA 
                    Produto_Venda.setNomeProduto(JTnome_mercadoria.getText());
                    Produto_Venda.setCodigoProduto(JTcodigo_mercadoria.getText());
                    Produto_Venda.setQuantidadeProduto(Integer.parseInt(JtQuantidade_mercadoria.getText()));

                } else {
                    verificao++;
                }

            }
            if (verificao == ListaProduto_Estoque.size()) {
                JTnome_mercadoria.setText(null);
                JtQuantidade_mercadoria.setText(null);
                JtPrecoVendaUnidade.setText(null);
                JOptionPane.showMessageDialog(null, "Produto não cadastrado");
            }
        }
    }

    private class FocoNome implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
            verificao = 0;

            for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
                Produto_Compra = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
                nome = Produto_Compra.getNomeProduto();
                precoVendaUnidadeF = Produto_Compra.getPrecoVenda_unidade();
                codigoText = JTcodigo_mercadoria.getText();
                codigoArquivo = Produto_Compra.getCodigoProduto();
                precoVendaUnidadeS = AlternavelCadastroProduto.converterFloatString(precoVendaUnidadeF);

                if (codigoArquivo.equalsIgnoreCase(codigoText)) {//COMPARA O CODIGO DO PRODUTO DO ARQUIVO COM O CODIGO QUE TÁ NO TXT                    

                    JTnome_mercadoria.setText(nome);
                    JtPrecoVendaUnidade.setText(precoVendaUnidadeS);

                } else {
                    verificao++;

                }

            }

            if (verificao == ListaProduto_Estoque.size()) {

                if (!JTcodigo_mercadoria.getText().isEmpty()) {

                    JTnome_mercadoria.setText(null);
                    JtQuantidade_mercadoria.setText(null);
                    JTcodigo_mercadoria.setText(null);
                    JtPrecoVendaUnidade.setText(null);
                    JTcodigo_mercadoria.requestFocus();
                    verificao = 0;
                }
            }
        }
    }

    public ArrayList<CadastroProduto> BuscaProduto(String str1) {

        int j, i;
        String str2;

        ListaProdutoCadastrados = ArquivoProdutos.retorna();
        this.ListaModelProdutoEstoque.clear();

        //str1 = this.txtBusca.getText();
        for (i = 0; i < this.ListaProdutoCadastrados.size(); i++) {
            try {
                str2 = ListaProdutoCadastrados.get(i).getNome_produto();

                if (str1.compareToIgnoreCase(str2) == 0) {
                    this.ListaModelProdutoEstoque.addElement(ListaProdutoCadastrados.get(i).getNome_produto());
                } else {
                    str1 = str1.toUpperCase(); // DEIXA TUDO MAIUSCULO
                    str2 = str2.toUpperCase();

                    for (j = 0; j < str1.length() && j < str2.length(); j++) {
                        if (str1.charAt(j) != str2.charAt(j)) {
                            break;
                        }
                    }
                    if (j == str1.length() || j == str2.length()) {
                        this.ListaModelProdutoEstoque.addElement(ListaProdutoCadastrados.get(i).getNome_produto());
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "Acesso a memoria indevida!");
                break;
            }
        }

        return ListaProdutoCadastrados;
    }

    //  public class Tabela{ 
    public ArrayList<String> criaTabela(PRODUTO_ESTOQUE produto, ArrayList dados) {
        vrifcaComp_prod++;
        ArrayList<String> ListaBtFechar;
        colocaValoresP_resultado(JtQuantidade_mercadoria.getText(), produto.getPrecoVenda_unidade());// Metodo que colocar os valores no painel Resutado  

        float SubtotalF = (Integer.parseInt(JtQuantidade_mercadoria.getText()) * produto.getPrecoVenda_unidade()) - Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", "."));//DIMINUI O DESCONTO DO SUBTOTAL 
        String Subtotal = AlternavelCadastroProduto.converterFloatString(SubtotalF);

        String codigo = produto.getCodigoProduto();
        String nome = produto.getNomeProduto();
        String precoVenUni = AlternavelCadastroProduto.converterFloatString(produto.getPrecoVenda_unidade());
        String quantidade = String.valueOf(JtQuantidade_mercadoria.getText());
        String desconto = JtDescontoVendaUnidade.getText();
        //String Subtotal = JTsubTotal.getText();
        String[] produtos = new String[]{quantidade, nome, precoVenUni, desconto, Subtotal};
        dados.add(produtos);

        Table(dados);
        return dados;
    }

    public void remove() {
        dados.clear();
        Table(dados);
    }

    public void Table(ArrayList<String> dados) {

        String[] colunas = new String[]{"Quantidade", "Descrição", "Preço Uni", "desconto", "Subtotal"};
        PainelAltenavelCaixa.SimpleTableModel modelo = new PainelAltenavelCaixa.SimpleTableModel(dados, colunas);
        JTable jtable = new JTable(modelo);
        jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable.setLocation(1, 145);
        jtable.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
        jtable.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
        jtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(150);
        jtable.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtable.getColumnModel().getColumn(4).setPreferredWidth(20);

        TableColumn coluna = null;

        add(jtable);

        JScprodutosEstoque = new JScrollPane(jtable);
        JScprodutosEstoque.setLocation(1, 255);
        JScprodutosEstoque.setSize(770, 175);
        add(JScprodutosEstoque);

    }

    public class SimpleTableModel extends AbstractTableModel {

        private ArrayList linhas = null;
        private String[] colunas = null;

        //Construtor da classe SimpleTableModel 
        private SimpleTableModel(ArrayList dados, String[] colunas) {
            setLinhas(dados);
            setColunas(colunas);

        }

        public String[] getColunas() {
            return colunas;
        }

        public ArrayList getLinhas() {
            return linhas;
        }

        public void setColunas(String[] strings) {
            colunas = strings;
        }

        public void setLinhas(ArrayList list) {
            linhas = list;
        }

        @Override
        public int getRowCount() {
            return getLinhas().size();

        }

        @Override
        public int getColumnCount() {
            return getColunas().length;

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            // Obtem a linha, que é uma String []  
            String[] linha = (String[]) getLinhas().get(rowIndex);
            // Retorna o objeto que esta na coluna  
            return linha[columnIndex];
        }

        public String getColumnName(int column) {
            return colunas[column];
        }
    }

    public void colocaValoresP_resultado(String quantidade, float precoUni) {
        float SubtotalF = (Integer.parseInt(quantidade) * precoUni) - Float.parseFloat(JtDescontoVendaUnidade.getText().replace(",", "."));//DIMINUI O DESCONTO DO SUBTOTAL 
        String Subtotal = AlternavelCadastroProduto.converterFloatString(SubtotalF);
        Total = Total + SubtotalF;
        String TotalS = AlternavelCadastroProduto.converterFloatString(Total);

        P_resultado.ColocaNoSubTotal(Subtotal);
        P_resultado.ColocarNoTotal(TotalS);
    }

    private class EnterPrecionadoJTxtCodigo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == 10) {
                if (vrifcaComp_prod > 0) {

                    Entrada = new PainelAltenavelCaixa.PainelDinheiroEntrada();

                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    public class PainelDinheiroEntrada extends JFrame {

        JButton JBfecha = new JButton();
        JButton JBvoltar = new JButton();
        JLabel JLentrada = new JLabel();
        JLabel JLtroco = new JLabel();
        private JNumberFormatField JTentrada = new JNumberFormatField(new DecimalFormat("0.00"));
        private JNumberFormatField JTtroco = new JNumberFormatField(new DecimalFormat("0.00"));
        private int largura;
        private int altura;
        PainelAltenavelCaixa.PainelDinheiroEntrada.P_Entrada Entrada = new PainelAltenavelCaixa.PainelDinheiroEntrada.P_Entrada();
        PainelAltenavelCaixa.PainelDinheiroEntrada.P_Troco Troco = new PainelAltenavelCaixa.PainelDinheiroEntrada.P_Troco();
        private Container container;

        public PainelDinheiroEntrada() {

            container = getContentPane();

            this.container.add(Entrada);
            this.container.add(Troco);
            this.setLayout(null);
            this.setSize(300, 200); // alt, larg
            this.largura = 0;
            this.altura = 300;
            this.setBackground(Color.BLUE);
            this.setVisible(true);
            Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(490, 250);

            Troco.setVisible(false);

        }

        public class P_Entrada extends JPanel {

            PainelAltenavelCaixa.PainelDinheiroEntrada.pressEnterEntrada PrecionaEnter = new PainelAltenavelCaixa.PainelDinheiroEntrada.pressEnterEntrada();

            public P_Entrada() {

                this.setLayout(null);
                this.setSize(300, 200); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                JLentrada.setBounds(new Rectangle(168, 135, 400, 50));
                JLentrada.setText("Entrada");
                JLentrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 50));
                JLentrada.setLocation(20, 10);
                add(JLentrada);

                JTentrada.setBounds(new Rectangle(200, 135, 120, 17));
                JTentrada.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JTentrada.setSize(160, 50);
                JTentrada.setLocation(20, 65);//
                JTentrada.addKeyListener(PrecionaEnter);
                add(JTentrada);

            }
        }

        public class P_Troco extends JPanel {

            PainelAltenavelCaixa.PainelDinheiroEntrada.pressEntarBTfechar preciobarEnterBTfechar = new PainelAltenavelCaixa.PainelDinheiroEntrada.pressEntarBTfechar();

            public P_Troco() {

                this.setLayout(null);
                this.setSize(300, 200); // larg, alt
                this.setLocation(0, 0);
                this.setBackground(Color.white);

                JLtroco.setBounds(new Rectangle(168, 135, 400, 50));
                JLtroco.setText("Troco");
                JLtroco.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 50));
                JLtroco.setLocation(20, 10);
                add(JLtroco);

                JTtroco.setBounds(new Rectangle(200, 135, 120, 17));
                JTtroco.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
                JTtroco.setSize(160, 50);
                JTtroco.setLocation(20, 65);//
                add(JTtroco);

                JBfecha.setText("fechar");
                JBfecha.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                JBfecha.setSize(100, 25);
                JBfecha.setLocation(19, 125);
                JBfecha.addKeyListener(preciobarEnterBTfechar);
                add(JBfecha);

                JBvoltar.setText("voltar");
                JBvoltar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
                JBvoltar.setSize(100, 25);
                JBvoltar.setLocation(125, 125);
                add(JBvoltar);

            }
        }

        public class pressEnterEntrada implements KeyListener {

            int quant;

            public void pegaQuantidade(int quantidade) {
                quant = quantidade;

            }

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {

                    float dinheiroEntrada = Float.parseFloat(JTentrada.getText().replace(",", "."));
                    float total = Float.parseFloat(JTtotal.getText().replace(",", "."));

                    if (dinheiroEntrada < Total) {
                        JOptionPane.showMessageDialog(null, "A entrada deve ser Superior a " + Total);
                    } else {
                        Entrada.setVisible(false);
                        Troco.setVisible(true);
                        JTtroco.setText(AlternavelCadastroProduto.converterFloatString(dinheiroEntrada - Total));
                        JBfecha.requestFocus();

                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // throw new UnsupportedOperationException("Not supported yet.");
            }
        }

        public class pressEntarBTfechar implements KeyListener {

            @Override
            public void keyTyped(KeyEvent e) {
                // throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    Entrada.setVisible(true);
                    Troco.setVisible(false);
                    JTentrada.setText(null);
                    JTtroco.setText(null);
                    Total = 0;
                    P_resultado.LimpaTxt();

                    PainelAltenavelCaixa.PainelDinheiroEntrada.this.dispose();
                    dados.clear();
                    Table(dados);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }
        }

    }

    ///PAINEL DE PESQUISA///

    public class AlternavelPesquisa extends JComboBox implements JComboBox.KeySelectionManager {

        private ArrayList<PRODUTO_ESTOQUE> ListaProduto_Estoque = new ArrayList<>();
        private ArrayList<String> listaNomesProdutos = new ArrayList<>();
        private PRODUTO_ESTOQUE Produto_Estoque = new PRODUTO_ESTOQUE();
        private String searchFor;
        private long lap;
        int contador = 0;
        JFrame f = new JFrame("AutoCompleteComboBox");

        private AlternavelPesquisa() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AlternavelPesquisa(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);
            tf = null;
            if (getEditor() != null) {
                tf = (JTextField) getEditor().getEditorComponent();

                if (tf != null) {
                    //para fazer apenas uma verificação de enter precionado
                    tf.setDocument(new PainelAltenavelCaixa.AlternavelPesquisa.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            tf = (JTextField) getEditor().getEditorComponent();
                            tf.setSize(230, 25);
                            String text = tf.getText();

                            //JOptionPane.showMessageDialog(null, text);
                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {
                                        //JOptionPane.showMessageDialog(null, "prescionado");
                                        contador++;
                                    }
                                    tf.setText(current);
                                    tf.setSelectionStart(text.length());
                                    tf.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }
            tf.addKeyListener(tecladoExclusivo);
            tf.setText(null);
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();

                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }

        public String[] pegaTodosNomesProdutos() {
            int i, j = 0;
            String test;
            String[] names = {""};
            ArrayList<String> Sentinela = new ArrayList<>();

            ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();
            names = new String[ListaProduto_Estoque.size()];
            Sentinela.add("");

            for (i = 0; i < ListaProduto_Estoque.size(); i++) {

                Produto_Estoque = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);
                Sentinela.add(Produto_Estoque.getNomeProduto());
            }

            names = Sentinela.toArray(names);
            Arrays.sort(names, 0, ListaProduto_Estoque.size());

            return (names);
        }

        void createAndShowGUI_2() {

            // f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setSize(265, 200);
            f.setLocation(550, 200);
            Container cp = f.getContentPane();
            cp.setLayout(null);
            pegaTodosNomesProdutos();

            comboBox = new PainelAltenavelCaixa.AlternavelPesquisa(pegaTodosNomesProdutos());
            comboBox.setEditable(true);
            comboBox.setBounds(50, 50, 100, 25);
            comboBox.setMaximumRowCount(20);
            comboBox.setSize(250, 25);
            comboBox.setLocation(0, 0);
            cp.add(comboBox);
            new JavaApplication4(comboBox);

            Locale[] locales = Locale.getAvailableLocales();//
            /* JComboBox cBox = new AlternavelPesquisa(pegaTodosNomesProdutos());
             cBox.setBounds(50, 50, 100, 25);
             cBox.setMaximumRowCount(20);
             cBox.setSize(250, 25);
             cBox.setLocation(0, 0);
             cBox.setEditable(true);
             cp.add(cBox);*/
            f.setVisible(true);

        }

        String RetornaNome() {

            return tf.getText();
        }

        public void FechaPainel() {
            f.dispose();

        }
    }

    public class Teclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {

                alternavelPesquisa.createAndShowGUI_2();
            }

        }

    }

    //exclusivo para pesquisa

    public class TecladoExclusivo implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //acao caso a tecla pressionada for F1 
            if (e.getKeyCode() == 112) {

                alternavelPesquisa.createAndShowGUI_2();
            }
            if (e.getKeyCode() == 10) {

                ColocaComponentesEmTxt(alternavelPesquisa.RetornaNome());
                alternavelPesquisa.FechaPainel();
            }
        }
    }

    public void ColocaComponentesEmTxt(String nome) {
        ListaProduto_Estoque = ARQUIVO_ESTOQUE.retorna();

        for (int i = 0; i < ListaProduto_Estoque.size(); i++) {
            Produto_Compra = (PRODUTO_ESTOQUE) ListaProduto_Estoque.get(i);

            if (Produto_Compra.getNomeProduto().equalsIgnoreCase(nome)) {
                JTnome_mercadoria.setText(Produto_Compra.getNomeProduto());
                JTcodigo_mercadoria.setText(Produto_Compra.getCodigoProduto());
                JtPrecoVendaUnidade.setText(AlternavelCadastroProduto.converterFloatString(Produto_Compra.getPrecoVenda_unidade()));
                JtQuantidade_mercadoria.requestFocus();

            }

        }

    }
}
