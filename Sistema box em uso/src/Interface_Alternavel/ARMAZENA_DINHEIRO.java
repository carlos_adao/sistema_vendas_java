/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

/**
 *
 * @author User
 */
public class ARMAZENA_DINHEIRO {
    String nome;
    String dia_rmazenamento;
    String data_armazenamento;
    String observacao;
    float  valorArmazenamento;

    public ARMAZENA_DINHEIRO(String nome, String dia_rmazenamento, String data_armazenamento, String observacao, float valorArmazenamento) {
        this.nome = nome;
        this.dia_rmazenamento = dia_rmazenamento;
        this.data_armazenamento = data_armazenamento;
        this.observacao = observacao;
        this.valorArmazenamento = valorArmazenamento;
    }

    public ARMAZENA_DINHEIRO() {
        
    }

    public String getData_armazenamento() {
        return data_armazenamento;
    }

    public void setData_armazenamento(String data_armazenamento) {
        this.data_armazenamento = data_armazenamento;
    }

    public String getDia_rmazenamento() {
        return dia_rmazenamento;
    }

    public void setDia_rmazenamento(String dia_rmazenamento) {
        this.dia_rmazenamento = dia_rmazenamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public float getValorArmazenamento() {
        return valorArmazenamento;
    }

    public void setValorArmazenamento(float valorArmazenamento) {
        this.valorArmazenamento = valorArmazenamento;
    }
    
    @Override
    public String toString() {
        return   nome + ";" + dia_rmazenamento + ";" + data_armazenamento + ";" + observacao + ";" + valorArmazenamento + ";";
    }
    
}
