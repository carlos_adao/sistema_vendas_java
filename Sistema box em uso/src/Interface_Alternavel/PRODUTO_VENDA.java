/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_Alternavel;

import Gastos.BOX;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.CompareGenerator;

/**
 *
 * @author carlos
 */
public class PRODUTO_VENDA implements Comparable<Object> {
   private String nomeProduto;
   private String codigoProduto;
   private String data;
   private String Ano;
   private String mes;
   private int quantidadeProduto;
   private float precoVenda_unidade,LucroVenda,descontoVenda,subTotal, valor_Venda;

    
    public PRODUTO_VENDA(String nomeProduto, String codigoProduto,int quantidadeProduto,
                        float precoVenda_unidade,float descontoVenda,float subTotal, 
                        float LucroVenda,  float valor_Venda, String data, String mes, String Ano) {
        this.nomeProduto = nomeProduto;
        this.codigoProduto = codigoProduto;
        this.data = data;
        this.Ano = Ano;
        this.mes = mes;
        this.quantidadeProduto = quantidadeProduto;
        this.precoVenda_unidade = precoVenda_unidade;
        this.LucroVenda = LucroVenda;
        this.descontoVenda = descontoVenda;
        this.subTotal = subTotal;
        this.valor_Venda = valor_Venda;
    }

    public PRODUTO_VENDA() {
        
    }

    public float getLucroVenda() {
        return LucroVenda;
    }

    public float getValor_Venda() {
        return valor_Venda;
    }

    public String getAno() {
        return Ano;
    }

    public void setAno(String Ano) {
        this.Ano = Ano;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setValor_Venda(float valor_Venda) {
        this.valor_Venda = valor_Venda;
    }

    public void setLucroVenda(float LucroVenda) {
        this.LucroVenda = LucroVenda;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public float getDescontoVenda() {
        return descontoVenda;
    }

    public void setDescontoVenda(float descontoVenda) {
        this.descontoVenda = descontoVenda;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public float getPrecoVenda_unidade() {
        return precoVenda_unidade;
    }

    public void setPrecoVenda_unidade(float precoVenda_unidade) {
        this.precoVenda_unidade = precoVenda_unidade;
    }

    public int getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(int quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return (nomeProduto + ";" + codigoProduto + ";" + quantidadeProduto + ";" 
                + precoVenda_unidade + ";" + descontoVenda + ";"  + subTotal + ";" 
                + LucroVenda+" ; " + valor_Venda+";" + data + ";" + mes + ";" + Ano+";"+"\n");
    }
     
   
    
    @Override
    public int compareTo(Object o) {
        PRODUTO_VENDA outro = (PRODUTO_VENDA)o;
        
        return this.data.compareToIgnoreCase(outro.data);
    }

   
}
